import os


movies_list     = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Panorama/thermal_movies.txt"
output_folder   = "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/Thermal/Panorama"
cropW           = "300"
cropH           = "300"
projectPrefix   = "DualAir" 
changePolarity  = "False" 
is_old_chimera  = "False"
vmd_map_path    = "None"
frame_width     = "640"
frame_height    = "480"

united_gt_path = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Panorama/Bools/GT_Dual/NMS_United" 

txt_file_list = [line.rstrip('\n') for line in open(movies_list)]
script = open("/mnt/share/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/Thermal/panorama_thermal_crops.bat", 'w')

for i in range(len(txt_file_list)):
    input_folder = txt_file_list[i]
    _,extension = os.path.splitext(input_folder)
    basename = os.path.basename(input_folder)

    gt_dir = os.path.join("/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Panorama/Bools/GT_Dual/NMS_NotUnited", basename[:-len(extension)] + "_gt")
    united_gt_dir = os.path.join(united_gt_path, basename[:-len(extension)] + "_gt")

    runCommand = 'python3 /home/lior/workspace/algo-scripts/crops_scripts/crop_movie.py "' + input_folder + '" "' + output_folder + '" "' + gt_dir + '" ' + cropW + " " + cropH + " " + frame_width + " " + frame_height + " " + projectPrefix + ' "' + united_gt_dir + '" \n'
    script.write(runCommand)
    print(runCommand)


script.close()
print("Done")
 