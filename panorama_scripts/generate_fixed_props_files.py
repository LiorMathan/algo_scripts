import os
import numpy as np
import sys
import glob


INPUT_FOLDER = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Panorama"
vis_props_files = []
thermal_props_files = []

VALIDATE_ONLY = False
BOOL_COL_THERMAL = 27
BOOL_ROW_THERMAL = 26
BOOL_COL_VIS = 25
BOOL_ROW_VIS = 24


def validate_fixed_props(vis_fixed_file, thermal_fixed_file):
    movieID = vis_fixed_file.split(os.path.sep)[-1].split(".")[0].split("_")[0]
    if movieID in thermal_fixed_file:
        vis_props_lines = open(vis_fixed_file, "r").readlines()
        thermal_props_lines = open(thermal_fixed_file, "r").readlines()

        for vis_line, thermal_line in zip(vis_props_lines, thermal_props_lines):
            if (vis_line.split()[0] != thermal_line.split()[0]):
                print("Unmatched: " + vis_fixed_file + " and: " + thermal_fixed_file + " \n")
                return
            if (vis_line.split()[IS_PANORAMA_ACTIVE_VIS] == thermal_line.split()[IS_PANORAMA_ACTIVE_THERMAL] == "1"):
                if (vis_line.split()[BOOL_COL_VIS] != thermal_line.split()[BOOL_COL_THERMAL]):
                    print("Unmatched area : " + vis_fixed_file + " and: " + thermal_fixed_file + " \n")
                    return
                if (vis_line.split()[BOOL_ROW_VIS] != thermal_line.split()[BOOL_ROW_THERMAL]):
                    print("Unmatched area : " + vis_fixed_file + " and: " + thermal_fixed_file + " \n")
                    return


cnt = 1


for d in os.listdir(INPUT_FOLDER):
    print("current dir: " + d)
    try:
        dir_id = int(d.split('_')[0])
    except:
        continue
    if (dir_id < 18 or dir_id == 29):
        IS_PANORAMA_ACTIVE_THERMAL = 25
        IS_PANORAMA_ACTIVE_VIS = 23
    else:
        IS_PANORAMA_ACTIVE_THERMAL = -1
        IS_PANORAMA_ACTIVE_VIS = -1

    vis_props_files = []
    thermal_props_files = []
    for subdirs in os.listdir(os.path.join(INPUT_FOLDER, d)):
        if not (os.path.isdir(os.path.join(INPUT_FOLDER, d, subdirs))):
            continue
        if 'CMOS' in subdirs:
            if VALIDATE_ONLY:
                for file in glob.glob(os.path.join(INPUT_FOLDER, d, subdirs, "*_fixed.props")): 
                    vis_props_files.append(file)
            else:
                for file in glob.glob(os.path.join(INPUT_FOLDER, d, subdirs, "*.props")): 
                    vis_props_files.append(file)
        elif 'Thermal' in subdirs:
            if VALIDATE_ONLY:
                for file in glob.glob(os.path.join(INPUT_FOLDER, d, subdirs, "*_fixed.props")): 
                    thermal_props_files.append(file)
            else:
                for file in glob.glob(os.path.join(INPUT_FOLDER, d, subdirs, "*.props")): 
                    thermal_props_files.append(file)
    
    # in case there is no match between CMOS and THERMAL
    if len(vis_props_files) > len(thermal_props_files):
        fileslen = len(thermal_props_files)
    elif len(vis_props_files) < len(thermal_props_files):
        fileslen = len(vis_props_files)
    else:
        fileslen = len(vis_props_files)

    for i in range(fileslen):
        if ('_fixed') in vis_props_files[i]:
            if VALIDATE_ONLY:
                validate_fixed_props(vis_props_files[i], thermal_props_files[i])
            continue
        if not VALIDATE_ONLY:
            movieID = vis_props_files[i].split(os.path.sep)[-1].split(".")[0].split("_")[0]
            if movieID in thermal_props_files[i]:
                vis_props_file = open(vis_props_files[i],"r").readlines()
                thermal_props_file = open(thermal_props_files[i],"r").readlines()

                # open new fixed props file 
                vis_fixed_props_name = vis_props_files[i].split(os.path.sep)[-1].split(".")[0] + '_fixed.' + vis_props_files[i].split(os.path.sep)[-1].split(".")[1]
                vis_fixed_props_file = open(vis_props_files[i].split(movieID)[0] + vis_fixed_props_name,"w+")
                thermal_fixed_props_name = thermal_props_files[i].split(os.path.sep)[-1].split(".")[0] + '_fixed.' + thermal_props_files[i].split(os.path.sep)[-1].split(".")[1]
                thermal_fixed_props_file = open(thermal_props_files[i].split(movieID)[0] + thermal_fixed_props_name,"w+")

                # write fixed line to files
                for j in range(len(vis_props_file)):
                    try:
                        if vis_props_file[j].split()[IS_PANORAMA_ACTIVE_VIS] == '0' and thermal_props_file[j].split()[IS_PANORAMA_ACTIVE_THERMAL] == '0':
                            vis_fixed_props_file.write(vis_props_file[j])
                            thermal_fixed_props_file.write(thermal_props_file[j])

                        elif vis_props_file[j].split()[IS_PANORAMA_ACTIVE_VIS] == '1' and thermal_props_file[j].split()[IS_PANORAMA_ACTIVE_THERMAL] == '0':
                            vis_fixed_props_file.write(vis_props_file[j])
                            arr = thermal_props_file[j].split()
                            arr[IS_PANORAMA_ACTIVE_THERMAL] = '1'
                            fixedLine = ' '.join(arr) + '\n'
                            thermal_fixed_props_file.write(fixedLine)

                        elif vis_props_file[j].split()[IS_PANORAMA_ACTIVE_VIS] == '0' and thermal_props_file[j].split()[IS_PANORAMA_ACTIVE_THERMAL] == '1':
                            thermal_fixed_props_file.write(thermal_props_file[j])
                            arr = vis_props_file[j].split()
                            arr[IS_PANORAMA_ACTIVE_VIS] = '1'
                            fixedLine = ' '.join(arr) + '\n'
                            vis_fixed_props_file.write(fixedLine)

                        elif vis_props_file[j].split()[IS_PANORAMA_ACTIVE_VIS] == '1' and thermal_props_file[j].split()[IS_PANORAMA_ACTIVE_THERMAL] == '1':
                            vis_fixed_props_file.write(vis_props_file[j])
                            thermal_fixed_props_file.write(thermal_props_file[j])

                        else:
                            "ERROR IN CHARACTER"
                            continue
                    except:
                        "weird line in props file"
                        continue

                vis_fixed_props_file.close()
                thermal_fixed_props_file.close()
                cnt += 1
                print(vis_fixed_props_name + " CLOSED")
                print(thermal_fixed_props_name + " CLOSED")

                # reset

                

                



print("Total props files " + str(cnt))


# if 0018 , 0019 , 0020 , 0021 , 0022  --> new props