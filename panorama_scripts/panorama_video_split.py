import glob
import os
import shutil
from PIL import Image
from matplotlib import pyplot as plt
import matplotlib.animation as animation
import numpy as np
import sys
import cv2
import random

folder = '' 
out_folder = folder + '/out_images/'

length = len(sys.argv)
if(length < 2):
    if (not os.path.isdir(temp_path)):
        print ('Not enough arguments, Usage:')
        print ('python3 panorama_video_split.py <video_folder> <output_folder>')
        print ('exiting...')
        sys.exit(0)
elif (length == 2 and (not os.path.exists(sys.argv[1]))):
    print ('Folder doesn\'t exist')
    print ('exiting...')
    sys.exit(0)
elif (length == 3 and (os.path.exists(sys.argv[1]))):
    folder = sys.argv[1]
    out_folder = sys.argv[2]

else:
    folder = sys.argv[1]
    out_folder = folder + '/out_images/'

# cv2.namedWindow("Video")

if not os.path.exists(out_folder):
    os.makedirs(out_folder)

start_index = random.random()*1000000

for subdir, dirs, files in os.walk(folder):

    file_index = 0
    for work_file in files:

        cond = work_file.endswith(".mp4")
        if not cond:
            continue
        print(subdir + '/' + work_file + '\n')

        vid_day = cv2.VideoCapture(subdir + '/' + work_file)
        isOpen = vid_day.isOpened()

        day_width  = int(vid_day.get(cv2.CAP_PROP_FRAME_WIDTH))  # float
        day_height = int(vid_day.get(cv2.CAP_PROP_FRAME_HEIGHT)) # float

        if not isOpen:
            exit(1)

        vid_day.set(cv2.CAP_PROP_POS_FRAMES, 0)
        nr_of_frames_day = vid_day.get(cv2.CAP_PROP_FRAME_COUNT)
        half_row = int(day_width/2)
        # Fastforawrd the second movie
        frame_index = 0
        while(True):
            ret, frame = vid_day.read()
            if not ret:
                break
            frame_index += 1
            rgb_image = frame[0:day_height,0:day_width]
            first_row = frame[:1,:,:1]
            read_x = int(np.average(frame[:1,0:half_row-10,:1]))
            x = 0
            if read_x > 30:
                x = 1
            if read_x > 60:
                x = 2
            
            read_y = int(np.average(frame[:1,half_row+10:day_width,:1]))
            y = 0
            if read_y > 30:
                y = 1
            if read_y > 60:
                y = 2
            # print ('X = ' + str(x) + '  Y = ' + str(y))
            # im = cv2.imshow("Video",rgb_image)
            filename = out_folder + '/' + str(int(frame_index + start_index)) + '_x' + str(x) + '_y' + str(y) + '.jpg'
            cv2.imwrite(filename, rgb_image)
            # key = cv2.waitKey(1)


cv2.waitKey(1)
cv2.destroyAllWindows()
exit(0)   

