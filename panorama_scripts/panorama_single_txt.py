import os

INNER_ID_IN_MOVIE_NAME_INDEX = -1

ARGV_FOLDER_INDEX = 1
ARGV_OUTPUT_INDEX = 2
ARGV_INPUT_INDEX = 3


def main(argv):
    movies_folder = argv[ARGV_FOLDER_INDEX]
    output_folder = argv[ARGV_OUTPUT_INDEX]

    frames = {} # {frame_num, bool_num}

    with open(argv[ARGV_INPUT_INDEX], 'r') as input_file:
        for line in input_file.readlines():
            members = line.replace('\n', '').split(' ')
            frames[int(members[0])] = int(members[1])

    seperate_movies_by_pbool(movies_folder, output_folder, frames)



def seperate_movies_by_pbool(movies_folder: str, output_folder: str, frames: dict):
    movies = os.listdir(movies_folder)
    movie_dict = {}
    output_files = {}

    movie_format = ''
    for movie in movies:
        if os.path.isfile(member):
            movie_members = movie.split('.')
            if movie_members[1] != 'props':
                movie_dict[int(movie_members[0].split('_')[1])] = os.path.join(movies_folder, movie)
                movie_format = movie_members[1]
        else:
            pass

    for frame_num in frames.keys(): # {frame_num, bool_num}
        movie_id = int(frame_num / 1000)
        pbool_id = frames[frame_num]
        output_file_name = os.path.join(output_folder, f'CombinedMovie_{pbool_id}.{movie_format}')
        inner_frame_num = frame_num % 1000

        movie_path = movie_dict[movie_id]
        with open(movie_path, 'rb') as movie_file:
            movie_file.seek(get_frame_size(movie_format) * inner_frame_num)
            frame = movie_file.read(get_frame_size(movie_format))
        
        if pbool_id not in output_files.keys():
            output_files[pbool_id] = open(output_file_name, 'wb+')
        output_files[pbool_id].write(frame)

        print(frame_num)
    
    for open_file in output_files.values():
        open_file.close()
        


def get_frame_size(movie_format: str):
    if movie_format.endswith('rawc') or movie_format.endswith('rgb'):
        return 720 * 576 * 3 # width * height * pixel size
    elif movie_format.endswith('raw2'):
        return 640 * 480 * 2 # width * height * pixel size


if __name__ == "__main__":
    main(os.sys.argv)