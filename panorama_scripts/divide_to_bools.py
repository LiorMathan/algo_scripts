from cv2 import cv2
import numpy as np
import os
from Reader import *

directory = input("Please enter directory: ")
extension = input("Please enter extension: ")
NUM_ROWS_BOOLS = int(input("Please enter number of rows: "))
NUM_COLS_BOOLS = int(input("Please enter number of cols: "))
has_id = input("Has ID? (type y/n): ")
from_fixed_props = input("From 'fixed' props file? (type y/n): ")

if (extension == ".raw2"):
    is_thermal = True
else:
    is_thermal = False

if is_thermal:
    IS_PANORAMA_ACTIVE = -1
    #IS_PANORAMA_ACTIVE = 40
    BOOL_ROW = 26
    BOOL_COL = 27

else:
    IS_PANORAMA_ACTIVE = -1
    # IS_PANORAMA_ACTIVE = 23
    BOOL_ROW = 24
    BOOL_COL = 25

#dir_id = int(os.path.basename(os.path.dirname(directory)).split('_')[0])
#if (dir_id < 18 or dir_id == 29):
#    if is_thermal:
#        IS_PANORAMA_ACTIVE = 25
#    else:
#        IS_PANORAMA_ACTIVE = 23


def extract_id_number(elem):
    if '_' in elem:
        if 'Bools' in elem:
            return None
        if has_id == 'y':
            return(int(elem.split('_')[0]))
        else:
            return(int(elem.split('_')[1].split('.')[0]))            
    else:
        return None

def get_scenes():
    scene_extensions = ["raw2", "rawc", "rgb"]
    files = os.listdir(directory)
    files.sort(key=extract_id_number)
    scenes = []

    for file in files:
        if file.split('.')[-1] == extension[1:]:
            scenes.append(os.path.join(directory, file))
    
    return scenes

def get_area_from_row_col(row, col):
    if (NUM_ROWS_BOOLS - row) % 2 != 0:
        area = NUM_COLS_BOOLS * (NUM_ROWS_BOOLS - row - 1) + col + 1
    else:
        area = NUM_COLS_BOOLS * (NUM_ROWS_BOOLS - row - 1) + (NUM_COLS_BOOLS - col)
    
    return area

def get_props_data(props_filename, frame_number):
    with open(props_filename, 'r') as f:
        for i in range(frame_number + 1):
            line = f.readline()

        if (line == ''):
            raise Exception()

        split_line = line.split()
        row, col = int(split_line[BOOL_ROW]), int(split_line[BOOL_COL])
        is_panorama = int(split_line[IS_PANORAMA_ACTIVE])

        return (row, col, is_panorama, line)

def get_props_file(scene):
    for file in os.listdir(directory):
        if from_fixed_props == 'n':
            props_extension = ".props"
        else:
            props_extension = "fixed.props"
            
        if extract_id_number(os.path.basename(scene)) == extract_id_number(file) and file.endswith(props_extension):
            return os.path.join(directory, file)
    return None

def get_width_height(extension):
    if extension == '.raw2':
        width, height = (640, 480)
    elif extension == '.rawc' or extension == '.rgb':
        width, height = (720, 576)
    else:
        width, height = (None, None)
    
    return (width, height)

def devide_to_bools(scenes):
    prefix = input("Please enter desired prefix: ")
    out_path = os.path.join(directory, 'Bools')
    try:
        os.mkdir(out_path)
    except:
        pass

    raw_file        = [0] * NUM_ROWS_BOOLS * NUM_COLS_BOOLS
    text_file       = [0] * NUM_ROWS_BOOLS * NUM_COLS_BOOLS
    props_file      = [0] * NUM_ROWS_BOOLS * NUM_COLS_BOOLS
    frame_counter   = [0] * NUM_ROWS_BOOLS * NUM_COLS_BOOLS

    for i in range(NUM_ROWS_BOOLS * NUM_COLS_BOOLS):
        raw_file[i] = open(os.path.join(out_path, prefix + 'area' + str(i+1) + extension), 'wb')
        text_file[i] = open(os.path.join(out_path, prefix + 'area' + str(i+1) + '.txt'), 'w')
        props_file[i] = open(os.path.join(out_path, prefix + 'area' + str(i+1) + '.props'), 'w')

    width, height = get_width_height(extension)
    for scene in scenes:
        print(os.path.basename(scene) + " is being processed...")

        reader = Reader(path1=scene, width1=width, height1=height)
        props_filename = get_props_file(scene)

        i = 0
        while i < reader.num_frames1:
            row, col, is_panorama, line = get_props_data(props_filename, i)
            
            if is_panorama == 1:
                start = i
                while is_panorama == 1:
                    i += 1
                    if i >= reader.num_frames1:
                        break
                    row, col, is_panorama, line = get_props_data(props_filename, i)
                end = i - 1

                chosen_frame_idx = (start + end) // 2
                row, col, is_panorama, line = get_props_data(props_filename, chosen_frame_idx)

                area = get_area_from_row_col(row, col)
                frame = reader.get_frame(index=chosen_frame_idx, bgr=False)
                raw_file[area-1].write(frame)
                text_file[area-1].write(str(frame_counter[area - 1]) + ': ' + os.path.basename(scene) + ' ' + str(chosen_frame_idx) + ' (' + line.split()[0] + ') \n')
                props_file[area-1].write(line)
                frame_counter[area - 1] += 1

            else:
                i += 1

    for i in range(NUM_ROWS_BOOLS * NUM_COLS_BOOLS):
        raw_file[i].close()
        text_file[i].close()
        props_file[i].close()


### Main: ###
scenes = get_scenes()
devide_to_bools(scenes)
