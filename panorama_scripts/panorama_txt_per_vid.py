import os

INNER_ID_IN_MOVIE_NAME_INDEX = -1

ARGV_FOLDER_INDEX = 1
ARGV_OUTPUT_INDEX = 2

PANORAMA_EXT = 'panorama'
PROPS_EXT = 'props'

def main(argv):
    try:
        movies_folder = argv[ARGV_FOLDER_INDEX]
        output_folder = argv[ARGV_OUTPUT_INDEX]
    except:
        movies_folder = 'E:\CMOS_RAW_Movies_1'
        output_folder = 'E:\\Testing'

    # if not os.path.isdir(output_folder):
    os.system(f'mkdir {output_folder}')
    seperate_movies_by_pbool(movies_folder, output_folder)



def seperate_movies_by_pbool(movies_folder: str, output_folder: str):
    files = os.listdir(movies_folder)
    panorama_dict = {}
    movie_dict = {}
    output_files = {}

    movie_format = ''
    for file in files:
        if os.path.isfile(os.path.join(movies_folder, file)):
            file_name, file_format = file.split('.')

            movie_id = int(file_name.split('_')[1])
            file_path = os.path.join(movies_folder, file)

            if file_format == PANORAMA_EXT:
                panorama_dict[movie_id] = file_path
            
            elif file_format != PROPS_EXT:
                movie_dict[movie_id] = file_path
                movie_format = file_format
        else:
            pass

    for movie_id in movie_dict.keys():
        if movie_id not in panorama_dict.keys():
            continue

        frames = {}
        with open(panorama_dict[movie_id]) as panorama_file:
            for line in panorama_file.readlines():
                line = line.replace('\n', '').split(' ')
                frame_id = int(line[0])
                pbool_id = ' '.join([line[-2], line[-1]])  

                output_file_name = os.path.join(output_folder, f'CombinedMovie_{pbool_id}.{movie_format}')
                movie_path = movie_dict[movie_id]

                with open(movie_path, 'rb') as movie_file:
                    movie_file.seek(get_frame_size(movie_format) * frame_id)
                    frame = movie_file.read(get_frame_size(movie_format))
                
                if pbool_id not in output_files.keys():
                    output_files[pbool_id] = open(output_file_name, 'wb+')
                output_files[pbool_id].write(frame)

                print(frame_id)
    
    for open_file in output_files.values():
        open_file.close()
        


def get_frame_size(movie_format: str):
    if movie_format.endswith('rawc') or movie_format.endswith('rgb'):
        return 720 * 576 * 3 # width * height * pixel size
    elif movie_format.endswith('raw2'):
        return 640 * 480 * 2 # width * height * pixel size


if __name__ == "__main__":
    main(os.sys.argv)