from cv2 import cv2
import numpy as np
import os
import math
from os import path
from Reader import *
from Target import *
from scipy.spatial import distance
import matplotlib.pyplot as plt


ALL_FRAMES = True
NUM_ROWS_BOOLS = int(input("Please enter number of rows: "))
NUM_COLS_BOOLS = int(input("Please enter number of cols: "))

IS_PANORAMA_ACTIVE = 3
BOOL_COL = 2
BOOL_ROW = 1
ROLL = 0
YAW = 0
PITCH = 0

movie_filename      = input("Movie file path: ")
props_filename      = input("Props file path: ")
# detections_filename = input("Detections file path: ")

try:
    width = int(input("Movie's width: "))
    height = int(input("Movie's height: "))
except:
    width, height = (None, None)

reader = Reader(path1=movie_filename, width1=width, height1=height)
if reader.images_path is not None:
    width, height = (reader.width1, reader.height1)
frame_size = height * width

# globals
frames_data = []
is_stops_updated = False


class Frame_Data:
    def __init__(self):
        self.pitch = None
        self.roll = None
        self.yaw = None
        self.is_panorama = None
        self.area = None
        self.row = None
        self.col = None
        self.is_stable_frame = False


# clears screen
def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def get_detections_list(detections_filename):
    targets = [[] for i in range(len(frames_data))]

    with open(detections_filename, 'r') as f:
        lines = f.readlines()
        for line in lines:
            line = line.split(' ')
            frame_number = int(line[0])
            label        = int(line[1])
            confidence   = int(line[2])
            cx           = int(line[3])
            cy           = int(line[4])
            w            = int(line[5])
            h            = int(line[6])

            targets[frame_number].append(Target(label, cx, cy, w, h, 'AG', confidence))

    return targets


def table_from_txt_file(filename, delimiter=' '):
    result = [None] * NUM_ROWS_BOOLS * NUM_COLS_BOOLS
    with open(filename, "r") as f:
        for i in range(NUM_ROWS_BOOLS * NUM_COLS_BOOLS):
            line = f.readline()
            elements = line.split(delimiter)
            result[i].append([float(elements[1]), float(elements[2]), float(elements[3])])
    
    return result


# updates global list 'frames_data' with pitch, roll and area data for each frame
def update_frame_data_list():
    cls()
    print("Collecting frames' data...")

    for i in range(reader.num_frames1):
        current_frame = Frame_Data()
        try:
            if reader.images_path is not None:
                col = reader.images_path[i].split('_')[-1][1]
                row = reader.images_path[i].split('_')[-2][1]
                pitch, roll, yaw, is_panorama = (0, 0, 0, 1)
            else:
                pitch, roll, yaw, row, col, is_panorama = get_props_data(i)
            
            current_frame.pitch = float(pitch)
            current_frame.roll = float(roll)
            current_frame.yaw = float(yaw)
            current_frame.is_panorama = int(is_panorama)
            current_frame.row = int(row)
            current_frame.col = int(col)
            current_frame.area = get_area_from_row_col(current_frame.row, current_frame.col)
            frames_data.append(current_frame)
        except:
            pass


def get_area_from_row_col(row, col):
    if (NUM_ROWS_BOOLS - row) % 2 != 0:
        area = NUM_COLS_BOOLS * (NUM_ROWS_BOOLS - row - 1) + col + 1
    else:
        area = NUM_COLS_BOOLS * (NUM_ROWS_BOOLS - row - 1) + (NUM_COLS_BOOLS - col)
    
    return area


# reads one frame from the file and displays it
# overlays a header with information if needed
def show_frame(filename, index, header, scale=(height, width)):
    frame = reader.get_frame(index=index, bgr=True)
    if header is not None:
        overlay_text_on_image(frame, header)

    # resizing the window for convenience:
    if scale == (1440, 1920):
        cv2.namedWindow('frames', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('frames', 1344, 1008)
    cv2.imshow('frames', frame)


# converts a uint8 format nparray into uint16 format
def uint8_to_uint16_image(image):
    min_value = np.amin(image)
    max_value = np.amax(image)
    original_range = max_value - min_value
    # to avoid dividing by 0
    if original_range == 0:
        original_range = 255

    scale = 65535.0/np.float(original_range)
    transform_image = (image - min_value).astype(np.float) * scale
    transform_image = np.around(transform_image)
    transform_image = np.uint16(transform_image)

    return transform_image


# returns a string header with the frame's information
def build_image_header(frame_number, show_area):
    header = 'frame #' + str(frame_number) + ' pitch: ' + str(frames_data[frame_number].pitch) + ' roll: ' + str(frames_data[frame_number].roll) + ' yaw: ' + str(frames_data[frame_number].yaw)

    if (show_area is True):
        area = str(frames_data[frame_number].area)
        header = header + ' area: ' + area

    return header


# returns header string in the divided video format
def build_image_header_for_divided_video(area, frame_number):
    text_file = open('area'+str(area)+'.txt', 'r')
    for i in range(frame_number):
        line = text_file.readline()
    split_line = line.split(' ')
    header = 'area: ' + str(area) + ' original frame number: ' + str(split_line[3])
    text_file.close()
    return header


# returns 'image' with the overlay header
def overlay_text_on_image(image, header, scale=0.5):
    font = cv2.FONT_HERSHEY_DUPLEX
    bottom_left_corner_of_text = (10, 20)
    font_scale = scale
    font_color = (255, 255, 255)
    line_type = 1

    cv2.putText(image, header,
                bottom_left_corner_of_text,
                font,
                font_scale,
                font_color,
                line_type)

    return image


# displays camera film from frame 'start'.
# 'show_header' and 'show_area' indicate if header/area display is needed
def display_movie(start, show_header, show_area):
    header = None
    for i in range(start, reader.num_frames1):
        if show_header is True:
            header = build_image_header(i, show_area)
        show_frame(movie_filename, i, header)
        cv2.waitKey(10)
    cv2.destroyAllWindows()


# returns pitch, roll and yaw for 'frame_number' frame
def get_props_data(frame_number):
    with open(props_filename, 'r') as f:
        for i in range(frame_number + 1):
            line = f.readline()

        if (line == ''):
            raise Exception()

        split_line = line.split()
        pitch, roll, yaw = int(split_line[PITCH])/100.0, int(split_line[ROLL])/100.0, int(split_line[YAW])/100.0
        row, col = split_line[BOOL_ROW], split_line[BOOL_COL]
        is_panorama = split_line[IS_PANORAMA_ACTIVE]

        return (pitch, roll, yaw, row, col, is_panorama)


# returns the most stable frame index between frame indexes 'start' and 'end'
# which is the last sharp enough frame.
def get_stable_frame_index(start, end):
    if start == end:
        return start

    max_focus_quality = 0
    max_index = start

    fm_list = []
    for i in range(start, end+1):
        frame = reader.get_frame(index=i, bgr=True)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        fm = cv2.Laplacian(gray[200:250,:], cv2.CV_64F).var()
        fm_list.append(fm)
        if fm >= max_focus_quality:
            max_focus_quality = fm
            max_index = i
        
        # for debug:
    #     cv2.imshow(str(i), gray)
    # plt.plot(np.arange(start, end+1, 1), fm_list)
    # cv2.waitKey(0)
    # plt.show()
    
    threshold = 0.7 * max_focus_quality
    for i in range(end, start-1, -1):
        if fm_list[i-start] >= threshold:
            frames_data[i].is_stable_frame = True
            return i


# calculates and returns the distance (movement) between previous and next frame
# relatively to frame in index 'frame_number'
def get_previous_next_distance(frame_number):
    previous_point = (frames_data[frame_number - 1].pitch, frames_data[frame_number - 1].roll, frames_data[frame_number - 1].yaw)
    current_point = (frames_data[frame_number].pitch, frames_data[frame_number].roll, frames_data[frame_number].yaw)
    distance = compute_euclidean_distance(previous_point, current_point)
    if frame_number < reader.num_frames1 - 1:
        next_point = (frames_data[frame_number+1].pitch, frames_data[frame_number+1].roll, frames_data[frame_number + 1].yaw)
        distance += compute_euclidean_distance(current_point, next_point)

    return distance


# returns True if the frame is averagely black, False if not
def is_blackened_image(frame):
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    average = np.mean(gray_frame)
    
    if average < 50:
        return True
    else:
        return False


# calculates and returns the distance between p1 and p2
def compute_euclidean_distance(p1, p2):
    d = distance.euclidean(p1, p2)
    return d


# opens and updates .txt and .raw2 files with most stable frames for each area from frame 698
def create_divided_videos_by_area(all_frames=False):
    cls()
    print("The system finds the most stable frames")
    print("The process might take few seconds, please be patient...")
    # array of binary raw
    raw_file = [0] * NUM_ROWS_BOOLS * NUM_COLS_BOOLS
    # array of text files
    text_file = [0] * NUM_ROWS_BOOLS * NUM_COLS_BOOLS
    # array that stores c
    frame_counter = [0] * NUM_ROWS_BOOLS * NUM_COLS_BOOLS

    # open files:
    for i in range(NUM_ROWS_BOOLS * NUM_COLS_BOOLS):
        if reader.images_path is None:
            raw_file[i] = open(os.path.join('outs', 'area' + str(i+1) + "." + movie_filename.split('.')[-1]), 'wb')
        text_file[i] = open(os.path.join('outs', 'area' + str(i+1) + '.txt'), 'w')

    # update_frame_data_list()
    start = 0

    while start < reader.num_frames1:
        frames_indexes = []

        if all_frames is True:
            if (frames_data[start].is_panorama == 1):
                frames_indexes.append(start)
                frames_data[start].is_stable_frame = True
            end = start
        
        else:
            try:
                while start < reader.num_frames1 and frames_data[start].is_panorama == 0:
                    start += 1
                end = start
                while end < reader.num_frames1-1 and frames_data[start].is_panorama == frames_data[end].is_panorama == 1 and frames_data[end].area == frames_data[start].area:
                    end += 1
                if end > start:
                    end -= 1
            except:
                break

            frames_indexes.append(get_stable_frame_index(max(start-2, 0), min(end+2, reader.num_frames1-1)))
        
        for idx in frames_indexes:
            frame = reader.get_frame(index=idx, bgr=False)
            if frame is None:
                continue

            if reader.images_path is None:
                raw_file[frames_data[idx].area - 1].write(frame)
            text_file[frames_data[idx].area - 1].write('frame number ' + str(frame_counter[frames_data[idx].area - 1]) + ': ' + str(idx) + ' \n')
            frame_counter[frames_data[idx].area - 1] += 1
        
        start = end + 1

    global is_stops_updated
    is_stops_updated = True
    # close files:
    for i in range(NUM_ROWS_BOOLS * NUM_COLS_BOOLS):
        if reader.images_path is None:
            raw_file[i].close()
        text_file[i].close()


# displays all divided videos one by one
def display_divided_videos():
    # if path.exists('area1' + "." + movie_filename.split('.')[-1]) is False:
    create_divided_videos_by_area(ALL_FRAMES)
    for i in range(NUM_ROWS_BOOLS * NUM_COLS_BOOLS):
        display_divided_video_by_area(i + 1)


# displays the divided video for the specified area
def display_divided_video_by_area(area):
    filename = 'area' + str(area) + '.txt'
    num_lines = file_len(filename)

    for i in range(num_lines):
        header = build_image_header_for_divided_video(area, i+1)
        show_frame('area' + str(area) + "." + movie_filename.split('.')[-1], i, header)
        cv2.waitKey(10)
    cv2.destroyAllWindows()


# returns num of lines in 'filename'
def file_len(filename):
    with open(filename) as f:
        for i, l in enumerate(f):
            pass
    return i


# creates the area map image and displays it
def create_and_show_area_map():
    area_map = create_area_map()
    show_area_map(area_map)

    return area_map


# creates the area map image
def create_area_map():
    # if path.exists("area1." + movie_filename.split('.')[-1]) is False:
    create_divided_videos_by_area(ALL_FRAMES)
    area_images = [None] * NUM_ROWS_BOOLS * NUM_COLS_BOOLS

    for i in range(NUM_ROWS_BOOLS * NUM_COLS_BOOLS):
        filename = os.path.join('outs', 'area' + str(i+1) + '.' + movie_filename.split('.')[-1])
        area_reader = Reader(path1=filename)
        area_images[i] = area_reader.get_frame(index=0, bgr=True)

    panorama_width = width * NUM_COLS_BOOLS
    panorama_height = height * NUM_ROWS_BOOLS

    base_image = np.zeros((panorama_height, panorama_width, 3), np.uint8)

    for i in range(NUM_ROWS_BOOLS * NUM_COLS_BOOLS):
        position = (get_image_position_by_area(i+1))
        overlay_images_in_position(base_image, area_images[i], position)

    cv2.imwrite(os.path.join('outs', 'map_area_file.png'), base_image)
    # with open('area_map.txt', 'wb') as map_file:
    #     map_file.write(base_image)

    return base_image


# displays the area map image
def show_area_map(image):
    cv2.namedWindow('area map', cv2.WINDOW_NORMAL)
    cv2.imshow('area map', image)
    cv2.waitKey(0)  # waits until a key is pressed
    cv2.destroyAllWindows()  # destroys the window showing image


# overlays 'top_image' on top of 'base_image'
def overlay_images_in_position(base_image, top_image, position):
    try:
        base_image[position[1]:position[1] + top_image.shape[0], position[0]:position[0] + top_image.shape[1], :] = top_image
    except:
        pass
    return base_image


# returns the required position in the area map image according to the area
def get_image_position_by_area(area):
    position = [0] * 2

    row = NUM_ROWS_BOOLS - ((area - 1) // NUM_COLS_BOOLS) - 1
    if (NUM_ROWS_BOOLS - row) % 2 != 0:
        col = (area - 1) % NUM_COLS_BOOLS
    else:
        col = NUM_COLS_BOOLS - (area - ((NUM_ROWS_BOOLS - row - 1) * NUM_COLS_BOOLS))

    position[0] = col * width
    position[1] = row * height

    return position


# displays the area map video
def show_area_map_video(show_header):
    create_divided_videos_by_area(ALL_FRAMES)
    # targets = get_detections_list()

    cls()
    print("Writing frames into raw file")
    print("The process might take a minute, please be patient...")
    panorame_width = width * NUM_COLS_BOOLS
    panorama_height = height * NUM_ROWS_BOOLS
    base_frame = np.zeros((panorama_height, panorame_width, 3), np.uint8)

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    videowriter = cv2.VideoWriter(os.path.join('outs', 'area_map.mp4'), fourcc, 1, (panorame_width, panorama_height))

    for i in range(reader.num_frames1):
        if frames_data[i].is_stable_frame is True:
            top_frame = reader.get_frame(index=i, bgr=True)
            # for target in targets[i]:
            #     target.draw(img=top_frame, thickness=2, expand=True)

            position = get_image_position_by_area(frames_data[i].area)
            base_frame = overlay_images_in_position(base_frame, top_frame, position)

            if show_header is not False:
                text_background = np.zeros((35, panorame_width, 3), np.uint8)
                base_frame = overlay_images_in_position(base_frame, text_background, (0, 0))
                header = 'Current Frame in Area: ' + str(frames_data[i].area) + '   -   Frame number: ' + str(i)
                overlay_text_on_image(base_frame, header, 1)

            videowriter.write(base_frame)

    videowriter.release()
    display_area_map_video_from_file(os.path.join('outs', 'area_map.mp4'))


# reads from .raw2 file 'raw_filename' the area map video and displays it
def display_area_map_video_from_file(filename):
    area_map_reader = Reader(path1=filename)
    area_map_reader.display()


# displays the user's menu
def display_menu():
    update_frame_data_list()

    menu = {}
    menu['1'] = "Display Entire Video"
    menu['2'] = "Display Video From Selected Frame"
    menu['3'] = "Display Video - Show Frame#, Pitch, Roll, Yaw and Area"
    menu['4'] = "Create RAW File and Text File For Each Area"
    menu['5'] = "Display Divided Videos By Area"
    menu['6'] = "Create Area Map"
    menu['7'] = "Display Area Map Video"
    menu['8'] = "Display Area Map Video - Show Area and Frame#"
    menu['9'] = "Exit"

    while True:
        cls()

        print('======== System Menu ========')
        options = menu.keys()
        for entry in options:
            print(entry, menu[entry])

        selection = input("Please Select: ")

        if selection == '1':
            display_movie(0, False, False)
        elif selection == '2':
            from_frame = int(input("Select frame number to start from: "))
            display_movie(from_frame, False, False)
        elif selection == '3':
            display_movie(0, True, True)
        elif selection == '4':
            create_divided_videos_by_area(ALL_FRAMES)
        elif selection == '5':
            display_divided_videos()
        elif selection == '6':
            create_and_show_area_map()
        elif selection == '7':
            show_area_map_video(False)
        elif selection == '8':
            show_area_map_video(True)
        elif selection == '9':
            exit(1)
        else:
            print("Unknown Option Selected!")


display_menu()

