import os
import time
import threading

class FolderIterator():
    original_path = ""

    def __init__(self, path: str):
        self.original_path = path


    def for_each_entry(self, target_f, condition_f=lambda folder: True, thread_limit=100, recursive=True, *args, **kwargs):
        threads = []

        def manage_threads():
            while True:
                all_dead = True
                
                for thread in threads:
                    if thread.isAlive():
                        all_dead = False
                    else:
                        threads.remove(thread)
                
        
        manager_thread = threading.Thread(target=manage_threads)
        manager_thread.setDaemon(True)
        manager_thread.start()
        
        self.__for_each_entry_recursion__(target_f, condition_f, threads, thread_limit, True, "", *args, **kwargs)
        while len(threads) > 0:
            pass


    def __for_each_entry_recursion__(self, target_f, condition_f, threads, thread_limit, recursive, override_path, *args, **kwargs):
        root_path = self.original_path
        if override_path != "":
            root_path = os.path.join(root_path, override_path)
        
        files = []
        for entry in os.listdir(root_path):
            full_path = os.path.join(root_path, entry)
            thread = None

            if os.path.isfile(full_path):
                thread = threading.Thread(target=target_f, args=(full_path, entry) + args, kwargs=kwargs)
            
            elif os.path.isdir(full_path) and recursive and condition_f(full_path):
                thread = threading.Thread(target=self.__for_each_entry_recursion__, args=(target_f, condition_f, threads, thread_limit, True, os.path.join(override_path, entry)) + args, kwargs=kwargs)
            
            while len(threads) > thread_limit:
                pass
                
            if thread != None:
                thread.start()
                threads.append(thread)
    

if __name__ == "__main__":
    f = FolderIterator("Z:\OfflineDB\data-pre-processing-scripts\list_scripts")
    f.for_each_entry(print, end="AAA\n")