import os
from Reader import *


class Metadata:
    class File:
        def __init__(self, filename, frames_count=None, is_annotated=None, is_cropped=None):
            self.filename = filename
            self.frames_count = frames_count
            self.is_annotated = is_annotated
            self.is_cropped = is_cropped


    def __init__(self, directory: str):
        self.directory = directory
        self.files = []
        self.resolution = None
        self.spectrum = None
        self.extension = None
        self.focal_length = None
    
        self.init()
    

    def init(self):
        self.init_spectrum()
        self.init_focal_length()
        self.init_extension()
        self.init_files_list()


    def init_spectrum(self):
        if "CMOS" in self.directory:
            self.spectrum = "VIS"
        else:
            self.spectrum = "Thermal"
    

    def init_focal_length(self):
        dirname_tokens = self.directory.split('_')

        if self.spectrum == "VIS":
            focal_prefix = "FV"
        else:
            focal_prefix = "FT"
        
        for token in dirname_tokens:
            if focal_prefix in token:
                self.focal = float(token.replace(focal_prefix, ''))
                break
        
    
    def init_extension(self):
        possible_extensions = ['.raw', '.raw2', '.rawc', '.rgb']
        second_possible_extensions = ['.mp4', '.png', '.yuv']

        for file in os.listdir(self.directory):
            extension = os.path.splitext(file)[-1]
            if extension in possible_extensions:
                self.extension = extension
                break
        
        if self.extension is None:
            for file in os.listdir(self.directory):
                extension = os.path.splitext(file)[-1]
                if extension in second_possible_extensions:
                    self.extension = extension
                    break

    
    def init_files_list(self):
        for file in os.listdir(self.directory):
            if file.endswith(self.extension):
                reader = Reader(path1=os.path.join(self.directory, file))
                new_file = self.File(filename=file, frames_count=reader.num_frames1)
                self.files.append(new_file)

                if self.resolution is None:
                    self.resolution = (reader.width1, reader.height1)


metadata = Metadata('/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Panorama/0037_100121_0143_Air_VGA_FT8.5_T2000_A120_Dpanorama_KefarHess/Thermal_RAW_Movies_1')
                
