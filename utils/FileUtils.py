import os
import numpy as np
import cv2


def get_image_extension_in_folder(folder):
    image_formats = [".png", ".jpg", ".bmp", ".yuv", ".jpeg", ".j2k"]
    for file in os.listdir(folder):
        _,extension = os.path.splitext(file)
        if extension in image_formats:
            return extension


def get_image_width_height_in_folder(folder, extension):
    # receives folder path and desired image extension and returns image's width and height in the following format: (width, height)
    if extension == ".yuv":
        return (3840, 2160)
    elif extension == ".j2k":
        return(640, 480)
        
    for file in os.listdir(folder):
        if file.endswith(extension):
            frame = cv2.imread(os.path.join(folder, file))
            return (frame.shape[1], frame.shape[0])


def extract_images_paths(folder):
    image_extension = get_image_extension_in_folder(folder=folder)
    frames = []
    
    for file in os.listdir(folder):
        if file.endswith(image_extension):
            frames.append(os.path.join(folder, file))
    frames.sort()
    
    return frames

