from Target import Target
from LabelSet import LABEL_SET
import numpy as np
import cv2
import os

class Crop:

    def __init__(self, crop, image_path: str, annotation_path: str, width: int, height: int, label_set: LABEL_SET):
        self.__crop             = crop
        self.__width            = width
        self.__height           = height
        self.__label_set        = label_set
        self.__image_path       = image_path
        self.__annotation_path  = annotation_path
        self.__annotations      = []

        self.extract_annotations()
       

    def extract_annotations(self):
        if os.path.isfile(self.__annotation_path):            
            with open(self.__annotation_path) as annotation_file:
                for line in annotation_file.readlines():
                    self.__annotations.append(Target.init_from_yolo_line(line, self.__width, self.__height, self.__label_set, 0, 0))


    def get_image(self, with_annotations: bool):
        if not self.__crop:
            self.__crop = cv2.imread(self.__image_path)
            if with_annotations:
                image = self.__crop.copy()
                for annotation in self.__annotations:
                    annotation.draw(image, show_label=True)
                return image
                
        return self.__crop


    def set_annotation_path(self, annotation_path):
        self.__annotation_path = annotation_path


    def set_annotations(self, annotations):
        self.__annotations = annotations


    def validate_crop(self, png_16bit=False):
        valid = True

        if png_16bit:
            crop_read = cv2.imread(self.__image_path, cv2.IMREAD_UNCHANGED)       
        else:
            crop_read = cv2.imread(self.__image_path)
            
        if crop_read is None:
            valid = False
            os.remove(self.__image_path)
        if (png_16bit and crop_read.dtype == np.uint16 and crop_read.size != self.__height * self.__width):
                valid = False
        elif (not png_16bit):
            if (crop_read.dtype == np.uint8 and crop_read.size != self.__height * self.__width * crop_read.shape[2]):
                valid = False
        
        return valid


    def write_annotations(self, frame_width, frame_height):
        with open(self.__annotation_path, "w") as f:
            for annotation in self.__annotations:
                f.write(annotation.get_as_yolo_line(frame_width, frame_height) + ' \n')


    def write_crop(self, png_16bit=False):
        success = True

        if png_16bit:
            self.__crop = self.__crop.astype(np.int32)
            self.__crop += 32768
            self.__crop = self.__crop.astype(np.uint16)

        if self.__image_path.endswith(".png"):
            cv2.imwrite(self.__image_path, self.__crop, [cv2.IMWRITE_PNG_COMPRESSION, 0])
            success = self.validate_crop(png_16bit)

        else:
            with open(self.__image_path, "wb") as f:
                print(self.__image_path)
                self.__crop = np.ascontiguousarray(self.__crop, dtype=np.int16)
                f.write(self.__crop)

        return success
