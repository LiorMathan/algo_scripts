import os
import numpy as np
import sys
import cv2
import RawUtils
import FileUtils
from snr import *
from tkinter import messagebox
from scipy.spatial import distance

class Reader:
    def __init__(self, path1, path2=None, width1=None, height1=None, width2=None, height2=None, dual=False, encode=False, output=None, showLabels=False, change_polarity=False, offset=0, fps=20):
        self.input_path = path1
        self.second_input_path = path2
        self.width1 = width1
        self.height1 = height1
        self.width2 = width2
        self.height2 = height2
        self.is_dual = dual
        self.to_encode = encode
        self.show_labels = showLabels
        self.output = output
        self.change_polarity = change_polarity
        self.offset = offset

        self.num_frames1 = None
        self.num_frames2 = None
        self.videocap = None
        self.videocap2 = None
        self.images_path = None
        self.images_path2 = None

        self.index = 0

        self.raw_formats = [".raw", ".raw2", ".rawc", ".BIN", ".Bin", ".rgb", ".yuv"]
        self.image_formats = [".png", ".jpg", ".bmp", ".yuv", ".j2k"]
        self.video_formats = [".mp4", ".MP4", ".avi"]

        self.font             = cv2.FONT_HERSHEY_PLAIN
        self.text_location    = (10,30)
        self.font_scale       = 1
        self.font_color       = (0,0,255)

        self.video_writer = None
        self.fps = fps
        self.image_extension = None

        try:
            self.init()
        except:
            return


    def update_data_members(self):
        _,extension = os.path.splitext(self.input_path)

        if (extension in self.raw_formats):
            if extension == ".rawc":
                if (self.width1 is None):
                    self.width1 = 720
                    self.height1 = 576
                try:
                    self.num_frames1 = RawUtils.get_number_of_frames_in_binary_file(filename=self.input_path, frame_bytes_size=self.width1*self.height1*1.5)
                except:
                    self.width1 = 640
                    self.height1 = 480
                    self.num_frames1 = RawUtils.get_number_of_frames_in_binary_file(filename=self.input_path, frame_bytes_size=self.width1*self.height1*1.5)
            elif extension == ".rgb":
                if (self.width1 is None):
                    self.width1 = 720
                    self.height1 = 576
                self.num_frames1 = RawUtils.get_number_of_frames_in_binary_file(filename=self.input_path, frame_bytes_size=self.width1*self.height1*3)
            elif extension == ".yuv":
                if not self.width1:
                    self.width1 = 3840
                    self.height1 = 2160
                self.num_frames1 = RawUtils.get_number_of_frames_in_binary_file(filename=self.input_path, frame_bytes_size=self.width1*self.height1*1.5)
            else:
                if (self.width1 is None):
                    self.width1 = 640
                    self.height1 = 480
                self.num_frames1 = RawUtils.get_number_of_frames_in_binary_file(filename=self.input_path, frame_bytes_size=self.width1*self.height1*2)
                
        elif extension in self.video_formats:
            self.videocap = cv2.VideoCapture(self.input_path)
            self.num_frames1 = int(self.videocap.get(7))
            self.width1 = int(self.videocap.get(3))
            self.height1 = int(self.videocap.get(4))
        
        elif len(extension) == 0:
            self.image_extension = FileUtils.get_image_extension_in_folder(self.input_path)
            self.width1, self.height1 = FileUtils.get_image_width_height_in_folder(self.input_path, self.image_extension)
        
        if self.is_dual is True:
            _,extension = os.path.splitext(self.second_input_path)
            if (extension in self.raw_formats):
                if extension == ".rawc":
                    if (self.width2 is None):
                        self.width2 = 720
                        self.height2 = 576
                    self.num_frames2 = RawUtils.get_number_of_frames_in_binary_file(filename=self.second_input_path, frame_bytes_size=self.width2*self.height2*1.5)
                    if type(self.num_frames2) == TypeError:
                        self.width2 = 640
                        self.height2 = 480
                        self.num_frames2 = RawUtils.get_number_of_frames_in_binary_file(filename=self.second_input_path, frame_bytes_size=self.width2*self.height2*1.5)
                elif extension == ".rgb":
                    if (self.width2 is None):
                        self.width2 = 720
                        self.height2 = 576
                    self.num_frames2 = RawUtils.get_number_of_frames_in_binary_file(filename=self.second_input_path, frame_bytes_size=self.width2*self.height2*3)
                else:
                    self.width2 = 640
                    self.height2 = 480
                    self.num_frames2 = RawUtils.get_number_of_frames_in_binary_file(filename=self.second_input_path, frame_bytes_size=self.width2*self.height2*2)
            
            elif extension == ".yuv":
                if not self.width2:
                    self.width2 = 3840
                    self.height2 = 2160

            elif extension in self.video_formats:
                self.videocap2 = cv2.VideoCapture(self.second_input_path)
                self.num_frames2 = int(self.videocap2.get(7))
                self.width2 = int(self.videocap2.get(3))
                self.height2 = int(self.videocap2.get(4))
            elif len(extension) == 0:
                self.image_extension = FileUtils.get_image_extension_in_folder(self.second_input_path)
                self.width2, self.height2 = FileUtils.get_image_width_height_in_folder(self.second_input_path, self.image_extension)

    def verify_input(self):
        if not os.path.isdir(self.input_path) and not os.path.isfile(self.input_path):
            print ('Video does not exists!')
            # print ('exiting...')
            # sys.exit(0)
            raise Exception()
    
    def init(self):
        try:
            self.verify_input()
        except:
            raise Exception()
        self.update_data_members()
        _,extension = os.path.splitext(self.input_path)
        self.basename = os.path.basename(self.input_path)
        if (len(extension) > 0):
            self.basename = self.basename[:-len(extension)]

        if self.to_encode is True:
            if self.is_dual is True:
                width = max(self.width1, self.width2) * 2
                height = max(self.height1, self.height2)
            else:
                width = self.width1
                height = self.height1
            fourcc = cv2.VideoWriter_fourcc(*'mp4v')
            output_name = os.path.join(self.output, self.basename + "_encoded.mp4")
            self.video_writer = cv2.VideoWriter(output_name, fourcc, self.fps, (width, height))

        _, extension1 = os.path.splitext(self.input_path)

        if len(extension1) == 0:
            self.images_path = FileUtils.extract_images_paths(folder=self.input_path)
            self.images_path.sort()
            self.num_frames1 = len(self.images_path)

        if self.is_dual is True:
            _, extension2 = os.path.splitext(self.second_input_path)
            if len(extension2) == 0:
                self.images_path2 = FileUtils.extract_images_paths(folder=self.second_input_path)
                self.images_path2.sort()
                self.num_frames2 = len(self.images_path2)

        
    def adjust_frames_size(self, frame1, frame2):
        frame1_width = frame1.shape[1]
        frame1_height = frame1.shape[0]
        frame2_width = frame2.shape[1]
        frame2_height = frame2.shape[0]

        if frame1_width < frame2_width and frame1_height < frame2_height:
            frame1 = cv2.copyMakeBorder(src=frame1, top=0, bottom=frame2_height-frame1_height, left=0, right=frame2_width-frame1_width, borderType=cv2.BORDER_CONSTANT, value=0)
            # frame2 = cv2.resize(frame2, (frame1_width, frame1_height), interpolation = cv2.INTER_AREA)
            # frame2.shape = (frame1_height, frame1_width, 3)
        
        elif frame1_width > frame2_width and frame1_height > frame2_height:
            frame2 = cv2.copyMakeBorder(src=frame2, top=0, bottom=frame1_height-frame2_height, left=0, right=frame1_width-frame2_width, borderType=cv2.BORDER_CONSTANT, value=0)
            # frame1 = cv2.resize(frame1, (frame2_width, frame2_height), interpolation = cv2.INTER_AREA)
            # frame1.shape = (frame2_height, frame2_width, 3)
        
        return (frame1, frame2)

    
    def get_frame(self, index, second_movie=False, bgr=False, thermal_endian=False):
        if second_movie is False:
            _, extension = os.path.splitext(self.input_path)
        else:
            _, extension = os.path.splitext(self.second_input_path)
        try:
            if extension in self.raw_formats:
                if second_movie is False:
                    frame = RawUtils.read_frame(start=index, filename=self.input_path, height=self.height1, width=self.width1, bgr=bgr, header=self.offset, thermal_endian=thermal_endian)
                else:
                    frame = RawUtils.read_frame(start=index, filename=self.second_input_path, height=self.height2, width=self.width2, bgr=bgr, header=self.offset, thermal_endian=thermal_endian)
            elif extension in self.video_formats:
                if second_movie is False:
                    self.videocap.set(cv2.CAP_PROP_POS_FRAMES, index)
                    _, frame = self.videocap.read()
                else:
                    self.videocap2.set(cv2.CAP_PROP_POS_FRAMES, index)
                    _, frame = self.videocap2.read()
            elif len(extension) == 0: # images folder
                if second_movie is False:
                    _,image_extension = os.path.splitext(self.images_path[0])
                    if image_extension == ".yuv" or image_extension == ".j2k":
                        frame = RawUtils.read_frame(start=0, filename=self.images_path[index], height=self.height1, width=self.width1, bgr=bgr)
                    else:
                        frame = cv2.imread(self.images_path[index])
                        # frame = cv2.putText(frame, os.path.basename(self.images_path[index]), self.text_location, self.font, self.font_scale, self.font_color)
                else:
                    _,image_extension = os.path.splitext(self.images_path2[0])
                    if image_extension == ".yuv" or image_extension == ".j2k":
                        frame = RawUtils.read_frame(start=0, filename=self.images_path2[index], height=self.height2, width=self.width2, bgr=bgr)
                    else:
                        frame = cv2.imread(self.images_path2[index])
                        # frame = cv2.putText(frame, os.path.basename(self.images_path2[index]), self.text_location, self.font, self.font_scale, self.font_color)
        except:
            frame = None
        
        return frame

    
    def on_scale_change(self, new_value):
        if self.to_encode == False:
            self.index = new_value
            cv2.setTrackbarPos("Frame", "Video", new_value)
            # self.show_frame(index=new_value)


    def get_snr(self, frame):
        r = cv2.selectROI("Select area for SNR:", frame, False, False)
        cx = int(r[0] + (r[2] / 2))
        cy = int(r[1] + (r[3] / 2))
        width = r[2]
        height = r[3]
        snr = calc_object_snr(frame=frame, cx=cx, cy=cy, w=width, h=height)
        
        messagebox.showinfo("SNR", "Object SNR is: " + str(snr))
        cv2.destroyWindow("Select area for SNR:")   
        
    
    def get_distance(self, frame):
        points = []
        for i in range(2):
            r = cv2.selectROI("Select 2 points:", frame, False, False)
            points.append([int(r[0]+r[2]/2), int(r[1]+r[3]/2)])
            cv2.circle(frame, (points[i][0], points[i][1]), 2, (0, 0, 255), -1)
        
        dist = distance.euclidean(points[0], points[1])
        
        messagebox.showinfo("Distance", "Distance: " + str(dist))
        cv2.destroyWindow("Select 2 points:")   


    def save_frame(self, frame):
        filename = os.path.basename(self.input_path).split('.')[0] + "_" + str(self.index).zfill(6) + ".JPEG"
        cv2.imwrite(filename, frame)
        print("saved frame: " + str(self.index))


    def convert_to_mp4(self):
        for i in range(self.num_frames1):
            frame = self.get_frame(index=i, bgr=True)
            if self.change_polarity is True:
                frame = 255 - frame
            if (self.to_encode is True):
                self.video_writer.write(frame)
        
        self.video_writer.release()


    def show_frame(self, index):
        frame = self.get_frame(index=index, bgr=True)

        if self.change_polarity is True:
            frame = 255 - frame
        if (self.to_encode is True):
            # frame = cv2.resize(frame, (self.width1*16, self.height1*16), interpolation=cv2.INTER_NEAREST) 
            self.video_writer.write(frame)
        if (self.num_frames1 == 1):
            wait_time = 0
        else:
            wait_time = 10

        frame = cv2.putText(frame, "frame " + str(index), self.text_location, self.font, self.font_scale, self.font_color)
        cv2.imshow("Video", frame)
        
        key = cv2.waitKey(wait_time)
        if self.to_encode is False:
            if key == 32: # if enter in pressed, pause video
                cv2.waitKey(0)
            if key == 27: # if ESC is pressed, exit loop
                cv2.destroyAllWindows()
                return False
            if key == ord('s') or key == ord('S'): # if s or S is pressed: open SNR window
                self.get_snr(frame)
            if key == ord('d') or key == ord('D'): # if a or A is pressed: open Area indow
                self.get_distance(frame)
            if key == ord('f' or key == ord('F')): # if s or S is pressed: save frame as image
                self.save_frame(frame)
        
        return True


    def display_single(self): 
        if self.width1 > 1920:
            cv2.namedWindow("Video", cv2.WINDOW_NORMAL)
        else:
            cv2.namedWindow("Video", cv2.WINDOW_AUTOSIZE)
        cv2.createTrackbar("Frame", "Video", 0, self.num_frames1, self.on_scale_change)
        cv2.setTrackbarPos("Frame", "Video", 0)

        while (self.index < self.num_frames1):
            cv2.setTrackbarPos("Frame", "Video", self.index)
            try:
                if (not self.show_frame(index=self.index)):
                    break
                self.index += 1
            except:
                break
        
        cv2.destroyAllWindows()
    

    def display_dual(self):
        number_of_frames = self.num_frames1
        if self.num_frames2 < self.num_frames1:
            number_of_frames = self.num_frames2
        if (self.num_frames1 == 1):
            wait_time = 0
        else:
            wait_time = 1
        
        if self.width1 > 1920 or self.width2 > 1920:
            cv2.namedWindow("Video", cv2.WINDOW_NORMAL)
        else:
            cv2.namedWindow("Video", cv2.WINDOW_AUTOSIZE)
        cv2.createTrackbar("Frame", "Video", 0, number_of_frames, self.on_scale_change)
        cv2.setTrackbarPos("Frame", "Video", 0)
        
        while (self.index < number_of_frames):
            cv2.setTrackbarPos("Frame","Video", self.index)
            frame1 = self.get_frame(index=self.index, bgr=True)
            frame2 = self.get_frame(index=self.index, second_movie=True, bgr=True)
            frame1, frame2 = self.adjust_frames_size(frame1, frame2)
            dual_frames = np.concatenate((frame1, frame2), axis=1)
            if self.change_polarity is True:
                dual_frames = 255 - dual_frames
            if (self.to_encode is True):
                self.video_writer.write(dual_frames)
            dual_frames = cv2.putText(dual_frames, "frame " + str(self.index), self.text_location, self.font, self.font_scale, self.font_color)
            cv2.imshow("Video", dual_frames)
            
            key = cv2.waitKey(wait_time)
            if self.to_encode is False:
                if key == 32: # if enter in pressed, pause video
                    cv2.waitKey(0)
                if key == 27: # if ESC is pressed, exit loop
                    cv2.destroyAllWindows()
                    break
                if key == ord('s') or key == ord('S'): # if s or S is pressed: open SNR window
                    self.get_snr(dual_frames)
                if key == ord('d') or key == ord('D'): # if a or A is pressed: open Area indow
                    self.get_distance(dual_frames)
                if key == ord('f' or key == ord('F')): # if s or S is pressed: save frame as image
                    self.save_frame(dual_frames)

            self.index += 1

        cv2.destroyAllWindows()
    


    def display(self):
        if self.is_dual:
            self.display_dual()
        else:
            self.display_single()
        
        if self.to_encode is True:
            self.video_writer.release()

    
