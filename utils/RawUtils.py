import numpy as np
import cv2
import os
import random
# from openjpeg import decode


##### Streching Consts #####
MAX_RANGE = 4095
NEW_RANGE = 255
SAMPLE_SIZE = 10000
PCTL = 1
EXPANSION = 0.1


def quant_sample_balance(frame, max_range=MAX_RANGE, new_range=NEW_RANGE, sample_size=SAMPLE_SIZE, pctl=PCTL, expansion=EXPANSION):
    flattened_frame = [val for sublist in frame for val in sublist]
    sample = random.sample(flattened_frame, sample_size)
    range_min = np.percentile(sample, pctl)
    range_max = np.percentile(sample, 100 - pctl)
    dynamic_range = (range_max - range_min) * (1 + expansion)
    if dynamic_range < new_range:
        dynamic_range = new_range
    range_min = np.mean([range_min, range_max]) - (dynamic_range / 2)
    range_max = np.mean([range_min, range_max]) + (dynamic_range / 2)
    if dynamic_range > max_range:
        dynamic_range = max_range
        range_min = (np.median(sample) + np.mean(sample)) / 2 - (dynamic_range / 2)
        range_max = (np.median(sample) + np.mean(sample)) / 2 + (dynamic_range / 2)
    streched_frame = (new_range * (frame - range_min)) / dynamic_range
    streched_frame[streched_frame < 0] = 0
    streched_frame[streched_frame > new_range] = new_range
    streched_frame = streched_frame.astype(np.uint8)

    return streched_frame


def read_thermal_endian_3channels(filename, index, width, height):
    with open(filename, 'rb') as f:
        f.seek(index*height*width*2, os.SEEK_SET)
        current_frame = np.fromfile(f, dtype=np.int16, count=height*width)
        frame = current_frame.reshape((height, width))

        first_channel = quant_sample_balance(frame=frame)
        int32_frame = frame.astype(np.int32)
        uint16_frame = (int32_frame + (2 ** 15)).astype(np.uint16)
        second_channel = (uint16_frame // (2 ** 8)).astype(np.uint8)
        third_channel = (uint16_frame % (2 ** 8)).astype(np.uint8)

        frame = np.array([third_channel, second_channel, first_channel])
        frame = np.transpose(frame, axes=(1, 2, 0))
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        return frame


def read_thermal_frame(filename, index, width, height, header=0, bgr=False, min_max=False, thermal_endian=False):
    if thermal_endian is True:
        return read_thermal_endian_3channels(filename, index, width, height)
    
    with open(filename, 'rb') as f:
        f.seek(header + (index*height*width*2), os.SEEK_SET)
        frame = np.fromfile(f, dtype=np.int16, count=height*width)

        if bgr is True:
            if min_max is True:
                frame = cv2.normalize(frame, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
            else:
                frame = quant_sample_balance(frame=frame)

            frame = frame.reshape((height, width))
            frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
        else:
            frame = frame.reshape((height, width))
            
    return frame


def read_frame(start, filename, height, width, bgr=False, header=0, thermal_endian=False):
    frame_size = width * height
    _, extension = os.path.splitext(filename)

    if extension in [".raw2", ".raw", ".BIN", ".Bin"]:
        current_frame = read_thermal_frame(filename=filename, index=start, width=width, height=height, header=header, bgr=bgr, min_max=False, thermal_endian=thermal_endian)
    elif extension == ".rawc":
        with open(filename, 'rb') as f:
            f.seek(int(start*height*width*1.5), os.SEEK_SET)
            current_frame = np.fromfile(f, dtype=np.uint8, count=int(frame_size*1.5), sep='')
            current_frame.shape = (int(height*1.5), width)
            if bgr is True:
                current_frame = cv2.cvtColor(current_frame, cv2.COLOR_YUV2BGR_I420)
    elif extension == ".yuv":
        with open(filename, 'rb') as f:
            current_frame = np.fromfile(f, count=frame_size*2 ,dtype=np.uint8)
            current_frame = current_frame.reshape(height, width, 2)
            if bgr is True:
                current_frame = cv2.cvtColor(current_frame, cv2.COLOR_YUV2BGR_YUYV)
    elif extension == ".rgb":
        with open(filename, 'rb') as f:
            f.seek(int(start*height*width*3), os.SEEK_SET)
            current_frame = np.fromfile(f, count=frame_size*3 ,dtype=np.uint8)
            try:
                current_frame.shape = (height, width, 3)
                if bgr is True:
                    current_frame = cv2.cvtColor(current_frame, cv2.COLOR_RGB2BGR)
            except:
                raise Exception()
    elif extension == ".j2k":
        with open(filename, 'rb') as f:
            current_frame = decode(f)

    return current_frame


def get_number_of_frames_in_binary_file(filename, frame_bytes_size):
    # returns number of frames in binary file (like raw formats)
    array_length = os.stat(filename).st_size
    number_of_frames = int(array_length/frame_bytes_size)

    if array_length % frame_bytes_size != 0:
        raise Exception()
        # pass
        
    return number_of_frames


# converts a uint8 format nparray into uint16 format
def uint8_to_uint16_image(image):
    min_value = np.amin(image)
    max_value = np.amax(image)
    original_range = max_value - min_value
    # to avoid dividing by 0
    if original_range == 0:
        original_range = 255

    scale = 65535.0/np.float(original_range)
    transform_image = (image - min_value).astype(np.float) * scale
    transform_image = np.around(transform_image)
    transform_image = np.uint16(transform_image)

    return transform_image