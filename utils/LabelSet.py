from enum import Enum

class LABEL_SET(Enum):
    GA = {
        0 : 'drone',
        1 : 'airplane',
        2 : 'bird',
        3 : 'UFO',
        4 : 'airplane with lights',
        5 : 'balloon',
        6 : 'human',
        7 : 'vehicle',
        8 : 'drone with lights',
        9 : 'single front light'
    }
    AG = {
        0 : 'pedestrian',
        1 : 'bicycle',
        2 : 'motorcycle',
        3 : 'vehicle',
        4 : 'bus',
        5 : 'truck',
        6 : 'train',
        7 : 'system',
        999 : 'vmd'
    }
    AG_2_classes = {
        0 : 'pedestrian',
        1 : 'vehicle'
    }
    GG = {
        0 : 'pedestrian',
        1 : 'bicycle',
        2 : 'motorcycle',
        3 : 'vehicle',
        4 : 'bus',
        5 : 'truck',
        6 : 'train',
        7 : 'system',
        999 : 'vmd'
    }
    GALASH = {
        0 : 'blob'
    }
    COCO = {
        0 : "person",
        1 : "bicycle",
        2 : "car",
        3 : "motorcycle",
        4 : "airplane",
        5 : "bus",
        6 : "train",
        7 : "truck",
        8 : "boat",
        9 : "traffic light",
        10:  "fire hydrant",
        12:  "stop sign",
        13:  "parking meter",
        14:  "bench",
        15:  "bird",
        16:  "cat",
        17:  "dog",
        18:  "horse",
        19:  "sheep",
        20:  "cow",
        21:  "elephant",
        22:  "bear",
        23:  "zebra",
        24:  "giraffe",
        26:  "backpack",
        27:  "umbrella",
        30:  "handbag",
        31:  "tie",
        32:  "suitcase",
        33:  "frisbee",
        34:  "skis",
        35:  "snowboard",
        36:  "sports ball",
        37:  "kite",
        38:  "baseball bat",
        39:  "baseball glove",
        40:  "skateboard",
        41:  "surfboard",
        42:  "tennis racket",
        43:  "bottle",
        45:  "wine glass",
        46:  "cup",
        47:  "fork",
        48:  "knife",
        49:  "spoon",
        50:  "bowl",
        51:  "banana",
        52:  "apple",
        53:  "sandwich",
        54:  "orange",
        55:  "broccoli",
        56:  "carrot",
        57:  "hot dog",
        58:  "pizza",
        59:  "donut",
        60:  "cake",
        61:  "chair",
        62:  "couch",
        63:  "potted plant",
        64:  "bed",
        66:  "dining table",
        69:  "toilet",
        71:  "tv",
        72:  "laptop",
        73:  "mouse",
        74:  "remote",
        75:  "keyboard",
        76:  "cell phone",
        77:  "microwave",
        78:  "oven",
        79:  "toaster",
        80:  "sink",
        81:  "refrigerator",
        83:  "book",
        84:  "clock",
        85:  "vase",
        86:  "scissors",
        87:  "teddy bear",
        88:  "hair drier",
        89:  "toothbrush",
        999: "vmd"
    }

# class LabelSetConverter:
#     def __init__(self, from_label_set: str, to_label_set: str):
#         self.from_label_set = from_label_set
#         self.to_label_set   = to_label_set
        
#         self.class_map = self.get_class_map()


#     def get_class_map(self):
#         if self.from_label_set == "AG":
#             if self.to_label_set == "COCO":
#                 return(self.get_AG_2_coco_class_map())


#     def get_AG_2_coco_class_map(self):
#         class_map = {}
        
#         for label_id in LABEL_SET["AG"].keys():
#             if label_id == 0: # pedestrian
#                 class_map[label_id] = 0
#             elif label_id == 1: # bicycle
#                 class_map[label_id] = 1
#             elif label_id == 2: # motorcycle
#                 class_map[label_id] = 3
#             elif label_id == 3: # vehicle
#                 class_map[label_id] = 2
#             elif label_id == 4: # bus
#                 class_map[label_id] = 5
#             elif label_id == 4: # truck
#                 class_map[label_id] = 5
#             elif label_id == 4: # train
#                 class_map[label_id] = 5
#             elif label_id == 999: # vmd
#                 class_map[label_id] = 999
#             else: 
#                 class_map[label_id] = None
            
    
    # def get_coco_2_AG_class_map(self):
        # class_map = {}
        
        # for label_id in LABEL_SET["AG"].keys():
        #     if label_id == 0: # pedestrian
        #         class_map[label_id] = 0
        #     elif label_id == 1: # bicycle
        #         class_map[label_id] = 1
        #     elif label_id == 2: # motorcycle
        #         class_map[label_id] = 3
        #     elif label_id == 3: # vehicle
        #         class_map[label_id] = 2
        #     elif label_id == 4: # bus
        #         class_map[label_id] = 5
        #     elif label_id == 4: # truck
        #         class_map[label_id] = 5
        #     elif label_id == 4: # train
        #         class_map[label_id] = 5
        #     elif label_id == 999: # vmd
        #         class_map[label_id] = 999
        #     else: 
        #         class_map[label_id] = None