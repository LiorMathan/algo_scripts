import cv2
import os.path
import numpy as np
import sys
from scipy.spatial import distance


def calc_object_snr(frame, cx, cy, w, h):
    epsilon = 0.001
    inner_width_ratio = float(0.29)
    inner_height_ratio = float(0.25)
    middle_width_ratio = float(0.83)
    middle_height_ratio = float(2.0/3.0)

    frame_height = frame.shape[0]
    frame_width = frame.shape[1]

    outer_x = int(cx - (w/2))
    outer_y = int(cy - (h/2))
    if outer_x < 0:
        outer_x = 0
    elif outer_x >= frame_width:
        outer_x = frame_width - 1
    if outer_y < 0:
        outer_y = 0
    elif outer_y >= frame_height:
        outer_y = frame_height - 1
    
    inner_width = int(w * inner_width_ratio)
    inner_height = int(h * inner_height_ratio)
    inner_x = int(cx - inner_width/2.0)
    inner_y = int(cy - inner_height/2.0)

    middle_width = int(w * middle_width_ratio)
    middle_height = int(h * middle_height_ratio)
    middle_x = int(cx - middle_width/2.0)
    middle_y = int(cy - middle_height/2.0)
    try:
        depth = frame.shape[2]
    except:
        depth = 0
    
    if (depth == 3): # color image
        inner_values_blue = []
        inner_values_green = []
        inner_values_red = []

        for i in range(inner_width):
            for j in range(inner_height):
                if inner_x+i < frame_width and inner_y+j < frame_height:
                    inner_values_blue.append(frame[inner_y+j][inner_x+i][0])
                    inner_values_green.append(frame[inner_y+j][inner_x+i][1])
                    inner_values_red.append(frame[inner_y+j][inner_x+i][2])

        if (len(inner_values_blue) == 0):
            inner_average_blue = frame[cy][cx][0]
            inner_average_green = frame[cy][cx][1]
            inner_average_red = frame[cy][cx][2]
        else:
            inner_average_blue = np.average(inner_values_blue, axis=None)
            inner_average_green = np.average(inner_values_green, axis=None)
            inner_average_red = np.average(inner_values_red, axis=None)
        
        outer_values_blue = []
        outer_values_green = []
        outer_values_red = []  

        for i in range(int(w)):
            for j in range(int(h)):
                if outer_x+i >= middle_x and outer_x+i <= (middle_x + middle_width): # if (i, j) are in the middle area:
                    if outer_y+j >= middle_y and outer_y+j <= (middle_y + middle_height):
                        continue
                if outer_x+i < frame_width and outer_y+j < frame_height:
                    outer_values_blue.append(frame[outer_y+j][outer_x+i][0])
                    outer_values_green.append(frame[outer_y+j][outer_x+i][1])
                    outer_values_red.append(frame[outer_y+j][outer_x+i][2])

        if len(outer_values_blue) == 0:
            outer_average_blue = frame[outer_y][outer_x][0]
            outer_average_green = frame[outer_y][outer_x][1]
            outer_average_red = frame[outer_y][outer_x][2]

        else:
            outer_average_blue = np.average(outer_values_blue)
            outer_average_green = np.average(outer_values_green)
            outer_average_red = np.average(outer_values_red)
        
        outer_std_blue = np.std(outer_values_blue)
        outer_std_green = np.std(outer_values_green)
        outer_std_red = np.std(outer_values_red)

        delta_average_blue = inner_average_blue - outer_average_blue
        delta_average_green = inner_average_green - outer_average_green
        delta_average_red = inner_average_red - outer_average_red

        snr_blue = float(delta_average_blue / (outer_std_blue + epsilon))
        snr_green = float(delta_average_green / (outer_std_green + epsilon))
        snr_red = float(delta_average_red / (outer_std_red + epsilon))

        snr = np.average([snr_blue, snr_green, snr_red], axis=None)

    else: # gray frame
        inner_values = []

        for i in range(inner_width):
            for j in range(inner_height):
                if inner_x+i < frame_width and inner_y+j < frame_height:
                    inner_values.append(frame[inner_y+j][inner_x+i])

        if (len(inner_values) == 0):
            inner_average = frame[cy][cx]
        else:
            inner_average = np.average(inner_values, axis=None)

        outer_values = []
        for i in range(int(w)):
            for j in range(int(h)):
                # if (i, j) are in the middle area:
                if outer_x+i >= middle_x and outer_x+i <= (middle_x + middle_width):
                    if outer_y+j >= middle_y and outer_y+j <= (middle_y + middle_height):
                        continue
                if outer_x+i < frame_width and outer_y+j < frame_height:
                    outer_values.append(frame[outer_y+j][outer_x+i])
        if len(outer_values) == 0:
            outer_average = frame[middle_y][middle_x]
        else: 
            outer_average = np.average(outer_values, axis=None)

        outer_std = np.std(outer_values)

        delta_average = inner_average - outer_average

        snr = float(delta_average / (outer_std + epsilon))

    return snr


# calculates and returns the distance between p1 and p2
def compute_euclidean_distance(p1, p2):
    d = distance.euclidean(p1, p2)
    return d