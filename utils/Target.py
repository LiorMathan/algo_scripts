from LabelSet import LABEL_SET
import cv2
import numpy as np


class Target:
    def __init__(self, label: int, cx: int, cy: int, width: int, height: int, label_set: str, confidence=100, snr=0):
        self.label = label
        self.cx = cx
        self.cy = cy
        self.width = width
        self.height = height 
        self.confidence = confidence
        self.snr = snr
        self.label_set = label_set
        self.label_str = LABEL_SET[label_set].value[label]


    @staticmethod
    def init_from_yolo_line(line: str, frame_width: int, frame_height: int, label_set: str, confidence=100, snr=0):
        line = line.replace('\n', '').split(' ')
        
        label = int(line[0])
        cx = int(float(line[1]) * frame_width)
        cy = int(float(line[2]) * frame_height)
        width = int(float(line[3]) * frame_width)
        height = int(float(line[4]) * frame_height)

        return Target(label, cx, cy, width, height, label_set, confidence, snr)

    
    @staticmethod
    def init_from_detection_line(line: str, label_set: str, top_left=False):
        line = line.replace('\n', '').split(' ')
        
        label = int(line[1])
        confidence = int(line[2])
        cx = int(line[3])
        cy = int(line[4])
        width = int(line[5])
        height = int(line[6])

        if top_left:
            cx = cx + (width / 2)
            cy = cy + (height / 2)
        
        target = Target(label, cx, cy, width, height, label_set, confidence)

        try:
            target.set_id(int(line[7]))
            target.set_snr(int(line[10]))
        except:
            pass

        return target 


    def set_label(self, label: int) -> None:
        self.label = label
        try:
            self.label_str = LABEL_SET[self.label_set].value[label]
        except:
            raise ValueError(f'Label {label} is not defined in the label set')
    

    def get_label(self) -> int:
        return self.label


    def get_label_str(self) -> str:
        return self.label_str


    def set_id(self, target_id: int) -> None:
        self.id = target_id
        
    
    def get_id(self) -> int:
        return self.id


    def set_cx(self, cx: int) -> None:
        self.cx = cx
    
    
    def get_cx(self) -> int:
        return self.cx


    def set_cy(self, cy: int) -> None:
        self.cy = cy
    
    
    def get_cy(self) -> int:
        return self.cy


    def set_width(self, width: int) -> None:
        self.width = width
    
    
    def get_width(self) -> int:
        return self.width


    def set_height(self, height: int) -> None:
        self.height = height
    
    
    def get_height(self) -> int:
        return self.height


    def set_confidence(self, confidence: int) -> None:
        self.confidence = confidence
    
    
    def get_confidence(self) -> int:
        return self.confidence


    def set_snr(self, snr: float) -> None:
        self.snr = snr
    
    
    def get_snr(self) -> float:
        return self.snr


    def set_label_set(self, label_set: str) -> None:
        self.label_set = label_set
        try:
            self.label_str = LABEL_SET[label_set].value[self.label]
        except:
            raise ValueError(f'Label {self.label} is not defined in the label set')    


    def get_as_yolo_line(self, frame_width: int, frame_height: int) -> str:
        YOLO_LINE_FORMAT = '{label} {normalized_cx} {normalized_cy} {normalized_width} {normalized_height}'

        return YOLO_LINE_FORMAT.format(
            label=self.label, 
            normalized_cx=self.cx / frame_width,
            normalized_cy=self.cy / frame_height,
            normalized_width=self.width / frame_width,
            normalized_height=self.height / frame_height)

    
    def get_as_yolo_list(self, frame_width=None, frame_height=None, normalized=False) -> list:
        yolo_list = [self.label, self.cx, self.cy, self.width, self.height]

        if normalized:
            yolo_list[1] /= frame_width
            yolo_list[2] /= frame_height            
            yolo_list[3] /= frame_width
            yolo_list[4] /= frame_height

        return yolo_list
    

    def get_as_detection_line(self, frame_number) -> str:
        DETECTION_LINE_FORMAT = '{frame_number} {label} {confidence} {cx} {cy} {width} {height}'

        return DETECTION_LINE_FORMAT.format(
            frame_number=frame_number,
            label=self.label,
            confidence=self.confidence,
            cx=self.cx,
            cy=self.cy,
            width=self.width,
            height=self.height)


    def get_as_rectangle_start_end(self) -> tuple:
        return (
            (int(self.cx - self.width / 2), int(self.cy - self.height / 2)), 
            (int(self.cx + self.width / 2), int(self.cy + self.height / 2)))


    def draw(self, img, color=(0, 0, 255), thickness=1, show_label=False, expand=False, text=None, bottom_left_text=False, text_scale=0.4):
        rectangle = self.get_as_rectangle_start_end()
        if expand:
            rectangle = ((int(rectangle[0][0] - 5), int(rectangle[0][1] - 5)), (int(rectangle[1][0] + 5), int(rectangle[1][1] + 5)))
        
        cv2.rectangle(img, rectangle[0], rectangle[1], color, thickness) 

        if show_label:
            text_size = cv2.getTextSize(text=self.label_str, fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=text_scale, thickness=1)[0]
            if bottom_left_text:
                label_position = (rectangle[0][0], rectangle[1][1] + text_size[1])
            else:
                label_position = rectangle[0]
            cv2.rectangle(img=img, pt1=(label_position[0], int(label_position[1] - text_size[1])), pt2=(label_position[0]+text_size[0], label_position[1]), color=color, thickness=-1) 
            cv2.putText(img=img, text=self.label_str, org=label_position, fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=text_scale, color=(255,255,255), thickness=1)
        
        if text is not None:
            cv2.putText(img, text, rectangle[0], cv2.FONT_HERSHEY_SIMPLEX, 1, color, 1)


    def get_centered_crop(self, frame_width: int, frame_height: int, crop_size: int):
        if crop_size > frame_width or crop_size > frame_height:
            raise ValueError(f'Crop size "{crop_size}" must be smaller than frame size "{frame_width}x{frame_height}"')
        
        if crop_size < self.width or crop_size < self.height:
            raise ValueError(f'Crop size "{crop_size}" must be bigger than target size "{self.width}x{self.height}"')

        crop_start_x = self.cx - int(crop_size / 2)
        crop_start_y = self.cy - int(crop_size / 2)
        
        crop_end_x = self.cx + int(crop_size / 2)
        crop_end_y = self.cy + int(crop_size / 2)

        if crop_start_x < 0:
            crop_end_x += 0 - crop_start_x
            crop_start_x = 0
        
        if crop_start_y < 0:
            crop_end_y += 0 - crop_start_y
            crop_start_y = 0

        if crop_end_x > frame_width:
            crop_start_x -= crop_end_x - frame_width
            crop_end_x = frame_width

        if crop_end_y > frame_height:
            crop_start_y -= crop_end_y - frame_height
            crop_end_y = frame_height

        return ((crop_start_x, crop_start_y),
                (crop_end_x, crop_end_y))
            

    def is_center_in_frame(self, frame_width, frame_height):

        return (frame_width > self.cx >= 0 and frame_height > self.cy >= 0)


    def __repr__(self):

        return "<Target> label: {}, cx: {}, cy: {}, w: {}, h: {}".format(self.label, self.cx, self.cy, self.width, self.height)