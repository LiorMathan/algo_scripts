import subprocess
import os
import cv2
import csv
import random
import json
import itertools
import glob
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from numpy.lib.financial import _irr_dispatcher


vis_dirs            = []
ir_dirs             = []

OUTPUT_DIR          = '/home/lior/workspace/AlgoVersions/algowrapper/bin64/outs'
GT_DIR              = '/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/GT'
TRANSFORMATIONS_DIR = '/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Affine_Transformation_Matrix'
CM_PER_PIXEL_FILE   = None #'/home/lior/workspace/OptimizationSets/AG-High-OptimizationSet/cmPerPixel.txt'

##########################################
# settings
algowrapper_path    = "./home/lior/workspace/AlgoVersions/algowrapper/bin64/AlgoWrapper"
tester_type         = 7
csv_file            = 'outs/AG-Panorama-Optimization.csv'
sensorType          = 'IR'  # 'VIS' / 'IR'
in_json_file        = '/home/lior/workspace/AlgoVersions/algowrapper/bin64/algo/algo_DUAL_benchmark.json'
frames_interval     = -1   # [ -1 = ALL ]

dual_benchmark = True
num_samples = 1
sample_size = -1 # [ -1 = ALL ]
warm_up = 20

custom_skips = False
analytics_fps = 0.14
TIMESTAMP_COLUMN = 10

calibrate_manually = True
desired_cm_per_pixel = None # for example: 15

##########################################

ERROR = -1


def calc_fps_from_props(movie_filename):
    props_filename = get_props_filename(movie_filename)
    if props_filename is None:
        return None
        
    timestamps_diffs = []
    with open(props_filename, 'r') as f:
        line = f.readline()
        if len(line) < 2:
            return None
            
        prev_timestamp = int(line.split()[TIMESTAMP_COLUMN])
        line = f.readline()
        while(line):
            current_timestamp = int(line.split()[TIMESTAMP_COLUMN])
            diff = current_timestamp - prev_timestamp
            timestamps_diffs.append(diff)
            prev_timestamp = current_timestamp
            line = f.readline()
    
    mean_timestamp_diff = sum(timestamps_diffs) / len(timestamps_diffs)
    fps = int(1000 / mean_timestamp_diff)
    
    return fps


def get_cm_per_pixel_dict(filename=CM_PER_PIXEL_FILE):
    cm_per_pixel_dict = {}
    with open(filename, 'r') as f:
        lines = f.readlines()
        for line in lines:
            try:
                cm_per_pixel_dict[line.split()[0]] = float(line.split()[1])
            except:
                cm_per_pixel_dict[line.split()[0]] = None
    
    return cm_per_pixel_dict


def update_testester_json_file(tes_params):
    with open('tesTester.json', 'r+') as f:
        data = json.load(f)
        
        data['VideoReader']['NumSamples']   = str(tes_params['numSamples'])
        data['VideoReader']['SampleSize']   = str(tes_params['sampleSize'])
        data['VideoReader']['SepSize']      = str(tes_params['sepSize'])
        data['VideoReader']['Skips']        = str(tes_params['skips'])
        data['VideoReader']['StartFrame']   = str(tes_params['startFrame'])
        data['VideoReader']['EndFrame']     = str(tes_params['endFrame'])
        data['VideoReader']['WarmUp']       = str(tes_params['warmUp'])
        data['VideoConvert']['CmPerPixVIS'] = str(tes_params['cmPixVis'])
        data['VideoConvert']['CmPerPixIR']  = str(tes_params['cmPixTherm'])


        f.seek(0)  # <--- should reset file position to the beginning.
        json.dump(data, f, indent=4)
        f.truncate()  # remove remaining part

    
def get_num_of_frames(file, img_size):
    bytes = os.path.getsize(file)
    if (bytes % img_size != 0):
        return ERROR
    num_of_frames = int(bytes / img_size)
    return num_of_frames


def get_params_dict():

    scores_to_test      = np.arange(start=30, stop=100, step=5)       
    history_length      = np.arange(start=1, stop=2, step=1)        
    # consist_len         = np.arange(start=1, stop=2, step=1)  
    # max_unmatch_frames  = np.arange(start=1, stop=2, step=1)   
    source_type         = [2, 3]  
    conf_bonus          = np.arange(start=0, stop=30, step=10)   
    # Blobing:
    min_blob_width      = np.arange(start=1, stop=2, step=1)
    min_blob_height     = np.arange(start=1, stop=2, step=1)
    # VMD:
    match_var_th        = np.arange(start=0.8, stop=5, step=0.3)       
    min_width           = np.arange(start=1, stop=2, step=1)         
    min_height          = np.arange(start=1, stop=2, step=1)         
    max_blob_th         = np.arange(start=100, stop=101, step=1)
    resize_half         = ["false"]
    kernel_size         = np.arange(start=0, stop=1, step=2)
    block_size          = np.arange(start=2, stop=3, step=1)
    max_age             = np.arange(start=20, stop=21, step=1)
    grid_width          = np.arange(start=36, stop=37, step=1)
    grid_height         = np.arange(start=28, stop=29, step=1)
    window_size         = np.arange(start=21, stop=22, step=1)
    ransac_error_th     = np.arange(start=0.5, stop=0.6, step=0.5)
    reset_on_saturation = ["false"]
    mute_on_saturation  = ["false"]
    high_saturation_th  = np.arange(start=0.1, stop=0.2, step=0.1)
    low_saturation_th   = np.arange(start=0.05, stop=0.06, step=0.01)   
    morph_size          = np.arange(start=1, stop=5, step=1) 
    # DualFrameAlgo:
    nms_razor_threshold = np.arange(start=0.4, stop=0.8, step=0.1)
    intersection_method = [0, 1]
    # LabelFiltering:
    car_confidence      = np.arange(start=20, stop=100, step=5)
    human_confidence    = np.arange(start=20, stop=100, step=5)
    # car_min_size        = np.arange(start=1, stop=2, step=1)
    human_max_size      = np.arange(start=70, stop=150, step=10)
    
    
    conf_dict = {}

    conf_dict['history_len']                  = random.choice(history_length)
    conf_dict['score']                        = "0 100 100 0" #str(random.choice(scores_to_test)) + " 100 100 100" #+ str(random.choice(scores_to_test)) #" ".join(map(str, random.choices(scores_to_test, k=4)))
    conf_dict['consist_len']                  = conf_dict['history_len']
    conf_dict['consist_amount']               = random.choice(np.arange(start=1, stop=max(2, int(conf_dict['consist_len'])), step=1))
    conf_dict['max_unmatch_frames']           = conf_dict['consist_len'] - conf_dict['consist_amount'] + 1
    conf_dict['conf_bonus']                   = random.choice(conf_bonus)
    conf_dict['vmd_bonus']                    = random.choice(conf_bonus)
    conf_dict['history_len_therm']            = conf_dict['history_len']
    conf_dict['score_therm']                  = conf_dict['score']
    conf_dict['consist_len_therm']            = conf_dict['consist_len']
    conf_dict['consist_amount_therm']         = conf_dict['consist_amount'] 
    conf_dict['max_unmatch_frames_therm']     = conf_dict['max_unmatch_frames']
    conf_dict['conf_bonus_therm']             = conf_dict['conf_bonus']
    conf_dict['vmd_bonus_therm']              = conf_dict['vmd_bonus']

    source_list = random.sample(source_type, random.choice([1, 2]))
    source_list.sort()
    conf_dict['source_type']                  = "2 3" # " ".join(map(str, source_list))

    # Blobing:
    conf_dict['min_blob_width']               = random.choice(min_blob_width)
    conf_dict['min_blob_height']              = random.choice(min_blob_height)
    conf_dict['max_blob_width']               = 1000000 #random.choice(np.arange(start=conf_dict['min_blob_width'], stop=101, step=1))
    conf_dict['max_blob_height']              = 1000000 #random.choice(np.arange(start=conf_dict['min_blob_height'], stop=101, step=1))

    # VMD:    
    conf_dict['match_var_th']                 = random.choice(match_var_th)
    conf_dict['det_var_th']                   = random.choice(np.arange(start=conf_dict['match_var_th']+0.5, stop=6.5, step=0.3))
    conf_dict['min_width']                    = random.choice(min_width)
    conf_dict['min_height']                   = random.choice(min_height)
    conf_dict['max_width']                    = 1000000 #random.choice(np.arange(start=conf_dict['min_width'] + 1, stop=101, step=1))
    conf_dict['max_height']                   = 1000000 #random.choice(np.arange(start=conf_dict['min_height'] + 1, stop=101, step=1))
    conf_dict['max_blob_th']                  = random.choice(max_blob_th)
    conf_dict['resize_half']                  = random.choice(resize_half)
    conf_dict['kernel_size']                  = random.choice(kernel_size)
    conf_dict['block_size']                   = random.choice(block_size)
    conf_dict['max_age']                      = random.choice(max_age)
    conf_dict['grid_width']                   = 24 #random.choice(grid_width)
    conf_dict['grid_height']                  = 19 #random.choice(grid_height)
    conf_dict['window_size']                  = random.choice(window_size)
    conf_dict['ransac_error_th']              = random.choice(ransac_error_th)
    conf_dict['reset_on_saturation']          = random.choice(reset_on_saturation)
    conf_dict['mute_on_saturation']           = random.choice(mute_on_saturation)
    conf_dict['high_saturation_th']           = random.choice(high_saturation_th)
    conf_dict['low_saturation_th']            = random.choice(low_saturation_th)
    conf_dict['morph_size_first']             = 1 #random.choice(morph_size)
    conf_dict['morph_size_second']            = 2 #random.choice(morph_size)
    
    conf_dict['match_var_th_therm']           = random.choice(match_var_th)
    conf_dict['det_var_th_therm']             = random.choice(np.arange(start=conf_dict['match_var_th_therm']+0.5, stop=6.5, step=0.3))
    conf_dict['min_width_therm']              = random.choice(min_width)
    conf_dict['min_height_therm']             = random.choice(min_height)
    conf_dict['max_width_therm']              = 1000000 #random.choice(np.arange(start=conf_dict['min_width_therm'] + 1, stop=101, step=1))
    conf_dict['max_height_therm']             = 1000000 #random.choice(np.arange(start=conf_dict['min_height_therm'] + 1, stop=101, step=1))
    conf_dict['max_blob_th_therm']            = random.choice(max_blob_th)
    conf_dict['resize_half_therm']            = random.choice(resize_half)
    conf_dict['kernel_size_therm']            = random.choice(kernel_size)
    conf_dict['block_size_therm']             = random.choice(block_size)
    conf_dict['max_age_therm']                = random.choice(max_age)
    conf_dict['grid_width_therm']             = 21 #random.choice(grid_width)
    conf_dict['grid_height_therm']            = 16 #random.choice(grid_height)
    conf_dict['window_size_therm']            = random.choice(window_size)
    conf_dict['ransac_error_th_therm']        = random.choice(ransac_error_th)
    conf_dict['reset_on_saturation_therm']    = random.choice(reset_on_saturation)
    conf_dict['mute_on_saturation_therm']     = random.choice(mute_on_saturation)
    conf_dict['high_saturation_th_therm']     = random.choice(high_saturation_th)
    conf_dict['low_saturation_th_therm']      = random.choice(low_saturation_th)
    conf_dict['morph_size_first_therm']       = 1 #random.choice(morph_size)
    conf_dict['morph_size_second_therm']      = 2 #random.choice(morph_size)

    # DualAlgo:
    conf_dict['transformation']               = "1 0 0 0 1 0"
    conf_dict['nms_razor_threshold']          = 0.2 #random.choice(nms_razor_threshold)
    conf_dict['intersection_method']          = 0 #random.choice(intersection_method)

    # LabelFiltering:
    conf_dict['car_confidence']               = random.choice(car_confidence)
    conf_dict['human_confidence']             = random.choice(human_confidence)
    conf_dict['human_vmd_max_size']           = random.choice(human_max_size)
    conf_dict['human_atr_max_size']           = conf_dict['human_vmd_max_size'] * 4
    conf_dict['car_vmd_min_size']             = conf_dict['human_vmd_max_size']
    conf_dict['car_atr_min_size']             = conf_dict['human_vmd_max_size']
    

    return conf_dict


def update_json_file(json_name, conf_dict):
    json_basename = os.path.basename(json_name)[:-len('.json')]
    json_header = json_basename.replace('algo_', '').replace('_benchmark', '')

    with open(json_name, 'r+') as file_in, open('algo/algo_' + json_header + '.json', 'w') as file_out:
        data = json.load(file_in)
        data[json_header]['targetsManager']['MinConfidence']            = str(conf_dict['score'])
        data[json_header]['targetsManager']['HistoryLength']            = str(conf_dict['history_len'])
        data[json_header]['targetsManager']['ConsistencyLength']        = str(conf_dict['consist_len'])
        data[json_header]['targetsManager']['ConsistencyAmount']        = str(conf_dict['consist_amount'])
        data[json_header]['targetsManager']['MaxUnmatchedFrames']       = str(conf_dict['max_unmatch_frames'])
        data[json_header]['targetsManager']['ConfidenceBonus']          = str(conf_dict['conf_bonus'])
        data[json_header]['targetsManager']['VMDBonus']                 = str(conf_dict['vmd_bonus'])

        data[json_header]['targetsManagerTherm']['MinConfidence']       = str(conf_dict['score_therm'])
        data[json_header]['targetsManagerTherm']['HistoryLength']       = str(conf_dict['history_len_therm'])
        data[json_header]['targetsManagerTherm']['ConsistencyLength']   = str(conf_dict['consist_len_therm'])
        data[json_header]['targetsManagerTherm']['ConsistencyAmount']   = str(conf_dict['consist_amount_therm'])
        data[json_header]['targetsManagerTherm']['MaxUnmatchedFrames']  = str(conf_dict['max_unmatch_frames_therm'])
        data[json_header]['targetsManagerTherm']['ConfidenceBonus']     = str(conf_dict['conf_bonus_therm'])
        data[json_header]['targetsManagerTherm']['VMDBonus']            = str(conf_dict['vmd_bonus_therm'])

        data[json_header]['targetsManager']['SourceType']               = conf_dict['source_type']

        # nn:
        data[json_header]['nn']['ImageResizeValueDay']                  = str(conf_dict['resize_vis'])
        data[json_header]['nn']['ImageResizeValueTherm']                = str(conf_dict['resize_therm'])

        # Blobing:
        data[json_header]['blobing']['MinBlobWidth']                    = str(conf_dict['min_blob_width'])
        data[json_header]['blobing']['MinBlobHeight']                   = str(conf_dict['min_blob_height'])
        data[json_header]['blobing']['MaxBlobWidth']                    = str(conf_dict['max_blob_width'])
        data[json_header]['blobing']['MaxBlobHeight']                   = str(conf_dict['max_blob_height'])
        # VMD:
        data[json_header]['vmd']['MatchingVarianceTH']                  = str(conf_dict['match_var_th'])
        data[json_header]['vmd']['DetectionVarianceTH']                 = str(conf_dict['det_var_th'])
        data[json_header]['vmd']['MinBlobWidth']                        = str(conf_dict['min_width'])
        data[json_header]['vmd']['MinBlobHeight']                       = str(conf_dict['min_height'])
        data[json_header]['vmd']['MaxBlobWidth']                        = str(conf_dict['max_width'])
        data[json_header]['vmd']['MaxBlobHeight']                       = str(conf_dict['max_height'])
        data[json_header]['vmd']['MaxBlobsTH']                          = str(conf_dict['max_blob_th'])
        data[json_header]['vmd']['ResizeHalf']                          = str(conf_dict['resize_half'])
        data[json_header]['vmd']['KernelSize']                          = str(conf_dict['kernel_size'])
        data[json_header]['vmd']['BlockSize']                           = str(conf_dict['block_size'])
        data[json_header]['vmd']['MaxAge']                              = str(conf_dict['max_age'])
        data[json_header]['vmd']['GridWidth']                           = str(conf_dict['grid_width'])
        data[json_header]['vmd']['GridHeight']                          = str(conf_dict['grid_height'])
        data[json_header]['vmd']['WindowSize']                          = str(conf_dict['window_size'])
        data[json_header]['vmd']['RansacErrorTH']                       = str(conf_dict['ransac_error_th'])
        data[json_header]['vmd']['ResetOnSaturation']                   = str(conf_dict['reset_on_saturation'])
        data[json_header]['vmd']['MuteOnSaturation']                    = str(conf_dict['mute_on_saturation'])
        data[json_header]['vmd']['HighSaturationTH']                    = str(conf_dict['high_saturation_th'])
        data[json_header]['vmd']['LowSaturationTH']                     = str(conf_dict['low_saturation_th'])
        data[json_header]['vmd']['morphSizeFirstElem']                  = str(conf_dict['morph_size_first'])
        data[json_header]['vmd']['morphSizeSecondElem']                 = str(conf_dict['morph_size_second'])
        data[json_header]['vmd']['VMDImageResizeValue']                 = str(conf_dict['resize_vis'])

        data[json_header]['vmdTherm']['MatchingVarianceTH']             = str(conf_dict['match_var_th_therm'])
        data[json_header]['vmdTherm']['DetectionVarianceTH']            = str(conf_dict['det_var_th_therm'])
        data[json_header]['vmdTherm']['MinBlobWidth']                   = str(conf_dict['min_width_therm'])
        data[json_header]['vmdTherm']['MinBlobHeight']                  = str(conf_dict['min_height_therm'])
        data[json_header]['vmdTherm']['MaxBlobWidth']                   = str(conf_dict['max_width_therm'])
        data[json_header]['vmdTherm']['MaxBlobHeight']                  = str(conf_dict['max_height_therm'])
        data[json_header]['vmdTherm']['MaxBlobsTH']                     = str(conf_dict['max_blob_th_therm'])
        data[json_header]['vmdTherm']['ResizeHalf']                     = str(conf_dict['resize_half_therm'])
        data[json_header]['vmdTherm']['KernelSize']                     = str(conf_dict['kernel_size_therm'])
        data[json_header]['vmdTherm']['BlockSize']                      = str(conf_dict['block_size_therm'])
        data[json_header]['vmdTherm']['MaxAge']                         = str(conf_dict['max_age_therm'])
        data[json_header]['vmdTherm']['GridWidth']                      = str(conf_dict['grid_width_therm'])
        data[json_header]['vmdTherm']['GridHeight']                     = str(conf_dict['grid_height_therm'])
        data[json_header]['vmdTherm']['WindowSize']                     = str(conf_dict['window_size_therm'])
        data[json_header]['vmdTherm']['RansacErrorTH']                  = str(conf_dict['ransac_error_th_therm'])
        data[json_header]['vmdTherm']['ResetOnSaturation']              = str(conf_dict['reset_on_saturation_therm'])
        data[json_header]['vmdTherm']['MuteOnSaturation']               = str(conf_dict['mute_on_saturation_therm'])
        data[json_header]['vmdTherm']['HighSaturationTH']               = str(conf_dict['high_saturation_th_therm'])
        data[json_header]['vmdTherm']['LowSaturationTH']                = str(conf_dict['low_saturation_th_therm'])
        data[json_header]['vmdTherm']['morphSizeFirstElem']             = str(conf_dict['morph_size_first_therm'])
        data[json_header]['vmdTherm']['morphSizeSecondElem']            = str(conf_dict['morph_size_second_therm'])
        data[json_header]['vmdTherm']['VMDImageResizeValue']            = str(conf_dict['resize_therm'])

        # Dual Frame:
        data[json_header]['DualFrameAlgo']['TransformationMatrix']      = str(conf_dict['transformation'])
        data[json_header]['DualFrameAlgo']['NMSRazorThreshold']         = str(conf_dict['nms_razor_threshold'])
        data[json_header]['DualFrameAlgo']['IntersectionMethod']        = str(conf_dict['intersection_method'])

        # LabelFiltering:
        data[json_header]['LabelFiltering']['Car']['Confidence']        = str(conf_dict['car_confidence'])
        data[json_header]['LabelFiltering']['Car']['VMDMinSize']        = str(conf_dict['car_vmd_min_size'])
        data[json_header]['LabelFiltering']['Car']['ATRMinSize']        = str(conf_dict['car_atr_min_size'])
        data[json_header]['LabelFiltering']['Human']['Confidence']      = str(conf_dict['human_confidence'])
        data[json_header]['LabelFiltering']['Human']['VMDMaxSize']      = str(conf_dict['human_vmd_max_size'])
        data[json_header]['LabelFiltering']['Human']['ATRMaxSize']      = str(conf_dict['human_atr_max_size'])

        new_data = {
            json_header: data[json_header]
        }
        file_out.seek(0)  # <--- should reset file position to the beginning.
        json.dump(new_data, file_out, indent=4)
        file_out.truncate()  # remove remaining part

        file_in.close()
        file_out.close()

    return ('algo/algo_' + json_header + '.json')


def get_scene_list():
    dirs = []
    if sensorType == 'VIS':
        dirs = vis_dirs
        image_extensions = [".rawc", ".rgb"]
        # image_extensions = [".yuv"]
        video_extensions = []
    elif sensorType == 'IR':
        dirs = ir_dirs
        image_extensions = [".raw2"]
        video_extensions = []

    scene_list = []

    for i in dirs:
        print("Collecting scenes from " + i + "...")

        for directory in os.listdir(i):
            full_path_dir = os.path.join(i, directory)
            if os.path.isfile(full_path_dir):
                continue

            for directory2 in os.listdir(full_path_dir):
                if 'RAW' not in directory2:
                    continue
                full_path_dir = os.path.join(i, directory, directory2)
                if os.path.isfile(full_path_dir):
                    continue

                for file in os.listdir(full_path_dir):
                    frame_width = None
                    frame_height = None
                    num_frames = None
                    extension = []
                    scene_path = []
                    filename, extension = os.path.splitext(file)
                    full_path = os.path.join(full_path_dir, file)
                    if extension.lower() in image_extensions or extension.lower() in video_extensions:
                        if file.endswith(".yuv"):
                            frame_height = 2160
                            frame_width = 3840
                            num_frames = len([x for x in os.listdir(full_path_dir) if x.endswith(extension)])
                            scene_path = full_path_dir
                        elif 'FLIR' in full_path:
                            frame_height = 512
                            frame_width = 640
                            num_frames = get_num_of_frames(full_path, frame_height*frame_width*2)  # TODO
                            scene_path = full_path
                        elif file.endswith(".raw2"):
                            frame_height = 480
                            frame_width = 640
                            num_frames = get_num_of_frames(full_path, frame_height*frame_width*2)  # TODO
                            scene_path = full_path
                        elif file.endswith(".rawc"):
                            frame_height = 576
                            frame_width = 720
                            num_frames = get_num_of_frames(full_path, frame_height*frame_width*1.5)  # TODO
                            if num_frames == ERROR: # in case resolution is 640*480:
                                frame_height = 480
                                frame_width = 640
                                num_frames = get_num_of_frames(full_path, frame_height*frame_width*1.5)  # TODO 
                            scene_path = full_path
                        elif file.endswith(".rawc2"):
                            frame_height = 480
                            frame_width = 640
                            num_frames = get_num_of_frames(full_path, frame_height * frame_width * 1.5)  # TODO
                            scene_path = full_path
                        elif file.endswith(".rgb"):
                            frame_height = 576
                            frame_width = 720
                            num_frames = get_num_of_frames(full_path, frame_height * frame_width * 3)  # TODO
                            scene_path = full_path
                        elif extension in image_extensions:
                            image = cv2.imread(full_path)
                            frame_width = image.shape[1]
                            frame_height = image.shape[0]
                            num_frames = len([x for x in os.listdir(full_path_dir) if x.endswith(extension)])
                            scene_path = full_path_dir
                        else:  # video
                            vidcap = cv2.VideoCapture(full_path)
                            frame_width = int(vidcap.get(cv2.CAP_PROP_FRAME_WIDTH))
                            frame_height = int(vidcap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                            num_frames = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
                            scene_path = full_path
                        #break

                    if scene_path is not None and frame_height is not None and frame_width is not None and num_frames is not None:

                        tes_skips = 1
                        if custom_skips:
                            try:
                                record_fps = calc_fps_from_props(scene_path)
                                if record_fps is None:
                                    tes_skips = 1
                                else:
                                    tes_skips = float(record_fps / analytics_fps)
                                if (tes_skips < 1):
                                    tes_skips = 1
                                # if (num_frames / (tes_skips) < warm_up):
                                if tes_skips > 50:
                                    tes_skips = 30
                                print("record FPS: " + str(record_fps) + " skips: " + str(tes_skips))
                            except:
                                pass

                        start_frame = 0
                        end_frame = num_frames - 1

                        tmp_dict = {'sceneName': filename,
                                    'scenePath': scene_path,
                                    'type': extension,
                                    'imgWidth': frame_width,
                                    'imgHeight': frame_height,
                                    'vis_path': None,
                                    'vis_width': None,
                                    'vis_height': None,
                                    'frameCount': num_frames,
                                    'startFrame': start_frame,
                                    'endFrame': end_frame,
                                    'numSamples': num_samples,
                                    'sampleSize': sample_size,
                                    'sepSize': None,
                                    'skips': tes_skips,
                                    'warmUp': min(warm_up, num_frames),
                                    'resize_vis': None,
                                    'resize_therm': None}

                        scene_list.append(tmp_dict)


    print("********************************************************************************************************")
    print("total scenes found: " + str(len(scene_list)))

    return scene_list


def create_single_file(scene):
    extension = '.' + scene.get('scenePath').split('.')[-1]
    if extension == '.raw2':
        width, height = (640, 480)
    elif (extension == '.rawc'):
        width, height = (720, 576)
    elif (extension == '.rgb'):
        width, height = (720, 576)
    if ('FLIR' in scene.get('scenePath')):
        width, height = (640, 512)
    
    gt_file = os.path.join(GT_DIR, os.path.basename(scene.get('scenePath'))[:-len(extension)] + "_gt")
    if os.path.exists(gt_file):
        filename = 'optimization_GT.fus'
    else:
        filename = 'optimization.fus'

    with open(filename, 'w') as f:
        for i in range(2):
            f.write(scene.get('scenePath') + "\n")
            f.write(extension + '\n')
            f.write(str(width) + '\n')
            f.write(str(height) + '\n')
            if os.path.exists(gt_file):
                f.write(gt_file + '\n')

    return filename


def create_dual_file(scene):
    ir_scene = scene.get('scenePath')
    ir_basename = os.path.basename(ir_scene)
    ir_extension = '.' + ir_basename.split('.')[-1]
    dirname = os.path.dirname(os.path.dirname(ir_scene))
    vis_basename = ir_basename[:-len(ir_extension)].replace('r0', 'r2') + '.rawc'
    # vis_basename = ir_basename[:-len(ir_extension)] + '.rgb'
    vis_scene = None
    
    for path in Path(dirname).rglob('*' + vis_basename):
        vis_scene = path

    if vis_scene is None:
        vis_basename = vis_basename.replace('r2', 'r1')
        for path in Path(dirname).rglob('*' + vis_basename):
            vis_scene = path
        if vis_scene is None:
            vis_basename = vis_basename.replace('.rawc', '.rgb')
            for path in Path(dirname).rglob('*' + vis_basename):
                vis_scene = path
            if vis_scene is None:
                vis_basename = vis_basename.replace('r1', 'r2')
                for path in Path(dirname).rglob('*' + vis_basename):
                    vis_scene = path
                if vis_scene is None:
                    vis_scene = ir_scene
                    # return None
    
    _, vis_extension = os.path.splitext(vis_scene)

    gt_file_ir = os.path.join(GT_DIR, os.path.basename(ir_scene)[:-len(ir_extension)] + "_gt")
    gt_file_vis = os.path.join(GT_DIR, os.path.basename(vis_scene)[:-len(vis_extension)] + "_gt")

    if os.path.exists(gt_file_ir) and os.path.exists(gt_file_vis):
        filename = 'optimization_GT.fus'
    else:
        filename = 'optimization.fus'

    with open(filename, 'w') as f:
        f.write(str(vis_scene) + "\n")
        f.write(vis_extension + '\n')
        if (vis_extension in ".rawc"):
            f.write('720' + '\n')
            f.write('576' + '\n')
        elif (vis_extension in ".rgb"):
            f.write('720' + '\n')
            f.write('576' + '\n')
        else:
            if 'FLIR' in vis_scene:
                f.write('640' + '\n')
                f.write('512' + '\n')
            else:
                f.write('640' + '\n')
                f.write('480' + '\n')
        if "GT" in filename:
            f.write(gt_file_vis + '\n')
        f.write(ir_scene + '\n')
        f.write(ir_extension + '\n')
        if (ir_extension in ".raw2"):
            if 'FLIR' in ir_scene:
                f.write('640' + '\n')
                f.write('512' + '\n')
            else:
                f.write('640' + '\n')
                f.write('480' + '\n')
        else:
            if (vis_extension in ".rgb"):
                f.write('720' + '\n')
                f.write('576' + '\n')
            else:
                f.write('720' + '\n')
                f.write('576' + '\n')
        if "GT" in filename:
            f.write(gt_file_ir + '\n')

    return filename


def get_transformation_matrix(scene):
    for dir in os.listdir(TRANSFORMATIONS_DIR):
        if dir in scene['sceneName']:
            for file in os.listdir(os.path.join(TRANSFORMATIONS_DIR, dir)):
                with open(os.path.join(TRANSFORMATIONS_DIR, dir, file), "r") as f:
                    lines = f.readlines()
                    str_mat = lines[2][:-1] + lines[3][:-1]
                    return str_mat
    
    return "1 0 0 0 1 0"
    # return None


def update_precision_counters(statistics_counters):
    with open('Statistics_vis.csv', 'r') as f:
        for i in reversed(list(csv.reader(f))):
            last_row = i
            break
        try:
            statistics_counters["num_frames"][0]        += int(last_row[2])
            statistics_counters["TP"][0]                += int(last_row[3])
            statistics_counters["FP"][0]                += int(last_row[4])
            statistics_counters["FN"][0]                += int(last_row[5])
            statistics_counters["frames_with_FP"][0]    += int(last_row[6])
            statistics_counters["frames_with_GT"][0]    += int(last_row[7])
            statistics_counters["total_GTs"][0]         += int(last_row[8])
        except:
            pass
    with open('Statistics_ir.csv', 'r') as f:
        for i in reversed(list(csv.reader(f))):
            last_row = i
            break 
        try:       
            statistics_counters["num_frames"][1]        += int(last_row[2])
            statistics_counters["TP"][1]                += int(last_row[3])
            statistics_counters["FP"][1]                += int(last_row[4])
            statistics_counters["FN"][1]                += int(last_row[5])
            statistics_counters["frames_with_FP"][1]    += int(last_row[6])
            statistics_counters["frames_with_GT"][1]    += int(last_row[7])
            statistics_counters["total_GTs"][1]         += int(last_row[8])  
        except:
            pass


def get_resize(scene, cm_per_pixel_dict):
    resize_vis = 1
    resize_therm = 1

    if CM_PER_PIXEL_FILE is not None:
        try:
            resize_vis = cm_per_pixel_dict[os.path.basename(scene['vis_path'])] / desired_cm_per_pixel
            resize_therm = cm_per_pixel_dict[os.path.basename(scene['scenePath'])] / desired_cm_per_pixel
        # transformation = param_dict['transformation'].split()
        # transformation[2] = str(float(transformation[2]) * resize)
        # transformation[5] = str(float(transformation[5]) * resize)
        # param_dict['transformation'] = " ".join(transformation)
        except:
            pass
    
    return (resize_vis, resize_therm)


def update_resize_and_transformation(json_name, resize_vis, resize_therm, transformation):
    json_basename = os.path.basename(json_name)[:-len('.json')]
    json_header = json_basename.replace('algo_', '').replace('_benchmark', '')

    with open(json_name, 'r+') as file_in:
        data = json.load(file_in)
        data[json_header]["nn"]["ImageResizeValueDay"]                  = str(resize_vis)
        data[json_header]["nn"]["ImageResizeValueTherm"]                = str(resize_therm)        
        data[json_header]["vmd"]["VMDImageResizeValue"]                 = str(resize_vis)
        data[json_header]["vmdTherm"]["VMDImageResizeValue"]            = str(resize_therm)
        data[json_header]["DualFrameAlgo"]["TransformationMatrix"]      = "0.9422410170203824 0.025898297961756685 44.65313090985502 0.02059256146249191 0.9407438537507877 53.03908384114323"
    
        new_data = {
            json_header: data[json_header]
        }

    with open(json_name, 'w') as file_out:
        file_out.seek(0)  # <--- should reset file position to the beginning.
        json.dump(new_data, file_out, indent=4)
        file_out.truncate()  # remove remaining part


def calc_F_score(statistics_counters):
    eps=0.001
    fscore_vis = statistics_counters["TP"][0] / (eps+statistics_counters["TP"][0] + 0.5 * (statistics_counters["FP"][0] + statistics_counters["FN"][0]))
    fscore_ir = statistics_counters["TP"][1] / (eps+statistics_counters["TP"][1] + 0.5 * (statistics_counters["FP"][1] + statistics_counters["FN"][1]))

    return (fscore_vis, fscore_ir)
    

def update_f_score(statistics_counters, f_scores):
    fscore_vis, fscore_ir = calc_F_score(statistics_counters)
    
    f_scores[0].append(fscore_vis)
    f_scores[1].append(fscore_ir)


def open_csv_files_and_write_headers():
    csvfile_vis = open("Statistics_vis.csv", 'w', newline='')
    csvfile_ir  = open("Statistics_ir.csv", 'w', newline='')    
    fieldnames = ["Configuration", "Scene Path", "Num Frames", "TP", "FP", "FN", "Frames with FP > 0", "Frames with GTs > 0", "Total GTs", "F-Score"]
    writer_vis = csv.DictWriter(csvfile_vis, fieldnames=fieldnames)
    writer_ir = csv.DictWriter(csvfile_ir, fieldnames=fieldnames)
    writer_vis.writeheader()
    writer_ir.writeheader()
    csvfile_vis.close()
    csvfile_ir.close()

    summary_csvfile_vis = open("Statistics_summary_vis.csv", 'w', newline='')
    summary_csvfile_ir  = open("Statistics_summary_ir.csv", 'w', newline='')
    fieldnames = ["Configuration", "Num Frames", "TP", "FP", "FN", "Frames with FP > 0", "Frames with GTs > 0", "Total GTs", "F-Score"]
    writer_vis = csv.DictWriter(summary_csvfile_vis, fieldnames=fieldnames)
    writer_ir = csv.DictWriter(summary_csvfile_ir, fieldnames=fieldnames)
    writer_vis.writeheader()
    writer_ir.writeheader()
    summary_csvfile_vis.close()
    summary_csvfile_ir.close()


def get_props_filename(movie_path):
    _,extension = os.path.splitext(movie_path)
    props_filename = movie_path.replace(extension, '.props')

    if not os.path.exists(props_filename):
        props_filename = props_filename.replace('_r1_', '_r0_')
        props_filename = props_filename.replace('_r2_', '_r0_')

    if not os.path.exists(props_filename):
        return None
    
    return props_filename


# def get_cm_per_pixel_from_props(movie_path):
#     cm_per_pixel = None
#     props_filename = get_props_filename(movie_path)

#     if props_filename:
#         with open(props_filename, "r") as f:
#             props_lines = f.readlines()
#             mid_line = props_lines[len(props_lines) // 2]
#             altitude = float(mid_line.split()[1])
#             if movie_path.endswith(".raw2"):
#                 pitch = float(mid_line.split()[16])
#                 cm_per_pixel = (pixel_pitch_ir * altitude) / (10 * focal_len_ir * pitch)
#             else:
#                 pitch = float(mid_line.split()[17])
#                 cm_per_pixel = (pixel_pitch_vis * altitude) / (10 * focal_len_vis * pitch)
    
#     return cm_per_pixel


def process_scenes(scene_list):
    print("********************************************************************************************************")
    print("Processing " + str(len(scene_list)) + " scenes")
    print("********************************************************************************************************")

    tes_params = {}
    counter = 0

    open_csv_files_and_write_headers()

    cm_per_pixel_dict = None
    if CM_PER_PIXEL_FILE is not None:
        cm_per_pixel_dict = get_cm_per_pixel_dict()

    f_scores = [[], []]

    # while(True):
    for iterator in range(1):
        counter += 1

        if calibrate_manually is False:
            param_dict = get_params_dict()

            folder_name = "Configuration_" + str(counter)
            try: 
                os.mkdir(os.path.join(OUTPUT_DIR, folder_name))
            except:
                pass  

            csvfile = open(csv_file, 'a', newline='')
            fieldnames = ["folder", "scene#"]
            fieldnames.extend(list(param_dict.keys()))
            fieldnames.extend(list(scene_list[0].keys()))
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if counter == 1:
                writer.writeheader()

        statistics_counters = {"num_frames": [0, 0],
                               "TP": [0, 0],
                               "FP": [0, 0],
                               "FN": [0, 0],
                               "frames_with_FP": [0, 0],
                               "frames_with_GT": [0, 0],
                               "total_GTs": [0, 0]}
        
        # random_scenes = random.sample(scene_list, k=30)
        random.shuffle(scene_list)
        for i, scene in enumerate(scene_list):
            if dual_benchmark is True:
                dual_file = create_dual_file(scene)
                if dual_file is None:
                    continue
            else:
                dual_file = create_single_file(scene)
            
            # if "GT" not in dual_file:
            #     continue

            scene['sepSize'] = 0 #random.choice(np.arange(start=1, stop=(scene['frameCount'] - (num_samples * sample_size)) // num_samples, step=1))

            if frames_interval > 0 and scene['frameCount'] > frames_interval:
                start_frame = random.randrange(0, scene['frameCount'] - frames_interval, 1)
                scene['endFrame'] = start_frame + frames_interval
            else:
                start_frame = 0 #random.choice(np.arange(start=0, stop=scene['skips'], step=1))

            scene["startFrame"] = int(start_frame)

            with open(dual_file, 'r') as f:
                lines = f.readlines()
                scene["vis_path"] = lines[0][:-1]
                scene["vis_width"] = int(lines[2])
                scene["vis_height"] = int(lines[3])
            
            transformation = "1 0 0 0 1 0"
            if dual_benchmark is True:
                transformation = get_transformation_matrix(scene)
                if transformation is None:
                    continue
            
            resize_vis, resize_therm = get_resize(scene, cm_per_pixel_dict)

            if calibrate_manually is False:
                param_dict['transformation'] = transformation
                param_dict['resize_vis'] = resize_vis
                param_dict['resize_therm'] = resize_therm
                current_json_filename = update_json_file(json_name=in_json_file, conf_dict=param_dict)
                configuration_name = os.path.basename(current_json_filename)[:-len('.json')].replace('chimera_', '').replace('algo_', '')
                scene_dict = scene
                scene_dict.update(param_dict)
                scene_dict["folder"] = folder_name
                scene_dict["scene#"] = i
                writer.writerow(scene_dict)
            else:
                # resize_therm = resize_vis = 1
                update_resize_and_transformation("algo/algo_DUAL.json", resize_vis, resize_therm, transformation)

            tes_params['skips']         = scene['skips']
            tes_params['numSamples']    = scene['numSamples']
            tes_params['sampleSize']    = scene['sampleSize']
            tes_params['sepSize']       = scene['sepSize']
            tes_params['startFrame']    = scene['startFrame']
            tes_params['endFrame']      = scene['endFrame']
            tes_params['warmUp']        = scene['warmUp']
            try:
                tes_params['cmPixVis']      = cm_per_pixel_dict[os.path.basename(scene['vis_path'])]
                tes_params['cmPixTherm']    = cm_per_pixel_dict[os.path.basename(scene['scenePath'])]
            except:
                tes_params['cmPixVis']      = 17
                tes_params['cmPixTherm']    = 16
                # continue
            update_testester_json_file(tes_params)

            csvfile_vis = open("Statistics_vis.csv", 'a', newline='')
            csvfile_ir  = open("Statistics_ir.csv", 'a', newline='')
            csvfile_vis.write(str(counter))
            csvfile_ir.write(str(counter))
            csvfile_vis.close()
            csvfile_ir.close()

            subprocess.call([algowrapper_path, str(tester_type), "DUAL", dual_file])

            update_precision_counters(statistics_counters)

            if calibrate_manually is False:
                try:
                    list_of_files = glob.glob(os.path.join(OUTPUT_DIR, '*detections1.txt')) 
                    latest_file = max(list_of_files, key=os.path.getctime)
                    new_det_dir = os.path.join(OUTPUT_DIR, folder_name, os.path.basename(latest_file))
                    os.rename(latest_file, new_det_dir)
                    list_of_files = glob.glob(os.path.join(OUTPUT_DIR, '*detections2.txt')) 
                    latest_file = max(list_of_files, key=os.path.getctime)
                    new_det_dir = os.path.join(OUTPUT_DIR, folder_name, os.path.basename(latest_file))
                    os.rename(latest_file, new_det_dir)
                    list_of_files = glob.glob(os.path.join(OUTPUT_DIR, '*detections3.txt')) 
                    latest_file = max(list_of_files, key=os.path.getctime)
                    new_det_dir = os.path.join(OUTPUT_DIR, folder_name, os.path.basename(latest_file))
                    os.rename(latest_file, new_det_dir)
                except:
                    pass

        if calibrate_manually is False:  
            csvfile.close()
        
        update_f_score(statistics_counters, f_scores)

        summary_csvfile_vis = open("Statistics_summary_vis.csv", 'a', newline='')
        summary_csvfile_ir  = open("Statistics_summary_ir.csv", 'a', newline='')
        vis_summary_writer = csv.writer(summary_csvfile_vis)
        ir_summary_writer = csv.writer(summary_csvfile_ir)
        vis_summary_writer.writerow([str(counter), str(statistics_counters["num_frames"][0]), str(statistics_counters["TP"][0]), str(statistics_counters["FP"][0]), str(statistics_counters["FN"][0]), str(statistics_counters["frames_with_FP"][0]), str(statistics_counters["frames_with_GT"][0]), str(statistics_counters["total_GTs"][0]), str(f_scores[0][-1])])
        ir_summary_writer.writerow([str(counter), str(statistics_counters["num_frames"][1]), str(statistics_counters["TP"][1]), str(statistics_counters["FP"][1]), str(statistics_counters["FN"][1]), str(statistics_counters["frames_with_FP"][1]), str(statistics_counters["frames_with_GT"][1]), str(statistics_counters["total_GTs"][1]), str(f_scores[1][-1])])
        summary_csvfile_vis.close()
        summary_csvfile_ir.close()


### Main: ###

scene_list = get_scene_list()
process_scenes(scene_list)
