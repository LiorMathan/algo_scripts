##  AG / GG

    
| ID |String  |
|--|--|
| 0 | pedestrian |
| 1 | bicycle |
| 2 | motorcycle |
| 3 | vehicle |
| 4 | bus |
| 5 | truck |
| 6 | train |
| 7 | system |


##  GA

| ID |String  |
|--|--|
| 0 | drone |
| 1 | airplane |
| 2 | bird |
| 3 | UFO |
| 4 | airplane with lights |
| 5 | balloon |
| 6 | human |
| 7 | vehicle |
| 8 | drone with lights |
| 9 | single front light
 

## GALASH:
    
| ID |String  |
|--|--|
| 0 | blob |

## TAAS MissileInterception:

| ID |String  |
|--|--|
| 0 | drone |
| 1 | missle |
