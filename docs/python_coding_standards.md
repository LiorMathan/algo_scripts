## Python Coding Standards ##
  
  
**-   Variable and function names separated with '_':**

	my_variable = 10  
	is_valid = True  
	def open_reader()

~~myVariable = 10  
isValid = True  
def Open_Reader()~~
  
  
**-   Two lines separartion before and after function scope:**
    
	def is_positive(num):

		return num > 0    


	def is_negative(num):

		return num < 0

~~def is_positive(num):  
return num > 0  
def is_negative(num):  
return num < 0~~
  
  
**-   Single space before return:**
    
	counter += 1

	return counter

~~counter += 1  
return counter~~
  
  
**-   Single space before and after mathematics operations:**

	sum = num1 + num2  
	counter += 1

~~sum=num1+num2  
counter+=1~~
  
  
**-   Class names starts with capital letter, words are connected with Camel-Case:**
    
	class MyClass

~~class myClass  
class my_class~~
  
  
**-   Boolean functions start with “is_”:**
    
	def is_empty()

~~def list_is_empty()~~
  
  
**-   Execution code written only inside Main function:**
    
	if __name__ == '__main__':
~~#Main:~~
  
  
  **- Minimum comments, code should be self explanatory:**
    
	if positives_counter > 0:

	return True

~~is count > 0: # There are positive numbers  
return True~~
  
  
   **- Keep it short!**  
    **A function should do only one thing.**
