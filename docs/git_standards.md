Our Code is stored on [Bitbucket Cloud](https://bitbucket.org/ "https://bitbucket.org/") server.

Introduction to git:

-   [Beginner](https://www.atlassian.com/git/tutorials/what-is-version-control "https://www.atlassian.com/git/tutorials/what-is-version-control")
    
-   [Setting up](https://www.atlassian.com/git/tutorials/setting-up-a-repository "https://www.atlassian.com/git/tutorials/setting-up-a-repository")
    
-   [Syncing](https://www.atlassian.com/git/tutorials/syncing "https://www.atlassian.com/git/tutorials/syncing")
    

**We are working with Git-Feature-Branch workflow, with some adjustments.**  
The Feature Branch Workflow assumes a central repository, and master represents the official project history.  
Developers create a new branch every time they finish work on a new feature.  
Feature branches should have descriptive names, like animated-menu-items or issue-#1061.  
The idea is to give a clear, highly-focused purpose to each branch.  

  
-   “Master Branch”:
    
    -   Anything in the master branch is deployable.
    -   "Master" is also our developing branch.
    -   Only Lior M. has push permissions to master.
    
-   “Feature Branch”:
    
    -   When done working on a new feature, push your chages to a new branch with meaningful name:  
    _git push origin <source_branch>:<dest_branch>_
    - If dest_branch does not exists - it will open it.
        
-   Pull Request:
    
    -   Review Bitbucket [tutorial](https://zapier.com/blog/bitbucket-pull-request/ "https://zapier.com/blog/bitbucket-pull-request/").
        
    -   When you need feedback or help, or you think your feature branch is ready for merging, open a pull request.



**Only Dan K. and Lior M. has permissions to approve pull requests and merge to master.
