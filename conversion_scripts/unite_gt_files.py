import os

input_crops_dirs = ["/mnt/share/ssdHot/Crops/Frames/GG/Avatar/CMOS/DualGround_00012_r1_2",
                    "/mnt/share/ssdHot/Crops/Frames/GG/Avatar/CMOS/DualGround_00014_r1_4",
                    "/mnt/share/ssdHot/Crops/Frames/GG/Avatar/CMOS/DualGround_00050_r1_0",
                    "/mnt/share/ssdHot/Crops/Frames/GG/Avatar/CMOS/DualGround_00135_r1_5"]
output_filename  = "/mnt/share/ssdHot/Crops/Frames/GG/Avatar/TestSet/TestSet_cmos_gt.txt"


def extract_annotations(input_crops_dirs: list) -> list:
    annotations_list = []
    index = 0

    for crops_dir in input_crops_dirs:
        files = os.listdir(crops_dir)
        files.sort()

        for file in files:
            if file.endswith('.txt'):
                with open(os.path.join(crops_dir, file), 'r') as f:
                    for line in f.readlines():
                        new_index = str(index).zfill(6)
                        line_tokens = line.split()

                        annotations_list.append(new_index + ' ' + ' '.join(line_tokens))
                index += 1
    
    return annotations_list


def write_to_file(annotations_list: list, output_filename: str) -> None:
    with open(output_filename, 'w') as f:
        for annotation in annotations_list:
            f.write(annotation + '\n')


def unite_gt_files(input_crops_dirs, output_filename):
    annotations_list = extract_annotations(input_crops_dirs)
    write_to_file(annotations_list, output_filename)


if __name__ == '__main__':
    unite_gt_files(input_crops_dirs, output_filename)