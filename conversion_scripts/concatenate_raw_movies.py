import cv2
import numpy as np
import os
import shutil
import unite_gt_files as ugf
from Reader import *

cmos_movies = ["/mnt/ssdHot/WorkMovies/GG/Avatar/0001_090120_0001_Car_F19_T1430_KiriatASharon/CMOS_RAW_Movies_0/00012_r1_2.rawc",
                "/mnt/ssdHot/WorkMovies/GG/Avatar/0001_090120_0001_Car_F19_T1430_KiriatASharon/CMOS_RAW_Movies_0/00014_r1_4.rawc",
                "/mnt/ssdHot/WorkMovies/GG/Avatar/0002_120120_0002_Car_F19_T1000_KiriatASharon/CMOS_RAW_Movies_0/00041_r1_21.rawc",
                "/mnt/ssdHot/WorkMovies/GG/Avatar/0003_090220_0005_Car_F19_T1930_BeitYehoshua/CMOS_RAW_Movies_1/00050_r1_0.rawc"]
thermal_movies = [movie.replace("CMOS", "Thermal").replace("r1", "r0").replace("r2", "r0").replace("rawc", "raw2").replace("rgb", "raw2") for movie in cmos_movies]
width = 640
height = 480
gt_basedir = "/mnt/ssdHot/WorkMovies/GG/GT"
output_frames_basedir = "/mnt/ssdHot/Crops/Frames/GG/Avatar/TestSet2"


def create_dir(dir_path):
    try:
        os.mkdir(dir_path)
    except:
        pass


def copy_gt_file(frame_path, movie_name):
    gt_output_path = frame_path.replace(".png", ".txt")
    gt_input_path = os.path.join(gt_basedir, movie_name + "_gt", os.path.basename(gt_output_path))
    shutil.copyfile(gt_input_path, gt_output_path)


def extract_frames_to_folder(movie_frames_dir, movie_name, movie, is_thermal):
    print("extracting frames from", movie_name, "...")
    reader = Reader(path1=movie, width1=width, height1=height)
    for i in range(reader.num_frames1):
        frame = reader.get_frame(index=i, bgr=True)
        frame_path = os.path.join(movie_frames_dir, "{}_{}.png".format(movie_name, str(i).zfill(6)))
        cv2.imwrite(frame_path, frame, [cv2.IMWRITE_PNG_COMPRESSION, 0])
        copy_gt_file(frame_path, movie_name)


def get_input_frames_dirs(raw_movie_list, is_thermal):
    channel_name = "Thermal" if is_thermal else "CMOS"
    input_frames_dirs = []
    for movie in raw_movie_list:
        movie_name = os.path.basename(movie)[:-len(os.path.splitext(movie)[-1])]
        movie_frames_dir = os.path.join(output_frames_basedir, channel_name, "Original_frames", movie_name)
        create_dir(movie_frames_dir)
        extract_frames_to_folder(movie_frames_dir, movie_name, movie, is_thermal)
        
        input_frames_dirs.append(movie_frames_dir)
    
    return input_frames_dirs


def unite_frame_dirs(input_frames_dirs, output_frames_dirname):
    curr_index = 0
    for movie_frames_dir in input_frames_dirs:
        files = os.listdir(movie_frames_dir)
        files.sort()

        for file in files:
            if file.endswith('.png'):
                old_name = os.path.join(movie_frames_dir, file) 
                new_name = os.path.join(output_frames_dirname, file[:-10] + str(curr_index).zfill(6)) + ".png"
                shutil.copy(old_name, new_name)

                curr_index += 1
    

def concatenate_raw_movies(raw_movie_list, is_thermal):
    input_frames_dirs = get_input_frames_dirs(raw_movie_list, is_thermal)
    
    channel_name = "Thermal" if is_thermal else "CMOS"
    output_frames_dirname = os.path.join(output_frames_basedir, channel_name)

    unite_frame_dirs(input_frames_dirs, output_frames_dirname)

    output_gt_filename = os.path.join(output_frames_dirname, channel_name + "_gt.txt")
    ugf.unite_gt_files(input_frames_dirs, output_gt_filename)


def create_output_dirs():
    create_dir(output_frames_basedir)
    for channel in ("CMOS", "Thermal"):
        create_dir(os.path.join(output_frames_basedir, channel))
        create_dir(os.path.join(output_frames_basedir, channel, "Original_frames"))


if __name__ == '__main__':
    create_output_dirs()
    concatenate_raw_movies(raw_movie_list=cmos_movies, is_thermal=False)
    concatenate_raw_movies(raw_movie_list=thermal_movies, is_thermal=True)

