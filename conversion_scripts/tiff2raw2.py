import cv2
import os
import numpy as np

""" 
FLIR Documentation: https://www.flir.com/globalassets/imported-assets/document/flir-interface-requirements-tiff.pdf
"""

filename = None
width = 640
height = 512


def display_tiff(filename, width, height):
    with open(filename, 'rb') as f:
        endian = np.fromfile(f, dtype=np.uint16, count=1)
        constant = np.fromfile(f, dtype=np.uint16, count=1)
        ifd_offset = int(np.fromfile(f, dtype=np.uint32, count=1))
        while(ifd_offset != 0):
            try:
                f.seek(ifd_offset, os.SEEK_SET)
                entry_count = int(np.fromfile(f, dtype=np.uint16, count=1))
            except Exception as e:
                print(e)
                break

            for j in range(entry_count):
                tag = int(np.fromfile(f, dtype=np.uint16, count=1))
                entry_type = np.fromfile(f, dtype=np.uint16, count=1)
                count = np.fromfile(f, dtype=np.uint32, count=1)
                value = int(np.fromfile(f, dtype=np.uint32, count=1))
                
                # 273 is the code for image data offset:
                if tag == 273:
                    f.seek(value, os.SEEK_SET)
                    current_frame = np.fromfile(f, dtype=np.uint16, count=height*width)
                    current_frame = current_frame.reshape((height, width))
                    current_frame = cv2.normalize(current_frame, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
                    current_frame = cv2.cvtColor(current_frame, cv2.COLOR_GRAY2BGR)
                    cv2.imshow("test", current_frame)
                    cv2.waitKey(0)
                    f.seek(ifd_offset + 2 + (entry_count * 12), os.SEEK_SET)
                    ifd_offset = int(np.fromfile(f, dtype=np.uint32, count=1))
                    break


def convert_tiff_to_raw2(filename, width, height):
    output_filename = filename.replace(".tiff", ".raw2")
    out = open(output_filename, "wb")
    i = 0
    with open(filename, 'rb') as f:
        endian = np.fromfile(f, dtype=np.uint16, count=1)
        constant = np.fromfile(f, dtype=np.uint16, count=1)
        ifd_offset = int(np.fromfile(f, dtype=np.uint32, count=1))
        while(ifd_offset != 0):
            print(str(i))
            i += 1

            try:
                f.seek(ifd_offset, os.SEEK_SET)
                entry_count = int(np.fromfile(f, dtype=np.uint16, count=1))
            except Exception as e:
                print(e)
                ifd_offset = 0
                break

            for j in range(entry_count):
                tag = int(np.fromfile(f, dtype=np.uint16, count=1))
                entry_type = np.fromfile(f, dtype=np.uint16, count=1)
                count = np.fromfile(f, dtype=np.uint32, count=1)
                value = int(np.fromfile(f, dtype=np.uint32, count=1))
                
                # 273 is the code for image data offset:
                if tag == 273:
                    try:
                        f.seek(value, os.SEEK_SET)
                        current_frame = np.fromfile(f, dtype=np.uint16, count=height*width)
                        out.write(current_frame)
                        f.seek(ifd_offset + 2 + (entry_count * 12), os.SEEK_SET)
                        ifd_offset = int(np.fromfile(f, dtype=np.uint32, count=1))
                        break
                    except Exception as e:
                        print(e)
                        ifd_offset = 0
                        break
    
    out.close()


if __name__ == '__main__':
    while True:
        filename = input('Insert file path: ')
        display_tiff(filename=filename, width=width, height=height)
        convert_tiff_to_raw2(filename=filename, width=width, height=height)
