import os

directory = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Staging/Dual/Dual air/Panorama/240221_AirVGA_F35_T1430_A500_Panorama_Elbit_Golani/CMOS_RAW_Movies_0"
column    = 18

for root, dirs, files in os.walk(directory):
    for file in files:
        if file.endswith(".props"):
            with open(os.path.join(root, file), "r") as f:
                lines = f.readlines()
            
            with open(os.path.join(root, file), "w") as f:
                for line in lines:
                    fixed_value = str(max(0, int(line.split()[column]) - 245))
                    tokens = line.split()
                    tokens[column] = fixed_value
                    fixed_line = ' '.join(tokens) + '\n'

                    f.write(fixed_line)