import os
import pyScriptsUtilities as utils
import threading
import time


external_folder = r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\GT_Temp\\"
from_crops = False

overall_files_done = 0
overall_files_needed = 0

def avatar_to_vis_label(avatar_label):
    if(avatar_label == 2):
        return 3
    elif(avatar_label == 3):
        return 2
    elif(avatar_label == 4):
        return 5
    elif(avatar_label == 5):
        return 7
    
    return avatar_label


def vis_to_avatar_label(vis_label):
    if(vis_label == 2):
        return 3
    elif(vis_label == 3):
        return 2
    elif(vis_label == 4):
        return 7
    elif(vis_label == 5):
        return 4
    elif(vis_label == 7):
        return 5
    
    if vis_label < 0 or vis_label > 7: # label unknown
        return None
    
    return vis_label


def BP_to_avatar(BP_label):
    if (BP_label == 6):
        return 0
    elif (BP_label == 7):
        return 3

    return None

def proccess_folder(folder: str):
    global overall_files_done

    if not os.path.isdir(folder):
        print(folder + ' is not a folder!')
        return

    if from_crops is True:
        for file in os.listdir(folder):
            if file.endswith(".txt"):
                filepath = os.path.join(folder, file)
                annotations = utils.extract_detections_from_gt_file(gt_filename=filepath, yolo=True)
                with open(filepath, "w") as new_gt:
                    for row in range(len(annotations)):
                        label = vis_to_avatar_label(annotations[row].label)
                        if label is None:
                            pass
                        else:
                            label = str(vis_to_avatar_label(annotations[row].label))
                            annotation_str = label + " " +  str(annotations[row].cx) + " " +  str(annotations[row].cy) + " " +  str(annotations[row].w) + " " + str(annotations[row].h) + "\n"
                            new_gt.write(annotation_str)
                
            overall_files_done += 1


    else:
        yolo_annotations = utils.extract_annotations(gt_folder=folder, yolo=True)

        i = 0
        for file in os.listdir(folder):
            if (len(yolo_annotations[i]) > 0):
                filepath = os.path.join(folder, file)
                with open(filepath, "w") as new_gt:
                    for row in range(len(yolo_annotations[i])):
                        current_annotation = yolo_annotations[i][row]

                        new_label = str(BP_to_avatar(current_annotation.label))
                        if new_label == 'None':
                            pass
                        else:
                            current_annotation.label = new_label
                            new_gt.write(current_annotation.as_gt_line())
            i += 1
            overall_files_done += 1


def setup_progress_printing(parent_directory: str):
    global overall_files_needed
    global overall_files_done

    for directory in os.listdir(parent_directory):
        directory = os.path.join(parent_directory, directory)

        if os.path.isdir(directory):
            overall_files_needed += len(os.listdir(directory))

    
    progress = 0
    last_progress = 0

    while progress < 100:
        progress = int(overall_files_done / overall_files_needed * 1000) / 10.0 

        if last_progress != progress:
            print("Progress: " + str(progress) + "%", end="\r")
        
        last_progress = progress
        time.sleep(0.02)


def main(parent_folder: str):
    threading.Thread(target=setup_progress_printing, args=(parent_folder, )).start()

    if not os.path.isdir(parent_folder):
        print(parent_folder + ' is not a folder!')
        return

    print(parent_folder + " is being processed...")
        
    for sub_folder in os.listdir(parent_folder):
        threading.Thread(target=proccess_folder, args=(os.path.join(parent_folder + sub_folder), )).start()


if __name__ == '__main__':
    main(external_folder)