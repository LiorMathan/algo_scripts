import cv2
import os
import numpy as np
import sys
from Reader import *
from Target import *

input_path =   sys.argv[1]  
old_gt_path = sys.argv[2] #"/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/ThermApp Movies/ThermApp Air/CR_T/cr_t_1053.P.txt"
new_gt_path = sys.argv[3] #"/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/ThermApp Movies/ThermApp Air/GT/1053_r0_3_gt"
width = 384
height = 288

birds_DB_flag   = False
from_CR_T_flag  = True
debug_flag      = False


def validate_input():
    if not os.path.exists(input_path):
        print("Movie path does not exists! Exiting...")
        exit(1)
    if not os.path.exists(old_gt_path):
        print("GT path does not exists! Exiting...")
        exit(1)


def extract_annotations_from_old_labeled_file(filename, num_frames):
    ann_file = open(filename, "r")
    num_lines = sum(1 for line in ann_file)
    ann_file.seek(0, os.SEEK_SET)
    
    annotations = [[] for i in range(num_frames)]

    line = ann_file.readline()
    for j in range(num_lines):
        if (len(line) >= 2):  
            elements = line.split(',')  
            num_of_objs = int(elements[1])
            frame_num = int(elements[0])
            for i in range(1, num_of_objs + 1):
                cx = int(elements[2 + (i-1) * 5])
                cy = int(elements[3 + (i-1) * 5])
                w = int(elements[4 + (i-1) * 5])
                h = int(elements[5 + (i-1) * 5])
                if i == num_of_objs:
                    label = convert_str_label_to_int(elements[6 + (i-1) * 5][:-1])
                else:
                    label = convert_str_label_to_int(elements[6 + (i-1) * 5])  
                
                if label is not None:
                    new_annotation = Annotation(label, cx, cy, w, h)
                    annotations[frame_num].append(new_annotation)
        line = ann_file.readline()
    
    ann_file.close()
    return annotations


def extract_annotations_from_birds_format_file(filename, frames, num_frames):
    annotations = [[] for i in range(num_frames)]
    ann_file = open(filename, "r")
    line = ann_file.readline()
    result = line.find('.jpg')

    counter = 0
    while(result != -1):
        image_name = line
        frame_name = line[:-1]
        while(frame_name != frames[counter]):
            counter += 1
        line = ann_file.readline()
        if len(line) < 2:
            break
        result = line.find('.jpg')

        while(result == -1):
            if len(line) < 2:
                break
            elems = line.split(',')   #split line according to commas
            left_x = int(elems[0])
            top_y = int(elems[1])
            w = int(elems[2])
            h = int(elems[3])
            label = convert_bird_label_str_to_int(elems[4][:-1])

            cx = int(np.ceil(left_x + w/2))
            cy = int(np.ceil(top_y + h/2))

            new_annotation = Annotation(label, cx, cy, w, h)
            annotations[counter].append(new_annotation)

            line = ann_file.readline()
            result = line.find('.jpg')

        counter += 1
        print(counter)
    
    ann_file.close()

    return annotations


def extract_annotations_from_cr_t_file(filename, num_frames):
    annotations = [[] for i in range(num_frames)]
    
    with open(filename, "r") as f:
        for line in f.readlines():
            tokens = line.split()
            try:
                frame_index = int(tokens[0]) - 1
                left        = int(tokens[1])
                top         = int(tokens[2])
                right       = int(tokens[3])
                bottom      = int(tokens[4])

                w = int(right - left)
                h = int(bottom - top)
                cx = int(left + w / 2)
                cy = int(top + h / 2)
                annotations[frame_index].append(Target(label=0, cx=cx, cy=cy, width=w, height=h, label_set="AG"))
            except:
                continue
    
    return annotations


if __name__ == '__main__':
    print(input_path + " is being processed...")
    validate_input()
    reader = Reader(path1=input_path, width1=width, height1=height)

    movie_name = reader.basename
    num_frames = reader.num_frames1
    frames = reader.images_path

    if (os.path.isfile(old_gt_path)):
        if not os.path.isdir(new_gt_path):
            os.mkdir(new_gt_path)

        if birds_DB_flag:
            annotations = extract_annotations_from_birds_format_file(old_gt_path, frames, num_frames)
        elif from_CR_T_flag:
            annotations = extract_annotations_from_cr_t_file(old_gt_path, num_frames)
        else:
            annotations = extract_annotations_from_old_labeled_file(old_gt_path, num_frames)

        gt_summary_file = os.path.join(os.path.dirname(new_gt_path), movie_name + '_gt.txt')
        summary_file = open(gt_summary_file, "w")

        for i in range(num_frames):
            if reader.images_path is not None:
                new_gt_filename = frames[i].replace(reader.image_extension, ".txt")
            else:
                new_gt_filename = movie_name + "_" + str(i).zfill(6) + ".txt"
            ann_filename = os.path.join(new_gt_path, new_gt_filename)
            new_ann_file = open(ann_filename, "w")
            
            if debug_flag:
                frame = reader.get_frame(index=i, bgr=True)

            for annotation in annotations[i]:
                new_ann_file.write(annotation.get_as_yolo_line(width, height) + ' \n')
                summary_file.write(str(i) + " " + annotation.get_as_yolo_line(width, height) + ' \n')

                if debug_flag:
                    annotation.draw(frame)
            
            if debug_flag:
                if (len(annotations[i]) > 0):
                    cv2.imshow("frame", frame)
                    cv2.waitKey(1)
                        
            new_ann_file.close()
        
        summary_file.close()
