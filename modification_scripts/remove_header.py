CHUNK_SIZE = int(640*480*1.5)


filename = "/home/lior/Downloads/r_0.rawc"
with open(filename, "rb") as f:
    with open(filename.replace('.rawc', '_no_header.rawc'), "wb") as o:
        while True:
            f.seek(64, 1)
            chunk = f.read(CHUNK_SIZE)
            if chunk:
                o.write(chunk)
            else:
                break
