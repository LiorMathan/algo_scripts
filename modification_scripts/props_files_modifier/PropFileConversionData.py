from PropFile import PropFile
import numpy as np


class PropFileConversionData:
    __overlap = {} # {target_index: match_index_in_file}
    __default_overlay = {} #{target_index: default_value}
    __new_size_columns = 0
    __overlap_modified = False

    def __init__(self, overlap: dict, default_overlay: dict, new_size_columns):
        self.__overlap = overlap.copy()
        self.__default_overlay = default_overlay.copy()
        self.__new_size_columns = new_size_columns


    def get_modified_entries(self, prop_file: PropFile) -> list:
        new_entries = []
    
        if not self.__overlap_modified:
            for i in range(len(prop_file.entries[0])):
                if i not in self.__overlap.keys():
                    self.__overlap[i] = i

                elif self.__overlap[i] == None:
                    self.__overlap.pop(i)
        self.__overlap_modified = True

        if len(prop_file.entries) > 0:
            length = len(prop_file.entries)
        else:
            length = 1000
        
        for i in range(length):
            new_entries.append([])
            for j in range(self.__new_size_columns):
                new_entries[i].append('0')

        for i in range(len(new_entries)):
            for j in range(len(new_entries[i])):
                data_exists = j in self.__overlap.keys()
                default_exists = j in self.__default_overlay.keys()

                if default_exists:
                    new_entries[i][j] = self.__default_overlay[j]

                elif data_exists:
                    new_entries[i][j] = prop_file.entries[i][self.__overlap[j]]

        return new_entries

    def clone(self):
        return PropFileConversionData(self.__overlap.copy(), self.__default_overlay.copy(), self.__new_size_columns)