from PropFileBatch import PropFileBatch
from PropFile import PropFile
from PropFileConversionData import PropFileConversionData
from PropFileProfiler import PropFileProfiler
import os 

class PropFileModifier:
    @staticmethod
    def __dump_props_file(prop_file: PropFile, dest_folder: str):
        file_name = prop_file.name.split(os.sep)[-1]
        try:
            os.makedirs(dest_folder)
        except FileExistsError:
            pass

        with open(os.path.join(dest_folder, file_name), 'w+') as output_file:
            for line in prop_file.entries:
                output_file.write(' '.join(line) + '\n')

    @staticmethod
    def modify_all(batch: PropFileBatch, output_folder: str, conversion_data: PropFileConversionData, replace=False, backup=True) -> None:
        if backup:
            PropFileModifier.backup(batch, os.path.join(output_folder, 'backups_auto'))
        
        for file in batch.prop_files:
            prop_file = PropFile.from_path(file.name)
            new_entries = conversion_data.clone().get_modified_entries(prop_file)

            new_prop_file = PropFile(entries=new_entries, name=file.name, str_format=prop_file.str_format)
            PropFileModifier.__dump_props_file(new_prop_file, output_folder)

            if replace:
                prop_file_path = os.sep.join(new_prop_file.name.split(os.sep)[0: -1])
                PropFileModifier.__dump_props_file(new_prop_file, prop_file_path)


    @staticmethod
    def backup(batch: PropFileBatch, dest_folder: str) -> None:
        for file in batch.prop_files:
            prop_file = PropFile.from_path(file.name)
            PropFileModifier.__dump_props_file(prop_file, dest_folder)