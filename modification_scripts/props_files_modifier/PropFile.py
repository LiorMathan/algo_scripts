class PropFile:
    name = ""
    entries = []

    str_format = ""
    __line_format_count = {}

    def __init__(self, entries=[[]], name="", str_format=None):
        self.name = name
        self.entries = entries
        self.__line_format_count = {}
        if str_format == None:
            self.__register_formats()
        else:
            self.str_format = str_format


    def __register_formats(self):
        if len(self.entries) > 0 and len(self.entries[0]) > 0:
            for i in range(len(self.entries[0])):
                for j in range(len(self.entries)):
                    try:
                        entry = self.entries[j][i]
                    except:
                        continue

                    data_type = self.__get_data_type(entry)

                    if i not in self.__line_format_count.keys():
                        self.__line_format_count[i] = {}
                    
                    if data_type not in self.__line_format_count[i].keys():
                        self.__line_format_count[i][data_type] = 0
                    
                    self.__line_format_count[i][data_type] += 1

        self.str_format = ""
        stronget_data_types = []
        for column in self.__line_format_count.values():
            stronget_type = self.__get_stronget_type_in_column(column)
            stronget_data_types.append(stronget_type)


        self.str_format = ' '.join(stronget_data_types)


    @staticmethod
    def __get_stronget_type_in_column(column: dict):
        stronget_data_type = ''
        max_count = 0
        types = column.keys()

        if 'int' in types and 'bool' in types:
            column['int'] += column['bool']
            column.pop('bool')

        if 'str' in types and 'int' in types:
            column['str'] += column['int']
            column.pop('int')


        for data_type in column.keys():
            if column[data_type] > max_count:
                stronget_data_type = data_type
                max_count = column[data_type]

        return stronget_data_type


    @staticmethod
    def __get_data_type(data: str):
        if data == None or data == "":
            return 'null'
        
        elif '.' in data:
            try:
                f = float(data)
                return 'float'
            except:
                pass
            
        elif ':' in data:
            return 'timestamp'
        
        elif data == '0' or data == '1':
            return 'bool'

        elif data.isalpha():
            return 'str'

        else:
            try:
                if len(str(int(data))) != len(data):
                    return 'str'
                else:
                    return 'int'
            except:
                pass
        
        return 'null'


    @staticmethod
    def from_path(path: str):
        if path.endswith('.props'):
            lines = []

            with open(path) as file:
                lines = file.readlines()

            entries = [[]] * len(lines)
            for i in range(len(lines)):
                line_entries = lines[i].split()
                
                entries[i] = [None] * len(line_entries)
                
                for j in range(len(line_entries)):
                    entries[i][j] = str(line_entries[j])
            
            File = PropFile(entries=entries, name=path)
            return File