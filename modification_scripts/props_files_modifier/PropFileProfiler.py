import json
import os
import threading

from FolderIterator import FolderIterator

from PropFile import PropFile
from PropFileBatch import PropFileBatch


def get_progress(done, needed):
    return str(round(100.0 *  done / needed, 1)) + "%"

class PropFileProfiler:
    def __init__(self):
        pass

    def export_file_list(self, output_file_name: str, import_paths: list, debug=True) -> None:
        print(f'Exporting prop file list...')

        print_queue = []
        files = {}

        def add_file(full_path: str, file_name: str):
                if file_name.endswith(".props"):
                    if debug:
                        print_queue.append("wrote - " + file_name)

                    folder = full_path.split(os.sep)
                    folder = os.sep.join(folder[0:-2])

                    if folder not in files:
                        files[folder] = {}
                    
                    if 'CMOS_RAW' in full_path:
                        if 'CMOS' not in files[folder]:
                            files[folder]['CMOS'] = []

                        files[folder]['CMOS'].append(full_path)

                    elif "Thermal_RAW" in full_path:
                        if 'THERMAL' not in files[folder]:
                            files[folder]['THERMAL'] = []

                        files[folder]['THERMAL'].append(full_path)

                else:
                    if debug:
                        print_queue.append("skipped - " + file_name)


        def should_iterate_over_folder(folder):
            contents = ' '.join(os.listdir(folder))
            if folder.endswith('Dual_Movies'):
                return False
            
            if folder.endswith('GT_with_properties'):
                return False
            
            if 'GT' in contents:
                return False

            if '.jpg' in contents:
                return False

            if '.png' in contents:
                return False

            if '.csv' in contents:
                return False
            
            return True
        

        def manage_file_queue():
            while not is_done:
                if len(print_queue) > 0:
                    print(print_queue.pop())

        is_done = False
        if debug:
            print_thread = threading.Thread(target=manage_file_queue)
            print_thread.setDaemon(True)
            print_thread.start()
        
        for folder in import_paths:
            iterator = FolderIterator(folder)
            iterator.for_each_entry(add_file, should_iterate_over_folder)
        
        is_done = True

        with open(output_file_name, 'w+') as output_file:
            json.dump(files, output_file, indent=4)
        

    def import_file_list(self, all_files_db: str) -> dict:
        with open(all_files_db, 'r') as db:
            files = json.load(db)
        
        return files


    def export_prop_files(self, all_files_db: str, output_file_name: str, debug=True) -> None:
        print(f'Exporting prop files...')

        prop_files = {}
        files = self.import_file_list(all_files_db)

        global threads_done
        threads_done = 0
        global threads_ready
        threads_ready = 0
        global needed
        needed = 0

        for folder in files.keys():
            try:
                needed += len(files[folder]['CMOS'])
            except:
                pass
        
            try:
                needed += len(files[folder]['THERMAL'])
            except:
                pass


        def process_file(folder_path, prop_type, file):
            global threads_done

            if folder_path not in prop_files.keys():
                prop_files[folder_path] = {}

            if prop_type not in prop_files[folder_path]:
                prop_files[folder_path][prop_type] = {}

            prop_file = PropFile.from_path(file)
            prop_files[folder_path][prop_type][prop_file.name] = prop_file.str_format
            
            threads_done += 1

        threads = []
        for folder_path in files.keys():
            for prop_type in files[folder_path].keys():
                for file in files[folder_path][prop_type]:
                    if debug:
                        print("Preparing threads (ready - " + get_progress(threads_ready, needed) + ", done - " + get_progress(threads_done, needed) + ")...")

                    # process_file(folder_path, prop_type, file)
                    thread = threading.Thread(target=process_file, args=(folder_path, prop_type, file, ))
                    thread.start()
                    threads.append(thread)
                    threads_ready += 1

        while True:
            all_dead = True

            for thread in threads:
                if thread.isAlive():
                    all_dead = False
                else:
                    threads.remove(thread)
            
            if debug:
                print("All threads ready. Finishing remaining threads (" + get_progress(threads_done, needed) + ")...")
            
            if all_dead and len(threads) == 0:
                break
        
        print('Dumping to file...')
        with open(output_file_name, 'w+') as output_file:
            json.dump(prop_files, output_file, indent=4)


    def import_prop_files(self, prop_files_file: str) -> dict:
        print('Importing prop files...')
        prop_files = {}

        with open(prop_files_file, 'r') as file:
            prop_files = json.load(file)

        return prop_files


    def create_profiling_summery(self, prop_files: dict, output_path: str, debug=True) -> None:
        print(f'Profiling by groups...')

        done = 0
        needed = 0

        for folder_path in prop_files.keys():
            for prop_type in prop_files[folder_path].keys():
                for file in prop_files[folder_path][prop_type].keys():
                    needed += 1
        
        profile_groups = {}
        
        for folder_path in prop_files.keys():
            for prop_type in prop_files[folder_path].keys():
            
                prop_file_batch = PropFileBatch(folder_path + " - " + prop_type)

                for file in prop_files[folder_path][prop_type].keys():
                    prop_file = PropFile(entries=[[]], name=file, str_format=prop_files[folder_path][prop_type][file])
                    prop_file_batch.add_prop_file(prop_file)
                    done +=1

                    if debug:
                        print(f'Profiling by groups {get_progress(done, needed)}...')
                    
                prop_file_batch.evaluate_str_format()

                if prop_file_batch.str_format not in profile_groups.keys():
                    profile_groups[prop_file_batch.str_format] = {}

                if prop_file_batch.full_path not in profile_groups[prop_file_batch.str_format]:
                    profile_groups[prop_file_batch.str_format][prop_file_batch.full_path] = {}
                
                for prop_file in prop_file_batch.prop_files:
                    profile_groups[prop_file_batch.str_format][prop_file_batch.full_path][prop_file.name] = {'entries': [], 'str_format': prop_file.str_format}
        

        print('Dumping to file...')
        with open(output_path, 'w+') as output_file:
            json.dump(profile_groups, output_file, indent=4)


    def import_profiling_summery(self, input_path: str) -> dict:
        print("Importing profiles...")
        with open(input_path, 'r') as input_file:
            profiles = json.load(input_file)
        
        profile_groups = {}
        for str_format in profiles.keys():
            if str_format not in profile_groups.keys():
                profile_groups[str_format] = []

            for batch_name in profiles[str_format].keys():
                batch = PropFileBatch(batch_name, str_format)
                for prop_file_name in profiles[str_format][batch_name]:
                    raw_profile = profiles[str_format][batch_name][prop_file_name]
                    batch.add_prop_file(PropFile(raw_profile['entries'], prop_file_name, raw_profile['str_format']))

                profile_groups[str_format].append(batch)

        return profile_groups


if __name__ == "__main__":
    Profiler = PropFileProfiler()
    
    if 'linux' in os.sys.platform:
        # LINUX
        input_paths =  (   
                        # r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\High_Altitude", 
                        # r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Panorama",
                        # r"/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Resistant_Navigation", 
                        r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Low_Altitude",
                        )
        output_file_list = r"/mnt/share/Local/OfflineDB/data-pre-processing-scripts/modification_scripts/prop_files/resistant_navigation_file_list.json"
        output_prop_files = r"/mnt/share/Local/OfflineDB/data-pre-processing-scripts/modification_scripts/prop_files/resistant_navigation_prop_files.json"
        output_profile_summery = r"/mnt/share/Local/OfflineDB/data-pre-processing-scripts/modification_scripts/prop_files/resistent_navigation_profiles_summery.json"
    else:
        # WINDOWS
        input_paths =  (   
                        # r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\High_Altitude", 
                        r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Panorama",
                        # r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation"
                        # r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Low_Altitude",
                        )
        
        output_file_list = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama\file_list.json"
        output_prop_files = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama\prop_files.json"
        output_profile_summery = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama\profiles_summery.json"
    

    # Profiler.export_file_list(output_file_list, input_paths)
    Profiler.export_prop_files(output_file_list, output_prop_files)
    
    prop_files = Profiler.import_prop_files(output_prop_files)
    Profiler.create_profiling_summery(prop_files, output_profile_summery)
