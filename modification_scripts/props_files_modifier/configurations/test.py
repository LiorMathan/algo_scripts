os.sys.path.insert(1, r'Z:\OfflineDB\data-pre-processing-scripts\prop_files')

from PropFileProfiler import PropFileProfiler

key = "int int int bool bool bool bool int float float int bool bool bool bool bool bool bool"
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"Z:\OfflineDB\data-pre-processing-scripts\modification_scripts\prop_files\profiles_summery.json")

# int int int bool bool bool bool int float float int bool bool bool bool bool bool bool
conversion_data = PropFileConversionData(
    { # overlap
        0: 0,   # int 
        1: 1,   # int
        2: 2,   # int
        3: 3,   # bool
        4: 4,   # bool
        5: 5,   # bool
        # bool
        7: 7,   # int
        8: 8,   # float
        9: 9,   # float
        10: 10, # int
        # bool
        # bool
        # bool
        # bool
        # bool
        # bool
        # bool
    }, 
    { # default_overlay
        33: "720",
        34: "576",
        35: "5.4",
        36: "1.1"
    }, 
    40)

output = r"Z:\OfflineDB\data-pre-processing-scripts\modification_scripts\prop_files\output"
Modifier = PropFileModifier(conversion_data)

for batch in profiles[key]:
    Modifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]))