import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

key = "int int int bool bool bool int int float float int int timestamp str str int int int bool bool bool int float float float bool bool int int float float float float bool"
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama\profiles_summery.json")

default_overlay_8 = {
    33: "640", # Width
    34: "480", # Height
    35: "8.5", # Focal Length
    36: "17.0"  # Pixel Pitch
}

default_overlay_19 = {
    33: "640", # Width
    34: "480", # Height
    35: "12.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

default_overlay_35 = {
    33: "640", # Width
    34: "480", # Height
    35: "35.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

default_overlay_NULL = {
    33: "640", # Width
    34: "480", # Height
    36: "17.0"  # Pixel Pitch
}

overlap = {
    12: 14,
    13: 15,
    14: 16,
    15: 17,
    16: 18,
    17: 19,
    18: 20,
    19: 21,
    20: 22,
    21: 23,
    22: 24,
    23: 25,
    24: 26,
    25: 27,
    26: 28,
    27: 29,
    28: 30,
    29: 31,

    30: 12,
    31: 13,
}

conversion_data_8 = PropFileConversionData(
    overlap,    
    default_overlay_8,
    40
)

conversion_data_19 = PropFileConversionData(
    overlap,    
    default_overlay_19,
    40
)

conversion_data_35 = PropFileConversionData(
    overlap,    
    default_overlay_35,
    40
)

conversion_data_null_FL = PropFileConversionData(
    overlap,    
    default_overlay_NULL,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama"

backup = False
replace = True

for batch in profiles[key]:
    if "Panorama\\0019" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)
    
    if "Panorama\\0020" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_19, replace=replace, backup=backup)
    
    if "Panorama\\0021" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)
    
    if "Panorama\\0022" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_19, replace=replace, backup=backup)
    
    if "Panorama\\0023" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)
    
    if "Panorama\\0024" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0025" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)
    
    if "Panorama\\0026" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_19, replace=replace, backup=backup)
    
    if "Panorama\\0027" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0028" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0029" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)
    
    if "Panorama\\0030" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_19, replace=replace, backup=backup)
    
    if "Panorama\\0031" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)
    
    if "Panorama\\0032" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)
    
    if "Panorama\\0033" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)
    
    if "Panorama\\0034" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0035" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0036" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)