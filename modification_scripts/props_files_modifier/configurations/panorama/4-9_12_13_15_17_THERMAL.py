import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

key = "int int int bool bool bool int int float float int int timestamp str int int int bool bool bool int float float float bool bool int int float float float float"
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama\profiles_summery.json")

default_overlay_8 = {
    33: "640", # Width
    34: "480", # Height
    35: "8.0", # Focal Length
    36: "17"  # Pixel Pitch
}

default_overlay_19 = {
    33: "640", # Width
    34: "480", # Height
    35: "19.0", # Focal Length
    36: "17"  # Pixel Pitch
}

default_overlay_35 = {
    33: "640", # Width
    34: "480", # Height
    35: "35.0", # Focal Length
    36: "17"  # Pixel Pitch
}

overlap = {
    12: 14,
    13: 15,
    14: 16,
    15: 17,
    16: 18,
    17: 19,
    18: 20,
    19: 21,
    20: 22,
    21: 23,
    22: 24,
    23: 25,
    24: 26,
    25: 27,
    26: 28,
    27: 29,
    28: 30,
    29: 31,

    30: 12,
    31: 13,
}

conversion_data_8 = PropFileConversionData(
    overlap,    
    default_overlay_8,
    40
)

conversion_data_19 = PropFileConversionData(
    overlap,    
    default_overlay_19,
    40
)

conversion_data_35 = PropFileConversionData(
    overlap,    
    default_overlay_35,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama"

backup = False
replace = True

for batch in profiles[key]:
    if "Panorama\\0004" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0005" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0006" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)

    if "Panorama\\0007" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)
    
    if "Panorama\\0008" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_35, replace=replace, backup=backup)
    
    if "Panorama\\0009" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_19, replace=replace, backup=backup)
    
    if "Panorama\\0012" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0013" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0015" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0017" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_19, replace=replace, backup=backup)