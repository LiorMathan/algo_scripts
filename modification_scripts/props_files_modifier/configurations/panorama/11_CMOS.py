import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

key = "int int int bool bool bool int int float float int int int int int bool bool bool int float float float bool bool int int float float float float"
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama\profiles_summery.json")

conversion_data = PropFileConversionData(
    {

    },
    {
        33: "720", # Width
        34: "576", # Height
        35: "2.86", # Focal Length
        36: "1.1"  # Pixel Pitch
    },
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama"

backup = False
replace = True

for batch in profiles[key]:
    if "Panorama\\0011" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data, replace=replace, backup=backup)