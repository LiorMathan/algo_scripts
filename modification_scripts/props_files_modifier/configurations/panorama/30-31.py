import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

import time
from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

key = ""
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama\profiles_summery.json")

default_overlay_30_CMOS = {
    0: '0',
    1: '0',
    2: '0',
    3: '0',
    4: '0',
    5: '0',
    6: '0',
    7: '0',
    8: '0',
    9: '0',
    10: '0',
    11: '0',
    12: '0',
    13: '0',
    14: '0',
    15: '0',
    16: '0',
    17: '0',
    18: '0',
    19: '0',
    20: '0',
    21: '0',
    22: '0',
    23: '0',
    24: '0',
    25: '0',
    26: '0',
    27: '0',
    28: '0',
    29: '0',
    30: '0',
    31: '0',
    32: '0',
    33: "720", # Width
    34: "576", # Height
    35: "12.0", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_30_THERMAL = {
    0: '0',
    1: '0',
    2: '0',
    3: '0',
    4: '0',
    5: '0',
    6: '0',
    7: '0',
    8: '0',
    9: '0',
    10: '0',
    11: '0',
    12: '0',
    13: '0',
    14: '0',
    15: '0',
    16: '0',
    17: '0',
    18: '0',
    19: '0',
    20: '0',
    21: '0',
    22: '0',
    23: '0',
    24: '0',
    25: '0',
    26: '0',
    27: '0',
    28: '0',
    29: '0',
    30: '0',
    31: '0',
    32: '0',
    33: "640", # Width
    34: "480", # Height
    35: "19.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

default_overlay_31_CMOS = {
    0: '0',
    1: '0',
    2: '0',
    3: '0',
    4: '0',
    5: '0',
    6: '0',
    7: '0',
    8: '0',
    9: '0',
    10: '0',
    11: '0',
    12: '0',
    13: '0',
    14: '0',
    15: '0',
    16: '0',
    17: '0',
    18: '0',
    19: '0',
    20: '0',
    21: '0',
    22: '0',
    23: '0',
    24: '0',
    25: '0',
    26: '0',
    27: '0',
    28: '0',
    29: '0',
    30: '0',
    31: '0',
    32: '0',
    33: "720", # Width
    34: "576", # Height
    35: "12.0", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_31_THERMAL = {
    0: '0',
    1: '0',
    2: '0',
    3: '0',
    4: '0',
    5: '0',
    6: '0',
    7: '0',
    8: '0',
    9: '0',
    10: '0',
    11: '0',
    12: '0',
    13: '0',
    14: '0',
    15: '0',
    16: '0',
    17: '0',
    18: '0',
    19: '0',
    20: '0',
    21: '0',
    22: '0',
    23: '0',
    24: '0',
    25: '0',
    26: '0',
    27: '0',
    28: '0',
    29: '0',
    30: '0',
    31: '0',
    32: '0',
    33: "640", # Width
    34: "480", # Height
    35: "35.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

conversion_data_30_CMOS = PropFileConversionData(
    {
    },
    default_overlay_30_CMOS,
    40
)

conversion_data_30_THERMAL = PropFileConversionData(
    {
    },    
    default_overlay_30_THERMAL,
    40
)

conversion_data_31_CMOS = PropFileConversionData(
    {
    },    
    default_overlay_31_CMOS,
    40
)

conversion_data_31_THERMAL = PropFileConversionData(
    {
    },    
    default_overlay_31_THERMAL,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama"

backup = False
replace = True

for batch in profiles[key]:
    if "Panorama\\0030" in batch.full_path:
        if "CMOS" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_30_CMOS, replace=replace, backup=backup)
        if "THERMAL" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_30_THERMAL, replace=replace, backup=backup)
    
    if "Panorama\\0031" in batch.full_path:
        if "CMOS" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_31_CMOS, replace=replace, backup=backup)
        if "THERMAL" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_31_THERMAL, replace=replace, backup=backup)