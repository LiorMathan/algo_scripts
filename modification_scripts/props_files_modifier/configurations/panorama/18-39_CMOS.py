import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

import time
from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

key = "int int int bool bool bool int int float float int int int int int bool bool bool int float float float bool bool int int float float float float bool"
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama\profiles_summery.json")

default_overlay_2 = {
    33: "720", # Width
    34: "576", # Height
    35: "2.86", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_8 = {
    33: "720", # Width
    34: "576", # Height
    35: "8.0", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_12 = {
    33: "720", # Width
    34: "576", # Height
    35: "12.0", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_5 = {
    33: "720", # Width
    34: "576", # Height
    35: "5.4", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_NULL = {
    33: "720", # Width
    34: "576", # Height
    36: "1.1"  # Pixel Pitch
}

conversion_data_2 = PropFileConversionData(
    {
    },
    default_overlay_2,
    40
)

conversion_data_8 = PropFileConversionData(
    {
    },    
    default_overlay_8,
    40
)

conversion_data_12 = PropFileConversionData(
    {
    },    
    default_overlay_12,
    40
)

conversion_data_5 = PropFileConversionData(
    {
    },    
    default_overlay_5,
    40
)

conversion_data_null_FL = PropFileConversionData(
    {
    },    
    default_overlay_NULL,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama"

backup = False
replace = True

for batch in profiles[key]:
    if "Panorama\\0018" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2, replace=replace, backup=backup)
    
    if "Panorama\\0019" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_5, replace=replace, backup=backup)
    
    if "Panorama\\0021" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_12, replace=replace, backup=backup)
    
    if "Panorama\\0022" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_5, replace=replace, backup=backup)
    
    if "Panorama\\0023" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_12, replace=replace, backup=backup)
    
    if "Panorama\\0024" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2, replace=replace, backup=backup)
    
    if "Panorama\\0025" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2, replace=replace, backup=backup)
    
    if "Panorama\\0026" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
    
    if "Panorama\\0027" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2, replace=replace, backup=backup)
    
    if "Panorama\\0028" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2, replace=replace, backup=backup)
    
    if "Panorama\\0029" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_12, replace=replace, backup=backup)
    
    if "Panorama\\0030" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_12, replace=replace, backup=backup)
    
    if "Panorama\\0031" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_12, replace=replace, backup=backup)
    
    if "Panorama\\0032" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_12, replace=replace, backup=backup)
    
    if "Panorama\\0033" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_5, replace=replace, backup=backup)
    
    if "Panorama\\0034" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2, replace=replace, backup=backup)
    
    if "Panorama\\0035" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2, replace=replace, backup=backup)
    
    if "Panorama\\0036" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2, replace=replace, backup=backup)
    
    if "Panorama\\0038" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_null_FL, replace=replace, backup=backup)
    
    if "Panorama\\0039" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_null_FL, replace=replace, backup=backup)