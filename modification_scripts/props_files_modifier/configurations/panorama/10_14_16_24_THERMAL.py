import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

key = "int int int bool bool bool int int float float int int timestamp str int int int bool bool bool int float float float bool bool int int float float float float"
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama\profiles_summery.json")

default_overlay_19 = {
    33: "640", # Width
    34: "480", # Height
    35: "19.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

default_overlay_8 = {
    33: "640", # Width
    34: "480", # Height
    35: "8.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

overlay = {
    12: 14,
    13: 15,
    14: 16,
    15: 17,
    16: 18,
    17: 19,
    18: 20,
    19: 21,
    20: 22,
    21: 23,
    22: 24,
    23: 25,
    24: 26,
    25: 27,
    26: 28,
    27: 29,
    28: 30,
    29: 31,

    30: 12,
    31: 13,
}

conversion_data_8 = PropFileConversionData(
    overlay,
    default_overlay_8,
    40
)

conversion_data_19 = PropFileConversionData(
    overlay,
    default_overlay_19,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\panorama"

backup = False
replace = True

for batch in profiles[key]:
    if "Panorama\\0010" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_19, replace=replace, backup=backup)
    
    if "Panorama\\0014" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_19, replace=replace, backup=backup)
    
    if "Panorama\\0016" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_19, replace=replace, backup=backup)
    
    if "Panorama\\0024" in batch.full_path:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8, replace=replace, backup=backup)
