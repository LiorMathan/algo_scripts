import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\utils')
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

keys = [
    "int int int bool bool bool int int float float int int timestamp str int int int bool bool bool int float float float bool bool bool bool float float float float",
]
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_high\profiles_summery.json")

default_overlay_8_9_13 = {
    33: "640", # Width
    34: "480", # Height
    35: "8.5", # Focal Length
    36: "17"  # Pixel Pitch
}

default_overlay_10_12 = {
    33: "640", # Width
    34: "480", # Height
    35: "19", # Focal Length
    36: "17"  # Pixel Pitch
}

default_overlay_11_14 = {
    33: "640", # Width
    34: "480", # Height
    35: "35", # Focal Length
    36: "17"  # Pixel Pitch
}

overlap = {
    12: 14,
    13: 15,
    14: 16,
    15: 17,
    16: 18,
    17: 19,
    18: 20,
    19: 21,
    20: 22,
    21: 23,
    22: 24,
    23: 25,
    24: 26,
    25: 27,
    26: 28,
    27: 29,
    28: 30,
    29: 31,

    30: 12,
    31: 13
}

conversion_data_8_9_13 = PropFileConversionData(
    overlap,
    default_overlay_8_9_13,
    40
    )

conversion_data_10_12 = PropFileConversionData(
    overlap,
    default_overlay_10_12,
    40
    )

conversion_data_11_14 = PropFileConversionData(
    overlap,
    default_overlay_11_14,
    40
    )

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_high"

for key in keys:
    for batch in profiles[key]:
        if "High_Altitude\\0008" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8_9_13)
        
        if "High_Altitude\\0009" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8_9_13)
        
        if "High_Altitude\\0010" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_10_12)
        
        if "High_Altitude\\0011" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_11_14)
        
        if "High_Altitude\\0012" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_10_12)
        
        if "High_Altitude\\0013" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8_9_13)
        
        if "High_Altitude\\0014" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_11_14)