import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\utils')
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

keys = [
    "int int int bool bool bool int int float float int int int int int bool bool bool int float float float bool bool bool bool float float float float",
]
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_high\profiles_summery.json")

default_overlay_8_9_13_14 = {
    33: "720", # Width
    34: "576", # Height
    35: "2.86", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_10_12 = {
    33: "720", # Width
    34: "576", # Height
    35: "4.3", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_11 = {
    33: "720", # Width
    34: "576", # Height
    35: "12", # Focal Length
    36: "1.1"  # Pixel Pitch
}

conversion_data_8_9_13_14 = PropFileConversionData(
    {
        22: None
    },
    default_overlay_8_9_13_14,
    40
    )

conversion_data_10_12 = PropFileConversionData(
    {
        22: None
    },
    default_overlay_10_12,
    40
    )

conversion_data_11 = PropFileConversionData(
    {
        22: None
    },
    default_overlay_11,
    40
    )

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_high"

for key in keys:
    for batch in profiles[key]:
        if "High_Altitude\\0008" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8_9_13_14)
        
        if "High_Altitude\\0009" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8_9_13_14)
        
        if "High_Altitude\\0010" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_10_12)
        
        if "High_Altitude\\0011" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_11)
        
        if "High_Altitude\\0012" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_10_12)
        
        if "High_Altitude\\0013" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8_9_13_14)
        
        if "High_Altitude\\0014" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8_9_13_14)