import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\utils')
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

keys = [
    "int int int bool bool bool int int float float int int int int int bool bool bool int float float float bool bool bool int float float float float bool",
]
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_high\profiles_summery.json")

default_overlay = {
    33: "720", # Width
    34: "576", # Height
    35: "2.86", # Focal Length
    36: "1.1"  # Pixel Pitch
}

conversion_data = PropFileConversionData(
    {
        22: None,
        30: None
    },
    default_overlay,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_high"

for key in keys:
    for batch in profiles[key]:
        PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data)