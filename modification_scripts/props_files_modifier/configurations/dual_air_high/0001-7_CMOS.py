import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\utils')
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

keys = [
    "int int int bool bool bool bool int float float int bool bool bool bool bool bool bool",
    "int int int bool bool bool int int float float int int bool bool bool bool bool bool"
]
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_high\profiles_summery.json")

default_overlay_0001_3 = {
    33: "720", # Width
    34: "576", # Height
    35: "5.4", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_0004 = {
    33: "720", # Width
    34: "576", # Height
    35: "12", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_0005_7__3 = {
    33: "720", # Width
    34: "576", # Height
    35: "2.86", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_0007__1 = {
    33: "720", # Width
    34: "576", # Height
    35: "12", # Focal Length
    36: "1.1"  # Pixel Pitch
}

conversion_data_0001_3 = PropFileConversionData(
    {},
    default_overlay_0001_3,
    40
    )

conversion_data_0004 = PropFileConversionData(
    {},
    default_overlay_0004,
    40
    )

conversion_data_0005_7__3 = PropFileConversionData(
    {},
    default_overlay_0005_7__3,
    40
    )

conversion_data_0007__1 = PropFileConversionData(
    {},
    default_overlay_0007__1,
    40
    )

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_high"

for key in keys:
    for batch in profiles[key]:
        if "High_Altitude\\0001" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_0001_3)
        
        if "High_Altitude\\0002" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_0001_3)
        
        if "High_Altitude\\0003" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_0001_3)
        
        if "High_Altitude\\0004" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_0004)
        
        if "High_Altitude\\0005" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_0005_7__3)
        
        if "High_Altitude\\0006" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_0005_7__3)
        
        if "High_Altitude\\0007" in batch.full_path:
            if "F12-35" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_0007__1)
            if "F2-8" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_0005_7__3)