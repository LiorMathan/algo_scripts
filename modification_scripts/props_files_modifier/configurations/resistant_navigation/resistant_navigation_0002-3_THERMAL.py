import os
os.sys.path.insert(1, r'/mnt/share/Local/OfflineDB/data-pre-processing-scripts/modification_scripts/prop_files')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

key = "int int int bool bool bool int int float float int int timestamp str int int int bool bool bool"
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"/mnt/share/Local/OfflineDB/data-pre-processing-scripts/modification_scripts/prop_files/resistent_navigation_profiles_summery.json")

default_overlay = {
    33: "640", # Width
    34: "480", # Height
    35: "8.5", # Focal Length
    36: "17"  # Pixel Pitch
}

conversion_data = PropFileConversionData(
    {
        12: 14,
        13: 15,
        14: 16,
        15: 17,
        16: 18,
        17: 19,
        30: 12,
        31: 13
    },
    default_overlay,
    40
    )

output = r"/mnt/share/Local/OfflineDB/data-pre-processing-scripts/modification_scripts/prop_files/OUTPUT & BACKUPS - DO NOT TOUCH/resistant_navigation/"
Modifier = PropFileModifier(conversion_data)

for batch in profiles[key]:
    Modifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]))