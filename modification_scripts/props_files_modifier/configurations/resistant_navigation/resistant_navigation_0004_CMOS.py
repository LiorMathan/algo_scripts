import os
os.sys.path.insert(1, r'/mnt/share/Local/OfflineDB/data-pre-processing-scripts/modification_scripts/prop_files')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

key = "int int int bool bool bool int int float float int int float float float float int int int int int bool bool bool"
Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"/mnt/share/Local/OfflineDB/data-pre-processing-scripts/modification_scripts/prop_files/resistent_navigation_profiles_summery.json")

default_overlay_1500 = {
    33: "720", # Width
    34: "576", # Height
    35: "4.3", # Focal Length
    36: "1.1"  # Pixel Pitch
}

conversion_data = PropFileConversionData(
    {
        11: 16,
        12: None,
        13: None,
        14: None,
        15: None,
        16: None,
        17: None,
        18: 11,
        19: 12,
        20: 13,
        21: 14,
        22: 15
    },
    default_overlay_1500,
    40)

output = r"/mnt/share/Local/OfflineDB/data-pre-processing-scripts/modification_scripts/prop_files/OUTPUT & BACKUPS - DO NOT TOUCH/resistant_navigation"
Modifier = PropFileModifier(conversion_data)

for batch in profiles[key]:
    Modifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]))