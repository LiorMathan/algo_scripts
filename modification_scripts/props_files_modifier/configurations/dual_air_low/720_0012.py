import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\utils')
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

keys = [
    "int int float bool bool bool int int float float int bool",
]

Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low\profiles_summery.json")

default_overlay_CMOS = {
    33: "720", # Width
    34: "576", # Height
    35: "8.0", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_THERMAL = {
    33: "720", # Width
    34: "576", # Height
    35: "35.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

conversion_data_CMOS = PropFileConversionData(
    {},
    default_overlay_CMOS,
    40
)

conversion_data_THERMAL = PropFileConversionData(
    {},
    default_overlay_THERMAL,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low"

for key in keys:
    for batch in profiles[key]:
        if "CMOS" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_CMOS)
        elif "THERMAL" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_THERMAL)