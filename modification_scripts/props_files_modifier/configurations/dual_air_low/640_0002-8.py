import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\utils')
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

keys = [
    "int int float bool bool bool int int float float int",
    "int int float bool bool bool int int float float int int",
]

Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low\profiles_summery.json")

default_overlay_2_3_4_CMOS = {
    33: "640", # Width
    34: "480", # Height
    35: "2.86", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_5_6_7_CMOS = {
    33: "640", # Width
    34: "480", # Height
    35: "5.4", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_8_CMOS = {
    33: "640", # Width
    34: "480", # Height
    35: "12.0", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_2_3_4_THERMAL = {
    33: "640", # Width
    34: "480", # Height
    35: "19.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

default_overlay_5_6_7_8_THERMAL = {
    33: "640", # Width
    34: "480", # Height
    35: "13.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

conversion_data_2_3_4_CMOS = PropFileConversionData(
    {},
    default_overlay_2_3_4_CMOS,
    40
)

conversion_data_5_6_7_CMOS = PropFileConversionData(
    {},
    default_overlay_5_6_7_CMOS,
    40
)

conversion_data_8_CMOS = PropFileConversionData(
    {},
    default_overlay_8_CMOS,
    40
)

conversion_data_2_3_4_THERMAL = PropFileConversionData(
    {},
    default_overlay_2_3_4_THERMAL,
    40
)

conversion_data_5_6_7_8_THERMAL = PropFileConversionData(
    {},
    default_overlay_5_6_7_8_THERMAL,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low"

for key in keys:
    for batch in profiles[key]:
        if "Low_Altitude\\640-480\\0002" in batch.full_path:
            if "CMOS" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2_3_4_CMOS)
            elif "THERMAL" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2_3_4_THERMAL)
                
        if "Low_Altitude\\640-480\\0003" in batch.full_path:
            if "CMOS" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2_3_4_CMOS)
            elif "THERMAL" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2_3_4_THERMAL)
                
        if "Low_Altitude\\640-480\\0004" in batch.full_path:
            if "CMOS" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2_3_4_CMOS)
            elif "THERMAL" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_2_3_4_THERMAL)
                
        if "Low_Altitude\\640-480\\0005" in batch.full_path:
            if "CMOS" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_5_6_7_CMOS)
            elif "THERMAL" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_5_6_7_8_THERMAL)
                
        if "Low_Altitude\\640-480\\0006" in batch.full_path:
            if "CMOS" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_5_6_7_CMOS)
            elif "THERMAL" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_5_6_7_8_THERMAL)
                
        if "Low_Altitude\\640-480\\0007" in batch.full_path:
            if "CMOS" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_5_6_7_CMOS)
            elif "THERMAL" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_5_6_7_8_THERMAL)
                
        if "Low_Altitude\\640-480\\0008" in batch.full_path:
            if "CMOS" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_8_CMOS)
            elif "THERMAL" in batch.full_path:
                PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_5_6_7_8_THERMAL)