import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\utils')
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

keys = [
    "int int int bool bool bool int int float float int int bool bool bool bool bool bool",
]

Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low\profiles_summery.json")

default_overlay_1_9 = {
    33: "720", # Width
    34: "576", # Height
    35: "4.3", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_11_17 = {
    33: "720", # Width
    34: "576", # Height
    35: "12", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_13_14_16 = {
    33: "720", # Width
    34: "576", # Height
    35: "8", # Focal Length
    36: "1.1"  # Pixel Pitch
}

overlap = {
    12: None,
    13: None,
    14: None,
    15: None,
    16: None,
    17: None
}

conversion_data_1_9 = PropFileConversionData(
    overlap,
    default_overlay_1_9,
    40
)

conversion_data_11_17 = PropFileConversionData(
    overlap,
    default_overlay_11_17,
    40
)

conversion_data_13_14_16 = PropFileConversionData(
    overlap,
    default_overlay_13_14_16,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low"

for key in keys:
    for batch in profiles[key]:
        if "Low_Altitude\\720-576\\0001" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_1_9)
        
        if "Low_Altitude\\720-576\\0009" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_1_9)
        
        if "Low_Altitude\\720-576\\0011" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_11_17)
        
        if "Low_Altitude\\720-576\\0013" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_13_14_16)
        
        if "Low_Altitude\\720-576\\0014" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_13_14_16)
        
        if "Low_Altitude\\720-576\\0016" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_13_14_16)
        
        if "Low_Altitude\\720-576\\0017" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_11_17)