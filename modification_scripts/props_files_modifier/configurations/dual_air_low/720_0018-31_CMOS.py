import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\utils')
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

keys = [
    "int int int bool bool bool int int float float int int int int int bool bool bool int float float float bool bool bool bool float float float float bool",
]

Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low\profiles_summery.json")

default_overlay_F8 = {
    33: "720", # Width
    34: "576", # Height
    35: "8.0", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_F5 = {
    33: "720", # Width
    34: "576", # Height
    35: "5.4", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_F12 = {
    33: "720", # Width
    34: "576", # Height
    35: "12.0", # Focal Length
    36: "1.1"  # Pixel Pitch
}

conversion_data_F8 = PropFileConversionData(
    {},
    default_overlay_F8,
    40
)

conversion_data_F5 = PropFileConversionData(
    {},
    default_overlay_F5,
    40
)

conversion_data_F12 = PropFileConversionData(
    {},
    default_overlay_F12,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low"

for key in keys:
    for batch in profiles[key]:
        if "Low_Altitude\\720-576\\0018" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F8, backup=False, replace=True)
                
        if "Low_Altitude\\720-576\\0019" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F5, backup=False, replace=True)
                
        if "Low_Altitude\\720-576\\0020" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F12, backup=False, replace=True)
                
        if "Low_Altitude\\720-576\\0021" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F12, backup=False, replace=True)
                
        if "Low_Altitude\\720-576\\0022" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F8, backup=False, replace=True)
                
        if "Low_Altitude\\720-576\\0023" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F5, backup=False, replace=True)
                
        if "Low_Altitude\\720-576\\0024" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F8, backup=False, replace=True)
        
        if "Low_Altitude\\720-576\\0025" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F8, backup=False, replace=True)
        
        if "Low_Altitude\\720-576\\0026" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F5, backup=False, replace=True)
        
        if "Low_Altitude\\720-576\\0027" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F5, backup=False, replace=True)
        
        if "Low_Altitude\\720-576\\0028" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F8, backup=False, replace=True)
        
        if "Low_Altitude\\720-576\\0029" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F5, backup=False, replace=True)
        
        if "Low_Altitude\\720-576\\0030" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F8, backup=False, replace=True)
        
        if "Low_Altitude\\720-576\\0031" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F8, backup=False, replace=True)
        
        if "Low_Altitude\\720-576\\210121" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F5, backup=False, replace=True)

            