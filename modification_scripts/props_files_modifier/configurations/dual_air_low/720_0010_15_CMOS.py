import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\utils')
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

keys = [
    "int int int bool bool bool bool int float float int bool bool bool bool bool bool bool",
]

Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low\profiles_summery.json")

default_overlay_10 = {
    33: "720", # Width
    34: "576", # Height
    35: "12.0", # Focal Length
    36: "1.1"  # Pixel Pitch
}

default_overlay_15 = {
    33: "720", # Width
    34: "576", # Height
    35: "8.0", # Focal Length
    36: "1.1"  # Pixel Pitch
}

overlap = {
    12: None,
    13: None,
    14: None,
    15: None,
    16: None,
    17: None
}

conversion_data_10 = PropFileConversionData(
    overlap,
    default_overlay_10,
    40
)

conversion_data_15 = PropFileConversionData(
    overlap,
    default_overlay_15,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low"

for key in keys:
    for batch in profiles[key]:
        if "Low_Altitude\\720-576\\0010" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_10)
        
        if "Low_Altitude\\720-576\\0015" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_15)