import os
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\utils')
os.sys.path.insert(1, r'C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier')

from PropFileProfiler import PropFileProfiler
from PropFileConversionData import PropFileConversionData
from PropFileModifier import PropFileModifier

keys = [
    "int int int bool bool bool int int float float int int timestamp str int int int bool bool bool int float float float bool bool bool bool float float float float bool",
]

Profiler = PropFileProfiler()
profiles = Profiler.import_profiling_summery(r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low\profiles_summery.json")

default_overlay_F35 = {
    33: "640", # Width
    34: "480", # Height
    35: "35.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

default_overlay_F19 = {
    33: "640", # Width
    34: "480", # Height
    35: "19.0", # Focal Length
    36: "17.0"  # Pixel Pitch
}

overlap = {
    12: 14,
    13: 15,
    14: 16,
    15: 17,
    16: 18,
    17: 19,
    18: 20,
    19: 21,
    20: 22,
    21: 23,
    22: 24,
    23: 25,
    24: 26,
    25: 27,
    26: 28,
    27: 29,
    28: 30,
    29: 31,

    30: 12,
    31: 13,
} 

conversion_data_F35 = PropFileConversionData(
    overlap,
    default_overlay_F35,
    40
)

conversion_data_F19 = PropFileConversionData(
    overlap,
    default_overlay_F19,
    40
)

output = r"C:\Users\Omer Wexler\Desktop\tes\workspace\algo-scripts\modification_scripts\props_files_modifier\OUTPUT & BACKUPS\dual_air_low"
replace = True
backup = True

for key in keys:
    for batch in profiles[key]:
        if "Low_Altitude\\720-576\\0018" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F35, backup=backup, replace=replace)
                
        if "Low_Altitude\\720-576\\0019" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F35, backup=backup, replace=replace)
                
        if "Low_Altitude\\720-576\\0020" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F35, backup=backup, replace=replace)
                
        if "Low_Altitude\\720-576\\0021" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F35, backup=backup, replace=replace)
                
        if "Low_Altitude\\720-576\\0022" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F35, backup=backup, replace=replace)
                
        if "Low_Altitude\\720-576\\0023" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F19, backup=backup, replace=replace)
                
        if "Low_Altitude\\720-576\\0024" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F35, backup=backup, replace=replace)
        
        if "Low_Altitude\\720-576\\0025" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F35, backup=backup, replace=replace)
        
        if "Low_Altitude\\720-576\\0026" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F19, backup=backup, replace=replace)
        
        if "Low_Altitude\\720-576\\0027" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F19, backup=backup, replace=replace)
        
        if "Low_Altitude\\720-576\\0028" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F35, backup=backup, replace=replace)
        
        if "Low_Altitude\\720-576\\0029" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F19, backup=backup, replace=replace)
        
        if "Low_Altitude\\720-576\\0030" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F19, backup=backup, replace=replace)
        
        if "Low_Altitude\\720-576\\0031" in batch.full_path:
            PropFileModifier.modify_all(batch, os.path.join(output, batch.full_path.split(os.sep)[-1]), conversion_data_F35, backup=backup, replace=replace)