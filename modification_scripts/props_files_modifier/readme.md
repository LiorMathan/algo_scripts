
# The PropFileModifier Utility Sub-Lib Guide
These utilities help profile, transform, and preform mass backups and implementations for props files.

## *PropFile*

This data structure is used to contain data on a single prop file.
### Public Members
***

	name: str 
The `PropFile`'s full path.
***

	entries: list[list[str]]
A 2d matrix containing ALL the `PropFile`'s data, where the first dimension is the rows, and the second is columns.
***
	str_format: str
The `PropFile`'s line format, where every type is seperated by a space. 
An example - `int int int bool bool bool int float float`
This would represent a file with 9 flight parameters per line. 
Format types:
- `bool` - A column which contains only the characters `1` and `0`. Exculding float values such as `9`.
- `int` - A column which contains only numbers . In order to seperate `int`s and 			  `bool`s, a column marked as `int` must contain at least one value that is a number and is not `0` nor `1`.
- `timestamp` - Any string that contains `:`, usually in the format of `HH:MM:SS`.
- `float` - Any floating decimal number, including `0.0000` (will not be marked as `bool`).
- `str` - Any value which is not one of the previous types, yet still contains characters (such as a ms count - `001244`).
- `null` - An empty column (usually should be ignored).

### Methods
***
    PropFile(entries=[[]], name="", str_format=None)

Used  to create a single `PropFile`.

**Parameters**

- `entries` - A 2d matrix which specifies the file's contents (first dimension is rows, and the second is columns).
- `name` - The props file's full path.
- `str_format` - The override line format to register for this file. When empty, the constructor generates a format using the `entries` parameter. PLEASE NOTE that when the constructor generates a new format it iterates over the entire `entries` matrix.
***

	from_path(path: str) -> PropFile
Creates a new `PropFile` from a given `path`.
This method will also generate a `str_format` for the new `PropFile`.
This is a `@staticmethod`.
 
The generation proccess for `str_format` will count for every column in all the files which types repeat the most, and from that count will reassemble the new `str_format` of the batch. 

Note that some types "overpower" others. For example `bool` and `int`:
```
if  'int'  in column.keys() and  'bool'  in column.keys():
	column['int'] += column['bool']
	column.pop('bool')
```

so that all `bool` counts would sum up as `int` counts where the both of them repeat at the same column.

The same is done for `str` and `int`.

**Parameters**

- `path` - The path to read a prop file from.

## *PropFileBatch*

A data structure that represents a single batch of props files. A batch is considered as all the props files of a single format (THERMAL/CMOS) from a single video.

### Public Members
***

	prop_files: list[PropFile]
A list containing `PropFile`s that are linked to this batch.
***

	full_path: str
The full path of the batch, followed by a type:
`<path>` + `-` + `<type>`

`<type>` could be either `THERMAL` or `CMOS`.
***

	str_format: str
The consistant line format of the files inside the `PropFileBatch`.
This format follows the `PropFile`'s `str_format` data member, as specified above

### Methods
***
	PropFileBatch(self, full_path, str_format=None)
Creates a new `PropFileBatch`.
Differently from `PropFile`, this constructor doesn't generate it's own `str_format` automatically.

**Parameters**

- `full_path` - The batch's full path. 
Expected to be in the format of `<path>` + `-` + `<type>` where `<type>` could be either `THERMAL` or `CMOS`.
- `str_format` - The consistant format of all the `PropFile`s inside the batch. 
	This format follows the `PropFile`'s `str_format` data member, as specified above.
***
	add_prop_file(prop_file: PropFile) -> None
Adds a single `PropFile` to the batch.

**Parameters**

- `prop_file` - The `PropFile` to add.

***
	evaluate_str_format() -> None
Iterates over all the `PropFile`s in the batch in search of the most consistant format.
This method will count for every column in all the files which types repeat the most, and from that count will reassemble the new `str_format` of the batch. 

This procces is the same as the `PropFile`'s generation proccess, except it is done for the whole batch.
It is advised to see the `from_file` method in `PropFile` for further reference.

**Parameters**
None.

## *PropFileProfiler*
Utility Class Used to create JSON files that describe the different props formats in a single project folder (such as AG high altitude).

### Public Members
***
None.

### Methods
***
	PropFilePropfiler
Creates a new `PropFileProfiler.`

**Parameters**
None.
***

	export_file_list(output_file_name: str, import_paths: list, debug=True) -> None
This method is used to recursivly (and using multithreading) create a JSON file containing all the different props files from all given `import_paths`.

The file will be created in the path of `output_file_name`, and in the following format (pretty JSON format):

```
{
    <video_folder_path>: {
        "CMOS": [
            <the video's CMOS props files paths>
        ],
        "THERMAL": [
            <the video's THERMAL props files paths>
        ]
    },
    ...
}
```

Where  `<video_folder_path>` is a real, full, os path, and so is every video path inside the `CMOS` and `THERNAL` catagories.

**Parameters**

- `output_file_name` - The output file to write the file list into.
- `import_paths` - All the os paths to recursively search for .props files in.
- `debug` - Wether to print the file traces or not.
***
	import_file_list(all_files_db: str) -> dict
Reads a JSON file produced by `export_file_list` and retuns the data prepective to the JSON file's format described above:
```
dict{
    <video_folder_path>: dict{
        CMOS: list[
            <the video's CMOS props files paths>
        ],
        THERMAL: list[
            <the video's THERMAL props files paths>
        ]
    }
    ...
}
```

**Parameters**

- `all_files_db` - The name of the JSON file written by `export_file_list` to read data from.
***
	export_prop_files(all_files_db: str, output_file_name: str, debug=True) -> None

Creates JSON file which contains data on all the props file in `all_files_db`.
To save runtime and output size, this method exports into the JSON file only paths and formats (to be grouped later).

The output file's format is as follows:
```
{
    <video_folder_path>: {
        "CMOS": [
            <prop_file_path>: <prop_file_format>,
            <prop_file_path>: <prop_file_format>
            ...
        ],
        "THERMAL": [
            <prop_file_path>: <prop_file_format>,
            <prop_file_path>: <prop_file_format>
            ...        
        ]
    },
    ...
}
```

**Parameters**

- `all_files_db` - The JSON file produced by `export_file_list` to read all the file paths from.
- `output_file_name` - The JSON file to write data to.
- `debug` - Wether to track proggress or not.
***
	import_prop_files(prop_files_file: str) -> dict
Imports props file data from a JSON file produced by `export_prop_files`.
This method will return a dict in a format prespective to `export_prop_files`:
```
dict{
    str: dict{
        "CMOS": list[
            str: str,
            str: str
            ...
        ],
        "THERMAL": [
            str: str,
            str: str
            ...        
        ]
    },
    ...
}
```

Note this method doesn't convert data into actual `PropFile` objects.

**Parameters**

- `prop_files_file` - The JSON file to read props files data from.
***
	create_profiling_summery(prop_files: dict, output_path: str, debug=True) -> None

Used to create a JSON file containing grouped `PropFileBatch` objects data by the `str_format` of those batches.

The JSON data will be formatted as:
```
{
	<format>: {
		<batch_name>: {
			<prop_file_name>: {
				"entries": [],
				"str_format": <the prop file's str_format>
			},
			...
		},
		...
	},
	...
}
```

- `<format>` - Will be the same as described in `PropFile`'s `str_format` data member.
- `<batch_name>` - As mentioned in `PropFileBatch`, this will contain the batch's type.
- `<prop_file_name>` - Is a full path to the file.

Under every `<format>` key will be all the batches which follow that format.

**Parameters**

- `prop_files` - A dictionery produced as specified in `import_prop_files`.
- `output_path` - The JSON file to write the summery into.
- `debug` - Wether to print proggress or not.
***
	import_profiling_summery(input_path: str) -> dict
Imports data written by `create_profiling_summery` as an object oriented dictionery.
The dict's format will be:
```
dict{
	str: list[PropFileBatch],
	...
}
```

For every props format there is a list with ALL the `PropFileBatch` objects that match this format, each of those batches will contain it's prespective `PropFile`s and name (as described in `PropFileBatch`.

**Parameters**

- `input_path` - Path to a JSON file created by `create_profiling_summery`.

## *PropFileConversionData*

Data Structure Used to contain data about how to convert a single `PropFileBatch`, according to the batch's format.

The final purpose of this class is to convert source `PropFile`s into a destination `PropFile`s with an altered line format.

### Public Members
***
None.

### Methods
***
	PropFileConversionData(overlap: dict, default_overlay: dict, new_size_columns)
Used to create a new `PropFileConversionData` object to be later used to conver `PropFile` objects.

**Parameters**

- `overlap` - A dictionery specifing which index in the target format matches the source format. 
	For example this `overlap`:
	```
	{
		30: 12,
		31: 13
	}
	```
	Means the the 30th column in the target `PropFile` should contain data from the 12th column of the source `PropFile`.
	Same for the 31st target `PropFile` column, it should contain data from the 13th column in the source `PropFile`.

- `default_overlay` - Specifies the default values that should be placed into the target `PropFile`'s columns.
	A default_overlay that looks like this:
	```
	{
	    33: "720",
	    34: "576",
	    35: "2.86",
	    36: "1.1" 
	}
	```
	Means that, for example, the 33rd column of the target `PropFile` should contain the value `720` throughout the file.
	This is ment to pad `PropFile`s with retro-acquired data that needs to be added to the target `PropFile`.
- `new_size_columns` - The overall number of columns in the target `PropFile`.
	This is used to help pad the target `PropFile` with zeroes where there is missing data.
***
	get_modified_entries(prop_file: PropFile) -> list[list[str]]
This method is used to apply all the conversion data unto one `PropFile`.
It returns a modified copy of the `PropFile`'s entries so the given `PropFile` remains unchanged.

This method applies changes according to the `overlap` and `default_overlay` parameters in the `PropFileConversionData` constructor above.

**Parameters**

- `prop_file` - The prop file to modify.

## *PropFileModifier*
This is a Utility Class used to mass-modify `PropFileBatch` objects.

### Public Members
***
None.

### Methods
***
	backup(batch: PropFileBatch, dest_folder: str) -> None
Used to backup all the `PropFile`s in a `PropFileBatch`.
The backup is done by a simple file copy.

Is a `@staticmethod`.
**Parameters**

- `batch` - The batch that should be backed-up.
- `dest_folder` - The folder to write 
***
	modify_all(batch: PropFileBatch, output_folder: str, conversion_data: PropFileConversionData, replace=False, backup=True) -> None

This method modifies all the `PropFile`s  in a `PropFileBatch` according to a `PropFileConversionData`. 
The modification proccess is done with copies of the original `PropFile`s, as to avoid an error where the `PropFileConversionData`'s `overlap` or `default_overlay` are wrong, and end up ruining the `PropFile`.

After the modification has been performed succesfully, the method can be ran again with `replace=True` in order to modify the original `PropFile`s.

Is a `@staticmethod`.

**Parameters**

- `batch` - The batch to modify all of it's `PropFile`s.
- `output_folder` - The folder to write all the modified copies into.
- `conversion_data` - The `PropFileConversionData` to use for the modification proccess.
- `replace` - Wether the method should replace the original `PropFile`s with the modified `PropFiles`. 
	THIS WILL OVERRIDE THE ORIGINAL `PropFile`s! USE WITH CAUTION (EXPLOSIVE).
- `backup` - Wether to create backups of all the `PropFile`s in `batch`.
	It is recommended to use `backups=True` in the first run of the method to backups all the files in the case of a modification error.
***

## Library Usage
When using this library it is recommended to write all outputs and backups into the `OUTPUTS & BACKUPS` subfolder so all backups will be tracked and safely stored in the repo. 

For every project and movie folders a folder should be opened in `OUTPUTS & BACKUPS` to store all the JSON output and props files backups. When profiling a folder the output should be created in said folder.

When modifying a folder or project it is recommended to use seperate python files such as in the `configurations` subfolder. Each configuration file should follow the existing configuration files format.

Configuration file notes:
- It's name should specify which part of the project or folder it targets.
- The configuration file should target one or more keys in a profiling summery [^1] that all match a single format. 
- As far as formats may not appear as a match at first, but the data in the files themselves may contradict them, it is recommended to review to files after profiling and before modification. 

profiling_summery[^1] - a JSON summery generated by `PropFileProfiler.create_profiling_summery`.