from PropFile import PropFile

class PropFileBatch:
    prop_files = []
    full_path = ""
    str_format = ""

    def __init__(self, full_path, str_format=None):
        self.full_path = full_path
        self.prop_files = []
        self.str_format = str_format


    def add_prop_file(self, prop_file: PropFile):
        try:
            self.prop_files += prop_file
        except:
            self.prop_files.append(prop_file)
        

    def evaluate_str_format(self):
        count = {}
        str_format = []
        
        for file in self.prop_files:
            file_format = file.str_format
            types = file_format.split()
            
            for i in range(len(types)):
                type_ = types[i]

                if i not in count.keys():
                    count[i] = {}

                if type_ not in count[i]:
                    count[i][type_] = 0

                count[i][type_] += 1

        for column in count.values():
            if 'int' in column.keys() and 'bool' in column.keys():
                column['int'] += column['bool']
                column.pop('bool')

        for column in count.values():
            for type_ in column.keys():
                stronget_type = ''
                max_count = 0
                for type_ in column.keys():
                    if column[type_] > max_count:
                        max_count = column[type_]
                        stronget_type = type_

                str_format.append(stronget_type)

        self.str_format = ' '.join(str_format)