import cv2
import os.path
import numpy as np
import sys
from Reader import *
from Target import *
import MathUtils
from datetime import datetime



# # legal transitions between classes - BP:
# transition_table = [[1, 0, 0, 1, 0, 0, 0, 0, 1, 1], # drone
#                     [0, 1, 0, 1, 1, 0, 0, 0, 0, 1], # airplane
#                     [0, 0, 1, 1, 0, 0, 0, 0, 0, 0], # bird
#                     [1, 1, 1, 1, 1, 0, 0, 0, 1, 1], # UFO
#                     [0, 1, 0, 1, 1, 0, 0, 0, 0, 1], # airplane with light
#                     [0, 0, 0, 0, 0, 1, 0, 0, 0, 0], # balloon
#                     [0, 0, 0, 0, 0, 0, 1, 0, 0, 0], # human
#                     [0, 0, 0, 0, 0, 0, 0, 1, 0, 0], # vehicle
#                     [1, 0, 0, 1, 0, 0, 0, 0, 1, 1], # drone with light
#                     [1, 1, 0, 1, 1, 0, 0, 0, 1, 1]] # single front light 

# legal transitions between classes - Avatar:
transition_table = [[1, 0, 0, 0, 0, 0, 0, 0], # pedestrian
                    [0, 1, 0, 0, 0, 0, 0, 0], # bicycle
                    [0, 0, 1, 0, 0, 0, 0, 0], # motorcycle
                    [0, 0, 0, 1, 0, 0, 0, 0], # vehicle
                    [0, 0, 0, 0, 1, 0, 0, 0], # bus
                    [0, 0, 0, 0, 0, 1, 0, 0], # truck
                    [0, 0, 0, 0, 0, 0, 1, 0],# train
                    [0, 0, 0, 0, 0, 0, 0, 1]] # system



### Globals ###
input_path = sys.argv[1] 
gt_path = r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\GT"
output_gt_path = r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\GT_with_properties"

reader = Reader(path1=input_path)

project_suffix = "DualAir"
disable_movement_check = False
images_list = []
frame_width = reader.width1
frame_height = reader.height1
frame_size = frame_width * frame_height
NA = frame_size
distance_threshold = frame_width / 4
movement_threshold = frame_width / 10
last_used_id = 0
terminated_prev_detections = [[] for i in range(50)]
current_center_mass = (0, 0)
prev_center_mass = (0, 0)
center_mass_diff_vector = (0, 0)

change_polarity = False


def get_center_of_mass_coordinates(detection_list):
    x_center = 0
    y_center = 0
    total_mass = 0.001

    if (len(detection_list) == 0):
        for i in range(len(terminated_prev_detections)):
            if (len(terminated_prev_detections[i]) > 0):
                for j in range(len(terminated_prev_detections[i])):
                    x_center += ((terminated_prev_detections[i][j].cx) * (terminated_prev_detections[i][j].w * terminated_prev_detections[i][j].h))
                    y_center += ((terminated_prev_detections[i][j].cy) * (terminated_prev_detections[i][j].w * terminated_prev_detections[i][j].h))
                    total_mass += (terminated_prev_detections[i][j].w * terminated_prev_detections[i][j].h)
                break
    else: # there is detections in detection_list
        for i in range(len(detection_list)):
            x_center += ((detection_list[i].cx) * (detection_list[i].w * detection_list[i].h))
            y_center += ((detection_list[i].cy) * (detection_list[i].w * detection_list[i].h))
            total_mass += (detection_list[i].w * detection_list[i].h)

    x_center = x_center / total_mass
    y_center = y_center / total_mass

    return (x_center, y_center)


def is_coordinate_in_frame(coordinate):
    is_in_frame = True
    if coordinate[0] < 0 or coordinate[1] < 0 or coordinate[0] >= frame_width or coordinate[1] >= frame_height:
        is_in_frame = False
    return is_in_frame


def extract_detections(gt_folder):
    return (utils.extract_annotations(gt_folder=gt_folder, frame_width=frame_width, frame_height=frame_height)) 


def get_terminated_previous_detections(current_detections, prev_detections):
    terminated = []
    
    for i in range(len(prev_detections)):
        found = False
        for j in range(len(current_detections)):
            if prev_detections[i].id == current_detections[j].id:
                found = True
        if not found:
            terminated.append(prev_detections[i])
    
    return terminated


def get_matching_terminated_id(detection, terminated_detects, use_mass_diff):
    current_cx = detection.cx
    current_cy = detection.cy
    current_label = detection.label

    distances_mat = [0 for x in range(len(terminated_detects))]
    for i in range(len(terminated_detects)):
        prev_cx = terminated_detects[i].cx
        prev_cy = terminated_detects[i].cy
        if use_mass_diff:
            prev_cx += center_mass_diff_vector[0]
            prev_cy += center_mass_diff_vector[1]

        distances_mat[i] = MathUtils.compute_euclidean_distance((current_cx, current_cy), (prev_cx, prev_cy))

        prev_label = terminated_detects[i].label
        if (transition_table[current_label][prev_label] == 0): # not a valid class transition
            distances_mat[i] = NA
        if (distances_mat[i] > distance_threshold):
            distances_mat[i] = NA
        
    index_min = np.argmin(distances_mat)
    if distances_mat[index_min] == NA:
        return None
    else:
        continued_id = terminated_detects[index_min].id
        terminated_detects.remove(terminated_detects[index_min])
        return (continued_id)


def update_id_for_frame(current_detections, prev_detections):
    global last_used_id, current_center_mass, prev_center_mass, center_mass_diff_vector
    num_current = len(current_detections)
    num_prev = len(prev_detections)
    is_new_detection = False
    was_movement = False

    current_center_mass = get_center_of_mass_coordinates(current_detections)
    if (num_current == 0):
        current_center_mass = prev_center_mass
    elif (prev_center_mass == (0, 0)):
        prev_center_mass = current_center_mass
    center_mass_diff_vector = tuple(np.subtract(current_center_mass, prev_center_mass))

    distances_mat = [[NA for x in range(num_prev)] for y in range(num_current)]

    for i in range(num_current):
        for j in range(num_prev):
            current_cx = current_detections[i].cx
            current_cy = current_detections[i].cy
            prev_cx = prev_detections[j].cx
            prev_cy = prev_detections[j].cy
            distances_mat[i][j] = MathUtils.compute_euclidean_distance((current_cx, current_cy), (prev_cx, prev_cy))

            current_label = current_detections[i].label
            prev_label = prev_detections[j].label
            if (transition_table[current_label][prev_label] == 0): # not a valid class transition
                distances_mat[i][j] = NA
            if (distances_mat[i][j] > distance_threshold):
                distances_mat[i][j] = NA
    
    # camera movement handling
    if not disable_movement_check:
        if (num_current == num_prev and num_current > 0):
            if (np.min(distances_mat) >= movement_threshold):
                was_movement = True
                for i in range(num_current):
                    for j in range(num_prev):
                        current_cx = current_detections[i].cx 
                        current_cy = current_detections[i].cy
                        prev_cx = prev_detections[j].cx + center_mass_diff_vector[0]
                        prev_cy = prev_detections[j].cy + center_mass_diff_vector[1]
                        distances_mat[i][j] = MathUtils.compute_euclidean_distance((current_cx, current_cy), (prev_cx, prev_cy))

                        current_label = current_detections[i].label
                        prev_label = prev_detections[j].label
                        if (transition_table[current_label][prev_label] == 0): # not a valid class transition
                            distances_mat[i][j] = NA
                        if (distances_mat[i][j] > distance_threshold):
                            distances_mat[i][j] = NA

    i = 0
    # assigning IDs to objects:
    while (i < num_current):
        re_check = False
        if (len(distances_mat[i]) > 0): 
            index_min = np.argmin(distances_mat[i])
            if distances_mat[i][index_min] == NA:
                is_new_detection = True
            else: # found potential ID
                for j in range(num_current):
                    if (current_detections[j].id == prev_detections[index_min].id): # if ID is already assigned
                        if distances_mat[j][index_min] <= distances_mat[i][index_min]: # object with the same ID and shorter distance was found
                            distances_mat[i][index_min] = NA
                            re_check = True
                            break
                        else:
                            current_detections[j].id = last_used_id + 1
                            last_used_id += 1
                            break
                if not is_new_detection:
                    current_detections[i].id = prev_detections[index_min].id
        else:
            is_new_detection = True
        
        if is_new_detection:
            continued_id = None
            for j in range(len(terminated_prev_detections)):
                if (continued_id is None and len(terminated_prev_detections[j]) > 0):
                    continued_id = get_matching_terminated_id(current_detections[i], terminated_prev_detections[j], was_movement)
                    if continued_id:
                        break
           
            if continued_id:
                current_detections[i].id = continued_id
            else:
                current_detections[i].id = last_used_id + 1
                last_used_id += 1
        if not re_check:
            i += 1

    for i in range(len(terminated_prev_detections)-1, 0, -1):
        terminated_prev_detections[i] = terminated_prev_detections[i-1]
    terminated_prev_detections[0] = get_terminated_previous_detections(current_detections, prev_detections)
    prev_center_mass = current_center_mass


def print_progress(index, total):
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    if index % 100 == 0:
        print(str(index) + " out of " + str(total) + " " + current_time)


##### Main #####

_, extension = os.path.splitext(input_path)
movie_name = os.path.basename(input_path)
print(movie_name + " is in progress...")
if not os.path.isdir(input_path):
    movie_name = movie_name[0:-len(extension)]
    if gt_path.endswith("_gt"):
        gt_folder = gt_path
    else:
        gt_folder = os.path.join(gt_path, movie_name + "_gt")
    if "_r" in movie_name:
        movie_id = movie_name.split("_")[0]
    else:
        movie_id = movie_name
else: # images folder
    movie_id = movie_name
    gt_folder = os.path.join(input_path, movie_name + "_gt")
    gt_path = gt_folder
    output_gt_path = os.path.join(input_path, movie_name + "_gt_with_properties")

if os.path.isdir(gt_folder):
    if not os.path.isdir(output_gt_path):
        os.mkdir(output_gt_path)

    try:
        if reader.images_path is None: # not images folder
            if os.path.basename(output_gt_path).startswith("GT"):
                output_gt_path = os.path.join(output_gt_path, movie_name)
                if not os.path.isdir(output_gt_path):
                    os.mkdir(output_gt_path)
                else:
                    print("GT with properties already exists for this movie. exiting...")
                    exit()
    except OSError:
        print ("Creation of the directory %s failed" % output_gt_path)
        pass

    detected_objects = extract_detections(gt_folder)
else:
    print("No GT folder was found for " + movie_name + " Exiting...")
    exit()

fourcc = cv2.VideoWriter_fourcc(*'mp4v')
out = cv2.VideoWriter(os.path.join(output_gt_path, movie_name) + '.mp4', fourcc, 15, (frame_width, frame_height))

# open annotation summery file:
if reader.images_path is not None: # images folder
    summery_filename = os.path.join(os.path.dirname(input_path), movie_name + "_" + project_suffix + ".txt")
else:
    summery_filename = os.path.join(os.path.dirname(output_gt_path), movie_name + "_" + project_suffix + ".txt")
summery_ann_file = open(summery_filename, "w")

for i in range(0, reader.num_frames1):
    print_progress(index=i, total=reader.num_frames1)
    if last_used_id == 0: # first frame with objects
        for j in range(len(detected_objects[i])):
            detected_objects[i][j].id = last_used_id + 1
            last_used_id += 1
    else:
        update_id_for_frame(detected_objects[i], detected_objects[i-1])
    
    if reader.images_path is not None:
        ann_filename = os.path.join(output_gt_path, os.path.basename(reader.images_path[i])[:-len(reader.image_extension)] + ".txt")
    else:
        ann_filename = os.path.join(output_gt_path, movie_name) + "_" + str(i).zfill(6) + ".txt"
    
    ann_file = open(file=ann_filename, mode="w")

    if (len(detected_objects[i]) == 0):
        summery_ann_file.write(movie_id + " " + str(i).zfill(6) + " \n")

    frame = reader.get_frame(index=i, bgr=True)
    if change_polarity is True:
        frame = 255 - frame
    for j in range(len(detected_objects[i])):
        snr = MathUtils.calc_object_snr(frame=frame, cx=detected_objects[i][j].cx, cy=detected_objects[i][j].cy, w=detected_objects[i][j].w, h=detected_objects[i][j].h)
        detected_objects[i][j].snr = snr
        label = str(detected_objects[i][j].label)
        yolo_cx = str(float(detected_objects[i][j].cx/frame_width))
        yolo_cy = str(float(detected_objects[i][j].cy/frame_height))
        yolo_w = str(float(detected_objects[i][j].w/frame_width))
        yolo_h = str(float(detected_objects[i][j].h/frame_height))
        ID = str(detected_objects[i][j].id)

        ann_file.write(label + " " + yolo_cx + " " + yolo_cy + " " + yolo_w + " " + yolo_h + " " + ID + " " + str(snr) + " \n")
        summery_ann_file.write(movie_id + " " + str(i).zfill(6) + " " + label + " " + str(detected_objects[i][j].cx) + " " + str(detected_objects[i][j].cy) + " " + str(detected_objects[i][j].w) + " " + str(detected_objects[i][j].h) + " " + ID + " " + str(snr) + " \n")
    
    ann_file.close()

    # for debug:
    for object_index in range(len(detected_objects[i])):
        target = Target(label=detected_objects[i][object_index].label, cx=detected_objects[i][object_index].cx, cy=detected_objects[i][object_index].cy, width=detected_objects[i][object_index].w, height=detected_objects[i][object_index].h, label_set="AG")
        target.draw(frame, text="ID: "+str(detected_objects[i][object_index].id)+" SNR: "+str(detected_objects[i][object_index].snr))
    cv2.putText(frame, str(i), (15, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), thickness=1)
    # frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
    out.write(frame)


out.release()
summery_ann_file.close()



    # for debug:
#     if input_path.endswith(".mp4") or input_path.endswith(".MP4") or input_path.endswith(".avi"):
#         vidcap.set(cv2.CAP_PROP_POS_FRAMES, i)
#         success, frame = vidcap.read()
#     elif input_path.endswith(".raw2") or input_path.endswith(".rawc"):
#         frame = pyScriptsUtilities.read_frame(start=i, filename=input_path, height=frame_height, width=frame_width)
#         if extension == ".raw2":
#             frame = pyScriptsUtilities.imagesc_gs(frame)
#             frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
#         else: # ".rawc"
#             frame = cv2.cvtColor(frame, cv2.COLOR_YUV2BGR_I420)
#     for j in range(len(detected_objects[i])):
#         start_point, end_point = pyScriptsUtilities.find_start_end_point_rectangle(detected_objects[i][j].cx, detected_objects[i][j].cy, detected_objects[i][j].w, detected_objects[i][j].h)
#         cv2.rectangle(frame, start_point, end_point, (0,0,255), thickness=1)
#         cv2.putText(frame, str(detected_objects[i][j].id), (start_point[0]-10, start_point[1]-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), thickness=1)
#     cv2.putText(frame, str(i), (15, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), thickness=1)
#     if (success):
#         cv2.imshow("detections", frame)
#         cv2.waitKey(1)
#         out.write(frame)

# out.release()

# if input_path.endswith(".mp4") or input_path.endswith(".MP4") or input_path.endswith(".avi"):
#     vidcap.release()
# cv2.destroyAllWindows()






# for i in range(num_frames):
#     current_center_mass = get_center_of_mass_coordinates(detected_objects[i])
#     center_mass_diff_vector = tuple(np.subtract(current_center_mass, prev_center_mass))
#     if i == 0:
#         center_mass_diff_vector = (0, 0)

#     if i == 0: # first frame
#         for j in range(len(detected_objects[i])):
#             detected_objects[i][j].id = last_id + 1
#             last_id += 1
#     else:
#         if len(detected_objects[i]) == 0: # no object was detected
#             continue
#         elif len(detected_objects[i]) == 1: # if only 1 object was detected in the current frame
#             if len(detected_objects[i-1]) == 0: # no objects were detected in the previous frame
#                 detected_objects[i][0].id = last_id + 1 # new object, new id
#                 last_id += 1
#             elif len(detected_objects[i-1]) == 1: # if only 1 object was detected also in the previous frame
#                 if (detected_objects[i-1][0].is_threat == detected_objects[i][0].is_threat): # both are threats / both aren't threats
#                     detected_objects[i][0].id = detected_objects[i-1][0].id
#                 else:
#                     detected_objects[i][0].id = last_id + 1 # new object, new id
#                     last_id += 1
#             else: # several objects were detected in the previous frame
#                 last_id = update_detections_id(detected_objects, i, center_mass_diff_vector, last_id)
#         else: # several object were detected in the current frame:
#             last_id = update_detections_id(detected_objects, i, center_mass_diff_vector, last_id)

#     prev_center_mass = current_center_mass



