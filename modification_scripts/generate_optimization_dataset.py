import os
from shutil import copyfile
from Reader import *


input_file = "/home/lior/Desktop/dataset.txt"
input_gt_directory = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/GT"
output_path = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Panorama/AG-High-Optimization/Movies/IR_RAW"
output_gt_path = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Panorama/AG-High-Optimization/GT"


def create_split_copy(movie_path, start, stop):
    extension = '.' + movie_path.split('.')[-1]
    if extension == ".raw2":
        width, height = (640, 480)
    else:
        width, height = (720, 576)

    reader = Reader(path1=movie_path, width1=width, height1=height)

    new_movie_path = os.path.join(output_path, os.path.basename(movie_path))
    old_props_path = movie_path.replace(extension, ".props")
    if not os.path.exists(old_props_path):
        old_props_path = old_props_path.replace("r2", "r0").replace("r1", "r0")
    new_props_path = os.path.join(output_path, os.path.basename(old_props_path))

    with open(new_movie_path, "wb") as out_file:
        if stop is None:
            stop = reader.num_frames1
        for i in range(start, stop):
            frame = reader.get_frame(index=i, bgr=False)
            out_file.write(frame)

    with open(old_props_path, "r") as in_props_file, open(new_props_path, "w") as out_props_file:
        for i in range(stop):
            line = in_props_file.readline()
            if i >= start:
                out_props_file.write(line)


def create_gt_split_copy(gt_directory, start, stop):
    if os.path.exists(gt_directory):
        new_gt_path = os.path.join(output_gt_path, os.path.basename(gt_directory))
        try:
            os.mkdir(new_gt_path)
        except:
            pass

        gt_files = os.listdir(gt_directory)
        gt_files.sort()

        if stop is None:
            for file in gt_files:
                copyfile(os.path.join(gt_directory, file), os.path.join(new_gt_path, file))
        else:
            for i in range(start, stop):
                copyfile(os.path.join(gt_directory, gt_files[i]), os.path.join(new_gt_path, gt_files[i]).replace(str(i).zfill(6), str(i-start).zfill(6)))


### Main: ###
with open(input_file, "r") as f:
    line = f.readline()
    while(line):
        print("spliting: " + line)
        movie_path = line.replace('\n', '')
        start = int(f.readline())
        stop = int(f.readline())

        if start < 0 or stop < 0:
            start = 0
            stop = None

        gt_directory = os.path.join(input_gt_directory, os.path.basename(movie_path).split('.')[0]) + '_gt'
        
        create_split_copy(movie_path, start, stop)
        create_gt_split_copy(gt_directory, start, stop)

        line = f.readline()

