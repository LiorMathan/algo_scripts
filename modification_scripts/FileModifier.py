import os
import shutil
from Reader import *


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def add_movie_id():    
    def extract_id_number(elem):
        if '_' in elem:
            return(int(elem.split('_')[1].split('.')[0]))
        else:
            return None
    dirname = input("Please enter directory path: ")
    start_idx = int(input("Index to start from: "))

    files = os.listdir(dirname)
    files.sort(key=extract_id_number)
    prev_idx = int(files[0].split('_')[1].split('.')[0])

    for file in files:
        current_idx = int(file.split('_')[1].split('.')[0])
        if current_idx != prev_idx:
            start_idx += 1
        old_name = os.path.join(dirname, file)
        new_name = os.path.join(dirname, str(start_idx).zfill(5) + '_' + file)
        os.rename(old_name, new_name)
        prev_idx = current_idx


def remove_movie_id():
    dirname = input("Please enter directory path: ")

    for file in os.listdir(dirname):
        if not os.path.isdir(os.path.join(dirname, file)):
            movie_id = file.split('_')[0]
            old_name = os.path.join(dirname, file)
            new_name = os.path.join(dirname, file.replace(movie_id+'_', ''))
            os.rename(old_name, new_name)


def replace_filenames():
    dirname = input("Please enter directory path: ")
    src_str = input("Source substring: ")
    dst_str = input("Destination substring: ") 
    recursively = input("Recursively? (type y/n): ")
    if recursively == 'y':
        recursively = True
    else:
        recursively = False

    files = []
    if recursively:
        print("Please wait - finding all files recursively...")
        for root, directories, filenames in os.walk(dirname):
            for directory in directories: 
                local_files = os.listdir(os.path.join(root, directory))
                for file in local_files:
                    if src_str in file:
                        files.append(os.path.join(root, directory, file))
            for filename in filenames:  
                if src_str in filename:
                    files.append(os.path.join(root, filename))
    else:
        for file in os.listdir(dirname):
            files.append(os.path.join(dirname,file))
    
    print("Please wait - modifying all files...")
    for file in files:
        old_name = file
        new_name = file.replace(src_str, dst_str)
        try:
            os.rename(old_name, new_name)
        except:
            print("Failed to rename: " + file)


def convert_to_mp4():
    dirname = input("Please enter directory path: ")
    movie_format = input("Please enter movie extension: ")
    width = int(input("Please enter width: "))
    height = int(input("Please enter height: "))

    for file in os.listdir(dirname):
        _, extension = os.path.splitext(file)
        if extension in movie_format:
            if os.path.exists(os.path.join(dirname, file.replace(extension, "_encoded.mp4"))):
                continue
            print("writing " + file)
            reader = Reader(path1=os.path.join(dirname, file), width1=width, height1=height, encode=True, output=dirname)
            reader.display()


def unite_movies():
    dir_path    = input("Please enter directory path: ")
    extension   = input("Please enter movie extension: ")
    with_props  = input("Create a full props file? Please enter y/n: ")

    if (with_props == 'y' or with_props == 'Y'):
        with_props = True
    else:
        with_props = False

    movie_basename = os.path.basename(dir_path) + extension
    movie_path = os.path.join(dir_path, movie_basename)
    full_movie = open(movie_path, "wb")
    if with_props is True:
        props_basename = movie_basename.replace(extension, '.props')
        props_path = os.path.join(dir_path, props_basename)
        full_props = open(props_path, "w")

    files = os.listdir(dir_path)
    files.sort()

    for file in files:
        if file.endswith(extension) and file not in movie_basename:
            print("Adding " + file)
            current_movie = os.path.join(dir_path, file)
            reader = Reader(path1=current_movie)
            for i in range(reader.num_frames1):
                frame = reader.get_frame(index=i)
                full_movie.write(frame)
        
            if with_props is True:
                current_props = current_movie.replace(extension, '.props')
                current_props = current_props.replace('r2', 'r0')
                current_props = current_props.replace('r1', 'r0')
                with open(current_props, "r") as f:
                    props_lines = f.readlines()
                full_props.writelines(props_lines)

    full_movie.close()
    if with_props is True:
        full_props.close()


def unite_gt_folders():
    def extract_id_number(elem):
        if '_' in elem:
            return(int(os.path.basename(elem).split('_')[0]))
        else:
            return None

    dir_path     = input("Please enter external directory path: ")
    start_index  = int(input("Start index: "))
    stop_index   = int(input("Stop index: "))
    dual_param   = 'r' + input("Please enter 0/1/2 for 00000_rX_00 parameter: ")
    output_path  = input("Please enter full output directory: ")
    basename = os.path.basename(output_path)[:-len('_gt')]
    
    try:
        os.mkdir(output_path)
    except:
        pass

    input_dirs = [None] * (stop_index - start_index + 1)
    for dir in os.listdir(dir_path):
        try:
            dir_id = int(dir.split('_')[0])
            if dir_id >= start_index and dir_id <= stop_index and dual_param in dir:
                input_dirs[dir_id-start_index] = os.path.join(dir_path, dir)
        except:
            continue
    # input_dirs.sort(key=extract_id_number)

    # if len(input_dirs) < (stop_index - start_index + 1):
    #     print("Missing GT folders!!! Exiting...")
    #     exit(1)

    counter = 0
    for dir in input_dirs:
        if dir is None:
            for i in range(1000):
                dest_filename = os.path.join(output_path, basename + '_' + str(counter).zfill(6) + '.txt')
                empty_gt = open(dest_filename, 'w')
                empty_gt.close()
                counter += 1
        else:
            gt_files = os.listdir(dir)
            gt_files.sort()
            for gt_file in gt_files:
                dest_filename = os.path.join(output_path, basename + '_' + str(counter).zfill(6) + '.txt')
                shutil.copy(os.path.join(dir, gt_file), dest_filename)
                counter += 1


def display_menu():
    menu = {}
    menu['1'] = "Add movie ID for files in folder"
    menu['2'] = "Remove movie ID for files in folder"
    menu['3'] = "Change filenames of files in directory"
    menu['4'] = "Create mp4 for movies in folder"
    menu['5'] = "Unite movies in directory to one full movie"
    menu['6'] = "Unite GT folders"

    menu['7'] = "Exit"

    while True:
        cls()

        print('======== System Menu ========')
        options = menu.keys()
        for entry in options:
            print(entry, menu[entry])

        selection = input("Please Select: ")

        if selection == '1':
            add_movie_id()
        elif selection == '2':
            remove_movie_id()
        elif selection == '3':
            replace_filenames()
        elif selection == '4':
            convert_to_mp4()
        elif selection == '5':
            unite_movies()
        elif selection == '6':
            unite_gt_folders()
        else:
            exit(1)

display_menu()
