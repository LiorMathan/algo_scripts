from cv2 import cv2
import numpy as np
import csv
import os
import matplotlib.pyplot as plt
from sklearn import metrics
from Reader import *
from Target import *
import evaluation_functions

thresh_bins = np.arange(0, 101, 5)


class MatchedGT:
    def __init__(self, target):
        self.target = target
        self.matches = []
        self.max_certainty_of_matches = 0
        self.classification_dict = {n: "FN" for n in thresh_bins}


    def get_max_certainty_of_matches(self):
        max_certainty = 0

        for match in self.matches:
            if match.confidence > max_certainty:
                max_certainty = match.confidence

        self.max_certainty_of_matches = max_certainty
        self.update_classification_dict()

        return max_certainty


    def update_classification_dict(self):
        for certainty in self.classification_dict.keys():
            if certainty <= self.max_certainty_of_matches > 0:
                self.classification_dict[certainty] = "TP"
            else:
                self.classification_dict[certainty] = "FN"
    

class UnmatchedDetection:
    def __init__(self, target):
        self.target = target
        self.classification_dict = {n: "-" for n in thresh_bins}
        
        for k in self.classification_dict.keys():
            if k < target.confidence:
                self.classification_dict[k] = "FP"


class ROC_AUC_Tool:
    def __init__(self, detections_path: str, label_set: str, nms_method: callable, threshold: float, debug_display: bool, \
                 input_path: str, gt_path=None, width=None, height=None, interest_labels=None):
        self.detections_path    = detections_path
        self.gt_path            = gt_path
        self.input_path         = input_path
        self.label_set          = label_set
        self.debug_display      = debug_display
        self.nms_method         = nms_method
        self.threshold          = threshold
        self.interest_labels    = interest_labels
        self.per_class          = True
        self.crops_list         = None

        if input_path.endswith('.txt'):
            self.extract_crops_paths()
            self.reader = None
        else:
            self.reader         = Reader(path1=self.input_path, width1=width, height1=height)
            self.num_frames     = self.reader.num_frames1
            self.width          = self.reader.width1
            self.height         = self.reader.height1

        annotations_path        = input_path if self.crops_list else self.gt_path
        self.annotations_list   = self.get_targets_list(targets_path=annotations_path, is_yolo_format=True)
        self.detections_list    = self.get_targets_list(targets_path=detections_path, is_yolo_format=False)        


    def extract_crops_paths(self):
        with open(self.input_path, "r") as f:
            self.crops_list = f.read().splitlines()
        
        test_crop = cv2.imread(self.crops_list[0])
        self.height, self.width = test_crop.shape[:2]

        self.num_frames = len(self.crops_list)


    def get_targets_list(self, targets_path, is_yolo_format):
        if self.crops_list and is_yolo_format:
            with open(targets_path, "r") as f:
                files_list = f.read().splitlines()

            targets_list = [[] for i in range(self.num_frames)]
            for i, file in enumerate(files_list):
                with open(file.replace('.png', '.txt'), 'r') as f:
                    lines = f.readlines()
                    for line in lines:
                        self.append_new_target(targets_list, target_line=line, index=i, is_yolo_format=is_yolo_format)

        elif os.path.isdir(targets_path):
            files_list = []
            
            for file in os.listdir(targets_path):
                if file.endswith('.txt'):
                    files_list.append(os.path.join(targets_path, file))
            files_list.sort()         
            
            targets_list = [[] for i in range(len(files_list))]
            for i, file in enumerate(files_list):
                with open(file, 'r') as f:
                    lines = f.readlines()
                    for line in lines:
                        self.append_new_target(targets_list, target_line=line, index=i, is_yolo_format=is_yolo_format)

            self.num_frames = len(files_list)

        else:
            targets_list = [[] for i in range(self.num_frames)]
            with open(targets_path, 'r') as f:
                lines = f.readlines()
                for i, line in enumerate(lines):
                    line_tokens = line.split()
                    try:
                        index = int(line_tokens[0])
                    except:
                        index = i
                    self.append_new_target(targets_list, target_line=line, index=index, is_yolo_format=is_yolo_format)

        return targets_list


    def append_new_target(self, targets_list, target_line, index, is_yolo_format):
        if is_yolo_format:
            new_target = Target.init_from_yolo_line(target_line, frame_width=self.width, frame_height=self.height, label_set=self.label_set)
        else:
            new_target = Target.init_from_detection_line(target_line, label_set=self.label_set)
        
        if not new_target.is_center_in_frame(frame_width=self.width, frame_height=self.height):
            return

        if self.interest_labels is not None:
            if new_target.label in self.interest_labels:
                targets_list[index].append(new_target)
        else:
            targets_list[index].append(new_target)


    def evaluate_detections(self):
        matched_gts             = [[] for i in range(self.num_frames)]
        unmatched_detections    = [[] for i in range(self.num_frames)]
        
        for i in range(self.num_frames):
            combined_list  = self.annotations_list[i] + self.detections_list[i]
            razor_list     = self.create_razor_list(combined_list)
            
            merged_into_index_vector = evaluation_functions.fuse_lists(dual_list=combined_list, threshold=self.threshold, \
                                                                       razor=razor_list, method=self.nms_method, partition=len(self.annotations_list[i]))
            for j in range(len(self.annotations_list[i])):
                matched_gts[i].append(MatchedGT(target=self.annotations_list[i][j]))
            for j in range(len(self.annotations_list[i]), len(combined_list)):
                if np.isnan(merged_into_index_vector[j]):
                    unmatched_detections[i].append(UnmatchedDetection(target=combined_list[j]))
                else:
                    if self.per_class:
                        if matched_gts[i][int(merged_into_index_vector[j])].target.get_label() == combined_list[j].get_label():
                            matched_gts[i][int(merged_into_index_vector[j])].matches.append(combined_list[j])
                        else:
                            unmatched_detections[i].append(UnmatchedDetection(target=combined_list[j]))    
                    else:
                        matched_gts[i][int(merged_into_index_vector[j])].matches.append(combined_list[j])

            if self.debug_display:
                self.display(index=i, annotations=matched_gts[i], unmatched_detections=unmatched_detections[i])
                

        self.matched_gts = matched_gts
        self.unmatched_detections = unmatched_detections
        self.create_evaluation_graphs(matched_gts, unmatched_detections)

      
    def display(self, index, annotations, unmatched_detections):
        frame = None

        if self.reader:
            frame = self.reader.get_frame(index=index, bgr=True)
        elif self.crops_list:
            frame = cv2.imread(self.crops_list[index])

        if frame is not None:
            for gt in self.annotations_list[index]:
                gt.draw(frame, color=(147,112,220), show_label=True) # GT - PINK
            for det in self.detections_list[index]:
                det.draw(frame, color=(255, 0, 0), show_label=True) # DETECTION - BLUE
            for det in unmatched_detections:
                det.target.draw(frame, color=(0,0,255), show_label=True) # UNMATCHED DETECTION - RED
            for gt in annotations:
                if len(gt.matches) > 0:
                    gt.target.draw(frame, color=(0,255,0), show_label=True) # MATCHED GT - GREEN

            cv2.imshow('debug frame', frame)
            cv2.waitKey(10)


    def create_razor_list(self, targets_list):
        razor_list = []

        for target in targets_list:
            razor_list.append(target.confidence)
        
        return razor_list


    def get_auc(self):
        return self.auc_value
           

    def produce_graphs(self, recall_vector, precision_vector):
        _, axs = plt.subplots(3, constrained_layout=True)
        axs[0].plot(thresh_bins, recall_vector, linestyle='solid', label='Recall', color='g')
        axs[0].set_title('Recall')
        axs[0].set_xlabel('certainty')
        axs[0].set_ylabel('recall')
        axs[0].set_yticks(np.arange(start=min(recall_vector), stop=max(recall_vector), step=0.2)) 
        axs[1].plot(thresh_bins, precision_vector, linestyle='solid', label='Precision', color='b')
        axs[1].set_title('Precision')
        axs[1].set_xlabel('certainty')
        axs[1].set_ylabel('precision')
        axs[1].set_yticks(np.arange(start=min(precision_vector), stop=max(precision_vector), step=0.2)) 
        axs[2].plot(recall_vector, precision_vector, linestyle='solid', label='Recall / Precision', color='m')
        axs[2].set_title('Precision / Recall | AUC = ' + "{:.4f}".format(self.auc_value))
        axs[2].set_xlabel('recall')
        axs[2].set_ylabel('precision')
        axs[2].set_xticks(np.arange(start=0, stop=1.2, step=0.2)) 

        #fig.tight_layout()
        if self.input_path:
            filename = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'outs', 'Evaluation_graphs_' + os.path.basename(self.input_path).split('.')[0] + '.png')
        else:
            filename = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'outs', 'Evaluation_graphs_' + os.path.basename(self.detections_path).split('.')[0] + '.png')
        
        plt.suptitle(os.path.basename(self.detections_path).split('.')[0], fontsize=12)
        plt.savefig(filename)
        
        if self.debug_display:
            plt.show()


    def create_evaluation_graphs(self, matched_gts, unmatched_detections):
        TP_counter          = {k:0 for k in thresh_bins}

        recall_vector       = self.create_recall_graph(matched_gts, TP_counter, thresh_bins)
        precision_vector    = self.create_precision_graph(unmatched_detections, TP_counter, thresh_bins)

        self.auc_value = metrics.auc(recall_vector, precision_vector)
         
        print("AUC Value is: " + str(self.auc_value))
        
        self.produce_graphs(recall_vector, precision_vector)


    def create_recall_graph(self, matched_gts, TP_counter, thresh_bins):
        file_basename   = os.path.basename(self.input_path).split('.')[0]
        filename        = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'outs', 'Recall_' + file_basename + '.csv')

        recall_file     = open(filename, 'w')
        recall_writer   = csv.writer(recall_file)

        recall_header = ['Frame', 'GT#', 'Max certainty of matches'] + ['Certainty ' + str(x) + '%' for x in thresh_bins]
        recall_writer.writerow(recall_header)

        FN_counter = {k:0 for k in thresh_bins}

        for i in range(len(matched_gts)):
            for j, matched_gt in enumerate(matched_gts[i]):
                recall_writer.writerow([i, j, matched_gt.get_max_certainty_of_matches()] + [matched_gt.classification_dict[x] for x in thresh_bins])
                
                for certainty in thresh_bins:
                    if matched_gt.classification_dict[certainty] is "TP":
                        TP_counter[certainty] += 1
                    else:
                        FN_counter[certainty] += 1
        
        recall_writer.writerow(['Total TP', '', ''] + [TP_counter[k] for k in thresh_bins])        
        recall_writer.writerow(['Total FN', '', ''] + [FN_counter[k] for k in thresh_bins])        

        recall_vector = [(TP_counter[k] / (TP_counter[k] + FN_counter[k])) for k in thresh_bins]
        recall_writer.writerow(['Recall', '', ''] + recall_vector)

        recall_file.close()

        return recall_vector


    def create_precision_graph(self, unmatched_detections, TP_counter, thresh_bins):
        file_basename   = os.path.basename(self.input_path).split('.')[0]
        precision_file  = open(os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'outs', 'Precision_' + file_basename + '.csv'), 'w')

        precision_writer    = csv.writer(precision_file)

        precision_header = ['Frame', 'Detection#', 'Certainty'] + ['Certainty ' + str(x) + '%' for x in thresh_bins]
        precision_writer.writerow(precision_header)

        FP_counter = {k:0 for k in thresh_bins}

        for i in range(len(unmatched_detections)):
            for j, unmatched_detection in enumerate(unmatched_detections[i]):
                precision_writer.writerow([i, j, unmatched_detection.target.confidence] + [unmatched_detection.classification_dict[x] for x in thresh_bins])
                
                for certainty in thresh_bins:
                    if unmatched_detection.classification_dict[certainty] is "FP":
                        FP_counter[certainty] += 1

        precision_writer.writerow(['Total FP', '', ''] + [FP_counter[k] for k in thresh_bins])        
        precision_writer.writerow(['Total TP', '', ''] + [TP_counter[k] for k in thresh_bins])  

        precision_vector = [self.calc_precision(TP_counter[k], FP_counter[k]) for k in thresh_bins]
        precision_writer.writerow(['Precision', '', ''] + precision_vector)

        precision_file.close()

        return precision_vector

    @staticmethod
    def calc_precision(TP_counter, FP_counter):
        if (TP_counter + FP_counter) > 0:
            return (TP_counter / (TP_counter + FP_counter))
        else:
            return 1



if __name__ == '__main__':
    input_path = ""
    detections = ""
    annotations = None

    benchmark_tool = ROC_AUC_Tool(input_path=input_path, detections_path=detections, gt_path=annotations, label_set='GG', nms_method=evaluation_functions.iom, threshold=0.5, debug_display=True)
    benchmark_tool.evaluate_detections()
    print("finish")
    