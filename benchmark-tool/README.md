# Benchmark Tool

[Tool specification](https://docs.google.com/presentation/d/1vH5JSSoAQv29ZDRcK0EE-_2BWEbwqdkHunNjXPsk20E/edit#slide=id.g797ae8d882_0_0)

**How to run the tool:**  
Proceed to Main function, and fill up the following paths:  
  
*movie = "path/to/movie/or/crops/directory"*  
*detections = 'path/to/detections/file'*  
*annotations = 'path/to/gt/directory'*  
  
*benchmark_tool = BenchmarkTool(input_path=movie, detections_path=detections, gt_path=annotations, label_set='AG', nms_method="iom", threshold=0.5, debug_display=True)*  
*benchmark_tool.evaluate_detections()*  
*print("finish")*  
  
If you would like to evaluate a crops list - input_path will recieve the file path, gt_path is not necessary.  
  
Initialization might take few seconds.  
  
  
**Outputs:**  
Will be found at /outs directory.  
The tool produces 3 files:  
1. Precision csv file  
2. Recall csv file  
3. Evaluation graphs  
  
  
**Debug mode:**  
For debug display - activate the debug_display flag at initialization.  
When activated, the following overlays will be displayd:  
  
- ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) `FP`
- ![#1589F0](https://via.placeholder.com/15/1589F0/000000?text=+) `TP (Detection)`
- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) `TP (GT)`
- ![#FFC0CB](https://via.placeholder.com/15/FFC0CB/000000?text=+) `FN`
  