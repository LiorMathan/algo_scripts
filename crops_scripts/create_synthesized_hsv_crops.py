import numpy as np
import cv2
import os
from Reader import *
from tkinter import messagebox
import pyScriptsUtilities as utils
import sys
import pandas as pd
import random
from skimage.util import random_noise
from datetime import datetime


debug_flag = False
use_inclusive_tiles = True
PEDESTRIAN_LABEL = 0
INCLUSIVE_TILE_EDGE_WIDTH = 12
MIN_PEDESTRIAN_AREA = 5 * 12
wait_time = 20
raw_crops = False #utils.convertBoolToStr(sys.argv[12])


class CropsTool:
    def __init__(self, input_path, gt_path, output_path, crop_width, crop_height, 
                 frame_width=None, frame_height=None, project_prefix=None, is_old_chimera=False, change_polarity=False, labels_conversion_file=None):
        self.input_path = input_path
        self.gt_path = gt_path
        self.output_path = output_path
        self.crop_width = crop_width
        self.crop_height = crop_height
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.change_polarity = change_polarity
        self.is_old_chimera = is_old_chimera
        self.min_objects_in_tile = 1
        self.max_objects_in_tile = 100
        self.min_pedestrian_area = 5 * 12
        self.inclusive_tile_edge_width = 12
        self.project_prefix = project_prefix
        _,self.extension = os.path.splitext(self.input_path)
        self.basename = os.path.basename(self.input_path)
        if len(self.extension) > 0:
            self.basename = os.path.basename(self.input_path)[:-len(self.extension)]
        if self.project_prefix is not None:
            self.basename = self.project_prefix + "_" + self.basename
        self.output_path = os.path.join(self.output_path, self.basename)
        
        self.reader = Reader(path1=self.input_path, width1=self.frame_width, height1=self.frame_height)


    def validate_input(self):
        if not os.path.exists(self.input_path):
            print("Input Path Does Not Exists!")
            exit()
        if not os.path.exists(self.gt_path):
            print("GT Path Does Not Exists!")
            exit()
        

    def get_table_from_file(self, filename, dtype='i', delimiter=' '):
        if filename is None or not os.path.exists(filename):
            table = None
        else:
            table = pd.read_csv(filename, dtype=dtype, sep=delimiter, header=None)
            
            if 'numpy.float32' in str(type(table[0])):
                table = [table]
            table = np.array(table)

        return table
    

    def get_updated_label(self, label):
        if self.is_old_chimera is True:
            if label == 6: # human
                label = 0
            elif label == 7: # vehicle
                label = 3
        
        return label

    
    def get_included_annotations(self, annotations, tile):
        # tile is [left, top, width, height]
        # annotations is a table in which contains cells in format: [label, cx, cy, width, height]
        included = []

        for i in range(len(annotations)):
            try:
                if use_inclusive_tiles is True:
                    # include only objects that have at least <INCLUSIVE_TILE_EDGE_WIDTH> pixels inside the crop:
                    if (annotations[i].cx + (annotations[i].w / 2) < tile[0] + INCLUSIVE_TILE_EDGE_WIDTH):
                        continue
                    if (annotations[i].cy + (annotations[i].h / 2) < tile[1] + INCLUSIVE_TILE_EDGE_WIDTH):
                        continue
                    if (annotations[i].cx - (annotations[i].w / 2) > tile[0] + tile[2] - INCLUSIVE_TILE_EDGE_WIDTH):
                        continue
                    if (annotations[i].cy - (annotations[i].h / 2) > tile[1] + tile[3] - INCLUSIVE_TILE_EDGE_WIDTH):
                        continue
                else:
                    # include only objects that their center is inside the crop:
                    cx = annotations[i].cx
                    cy = annotations[i].cy
                    if (cx < tile[0] or cx > tile[0] + tile[2]):
                        continue
                    if (cy < tile[1] or cy > tile[1] + tile[3]):
                        continue
            except Exception as e:
                print(e)
                continue

            # update annotations to new tile:
            start_x = max(tile[0], annotations[i].cx - (annotations[i].w/2))
            start_y = max(tile[1], annotations[i].cy - (annotations[i].h/2))
            end_x = min(tile[0] + tile[2], annotations[i].cx + (annotations[i].w/2))
            end_y = min(tile[1] + tile[3], annotations[i].cy + (annotations[i].h/2))
            updated_width = end_x - start_x
            updated_height = end_y - start_y
            
            yolo_cx = (start_x - tile[0] + (updated_width / 2)) / tile[2]
            yolo_cy = (start_y - tile[1] + (updated_height / 2)) / tile[3]
            yolo_w = updated_width / tile[2]
            yolo_h = updated_height / tile[3]
            label = annotations[i].label

            included_annotation = utils.Annotation(label=label, cx=yolo_cx, cy=yolo_cy, w=yolo_w, h=yolo_h)
            included.append(included_annotation)

        return (included)


    def print_progress(self, index):
        if index % 100 == 0:
            print(str(index) + ' out of ' + str(self.reader.num_frames1))


    def write_crop(self, h_channel, s_channel, v_channel, filename, included_annotations):
        success = 1

        fused_crop = np.array([h_channel, s_channel, v_channel])
        fused_crop = np.transpose(fused_crop, axes=(1, 2, 0))
        cv2.imwrite(filename, fused_crop, [cv2.IMWRITE_PNG_COMPRESSION, 0])
        # delete crop if writing had failed:
        check_not_none = cv2.imread(filename)
        if check_not_none is None:
            print("Bad crop: " + filename)
            os.remove(filename)
            success = -1
        
        if debug_flag is True:
            self.show_labaled_crop(crop=fused_crop, crop_filename=filename, included_annotations=included_annotations)

        return success


    def show_labaled_crop(self, included_annotations, crop, crop_filename=None):
        if debug_flag is True:
            debug_crop = crop.copy()

            if crop_filename is not None:
                original_crop = cv2.imread(crop_filename)
            for obj in range(len(included_annotations)):
                (start, end) = utils.find_start_end_point_rectangle(cx=included_annotations[obj].cx*self.crop_width, cy=included_annotations[obj].cy*self.crop_height, width=included_annotations[obj].w*self.crop_width, height=included_annotations[obj].h*self.crop_height)
                debug_crop = cv2.rectangle(img=debug_crop, pt1=start, pt2=end, color=(0, 255, 0), thickness=1)
                if crop_filename is not None:
                    original_crop = cv2.rectangle(img=original_crop, pt1=start, pt2=end, color=(255, 255, 255), thickness=1)
                    cv2.putText(img=original_crop, text=str(included_annotations[obj].label), org=(start[0], start[1]-5), fontFace=cv2.FONT_HERSHEY_COMPLEX, color=(255, 255, 255), thickness=1, fontScale=0.3)
                cv2.putText(img=debug_crop, text=str(included_annotations[obj].label), org=(start[0], start[1]-5), fontFace=cv2.FONT_HERSHEY_COMPLEX, color=(0, 255, 0), thickness=1, fontScale=0.3)
            cv2.imshow("Objects on crop", debug_crop)
            if crop_filename is not None:
                cv2.imshow("Original crop", original_crop)
            cv2.waitKey(wait_time)


    def read_frame(self, index):
        frame = self.reader.get_frame(index=index, bgr=True)

        if frame is None:
            raise ValueError("frame is None. index: " + str(index))
        if (frame.shape[0] < self.crop_height or frame.shape[1] < self.crop_width):
            raise ValueError("frame's shape is smaller than crop size. index: " + str(index))

        return frame


    def create_shifted_crop(self, frame):
        random_y_shift = random.randint(0, self.reader.height1 - self.crop_height)
        random_x_shift = random.randint(0, self.reader.width1 - self.crop_width)
        y_end = random_y_shift + self.crop_height
        x_end = random_x_shift + self.crop_width

        crop = frame[random_y_shift : y_end, random_x_shift : x_end]

        if debug_flag is True:
            debug_frame = frame.copy()
            debug_frame = cv2.cvtColor(debug_frame, cv2.COLOR_GRAY2BGR)
            debug_frame = cv2.rectangle(img=debug_frame, pt1=(random_x_shift, random_y_shift), pt2=(x_end, y_end), color=(255, 255, 255), thickness=1)         
            cv2.imshow("frame", debug_frame)
            cv2.waitKey(wait_time)

        return crop


    def corrupt_image(self, frame):
        noised = random_noise(frame, mode='gaussian', seed=None, clip=True)
        # noised = random_noise(frame, mode='s&p', seed=None, clip=True)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(20,20)) / 400
        # kernel = np.ones((20,20),np.float32)/400
        smoothed = cv2.filter2D(noised, -1, kernel)

        smoothed = cv2.normalize(smoothed, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

        if debug_flag is True:
            cv2.imshow("noised", noised)
            cv2.imshow("blurred", smoothed)
            cv2.waitKey(wait_time)

        return smoothed


    def crop_frame(self, index, ann_summary_file):
        try:
            frame = self.read_frame(index=index)
        except:
            return
        
        gt_filename = self.reader.images_path[index].replace(".png", ".txt")
        annotations = utils.extract_detections_from_gt_file(gt_filename=gt_filename, frame_width=self.reader.width1, frame_height=self.reader.height1)

        hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        h_channel = hsv_frame[:,:,0]
        s_channel = hsv_frame[:,:,1]
        v_channel = hsv_frame[:,:,2]

        tile = [int((self.reader.width1/2) - (self.crop_width/2)), int((self.reader.height1/2) - (self.crop_height/2)), self.crop_width, self.crop_height]
        included_annotations = self.get_included_annotations(annotations=annotations, tile=tile)

        if debug_flag is True:
            debug_frame = frame.copy()
            debug_crop = debug_frame[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2], :]
            self.show_labaled_crop(included_annotations=included_annotations, crop=debug_crop)

        if len(included_annotations) == 0:
            return

        v_channel = self.corrupt_image(v_channel)
        v_crop = v_channel[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2]]
        h_crop = self.create_shifted_crop(frame=h_channel)
        s_crop = self.create_shifted_crop(frame=s_channel)

        filename = os.path.join(self.output_path, self.basename + "_" + str(index).zfill(6) + ".png")
        new_gt_filename = filename.replace(".png", ".txt")
        success = self.write_crop(h_crop, s_crop, v_crop, filename, included_annotations)

        if (success != -1):
            with open(new_gt_filename, "w") as f:
                for ann in range(len(included_annotations)):
                    f.write(str(included_annotations[ann].label) + ' ' + str(included_annotations[ann].cx) + ' ' + str(included_annotations[ann].cy) + ' ' + str(included_annotations[ann].w) + ' ' + str(included_annotations[ann].h) + ' \n')
                    ann_summary_file.write(str(index).zfill(6)+ ' ' + "1" + ' ' + str(included_annotations[ann].label) + ' ' + str(included_annotations[ann].cx) + ' ' + str(included_annotations[ann].cy) + ' ' + str(included_annotations[ann].w) + ' ' + str(included_annotations[ann].h) + ' \n')


    def create_crops(self):
        self.validate_input()
        try:
            os.mkdir(self.output_path)
        except:
            pass
        
        ann_summary_file = open(os.path.join(os.path.dirname(self.output_path), "CropsMovie_" + self.basename + "_annotationSummery.txt"), "w+")

        num_frames = self.reader.num_frames1
        for i in range(num_frames):
            if i % 10 == 0:
                print(str(i) + " out of " + str(num_frames))
                now = datetime.now()
                current_time = now.strftime("%H:%M:%S")
                print("Current Time =", current_time)
            self.crop_frame(index=i, ann_summary_file=ann_summary_file)


if __name__ == '__main__':
    cropper = CropsTool(input_path=r"Z:\OfflineDB\SSDMoblieNet_AG\DualAir_MultiObjects\CMOS\Low_Altitude\DualAir_00035_r2_5",
                        gt_path=r"Z:\OfflineDB\SSDMoblieNet_AG\DualAir_MultiObjects\CMOS\Low_Altitude\DualAir_00035_r2_5",
                        output_path=r"Z:\OfflineDB\Corrupted_Tiles\HSV_synthesized\DualAir_230\CMOS",
                        crop_width=230,
                        crop_height=230,
                        project_prefix=None,
                        is_old_chimera=False,
                        change_polarity=False)
    
    print(cropper.basename + " is being processed...")
    cropper.create_crops()
    
