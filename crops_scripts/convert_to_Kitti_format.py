import cv2
import os.path
import numpy as np
import kitti_format_utillities as kitti

# #### ArgParser: ####
# parser = argparse.ArgumentParser(description='Receives arguments from user to configure the script')
# parser.add_argument('-p', '--path', type=str, metavar='', required=True, help='Crops folder path')
# parser.add_argument('-r', '--resolution', type=int, metavar='', required=True, help='Crops width/height')
# args = parser.parse_args()

external_folder = "/home/ironman/workspace/share/storage_server/OfflineDB/SSDMoblieNet_AG/3ChannelChimera_300"
input_folder = "Z:\OfflineDB\SSDMoblieNet_AG\\3ChannelChimera_300\Chimera_5490_r0_3" #args.path
output_folder = "/home/ironman/workspace/share/storage_server/OfflineDB/SSDMoblieNet_AG/3ChannelChimera_300_Kitti_Format"
crop_width = 300 #args.resolution
crop_height = 300 #args.resolution


def convert_to_Kitti_format(input_dir):
    foldername = os.path.basename(input_dir)  
    out_path = os.path.join(output_folder, foldername)
    if not os.path.isdir(out_path):
        os.mkdir(out_path)
    annotation_summery_filename = os.path.join(output_folder, foldername+"_annotationSummery.txt")
    ann_summery = open(annotation_summery_filename, "w")

    i = 0
    for file in os.listdir(input_dir):
        if file.endswith(".txt"):
            basename = file
            # read crop:
            # crop = cv2.imread(os.path.join(input_dir, file))
            # write crop in the output location:
            # cv2.imwrite(os.path.join(out_path, basename), crop)

            annotation_filename = os.path.join(input_dir, basename)
            try:
                ann_file = open(annotation_filename, "r")
            except:
                print("file: " + annotation_filename + " was not found!")
                break

            ann_line = ann_file.readline()
            elements = ann_line.split(' ')
            label = int(elements[0])
            cx = int(float(elements[1])*crop_width)
            cy = int(float(elements[2])*crop_height)
            w = int(float(elements[3])*crop_width)
            h = int(float(elements[4][:-1])*crop_height)

            ann_file.close()

            kitti_label = "person"
            kitti_trancated = str(float(0)) # 0 = non-truncated
            kitti_occulded = "0" # 3 = fully visible
            kitti_alpha = "0" # default
            kitti_left = str(float(cx - (w/2)))
            kitti_right = str(float(cx + (w/2)))
            kitti_top = str(float(cy - (h/2)))
            kitti_buttom = str(float(cy + (h/2)))
            kitti_dimensions = "0 0 0" # default
            kitti_location = "0 0 0" # default
            kitti_rotation_y = "0" # default

            label = kitti_label + " " + kitti_trancated + " " + kitti_occulded + " " + kitti_alpha + " " + kitti_left + " " + kitti_top + " " + kitti_right + " " + kitti_buttom + " " + kitti_dimensions + " " + kitti_location + " " + kitti_rotation_y + " \n"

            # write label to file:
            kitti_annotation_filename = os.path.join(out_path, basename)
            try:
                kitti_ann_file = open(kitti_annotation_filename, "w")
            except:
                print("Problem occured while trying to open " + kitti_annotation_filename)
                break
            kitti_ann_file.write(label)
            ann_summery.write(label)
            kitti_ann_file.close()

            if i % 100 == 0:
                print(i)
            i += 1

    ann_summery.close()


#### Main ####
if external_folder:
    for file in os.listdir(external_folder):
        if os.path.isdir(os.path.join(external_folder, file)) and "Negatives" not in file:
            print(file + " is being proccesed...")
            convert_to_Kitti_format(os.path.join(external_folder, file))
else:
    convert_to_Kitti_format(input_folder)