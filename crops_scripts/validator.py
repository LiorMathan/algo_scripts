import os
from PIL import Image
from Target import Target

class CropValidator():
    @staticmethod
    def validate_crops(crop_folder: str, gt_folder: str, crop_size, frame_size: tuple):
        mismatch_count = 0
        missing_crops = 0
        missing_gts = 0

        crop_basename = crop_folder.split(os.sep)[-1]
        gt_basename = gt_folder.split(os.sep)[-1].replace('_gt', '')
        files = 0
        for i in range(0, 1000):
            all_files_exist = True
            gt_filename = gt_basename + f'_{str(i).zfill(6)}.txt'
            gt_filename = os.path.join(gt_folder, gt_filename)

            if not os.path.isfile(gt_filename):
                print(f"gt file for frame {i} is missing")
                return False
            
            gt_targets = []
            crop_targets = []
            
            with open(gt_filename) as gt_file:
                gt_lines = gt_file.readlines()
                for line in gt_lines:
                    gt_targets.append(Target.init_from_yolo_line(line, frame_size[0], frame_size[1], "AG"))
          
            j = 1
            while(True):
                crop_filename = crop_basename + f'_{str(i).zfill(6)}_{str(j).zfill(3)}'
                crop_filename = os.path.join(crop_folder, crop_filename)

                if not os.path.isfile(crop_filename + '.png'):
                    if j == 1:
                        print(f"crop png file for frame {i} is missing")
                        missing_crops += 1
                    break
                
                if not os.path.isfile(crop_filename + '.txt'):
                    if j == 1:
                        print(f"crop txt file for frame {i} is missing")
                        missing_gts += 1
                    break

                with open(crop_filename + '.txt') as crop_file:
                    crop_lines = crop_file.readlines()
                    for line in crop_lines:
                        crop_targets.append(Target.init_from_yolo_line(line, crop_size, crop_size, "AG"))

                j += 1
            
            for m in range(0, len(crop_targets)):
                crop_target = crop_targets[m]
                target_found = False
                
                for k in range(0, len(gt_targets)):
                    gt_target = gt_targets[k]

                    width_valid = abs(gt_target.width - crop_target.width) <= 1
                    if (not width_valid):
                        if crop_target.cx < crop_size / 2:
                            width_valid = gt_target.width / 2 > crop_target.cx
                        elif crop_target.cx > crop_size / 2:
                            width_valid = gt_target.width / 2 > crop_size - crop_target.cx

                    height_valid = abs(gt_target.height - crop_target.height) <= 1
                    if (not height_valid):
                        if crop_target.cy < crop_size / 2:
                            height_valid = gt_target.height / 2 > crop_target.cy
                        elif crop_target.cy > crop_size / 2:
                            height_valid = gt_target.height / 2 > crop_size - crop_target.cy
                    
                    # overall_score = (width_score + height_score + ratio_score) / 3
                    if height_valid and width_valid:
                        target_found = True
                        break

                if not target_found:
                    print(f"found crop crop mismatch at frame {i}")
                    mismatch_count += 1
                
            os.system('cls')
            print("Frame - " + str(i))
            print("Mismatchs - " + str(mismatch_count))
            print("Missing crops - " + str(missing_crops))
            print("Missing gts - " + str(missing_gts))
        return (mismatch_count + missing_crops + missing_gts) == 0


    @staticmethod
    def validate_resulotions(base_folder, resulotion, debug=False):
        return CropValidator.recursivly_find_exception(base_folder, (resulotion, resulotion), debug=debug)

    
    @staticmethod
    def get_file_size(file):
        if os.path.isfile(file):
            return os.path.getsize(file)
        else:
            return 0


    @staticmethod
    def recursivly_find_exception(base_folder: str, size: tuple, file_sort_reverse=False, debug=False) -> list:
        exceptions = []
        members = os.listdir(base_folder)
        for i in range(len(members)):
            members[i] = os.path.join(base_folder, members[i])

        members.sort(key=CropValidator.get_file_size, reverse=file_sort_reverse)
        
        for member in members:
            if member in exceptions or base_folder in exceptions:
                break

            if os.path.isdir(member):
                if len(os.listdir(member)) == 0:
                    exceptions.append(member)
                else:
                    exceptions.extend(CropValidator.recursivly_find_exception(member, size))
            elif os.path.isfile(member) and member.endswith('.png'):
                try:
                    image = Image.open(member)
                    if image.size != size:
                        exceptions.append(base_folder)
                        if debug:
                            print(f'Exception - {base_folder}')
                    else:
                        if debug:
                            print(f'Valid dir - {base_folder}')
                        if (not file_sort_reverse):
                            exceptions = CropValidator.recursivly_find_exception(base_folder, size, file_sort_reverse=True, debug=debug) 
                    break
                except:
                    pass

        return exceptions


if __name__ == '__main__':
    CropValidator.validate_crops(r'Z:\OfflineDB\data-pre-processing-scripts\crops_scripts\test\random\Random_Fixed_00780_r2_3', r'X:\WorkMovies\AG\Dual_GT\NMS_NotUnited\00780_r2_3_gt', 300, (720, 576))
    
    # exceptions = CropValidator.validate_resulotions(r'C:\Users\Omer Wexler\Desktop\tes\trash', 300, True)        
    # print('\n\nExceptions:')
    # for exception in exceptions:
    #     print(exception)