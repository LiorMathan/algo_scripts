import numpy as np
from cv2 import cv2
import os.path
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import random
from pyScriptsUtilities import generateRandomCrop,convertBoolToStr

##### assumption: this script receives only images folder in format .png #####

class Annotation_Frame:
    def __init__(self):
        self.frame = None
        self.annotationBB = None

# FILL THE FOLLOWING DETAILS BEFORE RUNNING THE SCRIPT:
imagesInMosaic = 4
rows = 2
cols = 2
height = 300
weight = 300
input_dir = "/home/ironman/workspace/share/storage_server/OfflineDB/SSDMobileNet_DualGround/CMOS"
outputFolder = "/home/ironman/workspace/share/storage_server/OfflineDB/SSDMobileNet_DualGround/CMOS_mosaics"
dirs_file_path = None
gtDir = None
cropW = 150
cropH = 150

isFromFile = False
isFalsePositive = False

directories = []


def extract_directories_from_file():
    # open directories file:
    try:
        dirs_file = open(dirs_file_path)
    except:
        print("file: " + dirs_file_path + " wasn't found")
        pass
    
    file_lines = dirs_file.readlines()

    for line in file_lines:
        if not (len(line) >= 2):  
            continue
        else: # line is not empty
            line = line[:-1]
            line = line.replace("/mnt/share/Local","/home/ironman/workspace/share/storage_server", 1)
            directories.append(line)


def create_mosaic_for_all_dirs(dir_path):
    if isFromFile is False:
        subfolders = [ f.path for f in os.scandir(dir_path) if f.is_dir() ]
        for subfolder in subfolders:
            if "cropsMovies" in subfolder or "Negatives" in subfolder or "mosaic" in subfolder:
                subfolders.remove(subfolder)
            if isFalsePositive is False:
                if "falsePositives" in subfolder:
                    subfolders.remove(subfolder)
    else: # isFromFile is True
        subfolders = directories
        
    for subfolder in subfolders:             
        # open annotation summery files for writing:
        _, folderName = os.path.split(subfolder)
        folderName = folderName + '_mosaics'
        annSumerryFile = open(outputFolder + '/' + "cropsMovie_" + folderName +"_annotationSummery.txt", "w+")
        crop_images_and_create_mosaic(subfolder, folderName, annSumerryFile)
        annSumerryFile.close()



def crop_images_and_create_mosaic(inputFilePath, folderName, annSumerryFile):
    imageList = [] # images path list

    #gtDir = inputFilePath + '/' + folderName + '_gt'
    newpath = outputFolder + '/' + folderName

    # for each image in the folder, add image path to imageList
    for file in os.listdir(inputFilePath):
        if file.endswith(".png"):
            imageList.append(inputFilePath + '/' + file)
    frameNum = len(imageList)

    print(str(inputFilePath) + ' is processed')

    # create the new path directory:
    try :
        os.makedirs(newpath)
    except OSError:
        pass

    badFramesCnt = 0
    counter = 0
    mosaic_cnt = 0

    while counter < frameNum:
        current_annotation_frames_list = []
        # while the mosaic is not full:
        for i in range(imagesInMosaic):
            # if there are still images in the folder
            if counter < frameNum: 
                # open image's annotation file:
                try:
                    _, imageExtension = os.path.splitext(imageList[counter])
                    annFile = open(imageList[counter].replace(imageExtension,".txt"))
                except:
                    print("text file :"+imageList[counter].split('/')[-1].replace(imageExtension,".txt")+"isn't found")
                    continue
                annFileLines = annFile.readlines()

                # read frame:
                frame = cv2.imread(imageList[counter])

                if (frame is None):
                    badFramesCnt += 1
                    print('Bad frame number: ' + str(badFramesCnt))
                    counter += 1
                    continue

                frameHeight = frame.shape[0]
                frameWidth = frame.shape[1]
                counter += 1

                isSmallImage = False
                isHeightSmall = False
                isWidthSmall = False

                if isFalsePositive is False:
                    for line in annFileLines:
                        if not (len(line) >= 2):  
                            continue
                        else: # line is not empty
                            elems = line.split(' ')
                            label = int(elems[0])

                            cx = int(float(elems[1])*frameWidth)
                            cy = int(float(elems[2])*frameHeight)
                            w = int(float(elems[3])*frameWidth)
                            h = int(float(elems[4][:-1])*frameHeight)

                            # if small image - add padding:
                            if frameHeight < cropH and frameWidth < cropW: 
                                isSmallImage = True
                                addH = int(np.ceil((cropH-frameHeight)/2))
                                addW = int(np.ceil((cropW-frameWidth)/2))
                                frame = np.pad(frame,((addH,),(addW,),(0,)), mode='constant', constant_values=((0,0),))
                                frameHeight, frameWidth, _ = frame.shape
                            elif frameHeight < cropH: 
                                isHeightSmall = True
                                addH = int(np.ceil((cropH-frameHeight)/2))
                                frame = np.pad(frame,((addH,),(0,),(0,)), mode='constant', constant_values=((0,0),))
                                frameHeight, frameWidth, _ = frame.shape
                            elif frameWidth < cropW:
                                isWidthSmall = True
                                addW = int(np.ceil((cropW-frameWidth)/2))
                                frame = np.pad(frame,((0,),(addW,),(0,)), mode='constant', constant_values=((0,0),))
                                frameHeight, frameWidth, _ = frame.shape

                            if isSmallImage:
                                cx = cx + addW
                                cy = cy + addH
                            elif isHeightSmall:
                                cy = cy + addH
                            elif isWidthSmall:
                                cx = cx + addW

                            annotationBB = [label,cx,cy,w,h]
                else: # isFalsePositive is true
                    annotationBB = None

                newBB, cropCoor = generateRandomCrop(cropH, cropW, frameHeight, frameWidth, annotationBB)

                # in case the object is in the edge:
                if cropCoor[0] < 0:
                    cropCoor[1] = cropH
                    cropCoor[0] = 0
                if cropCoor[2] < 0:
                    cropCoor[2] = 0
                    cropCoor[3] = cropW
                
                frameCrop = frame[cropCoor[0]:cropCoor[1], cropCoor[2]:cropCoor[3], :]

                frameCropHeight, frameCropWidth, _ = frameCrop.shape

                if frameCropHeight == 0 and frameCropWidth == 0:
                    frameCrop = frame
                elif frameCropHeight == 0:
                    frameCrop = frame[:,cropCoor[2]:cropCoor[3],:]
                elif frameCropWidth == 0:
                    frameCrop = frame[cropCoor[0]:cropCoor[1],:,:]
                        
                current_frame = Annotation_Frame()
                current_frame.annotationBB = newBB
                current_frame.frame = frameCrop
                current_annotation_frames_list.append(current_frame)
                
        # current_annotation_frames_list contains enough frames:
        annotationFileName = newpath + '/' + folderName + '_' + str(mosaic_cnt).zfill(6) + '.txt'
        mosaic = create_mosaic_image(current_annotation_frames_list, (cropW, cropH), rows, cols, annotationFileName, annSumerryFile, mosaic_cnt)
        frameName = newpath + '/' + folderName + '_' + str(mosaic_cnt).zfill(6) + '.png'
        mosaic_cnt += 1
        cv2.imwrite(frameName, mosaic, [cv2.IMWRITE_PNG_COMPRESSION, 0])  # save frame as PNG file ( lossless )
        frameCrop = None


# for example: frame_list, (208, 208), 2, 2
def create_mosaic_image(annotation_frames_list, crop_size, rows, cols, annotationFileName, annSumerryFile, mosaic_cnt):
    # open annotation file for writing:
    annFile = open(annotationFileName, "w+")
    mosaic_size = (crop_size[0]*rows, crop_size[1]*cols, 3)
    # base image:
    base_image = np.zeros(mosaic_size, np.uint8)
    # current frame position in the base mosaic:
    current_position = [0]*2

    for i in range(len(annotation_frames_list)):
        # current column position:
        current_position[0] = int(i % cols) * crop_size[0]
        # current row position:
        current_position[1] = int(i / rows) * crop_size[1]
        if isFalsePositive is False:
            prev_cx = annotation_frames_list[i].annotationBB[1] * crop_size[0]
            cx = (prev_cx + current_position[0]) / (crop_size[0]*cols) # normalized
            prev_cy = annotation_frames_list[i].annotationBB[2] * crop_size[1]
            cy = (prev_cy + current_position[1]) / (crop_size[1]*rows) # normalized
            # update height and width:
            prev_width = annotation_frames_list[i].annotationBB[3] * crop_size[0]
            prev_height = annotation_frames_list[i].annotationBB[4] * crop_size[1]
            height = prev_height / (crop_size[1]*rows) # normalized
            width = prev_width / (crop_size[0]*cols) # normalized
            annFile.write(str(annotation_frames_list[i].annotationBB[0])+ ' ' +str(cx)+ ' ' + str(cy)+ ' '+ str(width)+ ' '+str(height)+ '\n')
            annSumerryFile.write(str(mosaic_cnt).zfill(6)+ ' ' + str(i) + ' ' + str(annotation_frames_list[i].annotationBB[0])+ ' ' +str(cx)+ ' ' + str(cy)+ ' '+ str(width)+ ' '+str(height)+ '\n')
        # overlay images:
        overlay_images_in_position(base_image, annotation_frames_list[i].frame, current_position)
        
    
    annFile.close()
    return base_image


# overlays 'top_image' on top of 'base_image'
def overlay_images_in_position(base_image, top_image, position):
    base_image[position[1]:position[1] + top_image.shape[0], position[0]:position[0] + top_image.shape[1]] = top_image
    return base_image


######## MAIN ########
if isFromFile is True:
    extract_directories_from_file()
create_mosaic_for_all_dirs(input_dir)


######### FOR DEBBUG #########
# image = cv2.imread('flower.jpg')
# resized = cv2.resize(image, (208, 208))

# mosaic = create_mosaic_image([resized, resized, resized], (208, 208), 2, 2, None)
# cv2.imshow('mosaic', mosaic)
# cv2.waitKey(0)