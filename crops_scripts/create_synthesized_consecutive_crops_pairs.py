import numpy as np
import cv2
import os
from Reader import *
from tkinter import messagebox
import pyScriptsUtilities as utils
import sys
import pandas as pd
import random
from datetime import datetime
# import pyautogui
# from skimage.util import random_noise
# from tkinter import Tk
# import subprocess

debug_flag = False
all_annotations_must_be_in_crop = False
use_inclusive_tiles = True
PEDESTRIAN_LABEL = 0
INCLUSIVE_TILE_EDGE_WIDTH = 12
MIN_PEDESTRIAN_AREA = 5 * 12
wait_time = 0
raw_crops = False #utils.convertBoolToStr(sys.argv[12])
bb_limit = 1.1
relevant_object_labels = [] # empty list => all labels are relevant
curr_IDs = [] # for each trio of consecutive frames, clears and then recieves all relevant object IDs
start_index = 0

CMOS = 0
THERMAL = 1
movie_type = CMOS

CROP_SIZE = 300
# fps parameters:
record_fps = 1
analytics_fps = 1 #
curr_frames = [start_index,0]
skip_parameter = float(record_fps / analytics_fps)
num_of_float_digits = str(skip_parameter)[::-1].find('.')

def set_curr_frames(start_index, remainder):
    for i in range(2):
        curr_frames[i] = int(start_index + remainder + round(i*skip_parameter, num_of_float_digits))
    return round((remainder + skip_parameter)%1, num_of_float_digits)

# display parameters:
def get_display_parameters():
    # root = Tk()
    # output = [l for l in subprocess.check_output(["xrandr"]).decode("utf-8").splitlines()]
    # num_of_screens = len([l.split()[0] for l in output if " connected " in l])
    # screen_height = root.winfo_screenheight()
    # screen_width = int(root.winfo_screenwidth()/num_of_screens)
    # return screen_height, screen_width
    return None, None
    

(screen_height, screen_width) = get_display_parameters()
FRAME_DIM = 0.16
CROP_DIM = 1 #195/CROP_SIZE

def get_sorted_paths(path, suffixes):
    path_list = []
    for file in os.listdir(path):
        for s in suffixes:
            if file.endswith(s):
                path_list.append(os.path.join(path, file))
                break
    path_list.sort(key = lambda filename: os.path.basename(filename))
    return path_list

def count_crops(output_path, with_printing=False):
    total_count = 0
    dirs = [dI for dI in os.listdir(output_path) if os.path.isdir(os.path.join(output_path,dI))]
    for d in dirs:
        if not os.path.isfile(os.path.join(output_path, "CropsMovie_" + d + "_annotationSummery.txt")):
            if with_printing:
                print(d + " doesn't have a summary file")
            continue
        count = 0
        for file in os.listdir(os.path.join(output_path,d)):
            if file.endswith(".png"):
                count += 1
        if with_printing:
            print(d, ":", count)
        total_count += count
    if with_printing:
        print("total:", total_count//2)
    return total_count//2 # counting pairs

def get_out_of_borders_tiles(crop_lists, output_path, current_prefix, new_prefix, limit):
    ''' reads old lists, finds lines referring to 'bad crops' (with exceeding annotations), creates new lists with 'good crops' only. '''
    count = 0
    good_crops = []
    for i in range(len(crop_lists)):
        print("List #{}:".format(i))
        try:
            f = open(crop_lists[i], "r")
            crops = f.readlines()
            f.close()
        except:
            print("error in reading list file!")
            return
        length = len(crops)
        for j in range(length):
            if j%100 == 0:
                print("{} crops out of {} have been processed".format(j, length))
            old_path = crops[j]
            new_path = crops[j].replace(current_prefix, new_prefix, 1).replace(".png\n", ".txt", 1)
            # print(new_path)
            try:
                f = open(new_path, "r")
                next_crop_annotations = f.readlines()[0].split(' ')
                f.close()
            except:
                print("error in reading annotations file: {}!".format(new_path))
            cx = CROP_SIZE*float(next_crop_annotations[1])
            cy = CROP_SIZE*float(next_crop_annotations[2])
            w = CROP_SIZE*float(next_crop_annotations[3])
            h = CROP_SIZE*float(next_crop_annotations[4])
            left = round(cx - w/2)
            top = round(cy - h/2)
            right = round(cx + w/2)
            bottom = round(cy + h/2)
            if left < -round((limit-1)*CROP_SIZE) or top < -round((limit-1)*CROP_SIZE) or right > round(limit*CROP_SIZE) or bottom > round(limit*CROP_SIZE):
                count += 1
                if left >= -round((limit-1)*CROP_SIZE) and top >= -round((limit-1)*CROP_SIZE): 
                    s = "over " +str(round(limit*CROP_SIZE))
                else:
                    s = "under " +str(-round((limit-1)*CROP_SIZE))
                print("out-of-borders tile: {}! file: {}".format(s, os.path.basename(new_path)))
            else:
                good_crops.append(old_path)
    
    print("\n{} bad files!".format(count))

    num_crops = len(good_crops)
    num_train = int(float(num_crops) * 0.7)
    num_validation = num_crops - num_train

    # open train file for writing:
    try:
        out_file_train = open(output_path, 'w+')
    except:
        print("problem occured while trying to open the train output file.\n")
        pass
    
    print("training: writing {} crops...".format(num_train))
    for i in range(num_train):
        out_file_train.write(good_crops[i])
    out_file_train.close()

    # open validation file for writing:
    try:
        out_file_val = open(output_path.replace("_Training.txt", "_Validation.txt", 1), 'w+')
    except:
        print("problem occured while trying to open the validation output file.\n")
        pass

    print("validate: writing {} crops...".format(num_validation))
    for i in range(num_train, num_crops):
        out_file_val.write(good_crops[i])
    out_file_val.close()

def shift_list(lst, n=1):
    if len(lst) > 0:
        for times in range(n):
            tmp = lst[0]
            for i in range(len(lst)-1):
                lst[i] = lst[i+1]
            lst[-1] = tmp

class CroppedFrame:
    def __init__(self, index, frame):
        self.index = index
        self.frame = frame
        self.crops = []
        self.grayscale_crops = []
        self.fused_crops = []
        self.tiles = []
        self.included_annotations = []
        self.output_basenames = []

    def refresh(self):
        self.crops.clear()
        self.grayscale_crops.clear()
        self.fused_crops.clear()
        self.tiles.clear()
        self.included_annotations.clear()
        self.output_basenames.clear()

    def shift(self): # for screenshots purposes
        shift_list(self.crops)
        shift_list(self.grayscale_crops)
        shift_list(self.fused_crops)
        shift_list(self.tiles)
        shift_list(self.included_annotations)
        shift_list(self.output_basenames)

class CropsTool:
    def __init__(self, input_path, gt_path, output_path, crop_width, crop_height, 
                 frame_width=None, frame_height=None, project_prefix=None, is_old_chimera=False, change_polarity=False, labels_conversion_file=None):
        self.input_path = input_path
        self.gt_path = gt_path
        self.output_path = output_path
        self.crop_width = crop_width
        self.crop_height = crop_height
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.change_polarity = change_polarity
        self.is_old_chimera = is_old_chimera
        self.min_objects_in_tile = 1
        self.max_objects_in_tile = 1 # originally 100
        self.min_pedestrian_area = MIN_PEDESTRIAN_AREA
        self.inclusive_tile_edge_width = INCLUSIVE_TILE_EDGE_WIDTH
        self.project_prefix = project_prefix
        _,self.extension = os.path.splitext(self.input_path)
        self.basename = os.path.basename(self.input_path)
        if len(self.extension) > 0:
            self.basename = os.path.basename(self.input_path)[:-len(self.extension)]
        if self.project_prefix is not None:
            self.basename = self.project_prefix + "_" + self.basename
        self.output_path = os.path.join(self.output_path, self.basename)
        
        self.reader = Reader(path1=self.input_path, width1=self.frame_width, height1=self.frame_height)

    def is_relevant_label(self, label):
        if len(relevant_object_labels) < 1:
            return True
        return label in relevant_object_labels

    def print_frames_with_multiple_objects(self):
        ''' finds frames with multiple objects and prints their paths '''
        # multi_objects_frames = []
        # gts_list = self.get_sorted_paths(path=self.gt_path, suffixes=[".txt"])
        # for i in range(len(gts_list)):
        #     count = 0
        #     current_gt = open(gts_list[i], "r")
        #     gt_lines = current_gt.readlines()
        #     for line in gt_lines:
        #         if(len(line) >= 2):
        #             label = int(line.split(' ')[0])
        #             if label in relevant_object_labels:
        #                 count += 1
        #                 if count > 1:
        #                     multi_objects_frames.append(i)
        #                     break
        #     current_gt.close()
        # print("frames with multiple objects:", str(multi_objects_frames))
        three_objects_frames = []
        four_objects_frames = []
        five_or_more_objects_frames = []
        gts_list = self.get_sorted_paths(path=self.gt_path, suffixes=[".txt"])
        for i in range(len(gts_list)):
            count = 0
            current_gt = open(gts_list[i], "r")
            gt_lines = current_gt.readlines()
            for line in gt_lines:
                if(len(line) >= 2):
                    label = int(line.split(' ')[0])
                    if self.is_relevant_label(label):
                        count += 1
            if count > 2:
                if count == 3:
                    three_objects_frames.append(i)
                elif count == 4:
                    four_objects_frames.append(i)
                else:
                    five_or_more_objects_frames.append(i)
            current_gt.close()
        print("frames with 3 objects:", str(three_objects_frames))
        print("frames with 4 objects:", str(four_objects_frames))
        print("frames with 5+ objects:", str(five_or_more_objects_frames))

    def validate_input(self):
        if not os.path.exists(self.input_path):
            print("Input Path Does Not Exists!")
            exit()
        if not os.path.exists(self.gt_path):
            print("GT Path Does Not Exists!")
            exit()

    def extract_detections_from_written_gt_files(self, gt_paths, yolo):
        yolo_annotations = []
        for i in range(len(gt_paths)):
            annotations = []
            current_gt = open(gt_paths[i], "r")
            gt_lines = current_gt.readlines()
            current_gt.close()
            for line in gt_lines:
                if(len(line) >= 2):
                    elements = line.split(' ')
                    label = int(elements[0])
                    cx = float(elements[1])
                    cy = float(elements[2])
                    w = float(elements[3])
                    h = float(elements[4])
                    if yolo is False:
                        cx = round(cx * self.reader.width1)
                        cy = round(cy * self.reader.height1)
                        w = round(w * self.reader.width1)
                        h = round(h * self.reader.height1)
                    annotation = utils.Annotation(label, cx, cy, w, h)
                    annotations.append(annotation)
            yolo_annotations.append(annotations)
        return yolo_annotations

    def extract_selective_detections_from_gt_files(self, gt_paths, yolo=False):
        ''' 
        - extracts relevant annotations from gt file
        - updates the global parameter curr_IDs
        - returns a list of the extracted annotations, sorted by object's ID 
        '''
        
        yolo_annotations = []
        all_gt_lines = []
        temp_IDs = {} # dictionary- key: ID, value: number of frames containing this ID
        curr_IDs.clear()
        
        for i in range(len(gt_paths)):
            current_gt = open(gt_paths[i], "r")
            all_gt_lines.append([line.split(' ') for line in current_gt.readlines() if len(line) >= 2])
            current_gt.close()
        for gt_lines in all_gt_lines:
            for elements in gt_lines:
                # if label is a relevant target object, add the ID to the dictionary
                if self.is_relevant_label(int(elements[0])): 
                    curr_id = int(elements[5])
                    if curr_id in temp_IDs:
                        temp_IDs[curr_id] += 1
                    else:
                        temp_IDs[curr_id] = 1
                else:
                    gt_lines.remove(elements)
        # updates curr_IDs to contain only IDs of objects that appear in all three frames
        curr_IDs.extend([temp_id for temp_id in temp_IDs if temp_IDs[temp_id] == len(gt_paths)])

        for gt_lines in all_gt_lines:
            annotations = []
            for elements in gt_lines:
                curr_id = int(elements[5])
                if curr_id not in curr_IDs:
                    continue
                label = int(elements[0])
                cx = float(elements[1])
                cy = float(elements[2])
                w = float(elements[3])
                h = float(elements[4])
                if yolo is False:
                    cx = round(cx * self.reader.width1)
                    cy = round(cy * self.reader.height1)
                    w = round(w * self.reader.width1)
                    h = round(h * self.reader.height1)
                annotation = utils.Annotation(label, cx, cy, w, h)
                annotations.append((curr_id, annotation))
            # annotations = a list of the extracted annotations, sorted by object's ID
            annotations = [tup[1] for tup in sorted(annotations, key=lambda x: x[0])]
            yolo_annotations.append(annotations)
        return yolo_annotations

    def get_visdrone_format_annotations(self, yolo_annotations):
        visdrone_annotation = []
        for i in range(len(yolo_annotations)):
            left = int(yolo_annotations[i].cx - (yolo_annotations[i].w / 2))
            top = int(yolo_annotations[i].cy - (yolo_annotations[i].h / 2))
            width = int(yolo_annotations[i].w)
            height = int(yolo_annotations[i].h)
            label = int(yolo_annotations[i].label)
            new_annotation = np.array([left, top, width, height, 0, label])
            visdrone_annotation.append(new_annotation.astype(int))
        
        return np.array(visdrone_annotation)
   
    def get_included_annotations(self, annotations, tile):
        # tile is [left, top, width, height]
        # annotations is a table in which contains cells in format: [label, cx, cy, width, height]
        included = []

        for i in range(len(annotations)):
            try:
                if all_annotations_must_be_in_crop:
                    # ensure the crop contains 3 objects: one from each layer
                    if use_inclusive_tiles is True:
                        # include only objects that have at least <INCLUSIVE_TILE_EDGE_WIDTH> pixels inside the crop:
                        if (annotations[i].cx + (annotations[i].w / 2) < tile[0] + INCLUSIVE_TILE_EDGE_WIDTH):
                            continue
                        if (annotations[i].cy + (annotations[i].h / 2) < tile[1] + INCLUSIVE_TILE_EDGE_WIDTH):
                            continue
                        if (annotations[i].cx - (annotations[i].w / 2) > tile[0] + tile[2] - INCLUSIVE_TILE_EDGE_WIDTH):
                            continue
                        if (annotations[i].cy - (annotations[i].h / 2) > tile[1] + tile[3] - INCLUSIVE_TILE_EDGE_WIDTH):
                            continue
                    else:
                        # include only objects that their center is inside the crop:
                        cx = annotations[i].cx
                        cy = annotations[i].cy
                        if (cx < tile[0] or cx > tile[0] + tile[2]):
                            continue
                        if (cy < tile[1] or cy > tile[1] + tile[3]):
                            continue
            except Exception as e:
                print(e)
                continue

            start_x = annotations[i].cx - (annotations[i].w/2)
            start_y = annotations[i].cy - (annotations[i].h/2)
            end_x = annotations[i].cx + (annotations[i].w/2)
            end_y = annotations[i].cy + (annotations[i].h/2)
            updated_width = end_x - start_x
            updated_height = end_y - start_y
            
            yolo_cx = (start_x - tile[0] + (updated_width / 2)) / tile[2]
            yolo_cy = (start_y - tile[1] + (updated_height / 2)) / tile[3]
            yolo_w = updated_width / tile[2]
            yolo_h = updated_height / tile[3]
            label = annotations[i].label

            included_annotation = utils.Annotation(label=label, cx=yolo_cx, cy=yolo_cy, w=yolo_w, h=yolo_h)
            included.append(included_annotation)

        return (included)

    def read_frame(self, index):
        try:
            bgr = self.input_path.endswith(".rawc") or self.input_path.endswith(".raw2")
            frame = self.reader.get_frame(index=index, bgr=bgr) # to deal with "raw" => bgr = True
            if frame is None:
                raise ValueError("frame is None. index: " + str(index))
            if (frame.shape[0] < self.crop_height or frame.shape[1] < self.crop_width):
                raise ValueError("frame's shape is smaller than crop size. index: " + str(index))

            if self.reader.image_extension == '.yuv':
                frame = cv2.cvtColor(frame, cv2.COLOR_YUV2BGR_YUYV)
        except:
            return

        return frame

    def write_annotations(self, cropped_frames, ann_summary_list):
        '''
        writes each tile in both frames into one text file, to contain every object's annotations: [label, cx, cy, w, h]
        
        these two lines are added to the summary file:
            1. [frame_index, layer_index, number_of_tiles_in_frame, annotations ([label, cx, cy, w, h], as many as number_of_tiles_in_frame)]
            2. [frame_index, layer_index, number_of_tiles_in_frame, tiles ([left, top, width, height], as many as number_of_tiles_in_frame)]
        '''
        next_summary_lines = ""
        summary_text_annotations = ""
        summary_text_tiles = ""
        for object_index in range(len(curr_IDs)):
            if cropped_frames[0].output_basenames[object_index] is None:
                continue
            output_gt_file = open(cropped_frames[0].output_basenames[object_index][:-2] + ".txt", "w")
            for i in range(2):
                try:
                    included_annotations = cropped_frames[i].included_annotations[object_index]
                    for ann in range(len(included_annotations)):
                        str_ann = str(included_annotations[ann].label) + ' ' + str(included_annotations[ann].cx) + ' ' + str(included_annotations[ann].cy) + ' ' + str(included_annotations[ann].w) + ' ' + str(included_annotations[ann].h)
                        output_gt_file.write(str_ann + ' \n')
                        summary_text_annotations += ' ' + str_ann
                    
                    head = str(cropped_frames[i].index).zfill(6) + ' ' + str(i) + ' ' + str(len(cropped_frames[i].tiles))
                    next_summary_lines += head + summary_text_annotations + ' \n'
                    for tile in cropped_frames[i].tiles:
                        summary_text_tiles += ' ' + str(tile[0]) + ' ' + str(tile[1]) + ' ' + str(tile[2]) + ' ' + str(tile[3])
                    next_summary_lines += head + summary_text_tiles + ' \n'
                except:
                    continue
            output_gt_file.close()

        ann_summary_list.append(next_summary_lines)

        # next_summary_lines = ""
        # for i in range(4):
        #     head = str(cropped_frames[2].index).zfill(6) + ' ' + str(i) + ' ' + str(len(cropped_frames[i].tiles))
        #     summary_text_annotations = ""
        #     summary_text_tiles = ""
        #     extension = "_Layer{}.txt".format(i) if i < 3 else ".txt"
        #     try:
        #         for object_index in range(len(curr_IDs)):
        #             f = open(cropped_frames[2].output_basenames[object_index] + extension, "w")
        #             included_annotations = cropped_frames[i].included_annotations[object_index]
        #             for ann in range(len(included_annotations)):
        #                 str_ann = str(included_annotations[ann].label) + ' ' + str(included_annotations[ann].cx) + ' ' + str(included_annotations[ann].cy) + ' ' + str(included_annotations[ann].w) + ' ' + str(included_annotations[ann].h)
        #                 f.write(str_ann + ' \n')
        #                 summary_text_annotations += ' ' + str_ann
        #             f.close()
                
        #         next_summary_lines += head + summary_text_annotations + ' \n'
        #         for tile in cropped_frames[i].tiles:
        #             summary_text_tiles += ' ' + str(tile[0]) + ' ' + str(tile[1]) + ' ' + str(tile[2]) + ' ' + str(tile[3])
        #         next_summary_lines += head + summary_text_tiles + ' \n'
        #     except:
        #         continue
        # ann_summary_list.append(next_summary_lines)

    def write_crops(self, cropped_frames):
        written = False
        for cropped_frame in cropped_frames:
            if len(cropped_frame.crops) < 1:
                return False
        for object_index in range(len(curr_IDs)):
            for i in range(2):
                if cropped_frames[i].output_basenames[object_index] is None:
                    continue
                filename = cropped_frames[i].output_basenames[object_index] + ".png"
                cv2.imwrite(filename, cropped_frames[i].crops[object_index], [cv2.IMWRITE_PNG_COMPRESSION, 0])
                # delete crop if writing had failed:
                check_not_none = cv2.imread(filename)
                if check_not_none is None:
                    print("Bad crop: " + filename)
                    os.remove(filename)
                    return False
                else:
                    written = True
        return written
        
    def annotations_out_of_borders(self, annotations, index=None):
        annotations_to_check = annotations if index is None else [annotations[index]]
        for annotations in annotations_to_check:
            for annotation in annotations:
                cx = CROP_SIZE*annotation.cx
                cy = CROP_SIZE*annotation.cy
                w = CROP_SIZE*annotation.w
                h = CROP_SIZE*annotation.h
                left = round(cx - w/2)
                top = round(cy - h/2)
                right = round(cx + w/2)
                bottom = round(cy + h/2)
                if left < -round((bb_limit-1)*CROP_SIZE) or top < -round((bb_limit-1)*CROP_SIZE) or right > round(bb_limit*CROP_SIZE) or bottom > round(bb_limit*CROP_SIZE):
                    return True
        return False

    def generateRandomCrop(self, cropHeight,cropWidth,frameHeight,frameWidth,annotationBB):
        ''' works exactly like pyScriptsUtilities.generateRandomCrop, but without printing '''
        # annotationBB = [label, cx ,cy , w ,h]
        cropCenter = [0,0]
        cropCenter[1]=np.floor(min(max(cropWidth/2, cropCenter[1]), frameWidth-cropWidth/2-1))
        cropCenter[0]=np.floor(min(max(cropHeight/2, cropCenter[0]), frameHeight-cropHeight/2-1))

        if annotationBB is not None: # not false positive
            label = annotationBB[0]
            cx = annotationBB[1]
            cy = annotationBB[2]
            w = annotationBB[3]
            h = annotationBB[4]

            partialObjectFactor = 1
            rowRandRange = [max(cropHeight/2, cy-cropHeight/2 + round(partialObjectFactor*h/2)),
                            min(frameHeight-cropHeight/2, cy+cropHeight/2 - round(partialObjectFactor*h/2))]
            colRandRange = [max(cropWidth/2, cx-cropWidth/2 + round(partialObjectFactor*w/2)),
                            min(frameWidth-cropWidth/2, cx+cropWidth/2 - round(partialObjectFactor*w/2))]
            if rowRandRange[0] <= rowRandRange[1]:
                cropCenter[0]=random.randint(rowRandRange[0],rowRandRange[1]+1)
            else:
                cropCenter[0]=round(np.mean(rowRandRange))
            if colRandRange[0] <= colRandRange[1]:
                cropCenter[1]=random.randint(colRandRange[0],colRandRange[1]+1)
            else:
                cropCenter[1]=round(np.mean(colRandRange))

            newCenter=[np.floor(cy-(cropCenter[0]-cropHeight/2)), np.floor(cx-(cropCenter[1]-cropWidth/2))]

            newBBtmp=[label, newCenter[1]/cropWidth, newCenter[0]/cropHeight, w/cropWidth, h/cropHeight] # in normalized center-x-y formalism
            # dealing with extreme cases - when object is larger then crop
            dxm=-min(newBBtmp[1]-newBBtmp[3]/2,0)
            dxp=max(newBBtmp[1]+newBBtmp[3]/2,1)-1
            dym=-min(newBBtmp[2]-newBBtmp[4]/2,0)
            dyp=max(newBBtmp[2]+newBBtmp[4]/2,1)-1
            # newBB = [label , newCx , newCy , newW , newH]
            newBB=[label, (newCenter[1]/cropWidth)+dxm/2-dxp/2, (newCenter[0]/cropHeight)+dym/2-dyp/2, (w/cropWidth)-dxm-dxp, (h/cropHeight)-dym-dyp] # in normalized center-x-y formalism
        else:
            newBB = None
        # cropCoor = [topRow,bottomRow, leftColumn , rightColumn]
        cropCoor = [int(cropCenter[0]-cropHeight/2),int(cropCenter[0]+cropHeight/2), int(cropCenter[1]-cropWidth/2),int(cropCenter[1]+cropWidth/2)]
        
        return newBB,cropCoor

    def get_final_annotations(self, all_yolo_annotations, yolo_annotations):
        all_included_annotations = []
        annotations = self.get_visdrone_format_annotations(yolo_annotations=all_yolo_annotations)
        if len(annotations) < 1:
            return None, None
        
        min_values = np.amin(a=annotations, axis=0)
        max_values = np.amax(a=annotations, axis=0)
        left = max(0, min_values[0] - self.inclusive_tile_edge_width)
        top = max(0, min_values[1] - self.inclusive_tile_edge_width)
        right = max_values[0] + max_values[2] + self.inclusive_tile_edge_width
        bottom = max_values[1] + max_values[3] + self.inclusive_tile_edge_width
        width = right - left
        height = bottom - top
        # annotationBB = [label, cx ,cy , w ,h]
        annotationBB = [annotations[0][-1], int(left+width/2), int(top+height/2), width, height]

        # cropCoor = [topRow, bottomRow, leftColumn, rightColumn]
        newBB,cropCoor = self.generateRandomCrop(cropHeight=self.crop_height, cropWidth=self.crop_width, frameHeight=self.reader.height1, frameWidth=self.reader.width1, annotationBB=annotationBB)
        if newBB is None:
            print("newBB is None!")
            return None, None
        # tile = [left, top, width, height]
        tile=[cropCoor[2], cropCoor[0], cropCoor[3]-cropCoor[2], cropCoor[1]-cropCoor[0]]
        
        for i in range(2):
            included_annotations = self.get_included_annotations(yolo_annotations[i], tile)
            if len(included_annotations) >= self.min_objects_in_tile and len(included_annotations) <= self.max_objects_in_tile:
                all_included_annotations.append(included_annotations)
        
        if len(all_included_annotations) < 2 or self.annotations_out_of_borders(annotations=all_included_annotations):
            return None, None
        return all_included_annotations, tile

    def crop_frames(self, cropped_frames, gt_paths):
        ''' returns number of bad crops '''
        # older version (supports up to one object per frame) can be found in create_synthesized_consecutive_crops_old_version.py
        for cropped_frame in cropped_frames:
            cropped_frame.refresh()
        yolo_annotations = self.extract_selective_detections_from_gt_files(gt_paths=gt_paths)
        
        # assert every frame contains the same number of target objects, which must be >0 and equal to the number of IDs in curr_IDs
        lengths = {len(ya) for ya in yolo_annotations+[curr_IDs]}
        if 0 in lengths:
            return 0 # an empty GT file
        if len(lengths) > 1:
            print("error with ID handling in frames#{},{}!"\
            .format(cropped_frames[0].index, cropped_frames[1].index))
            return 0

        error_IDs = set()
        for object_index in range(len(curr_IDs)):
            curr_yolo_annotations = [[ya[object_index]] for ya in yolo_annotations]
            curr_all_yolo_annotations = []
            for ya in curr_yolo_annotations[:-1]:
                curr_all_yolo_annotations.extend(ya)
            (all_included_annotations, tile) = self.get_final_annotations(all_yolo_annotations=curr_all_yolo_annotations, yolo_annotations=curr_yolo_annotations)
            if all_included_annotations is None or tile is None:
                return 0
            pair_index = cropped_frames[0].index
            for i in range(2):     
                curr_crop = cropped_frames[i].frame[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2]]
                if curr_crop.shape[1] != self.crop_width or curr_crop.shape[0] != self.crop_height:
                    cropped_frames[i].tiles.append(None)
                    cropped_frames[i].included_annotations.append(None)
                    cropped_frames[i].crops.append(None)
                    cropped_frames[i].grayscale_crops.append(None)
                    cropped_frames[i].output_basenames.append(None)
                    error_IDs.add(curr_IDs[object_index])
                    continue
                cropped_frames[i].tiles.append(tile)
                cropped_frames[i].included_annotations.append(all_included_annotations[i])
                cropped_frames[i].crops.append(curr_crop)
                cropped_frames[i].grayscale_crops.append(cv2.cvtColor(curr_crop, cv2.COLOR_BGR2GRAY))
                cropped_frames[i].output_basenames.append(os.path.join(self.output_path, self.basename + '_' + str(pair_index).zfill(6) + "_" + str(len(cropped_frames[i].tiles)).zfill(3)) + "_" + str(i))
        return len(error_IDs)

    def get_color(self, index):
        # color = [0,0,0]
        # if index < 3:
        #     color[index] = 255
        # return tuple(color)
        if index > 2:
            return (255,255,255)
        color = [0,0,0]
        color[index] = 255
        return tuple(color)
        

    def get_annotations_from_written_files(self, gt_paths):
        yolo_annotations = self.extract_detections_from_written_gt_files(gt_paths=gt_paths, yolo=True)
        lengths = {len(ya) for ya in yolo_annotations}
        if 0 in lengths:
            return # an empty GT file
        if len(lengths) > 1:
            print("error with ID handling in reading output!")
            return
        return yolo_annotations

    def add_annotations_to_image(self, image, color, included_annotations_list, object_index_range, tiles=None, dim=1):
        crop_start_x = 0 
        crop_start_y = 0
        for object_index in object_index_range:
            included_annotations = included_annotations_list[object_index]
            if tiles is not None:
                if tiles[object_index] is not None:
                    crop_start_x = int(tiles[object_index][0]*dim)
                    crop_start_y = int(tiles[object_index][1]*dim)
            for obj in range(len(included_annotations)):
                if included_annotations[obj] is not None:
                    (start, end) = utils.find_start_end_point_rectangle(cx=included_annotations[obj].cx*self.crop_width, cy=included_annotations[obj].cy*self.crop_height, width=included_annotations[obj].w*self.crop_width, height=included_annotations[obj].h*self.crop_height)
                    start = (int(start[0]*dim), int(start[1]*dim))
                    end = (int(end[0]*dim), int(end[1]*dim))
                    start_x = start[0]+crop_start_x
                    start_y = start[1]+crop_start_y
                    img = cv2.rectangle(img=image, pt1=(start_x, start_y), pt2=(end[0]+crop_start_x, end[1]+crop_start_y), color=color, thickness=1)
                    text = str(included_annotations[obj].label)
                    if len(object_index_range) > 1:
                        text += " (ID={})".format(curr_IDs[object_index]) 
                    cv2.putText(img=img, text=text, org=(start_x, start_y-5), fontFace=cv2.FONT_HERSHEY_COMPLEX, color=color, thickness=1, fontScale=0.3)

    def show_frame(self, title, cropped_frame, color, index):
        dim = 1
        thickness = 1
        if self.reader.image_extension == '.yuv':
            dim = FRAME_DIM
            thickness = 4
        debug_frame = cropped_frame.frame.copy()
        for object_index in range(len(curr_IDs)):
            tile = cropped_frame.tiles[object_index]
            if tile is not None:
                debug_frame = cv2.rectangle(img=debug_frame, pt1=(int(tile[0]), int(tile[1])), pt2=(int(tile[0]+tile[2]), int(tile[1]+tile[3])), color=color, thickness=thickness)
        debug_frame = cv2.resize(debug_frame, (int(debug_frame.shape[1]*dim), int(debug_frame.shape[0]*dim)))
        self.add_annotations_to_image(image=debug_frame, color=color, included_annotations_list=cropped_frame.included_annotations, object_index_range=range(len(curr_IDs)), tiles=cropped_frame.tiles, dim=dim)
        new_title = "#{} {}".format(cropped_frame.index, title)
        cv2.imshow(new_title, debug_frame)
        # move window for screenshot purposes:
        window_x = int(0.025*screen_width)+(index%2)*debug_frame.shape[1]+10
        window_y = int(0.075*screen_height) if index < 2 else int(0.15*screen_height)+debug_frame.shape[0]
        cv2.moveWindow(new_title, window_x, window_y)

    def show_crops(self, title, cropped_frame, color, index):
        new_title = "#{} {}".format(cropped_frame.index, title)
        if index > 2:
            new_title += " - before writing"
        str_id = ""
        for object_index in range(len(curr_IDs)):
            debug_crop = cropped_frame.crops[object_index].copy()
            self.add_annotations_to_image(image=debug_crop, color=color, included_annotations_list=cropped_frame.included_annotations, object_index_range=[object_index])
            if len(curr_IDs) > 1:
                str_id = " (ID={})".format(curr_IDs[object_index])
            # resize and move window for screenshot purposes:
            debug_crop = cv2.resize(debug_crop, (int(debug_crop.shape[1]*CROP_DIM), int(debug_crop.shape[0]*CROP_DIM)))
            cv2.imshow(new_title+str_id, debug_crop)
            window_x = int(2*self.reader.width1*FRAME_DIM)
            if index == 0:
                window_y = int(0.075*screen_height)
            elif index == 1:
                window_x += int(debug_crop.shape[1]/CROP_DIM)+10
                window_y = int(0.075*screen_height)
            elif index == 2:
                window_y = int(0.15*screen_height)+debug_crop.shape[0]
            else: #index == 3
                window_y = int(0.225*screen_height)+2*debug_crop.shape[0]
            cv2.moveWindow(new_title+str_id, window_x, window_y)

    def print_annotations(self, from_file, included_annotations_list):
        for i in range(3):
            s = "{}{}: ".format("after: " if from_file else "before:", i)
            for ia in included_annotations_list[i]:
                for obj in range(len(ia)):
                    print(s+"label: ", ia[obj].label)
                    print(s+"cx: ", ia[obj].cx)
                    print(s+"cy: ", ia[obj].cy)
                    print(s+"h: ", ia[obj].h)
                    print(s+"w: ", ia[obj].w)

    def compare_annotations(self, before_annotations, after_annotations):
        # if len(before_annotations) != len(after_annotations):
        #     return False
        # for i in range(3):
        #     if len(before_annotations[i]) != len(after_annotations[i]):
        #         return False
        #     for ann_index in range(len(before_annotations[i])):
        #         if before_annotations[i][ann_index].cx != after_annotations[i][ann_index].cx \
        #             or before_annotations[i][ann_index].cy != after_annotations[i][ann_index].cy \
        #             or before_annotations[i][ann_index].h != after_annotations[i][ann_index].h \
        #             or before_annotations[i][ann_index].w != after_annotations[i][ann_index].w:
        #             return False
        return True

    def debug(self, cropped_frames):
        if not debug_flag:
            return
        for times in range(len(curr_IDs)):

            if times > 0:
                shift_list(curr_IDs)
                for cropped_frame in cropped_frames:
                    cropped_frame.shift()
            
            for i in range(2):
                self.show_frame(title="frame " + str(i), cropped_frame=cropped_frames[i], color=self.get_color(i), index=i)
            for i in range(2):
                self.show_crops(title="crop " + str(i), cropped_frame=cropped_frames[i], color=self.get_color(i), index=i)
            cv2.waitKey(wait_time)
            cv2.destroyAllWindows()
    
    def print_progress(self, num_of_checked_frames, total_num_of_frames, print_after_every):
        if num_of_checked_frames <= len(curr_frames):
            current_time = datetime.now().strftime("%H:%M:%S")
            print("0 out of " + str(total_num_of_frames) + ". Current Time =", current_time)
        elif (num_of_checked_frames) % print_after_every == 0:
            current_time = datetime.now().strftime("%H:%M:%S")
            print(str(num_of_checked_frames) + " out of " + str(total_num_of_frames) + ". Current Time =", current_time)

    def create_crops(self):
        self.validate_input()
        try:
            os.mkdir(self.output_path)
        except:
            pass
        
        # frames_list = self.get_sorted_paths(path=self.input_path, suffixes=[".yuv",".png"])
        gts_list = get_sorted_paths(path=self.gt_path, suffixes=[".txt"])
        ann_summary_list = []
        checked_frames = set()
        
        num_frames = min(self.reader.num_frames1, len(gts_list))
        limit = num_frames #start_index + int(10*skip_parameter)
        curr_start_index = start_index
        next_percent = 0
        while len(checked_frames) < limit - start_index:

            if curr_start_index - start_index > int(skip_parameter+1): # too many runs
                print("too many runs!")
                break

            # set curr_frames to be [prev_prev_frame_index, prev_frame_index, current_frame_index, next_frame_index] based on fps parameters
            remainder = set_curr_frames(start_index=curr_start_index, remainder=0)
            cropped_frames = [CroppedFrame(curr_frames[0], self.read_frame(curr_frames[0]))]

            bad_crops_count = 0
            while curr_frames[1] < limit:
                for frame_index in curr_frames:
                    checked_frames.add(frame_index)
                # print("curr_frames =", curr_frames)
                self.print_progress(num_of_checked_frames=len(checked_frames), total_num_of_frames=num_frames, print_after_every=100)
                cropped_frames.append(CroppedFrame(curr_frames[1], self.read_frame(curr_frames[1])))
                
                try:
                    bad_crops_count += self.crop_frames(cropped_frames=cropped_frames, gt_paths=[gts_list[frame_index] for frame_index in curr_frames])
                    success = self.write_crops(cropped_frames=cropped_frames)
                except:
                    print("exception! frames", curr_frames)
                    success = False
                
                if success: 
                    self.write_annotations(cropped_frames=cropped_frames, ann_summary_list=ann_summary_list)
                    self.debug(cropped_frames=cropped_frames)
                # else:
                #     print("ERROR!! failed fusing frames {},{},{}.".format(curr_frames[0], curr_frames[1], curr_frames[2]))
                cropped_frames = cropped_frames[1:]
                remainder = set_curr_frames(start_index=curr_frames[1], remainder=remainder)
            
            curr_start_index += 1
            print("run #{} finished!".format(curr_start_index - start_index))
            print("number of errors in crops' size =", bad_crops_count)

        ann_summary_file_path = os.path.join(os.path.dirname(self.output_path), "CropsMovie_" + self.basename + "_annotationSummery.txt")
        ann_summary_file = open(ann_summary_file_path, "w")
        for lines in sorted(ann_summary_list):
            ann_summary_file.write(lines)
        ann_summary_file.close()
 

if __name__ == '__main__':
    
    ## SINGLE FILE ###
    # input_path = "/home/shira/amram/test/INPUT"
    # gt_path = "/home/shira/amram/test/GT"
    # output_path = "/home/shira/amram/test/OUTPUT"

    # try:
    # cropper = CropsTool(input_path=input_path,
    #                     gt_path=gt_path, # needs to be with IDs!
    #                     output_path=output_path,
    #                     crop_width=CROP_SIZE,
    #                     crop_height=CROP_SIZE,
    #                     frame_width=width,  
    #                     frame_height=height,
    #                     project_prefix=None,
    #                     is_old_chimera=False,
    #                     change_polarity=False)
    # print(cropper.basename + " is being processed...")
    # cropper.create_crops()
    # except:
    #     print("cropping frames from {} has failed!!".format(input_path))
    

    ### MULTIPLE FILES - Dual ###
    if movie_type == CMOS:
        height, width = 576, 720
        movie_type_name = "CMOS"
        movie_type_suffix = ".rawc"
    else: # movie_type == THERMAL
        height, width = 480, 640
        movie_type_name = "Thermal"
        movie_type_suffix = ".raw2"
    input_basename = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Low_Altitude"
    gt_basename = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/GT_with_properties" 
    output_path = "/mnt/share/Local/OfflineDB/ConsecutiveFusedCrops/AG/DualAir/Low_Altitude/" + movie_type_name

    print("counting crops...")
    crop_count = count_crops(output_path=output_path, with_printing=False)
    failed_folders = []
    folders = [os.path.join(input_basename,dI) for dI in os.listdir(input_basename) if dI.startswith('0') and os.path.isdir(os.path.join(input_basename,dI))]
    all_dirs = []
    for i in range(len(folders)):
        # print("\n" + folders[i] + ":")
        folder = os.path.join(input_basename, folders[i])
        dirs = [os.path.join(folder,dI) for dI in os.listdir(folder) if os.path.isdir(os.path.join(folder,dI))]
        all_dirs.extend(dirs)
    checked_dirs = [dI for dI in os.listdir(output_path) if os.path.isdir(os.path.join(output_path,dI))]
    input_dirs = [d for d in all_dirs if os.path.basename(d).startswith(movie_type_name) and os.path.basename(d) not in checked_dirs]
    for j in range(len(input_dirs)):
        input_paths = get_sorted_paths(path=input_dirs[j], suffixes=[movie_type_suffix])# [d for d in os.listdir(input_dirs[j]) if d.endswith(movie_type_suffix)]
        for i in range(len(input_paths)):
            # print(i, input_paths[i])
            if crop_count > 100000:
                break
            input_path = input_paths[i]
            if int(os.path.basename(input_path)[:5]) < 75:
                continue
            gt_path = os.path.join(gt_basename, os.path.basename(input_path)[:-len(movie_type_suffix)])
            if not os.path.isdir(gt_path):
                print(gt_path + " doesn't exist!")
                continue
            try:
                cropper = CropsTool(input_path=input_path,
                                gt_path=gt_path,
                                output_path=output_path,
                                crop_width=CROP_SIZE,
                                crop_height=CROP_SIZE,
                                frame_width=width,     
                                frame_height=height,
                                project_prefix=None,
                                is_old_chimera=False,
                                change_polarity=False)
                print("crop_count =", crop_count)
                # print("#" + str(i) + " " + cropper.basename + " is being processed...")
                print("folder {}/{} file {}/{}: {} is being processed...".format(j+1, len(input_dirs), i+1, len(input_paths), cropper.basename))
                cropper.create_crops()
            except:
                print("cropping frames from {} has failed!! frames: {}".format(input_path, curr_frames))
                failed_folders.append(("j={}, i={}".format(j,i), os.path.basename(input_path)))
                continue  

            crop_count += len(get_sorted_paths(path=cropper.output_path, suffixes=[".png"]))//2 
    print("\ntotal crop_count =", crop_count)
    print(str(len(failed_folders))+" failed folders!")
    for folder in failed_folders:
        print(folder)

    ### SEARCHING FOR BAD ANNOTATION-FILES ###
    # output_path = "/mnt/share/Local/OfflineDB/ConsecutiveFusedCrops/AG/DualAir/Low_Altitude/CMOS"
    # total_count = 0
    # limit = 1.1
    # dirs = [dI for dI in os.listdir(output_path) if os.path.isdir(os.path.join(output_path,dI))]
    # # for i in range(len(dirs)):
    # #     print(i, dirs[i])
    # # dirs2 = [dirs[i] for i in [50,52,53,55,57]]
    # for d in dirs:
    #     # print(d + " is processing...")
    #     count = 0
    #     for file in os.listdir(os.path.join(output_path,d)):
    #         if file.endswith(".txt"):
    #             f = open(os.path.join(output_path,d,file), "r")
    #             lines = f.readlines()
    #             f.close()

    #             # try:
    #             #     f = open(new_path, "r")
    #             #     next_crop_annotations = f.readlines()[0].split(' ')
    #             #     f.close()
    #             # except:
    #                 # print("error in reading annotations file: {}!".format(new_path))
    #             for i in range(2):
    #                 next_crop_annotations = lines[i].split(' ')
    #                 cx = CROP_SIZE*float(next_crop_annotations[1])
    #                 cy = CROP_SIZE*float(next_crop_annotations[2])
    #                 w = CROP_SIZE*float(next_crop_annotations[3])
    #                 h = CROP_SIZE*float(next_crop_annotations[4])
    #                 left = round(cx - w/2)
    #                 top = round(cy - h/2)
    #                 right = round(cx + w/2)
    #                 bottom = round(cy + h/2)
    #                 if left < -round((limit-1)*CROP_SIZE) or top < -round((limit-1)*CROP_SIZE) or right > round(limit*CROP_SIZE) or bottom > round(limit*CROP_SIZE):
    #                     count += 1
    #                     if left >= -round((limit-1)*CROP_SIZE) and top >= -round((limit-1)*CROP_SIZE): 
    #                         s = "over " +str(round(limit*CROP_SIZE))
    #                     else:
    #                         s = "under " +str(-round((limit-1)*CROP_SIZE))
    #                     print("out-of-borders tile: {}! file: {}".format(s, os.path.basename(file)))
    #                     os.remove(os.path.join(output_path,d).replace(".txt", "_0.png"))
    #                     os.remove(os.path.join(output_path,d).replace(".txt", "_1.png"))
    #                     os.remove(os.path.join(output_path,d))
    #                     break

                
    #             # a = b = False
    #             # # print("length: " + str(len(lines[0].split(' '))) + " line:", lines[0].split(' '))   
    #             # if len(lines) != 2:
    #             #     a = True
    #             # if len(lines[0].split(' ')[:-1]) != 5:
    #             #     b = True
    #             # if a or b:
    #             #     count += 1
    #             #     print("Error in " + os.path.join(d, file) + ":")
    #             #     if a:
    #             #         print("has {} than two lines!".format("more" if len(lines) > 2 else "less"))
    #             #     if b:
    #             #         print("has too {} elements in a line!".format("few" if len(lines[0].split(' ')[:-1]) < 5 else "many"))
    #             #     # for line in lines:
    #             #     #     print(line)
    #     print(d, ":", count)
    #     total_count += count
    # print("total:", total_count)

    ### REMOVING BAD ANNOTATION-FILES ###
    # count = 0
    # debug_file = "/home/shira/amram/Documents/BP_concecutive_crops_debug" # manually made txt file, containing a list of bad annotation-files
    # path = "/mnt/share/Local/OfflineDB/ConsecutiveFusedCrops/GA/300/CMOS/BlackPanther"
    # suffixes = [".png", ".txt", "_Layer0.txt", "_Layer1.txt", "_Layer2.txt", "_Next.png"]
    # df = open(debug_file, "r")
    # lines = [line for line in df.readlines() if len(line) > 0]
    # df.close()
    # for line in lines:
    #     for s in suffixes:
    #         file = os.path.join(path, line[:-1] + s)
    #         if os.path.isfile(file):
    #             os.remove(file)
    #     count += 1
    # print(count, "files have been removed")
    
    ### SEARCHING FOR OUT-OF-BORDERS ANNOTATION FILES ###
    # ''' reads old lists, finds lines referring to 'bad crops' (with exceeding annotations), creates new lists with 'good crops' only. '''
    # training_crop_list = "/mnt/share/Local/OfflineDB/DLRuns/108_0/Training/AG_VIS_300_181020_MultiClass_Training.txt"
    # validation_crop_list = "/mnt/share/Local/OfflineDB/DLRuns/108_0/Training/AG_VIS_300_181020_MultiClass_Validation.txt"
    # output_training_path = "/mnt/share/Local/OfflineDB/DLRuns/108_0/Training/AG_VIS_300_181020_MultiClass_new_Training.txt"
    # current_prefix = "/home/shwarzeneger/workspace/share/storage_server"
    # # current_prefix = "/mnt/dbserver"
    # new_prefix = "/home/shwarzeneger/workspace/share/storage_server"
    # limit = 1
    # get_out_of_borders_tiles(crop_lists=[training_crop_list, validation_crop_list], output_path=output_training_path, current_prefix=current_prefix, new_prefix=new_prefix, limit=limit)
    
    
