import numpy as np
import cv2
import os
import random

debug_flag = False
CROP_SIZE = 300

def write_annotations(gt_file, output_name):
    input_file = open(gt_file, "r")
    text = input_file.read()
    input_file.close()
    for i in range(4):
        extension = "_Layer{}.txt".format(i) if i < 3 else ".txt"
        output_file = open(output_name + extension, "w")
        output_file.write(text)
        output_file.close()


def process_crop(crop_path, output_name):
    crop = cv2.imread(crop_path)
    if crop is None:
        return
    grayscale = cv2.cvtColor(crop, cv2.COLOR_BGR2GRAY)
    fused_crop = np.array([grayscale, grayscale, grayscale])
    fused_crop = np.transpose(fused_crop, axes=(1, 2, 0))
    cv2.imwrite(output_name + ".png", fused_crop, [cv2.IMWRITE_PNG_COMPRESSION, 0])
    cv2.imwrite(output_name + "_Next.png", crop, [cv2.IMWRITE_PNG_COMPRESSION, 0])

def show_crop_with_annotations(gt_file):
    crop = cv2.imread(gt_file.replace(".txt", ".png", 1))
    debug_crop = crop.copy()
    
    f = open(gt_file, "r")
    lines = f.readlines()
    f.close()
    for i in range(len(lines)):
        line = lines[i].split(' ')
        cx = round(CROP_SIZE*float(line[1]))
        cy = round(CROP_SIZE*float(line[2]))
        w = round(CROP_SIZE*float(line[3]))
        h = round(CROP_SIZE*float(line[4]))
        left = round(cx - w/2)
        top = round(cy - h/2)
        tile = [left, top, w, h]
        debug_crop = cv2.rectangle(img=debug_crop, pt1=(int(tile[0]), int(tile[1])), pt2=(int(tile[0]+tile[2]), int(tile[1]+tile[3])), color=(0,255,0), thickness=1)
        cv2.putText(img=debug_crop, text=line[0] + " #" + str(i), org=(left, top-5), fontFace=cv2.FONT_HERSHEY_COMPLEX, color=(0,0,255), thickness=1, fontScale=0.5)
    
    cv2.imshow("Debug Crop", debug_crop)
    cv2.waitKey(0)


def show_random_crops(folder_list, num_of_random_crops):
    for folder in folder_list:
        gt_files = [os.path.join(folder, file) for file in os.listdir(folder) if file.endswith(".txt") and "Layer" not in file]
        for gt_file in random.sample(gt_files, num_of_random_crops):
            show_crop_with_annotations(gt_file=gt_file)

if __name__ == '__main__':

    # input_path = "/home/shira/amram/test/INPUT"
    # gt_path = "/home/shira/amram/test/GT"
    # output_path = "/home/shira/amram/test/OUTPUT"

    input_basename = "/mnt/share/Local/OfflineDB/SSDMobileNet_VISdata"
    gt_basename = "/mnt/share/Local/OfflineDB/SSDMobileNet_VISdata" 
    output_path = "/mnt/share/Local/OfflineDB/ConsecutiveFusedCrops/VISdata"
    
    crop_count = 0
    failed_crops = []
    folders = [os.path.join(input_basename,dI) for dI in os.listdir(input_basename) if dI.startswith('VIS') and os.path.isdir(os.path.join(input_basename,dI))]
    num_of_folders = len(folders)
    for j in range(num_of_folders):
        folder = folders[j]
        output_folder = os.path.join(output_path, os.path.basename(folder))
        try:
            os.mkdir(output_folder)
        except:
            pass
        print("folder {}/{}: {} is being processed...".format(j+1, num_of_folders, os.path.basename(folder)))
        crops = []
        gt_files = []
        for file in os.listdir(folder):
            if file.endswith('.png'):
                crops.append(os.path.join(folder,file))
            elif file.endswith('.txt'):
                gt_files.append(os.path.join(folder,file))
        assert len(crops) == len(gt_files)
        length = len(crops)
        for i in range(length):
            if i%100 == 0:
                print("folder {}/{}: {}/{} crops have been processed".format(j+1, num_of_folders, i, length))
            try:
                output_name = os.path.join(output_folder, os.path.basename(crops[i])).replace(".png", "")
                process_crop(crop_path=crops[i], output_name=output_name)
                write_annotations(gt_file=gt_files[i], output_name=output_name)
                if debug_flag:
                    show_random_crops(folder_list=folders, num_of_random_crops=3)
                crop_count += 1
            except:
                failed_crops.append("{}/{}".format(os.path.basename(folder), os.path.basename(crops[i])))

    print("\ntotal crop_count =", crop_count)
    print(str(len(failed_crops))+" failed crops!")
    for crop_str in failed_crops:
        print(crop_str)
    