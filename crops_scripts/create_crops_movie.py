import cv2
import os
import numpy as np


input_path      = "/mnt/share/Local/SharingIsCaring/LiorMathan/detections_numeric_validation/MIXsetAllScenarios_500crops.txt"
output_path     = "/mnt/share/Local/SharingIsCaring/LiorMathan/detections_numeric_validation/MIXsetAllScenarios_500crops.rgb"
width           = 300
height          = 300
output_format   = os.path.splitext(output_path)[-1]


def create_movie():
    if output_format == ".mp4":
        fourcc          = cv2.VideoWriter_fourcc(*'mp4v')
        video_writer    = cv2.VideoWriter(output_path, fourcc, 5, (width, height))
    elif output_format == ".rgb":
        out_file = open(output_path, 'wb')

    with open(input_path, 'r') as f:
        crops_filenames = f.read().splitlines()
        
    for crop_filename in crops_filenames:
        crop = cv2.imread(crop_filename)

        if output_format == ".mp4":
            video_writer.write(crop)
        elif output_format == ".rgb":
            crop = cv2.cvtColor(crop, cv2.COLOR_RGB2BGR)
            binary_crop = np.ascontiguousarray(crop)
            out_file.write(binary_crop)

    if output_format == ".mp4":
        video_writer.release()
    elif output_format == ".rgb":
        out_file.close()


if __name__ == '__main__':
    create_movie()