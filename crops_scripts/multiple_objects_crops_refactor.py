import numpy as np
import cv2
import os
import sys
import platform
from Reader import *
from tkinter import messagebox
import pandas as pd
import ProgressBar
import random
import png
from Target import Target

# Description:
# This script creates crops (multiple objects) from annotated movie/frames.
# The output annotations files are in YOLO format.

# Usage:
# python3 multiple_objects_crops.py <input path> <gt path> <output path> <crop width> <crop height> 
#                                   (optional:) <frame width> <frame height> <project prefix> <change polarity> <old chimera> <visdrone> <labels conversion file> <vmd maps path>

# Example:
# python3 multiple_objects_crops.py "Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\High_Altitude\0001_170520_0001_Air_F8.5_T1700_Kfar Neter_A120_D90\Thermal_RAW_Movies_1\00023_r0_8.raw2" 
#                                   "Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\GT\00023_r0_8_gt" 
#                                   "Z:\OfflineDB\YOLO_AG\DualAir\Thermal\High_Altitude" 
#                                   416 416 640 480 DualAir


######### CONFIG: #########
debug_flag = False
use_inclusive_tiles = True
PEDESTRIAN_LABEL = 0
wait_time = 1
raw_crops = False
from_dual_GT = False
write_png16b = False

class CropsTool:
    def __init__(self, input_path, gt_path, output_path, crop_width, crop_height, frame_width=None, frame_height=None, project_prefix=None, change_polarity=False, is_old_chimera=False, is_visdrone=False, labels_conversion_file=None, vmd_maps_path=None, second_gt_path=None):
        #Inputs
        self.input_path = input_path
        self.gt_path = gt_path
        self.output_path = output_path
        self.crop_width = crop_width
        self.crop_height = crop_height
        self.frame_width = frame_width
        self.frame_height = frame_height
        if (self.frame_width == 0):
            self.frame_width = None
            self.frame_height = None
        self.change_polarity = change_polarity
        self.is_old_chimera = is_old_chimera
        self.is_visdrone = is_visdrone
        self.second_gt_path = second_gt_path
        self.project_prefix = project_prefix
        self.vmd_maps_path = vmd_maps_path
        
        # Parameters
        self.labels_conversion_table = self.get_table_from_file(labels_conversion_file, delimiter=',')
        self.min_objects_in_tile = 1
        self.max_objects_in_tile = 100
        self.min_pedestrian_area = 5 * 12
        self.inclusive_tile_edge_width = 12

        # Local setup
        if vmd_maps_path == "None":
            self.vmd_maps_path = None
        _,self.extension = os.path.splitext(self.input_path)
        self.basename = os.path.basename(self.input_path)
        
        if len(self.extension) > 0:
            self.basename = os.path.basename(self.input_path)[:-len(self.extension)]
        if self.project_prefix is not None:
            self.basename = self.project_prefix + "_" + self.basename
        if self.is_visdrone is False:
            self.output_path = os.path.join(self.output_path, self.basename)

        self.reader = Reader(path1=self.input_path, width1=self.frame_width, height1=self.frame_height)
        self.frames_done = 0
        self.progress_bar = ProgressBar.ProgressBar(self.reader.num_frames1, bar_length=100, end_char='\n')
        self.clear_command = ''

        if "linux" in sys.platform:
            self.clear_command = 'clear'
        elif sys.platform == "darwin":
            pass
        elif sys.platform == "win32":
            self.clear_command = 'cls'


    def create_vmd_crop(self, index, crop_coor, object_index, basename, second_input=False):
        # vmd_filename = os.path.join(self.vmd_maps_path, "ELTBinMap_" + basename + ".binmap")
        vmd_filename = self.vmd_maps_path
        if second_input is True:
            width = self.reader.width2
            height = self.reader.height2
        else:
            width = self.reader.width1
            height = self.reader.height1
        vmd = utils.read_binary_map(map=vmd_filename, index=index, width=width, height=height, normalize=False)
        vmd_crop = vmd[crop_coor[0]:crop_coor[0]+self.crop_height, crop_coor[2]:crop_coor[2]+self.crop_width]
        if debug_flag is True:
            vmd_debug = utils.read_binary_map(map=vmd_filename, index=index, width=width, height=height, normalize=True)
            vmd_crop_debug = vmd_debug[crop_coor[0]:crop_coor[0]+self.crop_height, crop_coor[2]:crop_coor[2]+self.crop_width]
            cv2.imshow("VMD Crop", vmd_crop_debug)
            cv2.waitKey(wait_time)

        # if is_dual is True:
        #     if second_input is True:
        #         suffix = "_vis.binmap"
        #     else:
        #         suffix = "_thermal.binmap"
        else:
            suffix = ".binmap"
        out_filename = os.path.join(self.output_path, self.basename + '_' + str(index).zfill(6) + "_" + str(object_index).zfill(3) + suffix)
        with open(out_filename, "wb") as f:
            vmd_crop = np.ascontiguousarray(vmd_crop)
            f.write(vmd_crop)     
    

    def validate_input(self):
        if not os.path.exists(self.input_path):
            # messagebox.showinfo("Input Error", "Input Path Does Not Exists!")
            print("Input Path Does Not Exists!")
            exit()
        
        if not os.path.exists(self.gt_path):
            # messagebox.showinfo("Input Error", "GT Path Does Not Exists!")
            print("GT Path Does Not Exists!")
            exit()


    def get_table_from_file(self, filename, dtype='i', delimiter=','):
        if filename is None or not os.path.exists(filename):
            table = None
        else:
            if self.is_visdrone is True:
                table = np.loadtxt(filename, dtype=np.float32, delimiter=delimiter)
            else:
                table = pd.read_csv(filename, dtype=dtype, sep=delimiter, header=None)
            
            if 'numpy.float32' in str(type(table[0])):
                table = [table]
            table = np.array(table)

        return table


    def get_visdrone_format_annotations(self, yolo_annotations):
        visdrone_annotation = []
        for i in range(len(yolo_annotations)):
            left = int(yolo_annotations[i][1] * self.reader.width1 - ((yolo_annotations[i][3] * self.reader.width1) / 2))
            top = int(yolo_annotations[i][2] * self.reader.height1 - ((yolo_annotations[i][4] * self.reader.height1) / 2))
            width = int(yolo_annotations[i][3] * self.reader.width1)
            height = int(yolo_annotations[i][4] * self.reader.height1)
            label = int(yolo_annotations[i][0])
            new_annotation = np.array([left, top, width, height, 0, label])
            visdrone_annotation.append(new_annotation.astype(int))
        
        return np.array(visdrone_annotation)
    

    def get_updated_label(self, label):
        if self.is_old_chimera is True:
            if label == 6: # human
                label = 0
            elif label == 7: # vehicle
                label = 3

        if self.labels_conversion_table is not None:
            label = self.labels_conversion_table[int(label)][1]
        
        return label
    

    def get_included_annotations(self, annotations, tile, width, height):
        # tile is [left, top, width, height]
        included = []

        for i in range(len(annotations)):
            try:
                if use_inclusive_tiles is True:
                    # include only objects that have at least <self.inclusive_tile_edge_width> pixels inside the crop:
                    if (annotations[i][0] + annotations[i][2] < tile[0] + self.inclusive_tile_edge_width):
                        continue
                    if (annotations[i][1] + annotations[i][3] < tile[1] + self.inclusive_tile_edge_width):
                        continue
                    if (annotations[i][0] > tile[0] + tile[2] - self.inclusive_tile_edge_width):
                        continue
                    if (annotations[i][1] > tile[1] + tile[3] - self.inclusive_tile_edge_width):
                        continue
                else:
                    # include only objects that their center is inside the crop:
                    cx = annotations[i][0] + (annotations[i][2] / 2)
                    cy = annotations[i][1] + (annotations[i][3] / 2)
                    if (cx < tile[0] or cx > tile[0] + tile[2]):
                        continue
                    if (cy < tile[1] or cy > tile[1] + tile[3]):
                        continue
            except:
                continue

            # update annotations to new tile:
            start_x = max(tile[0], annotations[i][0])
            start_y = max(tile[1], annotations[i][1])
            end_x = min(tile[0] + tile[2], annotations[i][0] + annotations[i][2])
            end_y = min(tile[1] + tile[3], annotations[i][1] + annotations[i][3])
            updated_width = end_x - start_x
            updated_height = end_y - start_y
            
            yolo_cx = (start_x - tile[0] + (updated_width / 2)) / width
            yolo_cy = (start_y - tile[1] + (updated_height / 2)) / height
            yolo_w = updated_width / width
            yolo_h = updated_height / height
            label = annotations[i][5]
            label = int(self.get_updated_label(label))

            if label == -1:
                continue
            # disqualify pedestrians with area smaller than minimum:
            if self.is_visdrone is True:
                if label == PEDESTRIAN_LABEL:
                    if ((updated_width * updated_height) < self.min_pedestrian_area):
                        continue

            included_annotation = [label, yolo_cx, yolo_cy, yolo_w, yolo_h]
            included.append(included_annotation)

        return (included)
    

    def has_3_duplicated_channels(self, crop):
        for i in range(len(crop)):
            for j in range(len(crop[i])):
                if len(set(crop[i][j])) != 1:
                    return False
        return True


    def write_crop(self, crop, filename, png16b=write_png16b):
        success = 1
        is_thermal = "_r0_" in filename

        if png16b and is_thermal: # save image as uint16 - can be read with io.imread(filename)
            # cast crop to int32 + 2^15, then to uint16
            
            crop = crop.astype(np.int32)
            crop += 32768
            crop = crop.astype(np.uint16)
            assert self.has_3_duplicated_channels(crop) == True
            crop = cv2.cvtColor(crop, cv2.COLOR_BGR2GRAY)
            filename = filename.replace(".raw2", ".png")

        if filename.endswith(".png"):
            cv2.imwrite(filename, crop, [cv2.IMWRITE_PNG_COMPRESSION, 0])
            # delete crop if writing had failed:
            if png16b:
                crop_read = cv2.imread(filename, cv2.IMREAD_UNCHANGED)       
            else:
                crop_read = cv2.imread(filename)
            if crop_read is None:
                print("Bad crop: " + filename)
                os.remove(filename)
                success = -1
            elif png16b:
                if is_thermal:
                    assert crop_read.dtype == np.uint16 and crop_read.size == self.crop_height * self.crop_width
                else:
                    assert crop_read.dtype == np.uint8 and crop_read.size == self.crop_height * self.crop_width * crop_read.shape[2]

        else:
            with open(filename, "wb") as f:
                print(filename)
                crop = np.ascontiguousarray(crop, dtype=np.int16)
                f.write(crop)

        return success


    def show_labaled_crop(self, crop, crop_filename, included_annotations):
        if debug_flag is True:
            debug_crop = crop.copy()
            if raw_crops is True:
                debug_crop = cv2.normalize(debug_crop, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
                debug_crop = cv2.cvtColor(debug_crop, cv2.COLOR_GRAY2BGR)
                with open(crop_filename, "rb") as f:
                    original_crop = np.fromfile(f, dtype=np.int16, count=self.crop_height*self.crop_width)
                    original_crop = original_crop.reshape((self.crop_height, self.crop_width))
                    original_crop = cv2.normalize(original_crop, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
                    original_crop = cv2.cvtColor(original_crop, cv2.COLOR_GRAY2BGR)
            else:
                original_crop = cv2.imread(crop_filename)
            for obj in range(len(included_annotations)):
                target = Target(label=included_annotations[obj][0], cx=included_annotations[obj][1]*self.crop_width, cy=included_annotations[obj][2]*self.crop_height, width=included_annotations[obj][3]*self.crop_width, height=included_annotations[obj][4]*self.crop_height, label_set="AG")
                target.draw(debug_crop, text=str(target.get_label()))
                # original_crop = cv2.rectangle(img=original_crop, pt1=start, pt2=end, color=(255, 255, 255), thickness=1)
                # cv2.putText(img=original_crop, text=str(included_annotations[obj][0]), org=(start[0], start[1]-5), fontFace=cv2.FONT_HERSHEY_COMPLEX, color=(255, 255, 255), thickness=1, fontScale=0.3)
            cv2.imshow("Objects on crop", debug_crop)
            # cv2.imshow("Original crop", original_crop)
            cv2.waitKey(wait_time)


    def get_annotations_from_file(self, filename, tile, width, height):
        included_annotations = []
        array_format_annotations = []

        try:
            if self.is_visdrone is True:
                delimiter = ','
            else:
                delimiter = ' '
            
            array_format_annotations = self.get_table_from_file(filename=filename, dtype='f', delimiter=delimiter)
            if self.is_visdrone == False:
                array_format_annotations = self.get_visdrone_format_annotations(array_format_annotations)
        except:
            return included_annotations
        
        included_annotations = self.get_included_annotations(array_format_annotations, tile=tile, width=width, height=height)
        
        return (included_annotations, array_format_annotations)


    def crops_for_frame(self, index, gts_filename, ann_summary_file, crops_summary_file, random_ratio=0.25):
        i = index
        os.system(self.clear_command)

        self.progress_bar.print_progress(i + 1, prefix=f'{self.basename} is being processed...\n')

        try:
            current_gt_filename = os.path.join(self.gt_path, gts_filename[i])
            if (os.stat(current_gt_filename).st_size == 0):
                return
        except:
            return

        if raw_crops is True or '_r0_' in self.basename or write_png16b:
            frame = self.reader.get_frame(index=i, bgr=False)
        else:
            frame = self.reader.get_frame(index=i, bgr=True)
        if frame is None:
            return
        if (frame.shape[0] < self.crop_height or frame.shape[1] < self.crop_width):
            return

        try:
            if self.is_visdrone is True:
                delimiter = ','
            else:
                delimiter = ' '
            annotations = self.get_table_from_file(filename=current_gt_filename, dtype='f', delimiter=delimiter)
            if self.is_visdrone == False:
                annotations = self.get_visdrone_format_annotations(annotations)
        except:
            return

        if debug_flag is True:
            debug_frame = frame.copy()
            if raw_crops is True:
                debug_frame = cv2.normalize(debug_frame, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
                debug_frame = cv2.cvtColor(debug_frame, cv2.COLOR_GRAY2BGR)
            all_annotations, array_format_annotations = self.get_annotations_from_file(filename=current_gt_filename, tile=[0, 0, frame.shape[1], frame.shape[0]], width=frame.shape[1], height=frame.shape[0])
            for obj in range(len(all_annotations)):
                target = Target(label=all_annotations[obj][0], cx=int(all_annotations[obj][1]*frame.shape[1]), cy=int(all_annotations[obj][2]*frame.shape[0]), width=int(all_annotations[obj][3]*frame.shape[1]), height=int(all_annotations[obj][4]*frame.shape[0]), label_set="AG")
                target.draw(debug_frame)
            
            if from_dual_GT:
                original_all_annotations, array_format_original_annotations = self.get_annotations_from_file(filename=os.path.join(self.second_gt_path, gts_filename[i]), tile=[0, 0, frame.shape[1], frame.shape[0]], width=frame.shape[1], height=frame.shape[0])
                for obj in original_all_annotations:
                    target = Target(label=obj[0], cx=int(obj[1]*frame.shape[1]), cy=int(obj[2]*frame.shape[0]), width=int(obj[3]*frame.shape[1]), height=int(obj[4]*frame.shape[0]), label_set="AG")
                    target.draw(debug_frame, color=(160, 75, 115))
        
        try:
            min_values = np.amin(a=annotations, axis=0)
            max_values = np.amax(a=annotations, axis=0)

            right_most_annotation = []
            bottom_most_annotation = []
            for annotation in annotations:
                if annotation[0] == max_values[0]: 
                    right_most_annotation = annotation

                if annotation[1] == max_values[1]: 
                    bottom_most_annotation = annotation
                    
                if len(bottom_most_annotation) > 0 and len(right_most_annotation) > 0:
                    break

            min_x   = max(0, min_values[0] - self.inclusive_tile_edge_width)
            min_y   = max(0, min_values[1] - self.inclusive_tile_edge_width)
            max_x   = max_values[0] + right_most_annotation[2] + self.inclusive_tile_edge_width
            max_y   = max_values[1] + bottom_most_annotation[3] + self.inclusive_tile_edge_width
            range_x = max_x - min_x
            range_y = max_y - min_y
            
            tiles_x = max(int(np.ceil(range_x / self.crop_width)), 1)
            tiles_y = max(int(np.ceil(range_y / self.crop_height)), 1)
        except:
            return

        tiles_count = 0
        for k in range(tiles_x):
            for m in range(tiles_y):
                rand_left = random.randint(int(min_x + (k * self.crop_width) - (self.crop_height * random_ratio)), int(min_x + (k * self.crop_width) + (self.crop_height * random_ratio)))
                rand_top  = random.randint(int(min_y + (m * self.crop_height) - (self.crop_height * random_ratio)), int(min_y + (m * self.crop_height) + (self.crop_height * random_ratio)))
                rand_left = max(0, rand_left)
                rand_top  = max(0, rand_top)
                
                tile = [rand_left, rand_top, self.crop_width, self.crop_height]
                if tile[0] + tile[2] >= frame.shape[1]:
                    tile[0] = frame.shape[1] - self.crop_width
                if tile[1] + tile[3] >= frame.shape[0]:
                    tile[1] = frame.shape[0] - self.crop_height
                
                if debug_flag is True:
                    debug_frame = cv2.rectangle(img=debug_frame, pt1=(int(tile[0]), int(tile[1])), pt2=(int(tile[0]+tile[2]), int(tile[1]+tile[3])), color=(255, 0, 0), thickness=1)
                    cv2.imshow("Tile on frame", debug_frame)
                    cv2.waitKey(0)

                included_annotations = self.get_included_annotations(annotations, tile, width=self.crop_width, height=self.crop_height)
                if len(included_annotations) >= self.min_objects_in_tile and len(included_annotations) <= self.max_objects_in_tile:
                    # pedestrian_in_crop = False
                    # for ann in included_annotations:
                    #     if ann.label == PEDESTRIAN_LABEL:
                    #         pedestrian_in_crop = True
                    # if pedestrian_in_crop is False:
                    #     continue
                    if raw_crops is True and self.extension == ".raw2" or write_png16b :
                        crop = frame[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2]]
                    else:
                        crop = frame[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2], :]
                    if crop.shape[1] != self.crop_width or crop.shape[0] != self.crop_height:
                        continue
                    if self.change_polarity is True and raw_crops is False:
                        crop = 255 - crop

                    tiles_count += 1

                    if self.vmd_maps_path is not None:
                        self.create_vmd_crop(index=i, crop_coor=[tile[1], tile[1]+tile[3], tile[0], tile[0]+tile[2]], object_index=tiles_count, basename=self.basename)
                    if raw_crops is True:
                        # suffix = self.extension
                        suffix = ".png"
                    else:
                        suffix = ".png"
                    image_filename = os.path.join(self.output_path, self.basename + '_' + str(i).zfill(6) + "_" + str(tiles_count).zfill(3) + suffix)
                    new_gt_filename = image_filename.replace(suffix, '.txt')

                    success = self.write_crop(crop=crop, filename=image_filename)
                    if (success != -1):
                        with open(new_gt_filename, "w") as f:
                            for ann in included_annotations:
                                f.write(str(ann[0]) + ' ' + str(ann[1]) + ' ' + str(ann[2]) + ' ' + str(ann[3]) + ' ' + str(ann[4]) + ' \n')
                                ann_summary_file.write(str(i).zfill(6)+ ' ' + str(tiles_count) + ' ' + str(ann[0]) + ' ' + str(ann[1]) + ' ' + str(ann[2]) + ' ' + str(ann[3]) + ' ' + str(ann[4]) + ' \n')
                        # crop summery contains - top left corner of crop , crop width, crop height
                        crops_summary_file.write(str(i).zfill(6) + ' ' + str(tiles_count) + ' ' + str(tile[0]) + ' ' + str(tile[1]) + ' ' + str(tile[2]) + ' ' + str(tile[3]) + ' \n')

                        if from_dual_GT:
                            original_included_annotations = self.get_included_annotations(annotations, tile, width=self.crop_width, height=self.crop_height)
                            new_gt_filename_united = os.path.join(self.output_path, 'Dual_GT_United', os.path.basename(new_gt_filename))

                            with open(new_gt_filename_united, "w") as f:
                                for ann in original_included_annotations:
                                    f.write(str(ann[0]) + ' ' + str(ann[1]) + ' ' + str(ann[2]) + ' ' + str(ann[3]) + ' ' + str(ann[4]) + ' \n')

                        self.show_labaled_crop(crop=crop, crop_filename=image_filename, included_annotations=included_annotations)


    def create_crops(self):
        global from_dual_GT
        if self.second_gt_path is not None:
            from_dual_GT = True

        self.validate_input()
        try:
            os.mkdir(self.output_path)
        except:
            pass

        if from_dual_GT:
            second_gts_dir = os.path.join(self.output_path, 'Dual_GT_United')
            try:
                os.mkdir(second_gts_dir)
            except:
                pass

        gts_filename = os.listdir(self.gt_path)
        gts_filename.sort()
        
        ann_summary_file = open(os.path.join(os.path.dirname(self.output_path), "cropsMovie_" + self.basename + "_annotationSummary.txt"), "w+")
        crops_summary_file = open(os.path.join(os.path.dirname(self.output_path), "cropsMovie_"  + self.basename + "_cropSummary.txt"), "w+")

        indexes = [int(gt[-10:-4]) for gt in gts_filename]
        for i in (indexes):
            self.crops_for_frame(index=i, gts_filename=gts_filename, ann_summary_file=ann_summary_file, crops_summary_file=crops_summary_file, random_ratio=0.3)


def str_to_bool(string):
    if 'True' in string or 'true' in string:
        return True
    else:
        return False


if __name__ == '__main__':
    cropper = CropsTool(input_path=sys.argv[1],
                        gt_path=sys.argv[2],
                        output_path=sys.argv[3],
                        crop_width=int(sys.argv[4]), 
                        crop_height=int(sys.argv[5]),
                        frame_width=int(sys.argv[6]),
                        frame_height=int(sys.argv[7]),
                        project_prefix=sys.argv[8],
                        change_polarity=str_to_bool(sys.argv[9]),
                        second_gt_path=sys.argv[10])

    # cropper = CropsTool(input_path="/mnt/share/Local/OfflineDB/external/visdrone2020/VisDrone2019-DET-test-dev/images",
    #                     gt_path="/mnt/share/Local/OfflineDB/external/visdrone2020/VisDrone2019-DET-test-dev/annotations",
    #                     output_path="/mnt/share/Local/OfflineDB/data-pre-processing-scripts/crops_scripts/test",
    #                     crop_width=300, 
    #                     crop_height=300,
    #                     frame_width=720,
    #                     frame_height=576,
    #                     project_prefix="Test",
    #                     is_old_chimera=False,
    #                     change_polarity=False,
    #                     vmd_maps_path=None,
    #                     original_gt_path=None,
    #                     is_visdrone=True, 
    #                     labels_conversion_file="/mnt/share/Local/OfflineDB/external/visdrone2020/convertVisdrone2avatar.txt")


    # cropper = CropsTool(input_path="/mnt/share/ssdHot/WorkMovies/AG/High_Altitude/0010_231020_0078_Air_VGA_F19-4.3_T0900_A100-160_D45_70_KefarNeter/Thermal_RAW_Movies_0/00780_r0_3.raw2",
    #                     gt_path="/mnt/share/ssdHot/WorkMovies/AG/Dual_GT/NMS_NotUnited/00780_r0_3_gt",
    #                     output_path="/mnt/share/Local/OfflineDB/data-pre-processing-scripts/crops_scripts/test",
    #                     crop_width=300, 
    #                     crop_height=300,
    #                     frame_width=640,
    #                     frame_height=480,
    #                     project_prefix="Test",
    #                     is_old_chimera=False,
    #                     change_polarity=False,
    #                     vmd_maps_path=None)

    # cropper = CropsTool(input_path=r"X:\WorkMovies\AG\High_Altitude\0010_231020_0078_Air_VGA_F19-4.3_T0900_A100-160_D45_70_KefarNeter\Thermal_RAW_Movies_0\00780_r0_3.raw2",
    #                     gt_path=r"X:\WorkMovies\AG\Dual_GT\NMS_NotUnited\00780_r0_3_gt",
    #                     output_path=r"Z:\OfflineDB\data-pre-processing-scripts\crops_scripts\test",
    #                     crop_width=300, 
    #                     crop_height=300,
    #                     frame_width=640,
    #                     frame_height=480,
    #                     project_prefix="Test",
    #                     is_old_chimera=False,
    #                     change_polarity=False,
    #                     vmd_maps_path=None)
    #                     # second_gt_path="/mnt/share/ssdHot/WorkMovies/AG/Dual_GT/NMS_United/00780_r0_3_gt")
    
    print(cropper.basename + " is being processed...")
    cropper.create_crops()
    
    # python3 multiple_objects_crops.py "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground/Avatar/0001_090120_0001_Car_F19_T1430_KiriatASharon/Thermal_RAW_Movies_0/00010_r0_0.raw2" "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground/Archive/GT_Dual/00010_r0_0_gt" "/mnt/share/Local/OfflineDB/Crops/SSDMobileNet/GG/Avatar/Dual_CMOS_for_Thermal" 300 300 640 480 DualGround