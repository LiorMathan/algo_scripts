import sheets_utils
from multiple_objects_crops_UI import CropManager
import time
import os 
import threading

SCOPES = [
    'https://www.googleapis.com/auth/spreadsheets.readonly'
]

CONFIG_SHEET = 'Cropper Config'
CONFIG_RANGE = '!A1:B5'
WORK_SHEET = 0
MOVIES_RANGE = 1
STATUS_RANGE = 2
GT_RANGE = 3
CROPPER_CONFIG = 4

SPREADSHEET_ID = '1ll7-UBXtXYoq8ss3vk53tlzmb746J0KffGKyiLJEbpQ'
    
class Cropper:
    def __init__(self):
        self.service = sheets_utils.get_service('credentials.json', SCOPES)
        self.sheet = self.service.spreadsheets()
        self.manager = None

        self.movies = []
        self.movies_queued = []
        self.movies_done = []

        self.gts = []
        self.stati = []
        self.status_range = []

        self.last_output = ''

        if os.sys.platform.startswith('linux'):
            self.clear_command = 'clear'
        elif os.sys.platform == 'win32':
            self.clear_command = 'cls'


    def _read_from_sheet(self, read_range):
        result = self.sheet.values().get(spreadsheetId=SPREADSHEET_ID, range=read_range).execute()
        return result.get('values', [])


    def read_configuration(self):
        print('Loading config...')
        sheet_config = self._read_from_sheet(CONFIG_SHEET + CONFIG_RANGE)
        self.status_range = sheet_config[STATUS_RANGE][1]

        self.movies = self._read_from_sheet(sheet_config[WORK_SHEET][1] + '!' + sheet_config[MOVIES_RANGE][1])
        for i in range(0, len(self.movies)):
            self.movies[i] = self.movies[i][0]

        self.gts = self._read_from_sheet(sheet_config[WORK_SHEET][1] + '!' + sheet_config[GT_RANGE][1])
        for i in range(0, len(self.gts)):
            self.gts[i] = self.gts[i][0]
        
        self.stati = self._read_from_sheet(sheet_config[WORK_SHEET][1] + '!' + self.status_range)
        for i in range(0, len(self.stati)):
            self.stati[i] = self.stati[i][0]

        config = sheet_config[CROPPER_CONFIG][1]
        self.manager = CropManager(config)


    def queue_print(self, data):
        if data != self.last_output:
            os.system(self.clear_command)
            print(data)
            self.last_output = data


    def print_progress(self):
        while True:
            data = self.manager.get_progress()
            data += 'Capacity - ' + str(self.manager.get_pss_number()) + '\n'
            self.queue_print(data)


    def proccess_crops(self, amount, parallel_limit):
        print('Starting crops...')
        movies_done = 0

        self.manager.start(False)
        threading.Thread(target=self.print_progress, daemon=True).start()
        
        for i in range(0, len(self.movies)):
            if self.stati[i] == 'TRUE' or self.gts[i] == 'FALSE':
                continue
            
            if amount == 0:
                break
                
            while self.manager.get_pss_number() >= parallel_limit:
                time.sleep(1)
            
            movie = self.movies[i].split('_')
            self.manager.set_movie(movie[0], movie[1][1], movie[2])
            self.manager.find_movie()
            self.manager.build_and_run_command()
            amount -= 1

        
        while self.manager.get_pss_number() > 0:
            pass

            
if __name__ == '__main__':
    cropper = Cropper()
    cropper.read_configuration()
    cropper.proccess_crops(int(os.sys.argv[1]), int(os.sys.argv[2]))