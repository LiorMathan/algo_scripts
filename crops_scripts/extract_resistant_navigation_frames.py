import cv2
import os
import numpy as np
import csv
import sys
import ProgressBar
from RawUtils import *

csv_only = True
GIS_flag = False


def extract_resistant_navigation_frames():
    # external_folder = r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0001_210720_0047_Air_VGA_F8.5_F2.8_TXXXX_A50_D45-80_Arsuf_Resistant Navigation\1500_A50_D45-80\CMOS_RAW_Movies_0"
    # output_path = r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0001_210720_0047_Air_VGA_F8.5_F2.8_TXXXX_A50_D45-80_Arsuf_Resistant Navigation\1500_A50_D45-80\CMOS_RAW_Movies_0_Frames"
    external_folder = sys.argv[1]
    output_path = external_folder + "_Frames"
    try:
        os.mkdir(output_path)
    except:
        pass

    for file in os.listdir(external_folder):
        if file.endswith(".rawc"):
            movie_path = os.path.join(external_folder, file)
            propsNav_path = movie_path.replace(".rawc", ".propsNav")
            propsNav_path = propsNav_path.replace("r1", "r0")
            propsNav_path = propsNav_path.replace("r2", "r0")
            props_file = open(propsNav_path, "r")
            gt_output_path = file.replace(".rawc", ".csv")
            gt_output_path = os.path.join(output_path, gt_output_path)
            output = open(gt_output_path, 'w', newline='')
            writer = csv.writer(output, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(['Frame', 'Lat', 'Lon', 'Speed', 'Heading'])
            num_frames = get_number_of_frames_in_binary_file(filename=movie_path, frame_bytes_size=576*720*1.5)
            current_output = os.path.join(output_path, file.split('.')[0])
            try:
                os.mkdir(current_output)
            except:
                if csv_only is True:
                    pass
                else:
                    continue
            progress_bar = ProgressBar.ProgressBar(num_frames, bar_length=75)

            for j in range(num_frames):
                os.system('clear')
                progress_bar.print_progress(j + 1, prefix=file + " is being processed...\n",suffix='(Frame ' + str(j + 1) + ' out of ' + str(num_frames) + ')')
                frame_name = os.path.join(current_output, file.split('.')[0] + "_" + str(j).zfill(6) + ".png")
                if csv_only is False:
                    frame = read_frame(start=j, filename=movie_path, height=576, width=720, bgr=True)
                    cv2.imwrite(frame_name, frame, [cv2.IMWRITE_PNG_COMPRESSION, 0])
                props_line = props_file.readline()
                props_elements = props_line.split(',')
                lat = props_elements[-2]
                lon = props_elements[-1][:-1]
                speed = props_elements[1]
                heading = props_elements[2]
                if "NaN" in lat or "NaN" in lon:
                    continue
                csv_frame_path = frame_name.replace("Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation", "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Resistant_Navigation")
                csv_frame_path = csv_frame_path.replace("\\", "/")
                writer.writerow([csv_frame_path, lat, lon, speed, heading])
            
            props_file.close()
            output.close()    

def extract_gis_frames():
    frames_folder = sys.argv[1]
    gt_file = sys.argv[2]
    gt_output_path = frames_folder + ".csv"
    output = open(gt_output_path, 'w', newline='')
    writer = csv.writer(output, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['Frame', 'Lat', 'Lon'])
    frames = {}

    for file in os.listdir(frames_folder):
        index = int(file.split('_')[1])
        frames[index] = os.path.join(frames_folder, file)
    
    progress_bar = ProgressBar.ProgressBar(len(frames), bar_length=75)

    with open(gt_file, "r") as f:
        index = 1
        line = f.readline()
        while(line):
            os.system('clear')
            progress_bar.print_progress(index, prefix=frames[index] + " is being processed...\n",suffix='(Frame ' + str(index) + ' out of ' + str(len(frames)) + ')')
            lat, lon = line.split(',')
            if 'NaN' not in lat and 'NaN' not in lon:
                try:
                    writer.writerow([frames[index], lat, lon[:-1]])
                except:
                    pass
            index += 1
            line = f.readline()
    
    output.close()


def merge_csv_files():
    # external_folders = [r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0001_210720_0047_Air_VGA_F8.5_F2.8_TXXXX_A50_D45-80_Arsuf_Resistant Navigation\1500_A50_D45-80\CMOS_RAW_Movies_0_Frames",
    #                     r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0001_210720_0047_Air_VGA_F8.5_F2.8_TXXXX_A50_D45-80_Arsuf_Resistant Navigation\1500_A50_D45-80\CMOS_RAW_Movies_1_Frames",
    #                     r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0001_210720_0047_Air_VGA_F8.5_F2.8_TXXXX_A50_D45-80_Arsuf_Resistant Navigation\1530_A50_D45-80\CMOS_RAW_Movies_1_Frames",
    #                     r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0001_210720_0047_Air_VGA_F8.5_F2.8_TXXXX_A50_D45-80_Arsuf_Resistant Navigation\1815_A50_D45-80\CMOS_RAW_Movies_0_Frames",
    #                     r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0002_040820_0051_Air_VGA_F8.5_T1630_A50_D45-60-75-90_Arsuf\CMOS_RAW_Movies_0_Frames",
    #                     r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0002_040820_0051_Air_VGA_F8.5_T1630_A50_D45-60-75-90_Arsuf\CMOS_RAW_Movies_1_Frames",
    #                     r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0003_040820_0054_Air_VGA_F8.5_T1930_A50_D45_Arsuf\CMOS_RAW_Movies_1_Frames",
    #                     r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0004_190820_0056_Air_T1500_A50_D75-90_Arsuf\CMOS_RAW_Movies_1_Frames",
    #                     r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0004_190820_0056_Air_T1500_A50_D75-90_Arsuf\CMOS_RAW_Movies_2_Frames"]
    
    external_folders = [r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0005_250820_0061_Air_T1300_F8.5-20_A50_D88_Arsuf\CMOS_RAW_Movies_0_Frames",
                        r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0005_250820_0061_Air_T1300_F8.5-20_A50_D88_Arsuf\CMOS_RAW_Movies_1_Frames",
                        r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0005_250820_0061_Air_T1300_F8.5-20_A50_D88_Arsuf\CMOS_RAW_Movies_2_Frames",
                        r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\0005_250820_0061_Air_T1300_F8.5-20_A50_D88_Arsuf\CMOS_RAW_Movies_3_Frames"]

    output = open(r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\Resistant_Navigation_0005.csv", 'w', newline='')
    writer = csv.writer(output, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['Frame', 'Lat', 'Lon', 'Speed', 'Heading'])

    for i in range(len(external_folders)):
        print(external_folders[i] + " is being processed...")
        for file in os.listdir(external_folders[i]):
            if file.endswith(".csv"):
                csv_file = open(os.path.join(external_folders[i], file), "r")
                line = csv_file.readline()
                line = csv_file.readline()
                while(line):
                    elements = line.split(',')
                    writer.writerow(elements)
                    line = csv_file.readline()
                csv_file.close()
    output.close()



# merge_csv_files()

if GIS_flag is True:
    extract_gis_frames()
else:
    extract_resistant_navigation_frames()
