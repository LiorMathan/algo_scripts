import numpy as np
import cv2
import os
import sys
from Reader import *
from Target import *
from Crop import *
import ProgressBar
import random
import shutil


class Cropper:
    def __init__(self, input_path, output_path, gt_path, crop_resolution, frame_resolution, \
                 prefix=None, second_gt_path=None, labels_conversion_file=None, random_ratio=0.25, suffix='.png'):
        self.input_path                 = input_path
        self.output_path                = output_path
        self.gt_path                    = gt_path
        self.crop_resolution            = crop_resolution
        self.frame_resolution           = frame_resolution
        self.prefix                     = prefix
        
        if second_gt_path is not None and second_gt_path == "None":
	        self.second_gt_path = None
        else:
	        self.second_gt_path = second_gt_path        
        
        self.random_ratio               = random_ratio
        self.suffix                     = suffix
        self.extension                  = os.path.splitext(self.input_path)[-1]
        self.basename                   = os.path.basename(self.input_path)

        if len(self.extension) > 0:
            self.basename               = os.path.basename(self.input_path)[:-len(self.extension)]
        if self.prefix is not None:
            self.basename               = self.prefix + "_" + self.basename
        self.output_path                = os.path.join(self.output_path, self.basename)

        self.validate_input()

        self.labels_conversion_file     = labels_conversion_file
        if labels_conversion_file:
            self.labels_conversion_dict = self.get_conversion_dictionary(labels_conversion_file)

        self.reader                     = Reader(path1=self.input_path, width1=self.frame_resolution[0], height1=self.frame_resolution[1])
        self.frame_resolution           = (self.reader.width1, self.reader.height1)

        self.frames_done                = 0
        self.progress_bar               = ProgressBar.ProgressBar(self.reader.num_frames1, bar_length=100, end_char='\n')
        self.clear_command              = ''

        self.annotations_summary_file   = None
        self.crops_summary_file         = None

        self.thermal_endian             = True
        self.png_16bit                  = "r0_" in self.basename and not self.thermal_endian
        self.label_set                  = "AG"
        self.use_inclusive_tiles        = True
        self.inclusive_tile_edge        = 12

        self.debug_display              = False
        self.wait_key                   = 100

    
    def get_conversion_dictionary(self, labels_conversion_file, delimiter=' '):
        conversion_dict = {}
        with open(labels_conversion_file, "r") as f:
            for line in f.readlines():
                tokens = line.split(delimiter)
                conversion_dict[int(tokens[0])] = int(tokens[1])
        
        return conversion_dict


    def validate_input(self):
        if not os.path.exists(self.input_path):
            print("Input Path Does Not Exists!")
            exit()
        if not os.path.exists(self.gt_path):
            print("GT Path Does Not Exists!")
            exit()


    def get_included_annotations(self, targets, tile):
        # tile is [left, top, width, height]
        included = []

        for i, target in enumerate(targets):
            try:
                if self.use_inclusive_tiles is True:
                    # include only objects that have at least <self.inclusive_tile_edge_width> pixels inside the crop:
                    if (target.cx + target.width / 2  < tile[0] + self.inclusive_tile_edge) or \
                       (target.cy + target.height / 2 < tile[1] + self.inclusive_tile_edge) or \
                       (target.cx - target.width / 2  > tile[0] + tile[2] - self.inclusive_tile_edge) or \
                       (target.cy - target.height / 2 > tile[1] + tile[3] - self.inclusive_tile_edge):
                            continue
                else:
                    # include only objects that their center is inside the crop:
                    if (target.cx < tile[0] or target.cx > tile[0] + tile[2]):
                        continue
                    if (target.cy < tile[1] or target.cy > tile[1] + tile[3]):
                        continue
            except:
                continue

            start_x = max(tile[0], target.cx - target.width / 2)
            start_y = max(tile[1], target.cy - target.height / 2)
            end_x = min(tile[0] + tile[2], target.cx + target.width / 2)
            end_y = min(tile[1] + tile[3], target.cy + target.height / 2)
            updated_width = end_x - start_x
            updated_height = end_y - start_y
            cx = start_x - tile[0] + (updated_width / 2)
            cy = start_y - tile[1] + (updated_height / 2)
            
            included_target = Target(label=target.label, cx=cx, cy=cy, width=updated_width, height=updated_height, label_set=target.label_set)
            included.append(included_target)

        return (included)
    

    def get_tile(self, left_most_annotation, top_most_annotation, row, col):
        min_x = int(left_most_annotation.cx - (left_most_annotation.width / 2) + (col * self.crop_resolution[0]) - (self.crop_resolution[0] * self.random_ratio))
        max_x = int(left_most_annotation.cx - (left_most_annotation.width / 2) + (col * self.crop_resolution[0]) + (self.crop_resolution[0] * self.random_ratio))
        min_y = int(top_most_annotation.cy  - (top_most_annotation.height / 2) + (row * self.crop_resolution[1]) - (self.crop_resolution[1] * self.random_ratio))
        max_y = int(top_most_annotation.cy  - (top_most_annotation.height / 2) + (row * self.crop_resolution[1]) + (self.crop_resolution[1] * self.random_ratio))
        left = max(0, random.randint(min_x, max_x))
        top  = max(0, random.randint(min_y, max_y))
                
        tile = [left, top, self.crop_resolution[0], self.crop_resolution[1]]
        
        if self.frame_resolution[0] < tile[2]:
            tile[0] = 0
        elif tile[0] + tile[2] >= self.frame_resolution[0]:
            tile[0] = self.frame_resolution[0] - self.crop_resolution[0]
        if self.frame_resolution[1] < tile[3]:
            tile[1] = 0
        elif tile[1] + tile[3] >= self.frame_resolution[1]:
            tile[1] = self.frame_resolution[1] - self.crop_resolution[1]

        return tile


    def get_top_left_bottom_right_annotations(self, annotations):
        right_most_annotation = left_most_annotation = top_most_annotation = bottom_most_annotation = annotations[0]
        
        for annotation in annotations:
            start, end = annotation.get_as_rectangle_start_end()
            if start[0] < left_most_annotation.cx - (left_most_annotation.width / 2):
                left_most_annotation = annotation
            if start[1] < top_most_annotation.cy - (top_most_annotation.height / 2):
                top_most_annotation = annotation
            if end[0] > right_most_annotation.cx + (right_most_annotation.width / 2):
                right_most_annotation = annotation
            if end[1] > bottom_most_annotation.cy + (bottom_most_annotation.height / 2):
                bottom_most_annotation = annotation
        
        return (right_most_annotation, left_most_annotation, top_most_annotation, bottom_most_annotation)


    def crop_frame(self, frame, tile):
        if len(frame.shape) > 2:
            crop_image = frame[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2], :]
        else:
            crop_image = frame[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2]]
        
        return crop_image


    def pad_frame(self, frame):
        if (self.frame_resolution[0] < self.crop_resolution[0]):
            frame = cv2.copyMakeBorder(frame, 0, 0, 0, self.crop_resolution[0]-self.frame_resolution[0], cv2.BORDER_REPLICATE, 0)
        if (self.frame_resolution[1] < self.crop_resolution[1]):
            frame = cv2.copyMakeBorder(frame, 0, self.crop_resolution[1]-self.frame_resolution[1], 0, 0, cv2.BORDER_REPLICATE, 0)

        return frame


    def create_crops_for_frame(self, index, frame, annotations, second_annotations=None):
        if len(annotations) == 0:
            return

        frame = self.pad_frame(frame)
        
        debug_frame = frame.copy()

        right_most_annotation, left_most_annotation, top_most_annotation, bottom_most_annotation = self.get_top_left_bottom_right_annotations(annotations)
        
        range_x = min(self.frame_resolution[0], (right_most_annotation.cx + (right_most_annotation.width / 2) + self.inclusive_tile_edge)) - \
                  max(0, (left_most_annotation.cx - (left_most_annotation.width / 2) - self.inclusive_tile_edge))
        range_y = min(self.frame_resolution[1], (bottom_most_annotation.cy + (bottom_most_annotation.height / 2) + self.inclusive_tile_edge)) - \
                  max(0, (top_most_annotation.cy - (top_most_annotation.height / 2) - self.inclusive_tile_edge))
            
        num_tiles_x = max(int(np.ceil(range_x / self.crop_resolution[0])), 1)
        num_tiles_y = max(int(np.ceil(range_y / self.crop_resolution[1])), 1)

        tiles_counter = 0

        for col in range(num_tiles_x):
            for row in range(num_tiles_y):
                tile = self.get_tile(left_most_annotation, top_most_annotation, row, col)
                
                included_annotations = self.get_included_annotations(annotations, tile)
                if len(included_annotations) > 0:

                    tiles_counter += 1

                    crop_image = self.crop_frame(frame, tile)
                    
                    crop_filename = os.path.join(self.output_path, self.basename + '_' + str(index).zfill(6) + "_" + str(tiles_counter).zfill(3) + self.suffix)
                    gt_filename = crop_filename.replace(self.suffix, '.txt')

                    crop = Crop(crop_image, crop_filename, gt_filename, self.crop_resolution[0], self.crop_resolution[1], included_annotations[0].label_set)
                    success = crop.write_crop(self.png_16bit)

                    if success:
                        crop.set_annotations(included_annotations)
                        crop.write_annotations(self.crop_resolution[0], self.crop_resolution[1])
                        
                        if second_annotations:
                            second_included_annotations = self.get_included_annotations(second_annotations, tile)
                            crop.set_annotations(second_included_annotations)
                            crop.set_annotation_path(os.path.join(self.output_path, 'Dual_GT_United', os.path.basename(gt_filename)))
                            crop.write_annotations(self.crop_resolution[0], self.crop_resolution[1])

                        for annotation in included_annotations:
                            self.annotations_summary_file.write(str(index).zfill(6) + ' ' + str(tiles_counter) + ' ' + annotation.get_as_yolo_line(self.crop_resolution[0], self.crop_resolution[1]) + ' \n')
                        
                        self.crops_summary_file.write(str(index).zfill(6) + ' ' + str(tiles_counter) + ' ' + str(tile[0]) + ' ' + str(tile[1]) + ' ' + str(tile[2]) + ' ' + str(tile[3]) + ' \n')
            
                    if self.debug_display:
                        self.display_crop(debug_frame, annotations, tile, crop_image, included_annotations, crop_filename, gt_filename)


    def display_crop(self, frame, all_annotations, tile, crop, included_annotations, crop_filename, gt_filename):
        saved_crop = cv2.imread(crop_filename)
        saved_annotations = self.extract_annotations(gt_filename, self.crop_resolution[0], self.crop_resolution[1])

        if self.png_16bit:
            frame   = cv2.normalize(frame, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
            crop    = cv2.normalize(crop, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
            frame   = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
            crop    = cv2.cvtColor(crop, cv2.COLOR_GRAY2BGR)

        for annotation in all_annotations:
            annotation.draw(frame)

        frame = cv2.rectangle(img=frame, pt1=(int(tile[0]), int(tile[1])), pt2=(int(tile[0]+tile[2]), int(tile[1]+tile[3])), color=(255, 0, 0), thickness=1)
        cv2.imshow("Frame", frame)
        
        for annotation in included_annotations:
            annotation.draw(crop)
        for annotation in saved_annotations:
            annotation.draw(saved_crop)

        cv2.imshow("Crop", crop)
        cv2.imshow("Saved Crop", saved_crop)
        
        cv2.waitKey(self.wait_key)

    
    def open_output_directories(self):
        if os.path.exists(self.output_path):
            print("Deleting existing crops...")
            shutil.rmtree(self.output_path)
        os.mkdir(self.output_path)

        if self.second_gt_path:
            second_gts_dir = os.path.join(self.output_path, 'Dual_GT_United')
            try:
                os.mkdir(second_gts_dir)
            except:
                pass


    def extract_annotations(self, gt_filename, width, height):
        annotations = []
        with open(gt_filename, "r") as f:
            lines = f.readlines()
        for line in lines:
            annotations.append(Target.init_from_yolo_line(line, width, height, self.label_set))
            if self.labels_conversion_file:
                annotations[-1].set_label(self.labels_conversion_dict[annotations[-1].get_label()])
        
        return annotations


    def crop(self):
        print(self.basename + " is being cropped...")
        self.open_output_directories()
        
        gts_filename = os.listdir(self.gt_path)
        gts_filename.sort()

        if self.second_gt_path:
            second_gts_filenames = os.listdir(self.second_gt_path)
            second_gts_filenames.sort()

        self.annotations_summary_file   = open(os.path.join(os.path.dirname(self.output_path), self.basename + "_annotationSummary.txt"), "w+")
        self.crops_summary_file         = open(os.path.join(os.path.dirname(self.output_path), self.basename + "_cropSummary.txt"), "w+")

        for i in range(self.reader.num_frames1):
            frame = self.reader.get_frame(index=i, bgr=(not self.png_16bit), thermal_endian=self.thermal_endian)
            annotations = self.extract_annotations(os.path.join(self.gt_path, gts_filename[i]), self.frame_resolution[0], self.frame_resolution[1])
            
            if self.second_gt_path:
                second_annotations = self.extract_annotations(os.path.join(self.second_gt_path, gts_filename[i]), self.frame_resolution[0], self.frame_resolution[1])
            else:
                second_annotations = None
            
            self.create_crops_for_frame(index=i, frame=frame, annotations=annotations, second_annotations=second_annotations)


if __name__ == '__main__':
    cropper = Cropper(  input_path              = sys.argv[1],
                        output_path             = sys.argv[2],
                        gt_path                 = sys.argv[3],
                        crop_resolution         = (int(sys.argv[4]), int(sys.argv[5])),
                        frame_resolution        = (int(sys.argv[6]), int(sys.argv[7])),
                        prefix                  = sys.argv[8],
                        second_gt_path          = sys.argv[9],
                        labels_conversion_file  = None )

    # cropper = Cropper(  input_path              = "/mnt/share/ssdHot/WorkMovies/CMOS Movies/Air - Ground/025_080719_026_AG_F16_T1400_HodHasharon_A30_D60/0266_07_07_2019-12_45_53.avi",
    #                     output_path             = "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/test",
    #                     gt_path                 = "/mnt/share/ssdHot/WorkMovies/CMOS Movies/Air - Ground/GT/0266_07_07_2019-12_45_53_gt",
    #                     crop_resolution         = (300, 300),
    #                     frame_resolution        = (None, None),
    #                     prefix                  = "Chimera",
    #                     second_gt_path          = None,
    #                     labels_conversion_file  = None )

    cropper.crop()