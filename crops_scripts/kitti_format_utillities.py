import cv2
import os.path
import numpy as np


def convert_annotation_file_to_kitti(source_annotation, destination_annotation, label_set, crop_width, crop_height):
    source_ann_file = open(source_annotation, "r")
    destination_ann_file = open(destination_annotation, "w")

    line = source_ann_file.readline()
    while line:
        if (len(line) >= 2):  # line is not empty
            elements = line.split(' ')
            label = int(elements[0])
            cx = int(float(elements[1])*crop_width)
            cy = int(float(elements[2])*crop_height)
            w = int(float(elements[3])*crop_width)
            h = int(float(elements[4][:-1])*crop_height)


            kitti_label = convert_label_to_Kitti(label, label_set)
            kitti_trancated = str(float(0)) # 0 = non-truncated
            kitti_occulded = "0" # 3 = fully visible
            kitti_alpha = "0" # default
            kitti_left = str(float(cx - (w/2)))
            kitti_right = str(float(cx + (w/2)))
            kitti_top = str(float(cy - (h/2)))
            kitti_buttom = str(float(cy + (h/2)))
            kitti_dimensions = "0 0 0" # default
            kitti_location = "0 0 0" # default
            kitti_rotation_y = "0" # default

            label = kitti_label + " " + kitti_trancated + " " + kitti_occulded + " " + kitti_alpha + " " + kitti_left + " " + kitti_top + " " + kitti_right + " " + kitti_buttom + " " + kitti_dimensions + " " + kitti_location + " " + kitti_rotation_y + " \n"
            destination_ann_file.write(label)

        line = source_ann_file.readline()

    source_ann_file.close()
    destination_ann_file.close()


def convert_label_to_Kitti(label, label_set):
    kitti_label = "dontcare"

    if label_set == "BP":
        if label == 0:
            kitti_label = "drone"
        elif label == 1:
            kitti_label = "airplane"
        elif label == 2:
            kitti_label = "bird"
        elif label == 3:
            kitti_label = "ufo"
        elif label == 4:
            kitti_label = "airplane-with-lights"
        elif label == 5:
            kitti_label = "balloon"
        elif label == 6:
            kitti_label = "person"
        elif label == 7:
            kitti_label = "vehicle"
        elif label == 8:
            kitti_label = "drone-with-lights"
        else:
            kitti_label = "single-front-light"
        
    elif label_set == "Avatar":
        if label == 0:
            kitti_label = "person" # maybe - person?
        elif label == 1:
            kitti_label = "bicycle"
        elif label == 2:
            kitti_label = "motorcycle"
        elif label == 3:
            kitti_label = "vehicle"
        elif label == 4:
            kitti_label = "bus"
        elif label == 5:
            kitti_label = "truck"
        elif label == 6:
            kitti_label = "train"
    
    elif label_set == "VIS":
        if label == 0:
            kitti_label = "person"
        elif label == 1:
            kitti_label = "bicycle"
        elif label == 2:
            kitti_label = "vehicle"
        elif label == 3:
            kitti_label = "motorcycle"
        elif label == 4:
            kitti_label = "airplane"
        elif label == 5:
            kitti_label = "bus"
        elif label == 6:
            kitti_label = "train"
        elif label == 7:
            kitti_label = "truck"
        
    return kitti_label
