import os
import threading
import subprocess
import validator
from io import StringIO
import json

help_msg = '''
This script is used as a tool to create and control multiple python interpeter
instances in order to create crops at a very large scale.

To use this tool you must first launch the script itself (no prarmeters).

After launch:
You will be asked to insert all the relevant crop prarmeters.
After inserting all parameters, you will reach the main menu. the main menu 
offer several functions:

1. Run - press ENTER and the above parameters will be used to run another python 
    interpeter proccess that will start generating crops.

2. Reset - if you wish to insert all the parameters from scratch, type RESET 
    to start the proccess.

3. Next - type NEXT to automatically change parameters to fit the next video (
    so that if the current video is 00001_r1_0 after NEXT it will be 00002_r1_1).

4. List history - type HISTORY to view all the movies that were ran previously (
    and are not necceraly still running).

5. Auto proccess - type AUTO to automatically run a movie and continue to the 
    next one.

    5.1 When in this mode you will be able to press ENTER to allow the displyed movie 
    proccess or type QUIT to return.
    
    5.2 When ENTER is pressed the displyed movie is being proccessed and the tool searches 
    and displys the next one. 

    5.3 When in this mode, you will be shown all the movies that are currently being 
    proccessed, just like in TRACK mode.

6. Track - type TRACK to view the proggress of all currently proccessed movies.

7. Clear - kills all outer procceses and all history.

9. Batch - go into AUTO mode and execute a certain amount of crop commands (batch 5 will
    start the cropping of the current movie and the 4 movies after it).

10. Batch cap - to set the batch cap type BATCH_CAP, this cap specifies the amount of processes
    that are allowed to run at the same time while batch mode is active.

11. Export - type EXPORT to export the current configuration to a json file. Which can be 
    later imported.

12. Import - type IMPORT to import an existing configuration from a json file.

13. Load - type LOADDB to go through the ENTIRE movie path recursively and load all the movie 
    files in to the current configuration.

14. HELP - print this message.

Omer Wexler, September of 2020.
'''

class CropManager:    

    MOVIE_ID = 'Movie id'
    MOVIE_TYPE = 'Movie type'
    INNER_ID = 'Inner id'
    MOVIE_PATH = 'Base movie path'
    GT_PATH = 'GT path'
    OUTPUT_PATH = 'Output path'
    RESULOTION_X = 'Resulotion x'
    RESULOTION_Y = 'Resulotion y'
    CROP_X = 'Crop x'
    CROP_Y = 'Crop y'
    PROJECT_PREFIX = 'Project prefix'

    def __init__(self, configuration_file=None):
        self.params = {
            'Movie id' : None,
            'Movie type' : None,
            'Inner id' : None,
            'Base movie path' : None,
            'GT path' : None,
            'Output path' : None,
            'Resulotion x' : None,
            'Resulotion y' : None,
            'Crop x' : None,
            'Crop y' : None,
            'Project prefix' : None,
        }

        self.functions = { # key is the input expected from the user and value is the corresponding action. 
            '': self.proccess_movie,
            'RESET': self.receive_parameters,
            'NEXT': self.incerment_movie,
            'HISTORY': self.print_history,
            'AUTO': self.auto_proccess,
            'TRACK': self.track,
            'CLEAR': self.clear_all_progress,
            'DEBUG': self.debug,
            'BATCH': self.batch,
            'BATCH_CAP': self.batch,
            'HELP': lambda : print(help_msg),
            'EXPORT': self.export_configuration,
            'IMPORT': self.import_configuration,
            'LOADDB': self.loadDB,
            'VALIDATE': self.validate
        }

        if os.sys.platform.startswith('linux'):
            self.clear_command = 'clear'
        elif os.sys.platform == 'win32':
            self.clear_command = 'cls'

        self.keys = []
        for key in self.params.keys():
            self.keys.append(key)

        # Loadables 
        self.cmos_movies = {}
        self.thermal_movies = {}
        self.movies_proccesed = []

        self.movie_path = ''
        self.last_configuration_name = configuration_file
        self.batch_cap = 10
        ##
        
        self.last_output = ''
        self.processes = {}
        self.batch_movies = 0

        self.input_looper = SingleValueLooper(input)

        if configuration_file != None:
            self.import_configuration(configuration_file)
        else:
            self.receive_parameters()
        
        self.main_menu()


    def receive_parameters(self):
        for key in self.params.keys():
            self.params[key] = input(f'Insert {key}: ')
        os.system(self.clear_command)


    def clear_all_progress(self):
        for process in self.processes:
            print(f'Killing - {str(process)}' )
            process.kill()

        self.movies_proccesed.clear()
        self.processes.clear()


    def proccess_movie(self):
        print('Searching for next movie...')
        self.find_movie()

        if self.movie_path == None:
            print('Movie not found...')
            return

        self.build_and_run_command()


    def print_history(self):
        if len(self.movies_proccesed) > 0:
            print('Movies done:')
            for movie in self.movies_proccesed:
                print(movie)
        else:
            print('No movies were proccesed so far.\n')


    def change_parameter(self, user_input: str):
        try:
            param_id, param_value = user_input.split('=')
            param_id = int(param_id)

            self.params[self.keys[param_id]] = param_value
        except Exception as e:
            print(e)
            print('Invalid selection.\n')
    

    def auto_proccess(self):
        self.input_looper.start()
        self.input_looper.add_callback('quit', self.input_looper.stop)

        self.find_movie()
        os.system(self.clear_command)

        while True:
            data = ''
            data += 'Next movie (press ENTER to run movie type QUIT to return to menu):\n'
            data += str(self.movie_path) + '\n\n'
            data += self.get_progress() + '\n'
            data += 'Capacity - ' + str(len(self.processes)) + '\n'
            data += 'Batch - ' + str(self.batch_movies) + '\n'
            data += 'Batch cap - ' + str(self.batch_cap) + '\n'
            

            if self.input_looper.has_new_data or self.batch_movies > 0:
                inpt = self.input_looper.get_value()
                self.input_looper.clear()
                inpt = str(inpt)
                
                is_batch_ready = self.batch_movies > 0 and len(self.processes) < self.batch_cap
                if self.movie_path != None and (is_batch_ready or inpt == ''):
                    self.build_and_run_command()
                    self.incerment_movie()

                    data += 'Searching for next movie...'
                    self.find_movie()

                    if self.batch_movies > 0:
                        self.batch_movies -= 1
                    
                elif inpt.upper() == 'QUIT':
                    self.batch_movies = 0
                    self.input_looper.remove_callback('quit')
                    self.input_looper.stop()
                    return

                elif inpt.find('=') != -1:
                    if inpt.split('=')[0].upper() == 'BATCH':
                        try:
                            self.batch_movies = int(inpt.split('=')[1])
                        except:
                            data += 'Couldn\'t pasre batch command'
                    elif inpt.split('=')[0].upper() == 'BATCH_CAP':
                        try:
                            self.batch_cap = int(inpt.split('=')[1])
                        except:
                            data += 'Couldn\'t pasre batch_cap'
                
            
            self.queue_print(data)


    def track(self):
        self.input_looper.start()
        self.input_looper.add_callback('quit', self.input_looper.stop)

        while True:
            data = self.get_progress()
            data += 'Capacity - ' + str(len(self.processes)) + '\n'
            data += '\nType QUIT to leave.'
                
            if self.input_looper.has_new_data:
                data = self.input_looper.get_value()
                if data.upper() == 'QUIT':
                    break
        
            self.queue_print(data)
        self.input_looper.remove_callback('quit')
        self.input_looper.stop()


    def queue_print(self, data):
        if data != self.last_output:
            os.system(self.clear_command)
            print(data)
            self.last_output = data
        
    
    def readlines(self, **kwargs):
        data = ''
        try:
            for i in range(0, kwargs['lines']):
                line = kwargs['ps'].stdout.readline()
                data += line
        except:
            pass
        return data
        

    def debug(self):
        print(self.processes)


    def get_progress(self):
        data = 'Current proggress:\n'
        to_remove = []
        for ps in self.processes.keys():
            if ps.poll() == None:
                new_data = self.processes[ps].get_value()
                if new_data == None:
                    new_data = 'Loading movie progress...\n'
            else:
                new_data = ''
                to_remove.append(ps)
            
            data += new_data

        for ps in to_remove:
            self.processes.pop(ps)
        return data


    def batch(self):
        self.batch_movies = int(input('Insert batch size: '))
        self.auto_proccess()


    def batch_cap(self):
        self.batch_cap = int(input('Insert batch_cap: '))


    def validate(self):
        print("Validating...")
        is_valid = validator.CropValidator.validate_crops(
            self.get_output_path(),
            self.get_GT_path(),
            int(self.params[self.CROP_X]),
            (int(self.params[self.RESULOTION_X]), int(self.params[self.RESULOTION_Y]))
        )

        print(is_valid)
        input("")


    def main_menu(self):
        while True:
            print('Current Parameters:\n' + self.get_description() + '\n')
            print('To change a parameter\'s value type \"PARAMETER_ID=NEW_VALUE\".')
            print('To reset all type RESET.')
            print('To update parameters to the next movie type NEXT.')
            print('To view all the movies that were schedualed to run type HISTORY.')
            print('To loop through movies automatically type AUTO.')
            print('To enter AUTO mode and execute a specific amount of video type BATCH.')
            print('To enter set the amount of auto movies that be run by batch type BATCH_CAP.')
            print('To show movie proggress type TRACK.')
            print('To debug processes type DEBUG.')
            print('To clear all proggress and kill all processes type CLEAR.')
            print('To export current configuration type EXPORT.')
            print('To import an existing configuration type IMPORT.')
            print('To load all movies from movie path type LOADDB.')
            print('To validate current movie and crops type VALIDATE.')
            print('To continue with current configuration press ENTER.')
            print('To quit type QUIT.')
            choice = input()

            if choice.upper() == 'QUIT':
                return
            
            os.system(self.clear_command)
            try:
                if choice.upper() in self.functions.keys():
                    self.functions[choice.upper()]()
                else:
                    self.change_parameter(choice)
            except Exception as e:
                print(e)


    def build_and_run_command(self):
        if os.sys.platform == 'linux' or os.sys.platform == 'linux2':
            cmd = ['python3', '/mnt/share/Local/OfflineDB/data-pre-processing-scripts/crops_scripts/multiple_objects_crops.py']
        elif os.sys.platform == 'win32':
            cmd = ['python', 'Z:\OfflineDB\data-pre-processing-scripts\crops_scripts\multiple_objects_crops.py']

        cmd.append(self.movie_path)
        cmd.append(self.get_GT_path())
        cmd.append(self.params[self.OUTPUT_PATH])
        cmd.append(self.params[self.CROP_X])
        cmd.append(self.params[self.CROP_Y])
        cmd.append(self.params[self.RESULOTION_X])
        cmd.append(self.params[self.RESULOTION_Y])
        cmd.append(self.params[self.PROJECT_PREFIX])
        cmd.append('False')
        cmd.append('False')
        cmd.append('None')
        print('\nExecuting cmd line:')
        cmd_string = cmd[0] + ' "' + '" "'.join(cmd[1:]) + '"'
        print(cmd_string)

        ps = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=False, encoding=os.sys.stdout.encoding)
        self.processes[ps] = SingleValueLooper(self.readlines, ps=ps, lines=2)
        self.processes[ps].start()

        self.movies_proccesed.append(self.movie_path)


    def incerment_movie(self):
        new_movie_id = str(int(self.params[self.MOVIE_ID]) + 1)

        while len(new_movie_id) < 5:
            new_movie_id = '0' + new_movie_id

        self.params[self.MOVIE_ID] = new_movie_id

        new_movie_inner_id = str(int(self.params[self.INNER_ID]) + 1)
        self.params[self.INNER_ID] = new_movie_inner_id


    def get_GT_path(self):
        return os.path.join(self.params[self.GT_PATH], self.get_movie_identifier() + '_gt')


    def get_output_path(self):
        return os.path.join(self.params[self.OUTPUT_PATH], self.params[self.PROJECT_PREFIX] + '_' + self.get_movie_identifier())


    def find_movie(self):
        try:
            # First try - search in the same folder as the last movie.
            movie_path = self.build_movie_path()
            if movie_path != None: 
                self.movie_path = movie_path
                return
                
            # Second try - try to skip gap in inner id
            new_inner_id = int(self.params[self.INNER_ID])

            for i in range(1, 10):
                new_inner_id = int(self.params[self.INNER_ID]) + i
                movie_path = self.build_movie_path(movie_inner_id=new_inner_id, recursive=False)
                
                if movie_path != None:
                    self.movie_path = movie_path
                    self.params[self.INNER_ID] = str(new_inner_id)
                    return

            # Third try - reset inner id        
            new_inner_id = int(self.params[self.INNER_ID])

            for i in range(0, 10):
                new_inner_id = i
                movie_path = self.build_movie_path(movie_inner_id=new_inner_id, recursive=False)
                
                if movie_path != None:
                    self.movie_path = movie_path
                    self.params[self.INNER_ID] = str(new_inner_id)
                    return

            # Fourth try - snap to next folder start        
            new_inner_id = int(self.params[self.INNER_ID])
            new_id = int(self.params[self.MOVIE_ID])

            while new_id % 10 != 0:
                new_id += 1

            new_id = str(new_id).zfill(5)

            for i in range(0, 10):
                new_inner_id = i
                movie_path = self.build_movie_path(movie_id=new_id, movie_inner_id=new_inner_id, recursive=False)
                
                if movie_path != None:
                    self.movie_path = movie_path
                    self.params[self.MOVIE_ID] = str(new_id)
                    self.params[self.INNER_ID] = str(new_inner_id)
                    return

            self.movie_path = None
        except:
            self.movie_path = None
            return
    
    
    def get_movie_identifier(self, movie_id=None, movie_type=None, movie_inner_id=None):
        if movie_id == None:
            movie_id = self.params[self.MOVIE_ID]

        if movie_type == None:
            movie_type = self.params[self.MOVIE_TYPE] 

        if movie_inner_id == None:
            movie_inner_id = self.params[self.INNER_ID]        
        
        movie_identifier = str(movie_id) + '_r' + str(movie_type) + '_' + str(movie_inner_id)
        return movie_identifier


    def get_movie_name(self, movie_id=None, movie_type=None, movie_inner_id=None):
        if movie_id == None:
            movie_id = self.params[self.MOVIE_ID]

        if movie_type == None:
            movie_type = self.params[self.MOVIE_TYPE]

        if movie_inner_id == None:
            movie_inner_id = self.params[self.INNER_ID]  

        movie_name = self.get_movie_identifier(movie_id, movie_type, movie_inner_id)   
        if movie_type == '1' or movie_type == '2':
            movie_name += '.rawc'
            return movie_name
        elif movie_type == '0':
            movie_name += '.raw2'
            return movie_name
        else:
            print('Invalid movie type - ' + str(movie_type))
            print('Movie type should be either 1 or 2 for rawc, or 0 for raw2.')
            return None


    def build_movie_path(self, movie_id=None, movie_type=None, movie_inner_id=None, recursive=False):
        if movie_id == None:
            movie_id = self.params[self.MOVIE_ID]

        if movie_type == None:
            movie_type = self.params[self.MOVIE_TYPE] 

        if movie_inner_id == None:
            movie_inner_id = self.params[self.INNER_ID]  
        
        movie_name = self.get_movie_name(movie_id, movie_type, movie_inner_id)

        movie_path = None

        if movie_name.endswith('raw2') and movie_name in self.thermal_movies.keys():
            movie_path = self.thermal_movies[movie_name]
        elif movie_name.endswith('rawc') and movie_name in self.cmos_movies.keys():
            movie_path = self.cmos_movies[movie_name]
        elif recursive:
            movie_path = self.get_path_of_movie(movie_name, self.params[self.MOVIE_PATH])
                        
        return movie_path


    def loadDB(self):
        print('Importing db...')
        self.get_path_of_movie(None, self.params[self.MOVIE_PATH], debug=True)


    def get_path_of_movie(self, to_search: str, base_folder: str, debug=False, tab=0):
        threads = []

        print(f"Searching - {base_folder}:")
        for candidate in os.listdir(base_folder):
            if candidate.endswith('.raw2'):
                self.thermal_movies[candidate] = os.path.join(base_folder, candidate)
                if debug:
                    print(f'{"    " * tab}Added - {candidate} to database')
            if candidate.endswith('.rawc'):
                self.cmos_movies[candidate] = os.path.join(base_folder, candidate)
                if debug:
                    print(f'{"    " * tab}Added - {candidate} to database')
            
            if to_search != None and candidate == to_search:
                return os.path.join(base_folder, candidate)
            elif os.path.isdir(os.path.join(base_folder, candidate)):
                if to_search == None:
                    new_base_folder = os.path.join(base_folder, candidate)
                    new_thread = threading.Thread(target=self.get_path_of_movie, args=(to_search, new_base_folder, debug, tab+1))
                    threads.append(new_thread)
                    new_thread.start()

                else:
                    result = self.get_path_of_movie(to_search, os.path.join(base_folder, candidate), debug=debug, tab=tab+1)
                    return result

        while True:
            all_dead = True
            for thread in threads:
                if thread.is_alive():
                    all_dead = False

            if all_dead:
                break

        return None
        
    
    def export_configuration(self):
        if self.last_configuration_name != None:
            print(f'Current cofiguration - {self.last_configuration_name}')
            configuration_name = input('Insert configuration name or press enter for current configuration: ')

            if configuration_name == '': 
                configuration_name = self.last_configuration_name
            else:
                self.last_configuration_name = configuration_name
        else:
            configuration_name = input('Insert configuration name: ')
            self.last_configuration_name = configuration_name
        

        if not configuration_name.endswith('.json'):
            configuration_name += '.json'

        configuration = {
            'params': self.params,
            'cmos_movies' : self.cmos_movies,
            'thermal_movies' : self.thermal_movies,
            'movies_proccesed' : self.movies_proccesed,
            'movie_path' : self.movie_path,
            'last_configuration_name': self.last_configuration_name,
            'batch_cap': self.batch_cap
        }

        config_file = os.path.join(os.getcwd(), configuration_name)
        with open(config_file, 'w') as file:
            json.dump(configuration, file)
        
    
    def import_configuration(self, config_file=None):
        if config_file != None:
            configuration_name = config_file
        elif self.last_configuration_name != None:
            print(f'Current cofiguration - {self.last_configuration_name}')
            configuration_name = input('Insert configuration name or press enter for current configuration: ')

            if configuration_name == '': 
                configuration_name = self.last_configuration_name
            else:
                self.last_configuration_name = configuration_name
        else:
            configuration_name = input('Insert configuration name: ')
            self.last_configuration_name = configuration_name
        
        if not configuration_name.endswith('.json'):
            configuration_name += '.json'

        config_file = os.path.join(os.getcwd(), configuration_name)
        with open(config_file, 'r') as file:
            configuration = json.load(file)
        
        try:
            self.params = configuration['params']
            self.cmos_movies = configuration['cmos_movies']
            self.thermal_movies = configuration['thermal_movies']
            self.movies_proccesed = configuration['movies_proccesed']
            self.movie_path = configuration['movie_path']
            self.last_configuration_name = configuration['last_configuration_name']
            self.batch_cap = configuration['batch_cap']
        except:
            pass


    def get_description(self):
        ret = ''

        i = 0
        for key in self.params.keys():
            value = self.params[key]
            ret += f'\n{i}) {key} - {value}'
            i += 1

        return ret
    
    def __del__(self):
        for ps in self.processes:
            ps.kill()        


class SingleValueLooper:
    def __init__(self, target_function, **kargs):
        self.value = None
        self.is_running = True
        self.has_new_data = False
        self.callbacks = {}
        
        self.target_function = target_function
        self.target_args = kargs
        
        self.thread = None  


    def start(self):
        self.value = None 
        self.is_running = True
        
        self.thread = threading.Thread(target=self._loop_)
        self.thread.setDaemon(True)

        self.thread.start()


    def _loop_(self):
        while self.is_running:
            self.value = self.target_function(**self.target_args)
            if self.value in self.callbacks.keys():
                self.callbacks[self.value]()
            
            self.has_new_data = True


    def stop(self):
        self.is_running = False


    def get_value(self):
        self.has_new_data = False
        return self.value


    def clear(self):
        self.has_new_data = False
        self.value = None


    def add_callback(self, trigger, callback):
        self.callbacks[trigger] = callback


    def remove_callback(self, trigger):
        if trigger in self.callbacks.keys():
            self.callbacks.pop(trigger)

    
    def __del__(self):
        self.stop()
        

if __name__ == '__main__':
    # config_file = None
    # if len(os.sys.argv) >= 2:
    #     config_file = os.sys.argv[1]
    # crop_manager = CropManager(config_file)
    crop_manager = CropManager("dual_air_thermal")