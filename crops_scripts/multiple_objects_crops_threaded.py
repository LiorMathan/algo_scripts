import numpy as np
import cv2
import os
from Reader import *
from tkinter import messagebox
import pyScriptsUtilities as utils
import sys
import pandas as pd
import ProgressBar
from threading import Thread, Lock
import time

# Description:
# This script creates crops (multiple objects) from annotated movie/frames.
# The output annotations files are in YOLO format.

# Usage:
# python3 multiple_objects_crops.py <input path> <gt path> <output path> <crop width> <crop height> 
#                                   (optional:) <frame width> <frame height> <project prefix> <change polarity> <old chimera> <visdrone> <labels conversion file> <vmd maps path>

# Example:
# python3 multiple_objects_crops.py "Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\High_Altitude\0001_170520_0001_Air_F8.5_T1700_Kfar Neter_A120_D90\Thermal_RAW_Movies_1\00023_r0_8.raw2" 
#                                   "Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\GT\00023_r0_8_gt" 
#                                   "Z:\OfflineDB\YOLO_AG\DualAir\Thermal\High_Altitude" 
#                                   416 416 640 480 DualAir


debug_flag = False
use_inclusive_tiles = True
PEDESTRIAN_LABEL = 0

wait_time = 20
raw_crops = False #utils.convertBoolToStr(sys.argv[12])

class CropsTool:
    def __init__(self, input_path, gt_path, output_path, crop_width, crop_height, frame_width=None, frame_height=None, project_prefix=None, change_polarity=False, is_old_chimera=False, is_visdrone=False, labels_conversion_file=None, vmd_maps_path=None):
        self.input_path = input_path
        self.gt_path = gt_path
        self.output_path = output_path
        self.crop_width = crop_width
        self.crop_height = crop_height
        self.frame_width = frame_width
        self.frame_height = frame_height
        if (self.frame_width == 0):
            self.frame_width = None
            self.frame_height = None
        self.change_polarity = change_polarity
        self.is_old_chimera = is_old_chimera
        self.is_visdrone = is_visdrone
        self.labels_conversion_table = self.get_table_from_file(labels_conversion_file, delimiter=',')
        self.min_objects_in_tile = 1
        self.max_objects_in_tile = 100
        self.min_pedestrian_area = 5 * 12
        self.inclusive_tile_edge_width = 12
        self.project_prefix = project_prefix
        self.vmd_maps_path = vmd_maps_path
        if vmd_maps_path == "None":
            self.vmd_maps_path = None
        _,self.extension = os.path.splitext(self.input_path)
        self.basename = os.path.basename(self.input_path)
        if len(self.extension) > 0:
            self.basename = os.path.basename(self.input_path)[:-len(self.extension)]
        if self.project_prefix is not None:
            self.basename = self.project_prefix + "_" + self.basename
        if self.is_visdrone is False:
            self.output_path = os.path.join(self.output_path, self.basename)
        self.reader = Reader(path1=self.input_path, width1=self.frame_width, height1=self.frame_height)
        self.frames_done = 0
        self.progress_bar = ProgressBar.ProgressBar(self.reader.num_frames1, proggress_getter=lambda : self.frames_done, bar_length=50, end_char='\n', prefix=self.basename + ' is being processed...\n')
        self.clear_command = ''

        self.ann_summery_file_data = {}
        self.crops_summery_file_data = {}

        self.labeled_crops_stack = []
        if debug_flag is True:
            self.frame_debug_stack = []
        

    def create_vmd_crop(self, index, crop_coor, object_index, basename, second_input=False):
        # vmd_filename = os.path.join(self.vmd_maps_path, "ELTBinMap_" + basename + ".binmap")
        vmd_filename = self.vmd_maps_path
        if second_input is True:
            width = self.reader.width2
            height = self.reader.height2
        else:
            width = self.reader.width1
            height = self.reader.height1
        vmd = utils.read_binary_map(map=vmd_filename, index=index, width=width, height=height, normalize=False)
        vmd_crop = vmd[crop_coor[0]:crop_coor[0]+self.crop_height, crop_coor[2]:crop_coor[2]+self.crop_width]
        if debug_flag is True:
            vmd_debug = utils.read_binary_map(map=vmd_filename, index=index, width=width, height=height, normalize=True)
            vmd_crop_debug = vmd_debug[crop_coor[0]:crop_coor[0]+self.crop_height, crop_coor[2]:crop_coor[2]+self.crop_width]
            cv2.imshow("VMD Crop", vmd_crop_debug)
            cv2.waitKey(wait_time)

        # if is_dual is True:
        #     if second_input is True:
        #         suffix = "_vis.binmap"
        #     else:
        #         suffix = "_thermal.binmap"
        else:
            suffix = ".binmap"
        out_filename = os.path.join(self.output_path, self.basename + '_' + str(index).zfill(6) + "_" + str(object_index).zfill(3) + suffix)
        with open(out_filename, "wb") as f:
            vmd_crop = np.ascontiguousarray(vmd_crop)
            f.write(vmd_crop)     
    

    def validate_input(self):
        if not os.path.exists(self.input_path):
            # messagebox.showinfo("Input Error", "Input Path Does Not Exists!")
            print("Input Path Does Not Exists!")
            exit()
        if not os.path.exists(self.gt_path):
            # messagebox.showinfo("Input Error", "GT Path Does Not Exists!")
            print("GT Path Does Not Exists!")
            exit()
        

    def get_table_from_file(self, filename, dtype='i', delimiter=','):
        if filename is None or not os.path.exists(filename):
            table = None
        else:
            if self.is_visdrone is True:
                table = np.loadtxt(filename, dtype=np.float32, delimiter=delimiter)
            else:
                table = pd.read_csv(filename, dtype=dtype, sep=delimiter, header=None)
            
            if 'numpy.float32' in str(type(table[0])):
                table = [table]
            table = np.array(table)

        return table


    def get_visdrone_format_annotations(self, yolo_annotations):
        visdrone_annotation = []
        for i in range(len(yolo_annotations)):
            left = int(yolo_annotations[i][1] * self.reader.width1 - ((yolo_annotations[i][3] * self.reader.width1) / 2))
            top = int(yolo_annotations[i][2] * self.reader.height1 - ((yolo_annotations[i][4] * self.reader.height1) / 2))
            width = int(yolo_annotations[i][3] * self.reader.width1)
            height = int(yolo_annotations[i][4] * self.reader.height1)
            label = int(yolo_annotations[i][0])
            new_annotation = np.array([left, top, width, height, 0, label])
            visdrone_annotation.append(new_annotation.astype(int))
        
        return np.array(visdrone_annotation)
    

    def get_updated_label(self, label):
        if self.is_old_chimera is True:
            if label == 6: # human
                label = 0
            elif label == 7: # vehicle
                label = 3

        if self.labels_conversion_table is not None:
            label = self.labels_conversion_table[int(label)][1]
        
        return label
    

    def get_included_annotations(self, annotations, tile, width, height):
        # tile is [left, top, width, height]
        included = []

        for i in range(len(annotations)):
            try:
                if use_inclusive_tiles is True:
                    # include only objects that have at least <self.inclusive_tile_edge_width> pixels inside the crop:
                    if (annotations[i][0] + annotations[i][2] < tile[0] + self.inclusive_tile_edge_width):
                        continue
                    if (annotations[i][1] + annotations[i][3] < tile[1] + self.inclusive_tile_edge_width):
                        continue
                    if (annotations[i][0] > tile[0] + tile[2] - self.inclusive_tile_edge_width):
                        continue
                    if (annotations[i][1] > tile[1] + tile[3] - self.inclusive_tile_edge_width):
                        continue
                else:
                    # include only objects that their center is inside the crop:
                    cx = annotations[i][0] + (annotations[i][2] / 2)
                    cy = annotations[i][1] + (annotations[i][3] / 2)
                    if (cx < tile[0] or cx > tile[0] + tile[2]):
                        continue
                    if (cy < tile[1] or cy > tile[1] + tile[3]):
                        continue
            except:
                continue

            # update annotations to new tile:
            start_x = max(tile[0], annotations[i][0])
            start_y = max(tile[1], annotations[i][1])
            end_x = min(tile[0] + tile[2], annotations[i][0] + annotations[i][2])
            end_y = min(tile[1] + tile[3], annotations[i][1] + annotations[i][3])
            updated_width = end_x - start_x
            updated_height = end_y - start_y
            
            yolo_cx = (start_x - tile[0] + (updated_width / 2)) / width
            yolo_cy = (start_y - tile[1] + (updated_height / 2)) / height
            yolo_w = updated_width / width
            yolo_h = updated_height / height
            label = annotations[i][5]
            label = int(self.get_updated_label(label))

            if label == -1:
                continue
            # disqualify pedestrians with area smaller than minimum:
            if self.is_visdrone is True:
                if label == PEDESTRIAN_LABEL:
                    if ((updated_width * updated_height) < self.min_pedestrian_area):
                        continue

            included_annotation = [label, yolo_cx, yolo_cy, yolo_w, yolo_h]
            included.append(included_annotation)

        return (included)


    def write_crop(self, crop, filename):
        success = 1
        if filename.endswith(".png"):
            cv2.imwrite(filename, crop, [cv2.IMWRITE_PNG_COMPRESSION, 0])
            # delete crop if writing had failed:
            check_not_none = cv2.imread(filename)
            if check_not_none is None:
                print("Bad crop: " + filename)
                os.remove(filename)
                success = -1
        else:
            with open(filename, "wb") as f:
                crop = np.ascontiguousarray(crop, dtype=np.int16)
                f.write(crop)

        return success


    def show_labaled_crop(self, crop, crop_filename, included_annotations):
        if debug_flag is True:
            debug_crop = crop.copy()
            if raw_crops is True:
                debug_crop = cv2.normalize(debug_crop, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
                debug_crop = cv2.cvtColor(debug_crop, cv2.COLOR_GRAY2BGR)
                with open(crop_filename, "rb") as f:
                    original_crop = np.fromfile(f, dtype=np.int16, count=self.crop_height*self.crop_width)
                    original_crop = original_crop.reshape((self.crop_height, self.crop_width))
                    original_crop = cv2.normalize(original_crop, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
                    original_crop = cv2.cvtColor(original_crop, cv2.COLOR_GRAY2BGR)
            else:
                original_crop = cv2.imread(crop_filename)
            for obj in range(len(included_annotations)):
                (start, end) = utils.find_start_end_point_rectangle(cx=included_annotations[obj][1]*self.crop_width, cy=included_annotations[obj][2]*self.crop_height, width=included_annotations[obj][3]*self.crop_width, height=included_annotations[obj][4]*self.crop_height)
                debug_crop = cv2.rectangle(img=debug_crop, pt1=start, pt2=end, color=(0, 255, 0), thickness=1)
                # original_crop = cv2.rectangle(img=original_crop, pt1=start, pt2=end, color=(255, 255, 255), thickness=1)
                cv2.putText(img=debug_crop, text=str(included_annotations[obj][0]), org=(start[0], start[1]-5), fontFace=cv2.FONT_HERSHEY_COMPLEX, color=(0, 255, 0), thickness=1, fontScale=0.3)
                # cv2.putText(img=original_crop, text=str(included_annotations[obj][0]), org=(start[0], start[1]-5), fontFace=cv2.FONT_HERSHEY_COMPLEX, color=(255, 255, 255), thickness=1, fontScale=0.3)
            cv2.imshow("Objects on crop", debug_crop)
            # cv2.imshow("Original crop", original_crop)
            cv2.waitKey(wait_time)


    def crops_for_frame(self, index, gts_filename):
        i = index
        try:
            current_gt_filename = os.path.join(self.gt_path, gts_filename[i])
            if (os.stat(current_gt_filename).st_size == 0): # if file is empty - continue
                return
        except:
            return

        if raw_crops is True:
            frame = self.reader.get_frame(index=i, bgr=False)
        else:
            frame = self.reader.get_frame(index=i, bgr=True)
        if frame is None:
            return
        if (frame.shape[0] < self.crop_height or frame.shape[1] < self.crop_width):
            return

        # could happen in mp4 encoded files:
        # if self.reader.index != i:
        #     i = self.reader.index

        try:
            if self.is_visdrone is True:
                delimiter = ','
            else:
                delimiter = ' '
            annotations = self.get_table_from_file(filename=current_gt_filename, dtype='f', delimiter=delimiter)
            if self.is_visdrone == False:
                annotations = self.get_visdrone_format_annotations(annotations)
        except:
            return

        if debug_flag is True:
            debug_frame = frame.copy()
            if raw_crops is True:
                debug_frame = cv2.normalize(debug_frame, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
                debug_frame = cv2.cvtColor(debug_frame, cv2.COLOR_GRAY2BGR)
            all_annotations = self.get_included_annotations(annotations, tile=[0, 0, frame.shape[1], frame.shape[0]], width=frame.shape[1], height=frame.shape[0])
            for obj in range(len(all_annotations)):
                (start, end) = utils.find_start_end_point_rectangle(cx=int(all_annotations[obj][1]*frame.shape[1]), cy=int(all_annotations[obj][2]*frame.shape[0]), width=int(all_annotations[obj][3]*frame.shape[1]), height=int(all_annotations[obj][4]*frame.shape[0]))
                debug_frame = cv2.rectangle(img=debug_frame, pt1=start, pt2=end, color=(0, 255, 0), thickness=1)         
        try:
            min_values = np.amin(a=annotations, axis=0)
            max_values = np.amax(a=annotations, axis=0)
            min_x = max(0, min_values[0] - self.inclusive_tile_edge_width)
            min_y = max(0, min_values[1] - self.inclusive_tile_edge_width)
            max_x = max_values[0] + self.inclusive_tile_edge_width
            max_y = max_values[1] + self.inclusive_tile_edge_width
            range_x = max_x - min_x
            range_y = max_y - min_y
            tiles_x = max(int(np.ceil(range_x / self.crop_width)), 1)
            tiles_y = max(int(np.ceil(range_y / self.crop_height)), 1)
        except:
            return

        tiles_count = 0
        for k in range(tiles_x):
            for m in range(tiles_y):
                tile=[int(min_x + (k * self.crop_width)), int(min_y + (m * self.crop_height)), self.crop_width, self.crop_height]
                if tile[0] + tile[2] >= frame.shape[1]:
                    tile[0] = frame.shape[1] - self.crop_width
                if tile[1] + tile[3] >= frame.shape[0]:
                    tile[1] = frame.shape[0] - self.crop_height
                
                if debug_flag is True:
                    debug_frame = cv2.rectangle(img=debug_frame, pt1=(int(tile[0]), int(tile[1])), pt2=(int(tile[0]+tile[2]), int(tile[1]+tile[3])), color=(255, 0, 0), thickness=1)
                    self.frame_debug_stack.append(debug_frame)
                    # debug_frame = cv2.rectangle(img=debug_frame, pt1=(int(tile[0]), int(tile[1])), pt2=(int(tile[0]+tile[2]), int(tile[1]+tile[3])), color=(255, 0, 0), thickness=1)
                    # cv2.imshow("Tile on frame", debug_frame)
                    # cv2.waitKey(wait_time)

                included_annotations = self.get_included_annotations(annotations, tile, width=self.crop_width, height=self.crop_height)
                if len(included_annotations) >= self.min_objects_in_tile and len(included_annotations) <= self.max_objects_in_tile:
                    if raw_crops is True and self.extension == ".raw2":
                        crop = frame[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2]]
                    else:
                        crop = frame[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2], :]
                    if crop.shape[1] != self.crop_width or crop.shape[0] != self.crop_height:
                        continue
                    if self.change_polarity is True and raw_crops is False:
                        crop = 255 - crop

                    tiles_count += 1

                    if self.vmd_maps_path is not None:
                        self.create_vmd_crop(index=i, crop_coor=[tile[1], tile[1]+tile[3], tile[0], tile[0]+tile[2]], object_index=tiles_count, basename=self.basename)
                    if raw_crops is True:
                        suffix = self.extension
                    else:
                        suffix = ".png"
                    image_filename = os.path.join(self.output_path, self.basename + '_' + str(i).zfill(6) + "_" + str(tiles_count).zfill(3) + suffix)
                    new_gt_filename = image_filename.replace(suffix, '.txt')
                    
                    success = self.write_crop(crop=crop, filename=image_filename)
                    if (success != -1):
                        with open(new_gt_filename, "w") as f:
                            for ann in range(len(included_annotations)):
                                f.write(str(included_annotations[ann][0]) + ' ' + str(included_annotations[ann][1]) + ' ' + str(included_annotations[ann][2]) + ' ' + str(included_annotations[ann][3]) + ' ' + str(included_annotations[ann][4]) + ' \n')
                                # ann_summary_file.write(str(i).zfill(6)+ ' ' + str(tiles_count) + ' ' + str(included_annotations[ann][0]) + ' ' + str(included_annotations[ann][1]) + ' ' + str(included_annotations[ann][2]) + ' ' + str(included_annotations[ann][3]) + ' ' + str(included_annotations[ann][4]) + ' \n')
                                self.ann_summery_file_data[i * 1000 + k * 100 + m * 10 + ann] = (str(i).zfill(6)+ ' ' + str(tiles_count) + ' ' + str(included_annotations[ann][0]) + ' ' + str(included_annotations[ann][1]) + ' ' + str(included_annotations[ann][2]) + ' ' + str(included_annotations[ann][3]) + ' ' + str(included_annotations[ann][4]) + ' \n')
                            
                        # crop summery contains - top left corner of crop , crop width, crop height
                        self.crops_summery_file_data[i * 100 + k * 10 + m] = (str(i).zfill(6) + ' ' + str(tiles_count) + ' ' + str(tile[0]) + ' ' + str(tile[1]) + ' ' + str(tile[2]) + ' ' + str(tile[3]) + ' \n')
                        self.labeled_crops_stack.append((crop, image_filename, included_annotations))


    def manage_frame_sampling(self):
        try:
            while not self.progress_bar.is_done() or len(self.frame_debug_stack) > 0 or len(self.labeled_crops_stack) > 0:
                if len(self.frame_debug_stack) > 0:
                    cv2.imshow("Tile on frame", self.frame_debug_stack.pop())
                    cv2.waitKey(wait_time)

                if len(self.labeled_crops_stack) > 0:
                    labeled_crop_params = self.labeled_crops_stack.pop()
                    self.show_labaled_crop(crop=labeled_crop_params[0], crop_filename=labeled_crop_params[1], included_annotations=labeled_crop_params[2])
                time.sleep(0.02)
        except:
            return


    def create_crops(self):
        self.validate_input()
        try:
            os.mkdir(self.output_path)
        except:
            pass
        gts_filename = os.listdir(self.gt_path)

        self.progress_bar.init_periodic() 

        thread = Thread(target=self.manage_frame_sampling)
        thread.setDaemon(True)
        thread.start()
        
        for i in range(self.reader.num_frames1):
            self.crops_for_frame(i, gts_filename)
            self.frames_done += 1

        self.progress_bar.stop_periodic()

        while not self.progress_bar.is_done():
            pass

        self.write_summery_files()


    def write_summery_files(self):
        ann_summary_file = open(os.path.join(os.path.dirname(self.output_path), "cropsMovie_" + self.basename + "_annotationSummery.txt"), "w+")
        if len(self.ann_summery_file_data) > 0:
            for i in range(max(self.ann_summery_file_data.keys()) + 1):
                try:
                    data = self.ann_summery_file_data[i]
                    ann_summary_file.write(data)
                except:
                    pass

        crops_summary_file = open(os.path.join(os.path.dirname(self.output_path), "cropsMovie_"  + self.basename + "_cropSummery.txt"), "w+")
        if len(self.crops_summery_file_data) > 0:
            for i in range(max(self.crops_summery_file_data.keys()) + 1):
                try:
                    data = self.crops_summery_file_data[i]
                    crops_summary_file.write(data)
                except:
                    pass


if __name__ == '__main__':
    cropper = CropsTool(input_path=sys.argv[1],
                        gt_path=sys.argv[2],
                        output_path=sys.argv[3],
                        crop_width=int(sys.argv[4]), 
                        crop_height=int(sys.argv[5]),
                        frame_width=int(sys.argv[6]),
                        frame_height=int(sys.argv[7]),
                        project_prefix=sys.argv[8],
                        is_old_chimera=utils.convertBoolToStr(sys.argv[9]),
                        change_polarity=utils.convertBoolToStr(sys.argv[10]),
                        vmd_maps_path=sys.argv[11])

    # cropper = CropsTool(input_path=r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Low_Altitude\0010_210520_0017_AG_F19_T1130_kfar neter_stability test\CMOS_RAW_Movies_1\00172_r2_8.rawc",
    #                     gt_path=r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\GT\00172_r2_8_gt",
    #                     output_path=r"Z:\OfflineDB\SSDMoblieNet_AG\DualAir_MultiObjects\CMOS\Test",
    #                     crop_width=300, 
    #                     crop_height=300,
    #                     frame_width=720,
    #                     frame_height=576,
    #                     project_prefix="DualAir",
    #                     is_old_chimera=False,
    #                     change_polarity=False,
    #                     is_visdrone=False, 
    #                     labels_conversion_file=None)
    
    cropper.create_crops()
    