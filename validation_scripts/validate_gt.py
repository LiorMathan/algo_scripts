import os
import shutil


external_gt_directories = ["/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/AG/Chimera_Dual/GT",
                           "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/AG/Chimera_Dual/Dual_GT/NMS_United",
                           "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/AG/Chimera_Dual/Dual_GT/NMS_NotUnited",
                           "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/GT",
                           "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Dual_GT/NMS_United",
                           "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Dual_GT/NMS_NotUnited"]


def validate_gt_dir(gt_dir):
    valid = False

    for file in os.listdir(gt_dir):
        if os.stat(os.path.join(gt_dir, file)).st_size > 0:
            valid = True
            break
    
    if not valid:
        print(gt_dir + " has no GTs! Deleting...")
        shutil.rmtree(gt_dir)


def validate_num_gts(gt_dir, max_files=1000):
    gt_files = []
    for file in os.listdir(gt_dir):
        if file.endswith('.txt'):
            gt_files.append(os.path.join(gt_dir, file))
        else:
            print(">>>>> Check: " + os.path.join(gt_dir, file))
    gt_files.sort()
    
    if len(gt_files) > max_files:
        print(gt_dir + " has " + str(len(gt_files)) + " gt files! Deleting...")
        to_delete = gt_files[max_files:]

        for file in to_delete:
            os.remove(file)


def validate():
    for external_dir in external_gt_directories:
        for root, dirs, files in os.walk(external_dir):
            for file in files:
                fullpath_file = os.path.join(root, file)
                if (fullpath_file.count(os.path.sep) - external_dir.count(os.path.sep) == 1) and os.stat(fullpath_file).st_size == 0:
                    print(file + " is empty! Deleting...")
                    os.remove(os.path.join(root, file))
            for dir in dirs:
                validate_gt_dir(os.path.join(root, dir))
                try:
                    validate_num_gts(os.path.join(root, dir))
                except:
                    continue


validate()