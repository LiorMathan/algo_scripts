import cv2
import os  


input_path = r"Z:\OfflineDB\SSDMobileNet_GG\FLIR_ADAS\Thermal\FLIR_ADAS_OLD_Validation"

i = 0
for file in os.listdir(input_path):
    if file.endswith('.png'):
        img = cv2.imread(os.path.join(input_path, file))
        if img is None:
            print(file)
        i += 1
        if i % 100 == 0:
            print(i)
