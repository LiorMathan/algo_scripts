import numpy as np
import cv2
import os
import sys
import shutil 

#### This script gets a folder path in the constants section, and shows all objects marked in BB ###

#### CONSTANTS ####
images_folder = "Z:\\OfflineDB\\RGB-T234\\orangeman1\\infrared"
gt_file_path = images_folder + ".txt"
output_folder = "Z:\\OfflineDB\\RGB-T234-AvatarClasses"
frame_width = 630
frame_height = 460
image_extension = '.jpg'
is_thermal = True

class YOLO_GT:
    def __init__(self, cx, cy, w, h):
        self.cx = cx
        self.cy = cy
        self.w = w
        self.h = h


def get_label_from_int(label):
    string_label = None
    if label == 0:
        string_label = 'Pedestrian'
    elif label == 1:
        string_label = 'Bicycle'
    elif label == 2:
        string_label = 'Motorcycle'
    elif label == 3:
        string_label = 'Vehicle'
    elif label == 4:
        string_label = 'Bus'
    elif label == 5:
        string_label = 'Truck'
    elif label == 6:
        string_label = 'Train'
    
    return string_label


##### MAIN #####

frames_list = []
yolo_annotations = []

for file in os.listdir(images_folder):
    if file.endswith(image_extension):
        frames_list.append(os.path.join(images_folder, file))
num_frames = len(frames_list)

gt_file = open(gt_file_path, "r")

for i in range(num_frames):
    image_path = frames_list[i]
    gt_line = gt_file.readline()

    # read frame:
    frame = cv2.imread(image_path)

    if not (len(gt_line) >= 2):  
        print("line " + str(i) + " was not found!!!")
        continue
    else: # line is not empty
        elements = gt_line.split(',')
        x = int(elements[0])
        y = int(elements[1])
        w = int(elements[2])
        h = int(elements[3][:-1])

        yolo_format = YOLO_GT(float((x+(w/2))/frame_width), float((y+(h/2))/frame_height), float(w/frame_width), float(h/frame_height))
        yolo_annotations.append(yolo_format)

        # Red color in BGR 
        color = (0, 0, 255) 
        # Line thickness of 1 px 
        thickness = 1
        # Bounding Box:
        cv2.rectangle(frame, (x, y), (x+w, y+h), color, thickness)

    # show frame:
    cv2.imshow('Frame', frame)
    key = cv2.waitKey(1) # pauses for 2 seconds before fetching next image
    if key == 32: # if enter in pressed, pause video
        cv2.waitKey(0)
    if key == 27: # if ESC is pressed, exit loop
        cv2.destroyAllWindows()
        break

cv2.destroyAllWindows()


##### Saving in Yolo Format: #####
print("Please enter the matching class:")
print("0 = pedestrian")
print("1 = bicycle")
print("2 = motorcycle")
print("3 = vehicle")
print("4 = bus")
print("5 = truck")
print("6 = train")
print("7 = not relevant!")

label = int(input())

if label != 7:
    print("Processing! please wait....")
    if is_thermal:
        destination = os.path.join(output_folder, "Thermal")
    else:
        destination = os.path.join(output_folder, "VIS")

    destination = os.path.join(destination, get_label_from_int(label))


    filename = os.path.basename(os.path.dirname(images_folder))

    for i in range(num_frames):
        current_filename = filename + "_" + str(i)

        yolo_ann = open(os.path.join(destination, current_filename+".txt"), "w")
        yolo_ann.write(str(label) + " " + str(yolo_annotations[i].cx) + " " + str(yolo_annotations[i].cx) + " " + str(yolo_annotations[i].w) + " " + str(yolo_annotations[i].h) + " \n")
        yolo_ann.close()

        shutil.copyfile(frames_list[i], os.path.join(destination, current_filename+image_extension))

print("Done")


    
