import numpy as np
import cv2
import os
import sys
import random
import argparse

####    This script gets a folder path and resolution as arguments:                     ####
####    -p for path, -r for resolution (416 or 300),                                    ####
####    and shows all objects marked in BB for up to 100 random crops, 4 at a time      ####
####    press SPACE to pause, press ESC to exit                                         ####


#### ArgParser: ####
parser = argparse.ArgumentParser(description='Receives arguments from user to configure the script')
parser.add_argument('-p', '--path', type=str, metavar='', required=True, help='Crops folder path')
parser.add_argument('-r', '--resolution', type=int, metavar='', required=True, help='Crops width/height')
parser.add_argument('-a', '--avatar', action='store_true', help='Use avatar labels?')
args = parser.parse_args()

crops_folder = args.path 
frame_width = args.resolution
frame_height = args.resolution

if args.avatar:
    isAvatar = True
else:
    isAvatar = False

#### CONSTANTS - Please update before running the script: ####

image_extension = '.png'
rows = 2
cols = 2
num_frames_to_show = 4
color = (0, 0, 255) 
thickness = 1

def find_start_end_point_rectangle(cx, cy, width, height):
    start_x = int(cx - (width/2))
    start_y = int(cy - (height/2))
    end_x = int(cx + (width/2))
    end_y = int((cy) + (height/2))

    return ((start_x, start_y), (end_x, end_y))


def get_label_from_int(label):
    string_label = None
    if label == 0:
        string_label = 'drone'
    elif label == 1:
        string_label = 'airplane'
    elif label == 2:
        string_label = 'bird'
    elif label == 3:
        string_label = 'UFO'
    elif label == 4:
        string_label = 'airplane with lights'
    elif label == 5:
        string_label = 'balloon'
    elif label == 6:
        string_label = 'human'
    elif label == 7:
        string_label = 'vehicle'
    elif label == 8:
        string_label = 'drone with lights'
    elif label == 9:
        string_label = 'single front light'
    else:
        string_label = 'Unknown'
    return string_label


def get_avatar_label_from_int(label):
    string_label = None
    if label == 0:
        string_label = 'pedestrian'
    elif label == 1:
        string_label = 'bicycle'
    elif label == 2:
        string_label = 'motorcycle'
    elif label == 3:
        string_label = 'vehicle'
    elif label == 4:
        string_label = 'bus'
    elif label == 5:
        string_label = 'truck'
    elif label == 6:
        string_label = 'train'
    else:
        string_label = 'Unknown'
    return string_label


def find_label_location(start_point, end_point):
    label_location = None
    if start_point[1]-10 > 0:
        label_location = (start_point[0], start_point[1]-2)
    else:
        label_location = (start_point[0], end_point[1]-2)
    return label_location


# overlays 'top_image' on top of 'base_image'
def overlay_images_in_position(base_image, top_image, position):
    base_image[position[1]:position[1] + top_image.shape[0], position[0]:position[0] + top_image.shape[1]] = top_image
    return base_image


def create_mosaic_image(frames_list, crop_size, rows, cols):
    mosaic_size = (crop_size[0]*rows, crop_size[1]*cols, 3)
    # base image:
    base_image = np.zeros(mosaic_size, np.uint8)
    # current frame position in the base mosaic:
    current_position = [0]*2

    for i in range(len(frames_list)):
        # current column position:
        current_position[0] = int(i % cols) * crop_size[0]
        # current row position:
        current_position[1] = int(i / rows) * crop_size[1]
            
        # overlay images:
        overlay_images_in_position(base_image, frames_list[i], current_position)

    return base_image


##### MAIN #####

frames_list = []

for file in os.listdir(crops_folder):
    if file.endswith(image_extension):
        frames_list.append(os.path.join(crops_folder, file))
num_frames = len(frames_list)

if num_frames >= 200:
    random.shuffle(frames_list)
    num_frames = int(num_frames/2)
    frames_list = frames_list[0:num_frames]

counter = 0
while counter < num_frames:
    current_frames = []
    for i in range(num_frames_to_show):
        if counter < num_frames:
            mosaic_path = frames_list[counter]

            # read frame:
            frame = cv2.imread(mosaic_path)
            current_frames.append(frame)
            
            # open annotation file:
            with open(mosaic_path.replace(image_extension,".txt")) as annotation_file:
                annotation_lines = annotation_file.readlines()

            for line in annotation_lines:
                if not (len(line) >= 2):  
                    continue
                else: # line is not empty
                    elements = line.replace('\n', '').split(' ')
                    label = int(elements[0])
                    cx = int(float(elements[1])*frame_width)
                    cy = int(float(elements[2])*frame_height)
                    w = int(float(elements[3])*frame_width)
                    h = int(float(elements[4])*frame_height)

                    start_point, end_point = find_start_end_point_rectangle(cx, cy, w, h)
                    # Bounding Box:
                    
                    cv2.rectangle(frame, start_point, end_point, color, thickness)
                    label_location = find_label_location(start_point, end_point)
                    if isAvatar is True:
                        string_label = get_avatar_label_from_int(label)
                    else:
                        string_label = get_label_from_int(label)
                    cv2.putText(frame, string_label, label_location, cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, thickness=1)
            
            counter += 1

            # add the image's name to the frame:
            cv2.putText(frame, os.path.basename(mosaic_path), (10, 10),  cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0,0,0), thickness=1)
        
    to_show = create_mosaic_image(current_frames, (frame_width, frame_height), rows, cols)
    count_down_location = (to_show.shape[0]-130, to_show.shape[1]-10)
    cv2.putText(to_show, str(counter)+" Out of "+str(num_frames), count_down_location, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0), thickness=1)

    # show frame:
    cv2.imshow('Crops', to_show)
    key = cv2.waitKey(0) # for 2 sec pause change to 2000
    
    # if key == 32: # if enter in pressed, pause video
    #     cv2.waitKey(0)
    if key == 27: # if ESC is pressed, exit loop
        cv2.destroyAllWindows()
        break
