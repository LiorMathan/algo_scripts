import os
from PIL import Image


def main():
    while True:
        base_folder = input('Insert folder: ')
        size = int(input('Insert size: '))
        exceptions = recursivly_find_exception(base_folder, (size, size))
        
        print('\n\nExceptions:')
        for exception in exceptions:
            print(exception)
    

def get_file_size(file):
    if os.path.isfile(file):
        return os.path.getsize(file)
    else:
        return 0


def recursivly_find_exception(base_folder: str, size: tuple, file_sort_reverse=False) -> list:
    exceptions = []
    members = os.listdir(base_folder)
    for i in range(len(members)):
        members[i] = os.path.join(base_folder, members[i])

    members.sort(key=get_file_size, reverse=file_sort_reverse)
    
    for member in members:
        if member in exceptions or base_folder in exceptions:
            break

        if os.path.isdir(member):
            if len(os.listdir(member)) == 0:
                exceptions.append(member)
            else:
                exceptions.extend(recursivly_find_exception(member, size))
        elif os.path.isfile(member) and member.endswith('.png'):
            try:
                image = Image.open(member)
                if image.size != size:
                    exceptions.append(base_folder)
                    print(f'Exception - {base_folder}')
                else:
                    print(f'Valid dir - {base_folder}')
                    if (not file_sort_reverse):
                        exceptions = recursivly_find_exception(base_folder, size, file_sort_reverse=True) 
                break
            except:
                pass

    return exceptions


if __name__ == '__main__':
    main()