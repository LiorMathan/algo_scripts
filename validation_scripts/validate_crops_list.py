import os
import cv2

list_path = "/mnt/share/Local/SharingIsCaring/LiorMathan/Test.txt"


def extract_crops_paths(list_path):
    with open(list_path, "r") as f:
        return f.read().splitlines()


def validate_crops(crops_list):
    for i, crop_path in enumerate(crops_list):
        if i % 10000 == 0:
            print(str(i).zfill(6) + " | " + str(len(crops_list)))

        crop_path = crop_path.replace("/mnt/ssdHot", "/mnt/share/ssdHot")

        if not os.path.exists(crop_path):
            print("Not existing file:   " + crop_path)
            continue
            
        # crop = cv2.imread(crop_path)
        # if crop is None:
        #     print("Bad crop:   " + crop_path)
        #     continue

        gt_filename = crop_path.replace('.png', '.txt')
        if not os.path.exists(gt_filename):
            print("Not existing file:   " + gt_filename)
            continue


def validate_list(list_path):
    crops_list = extract_crops_paths(list_path)
    validate_crops(crops_list)


if __name__ == '__main__':
    validate_list(list_path=list_path)
    print("Done!")