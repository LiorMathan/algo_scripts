import os
import cv2
import math
import itertools
import numpy as np
import random


img_width = 640
img_height = 480
rec_color = (0, 0, 0)
rec_thickness = -1
blank_image = 255 * np.ones((img_height, img_width, 3), np.uint8)

font = cv2.FONT_HERSHEY_SIMPLEX
fontScale = 0.5
txt_color = (0, 255, 0)
txt_thickness = 1

def random_rectangle(width, height, flag, i, j):
    x1 = random.randrange(int(width*0.9))
    y1 = random.randrange(int(height*0.9))
    ran = random.choice(np.arange(0.03, 0.1, 0.005))
    ran2 = random.choice(np.arange(0.03, 0.1, 0.005))
    x2 = x1 + int(ran*width)
    y2 = y1 + int(ran2*height)
    xc = x1 + (abs(x2-x1)/2)
    yc = y1 + (abs(y2-y1)/2)
    movie_id = ('10').zfill(5)
    frame_id = str(j).zfill(6)
    class_id = random.choice([0, 1, 2, 3])
    org = (int(x1 + ((x2-x1)/2.5)), int(y2 - ((y2-y1)/2.5)))
    # gt_list.write(str(movie_id) + ' ' + str(frame_id) + ' ' + str(class_id) + ' ' + str(int(xc)) + ' ' + str(int(yc))
    #               + ' ' + str(x2-x1) + ' ' + str(y2-y1) + '\n')
    gt_list.write(str(class_id) + ' ' + str(int(xc)) + ' ' + str(int(yc))
                  + ' ' + str(x2-x1) + ' ' + str(y2-y1) + '\n')
    if flag is 'first':
        im = cv2.rectangle(blank_image, (x1, y1), (x2, y2), rec_color, rec_thickness)
        cv2.putText(im, str(i), org, font, fontScale, txt_color, txt_thickness, cv2.LINE_AA)
        return im
    else:
        im = cv2.rectangle(rec, (x1, y1), (x2, y2), rec_color, rec_thickness)
        cv2.putText(im, str(i), org, font, fontScale, txt_color, txt_thickness, cv2.LINE_AA)
        return im


def random_outline_rectangle(width, height, i, j):
    x1 = random.randrange(int(width*0.9))
    y1 = random.randrange(int(height*0.9))
    ran = random.choice(np.arange(0.03, 0.1, 0.005))
    ran2 = random.choice(np.arange(0.03, 0.1, 0.005))
    x2 = x1 + int(ran*width)
    y2 = y1 + int(ran2*height)
    xc = x1 + (abs(x2-x1)/2)
    yc = y1 + (abs(y2-y1)/2)
    movie_id = ('10').zfill(5)
    frame_id = str(j)
    class_id = random.choice([0, 1, 2, 3])
    conf = random.choice(np.arange(0, 100, 1))
    target = random.choice(np.arange(0, 100, 1))
    dono = random.choice([0, -2])
    org = (x1, y1)
    det_list.write(str(frame_id) + ' ' + str(class_id) + ' ' + str(conf) + ' ' + str(int(xc)) + ' ' + str(int(yc))
                  + ' ' + str(x2-x1) + ' ' + str(y2-y1) + ' ' + str(target) + ' ' + str('2') + ' ' + str(dono) + '\n')
    im = cv2.rectangle(rec, (x1, y1), (x2, y2), (255, 0, 0), 1)
    cv2.putText(im, str(i), org, font, fontScale, (255, 0, 0), txt_thickness, cv2.LINE_AA)
    return im


for j in range(10):
    gt_list = open('/home/shira/workspace/workingzone/Thermal_random/RANDOM_00010_r0_2/RANDOM_00010_r0_2_gt/' + 'RANDOM_00010_r0_2_' + str(str(j).zfill(6)) + '.txt', "w+")
    for i in range(random.choice(np.arange(10, 15))):
        if i == 0:
            flag = 'first'
        else:
            flag = ''
        rec = random_rectangle(640, 480, flag, i, j)
        cv2.imshow('img', rec)
        cv2.waitKey(10)

    det_list = open('/home/shira/workspace/workingzone/random_det_list' + '_' + str(str(j).zfill(3)) + '.txt', "w+")
    for i in range(random.choice(np.arange(20, 25))):
        rec = random_outline_rectangle(640, 480, i, j)
        cv2.imshow('img', rec)
        cv2.waitKey(10)


# cv2.destroyAllWindows()
print("Done")

