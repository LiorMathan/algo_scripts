import os
import cv2
import csv
import math
import pickle
import itertools
import numpy as np
import configparser
from datetime import date
import matplotlib.pyplot as plt

path = r"Z:\Benchmarking\BP\bp_allsampled_cam_motion_130920\BlackPanther-s30-c5-7_res\false_alarm_analysis_130920.csv"
path_trk = r"Z:\Benchmarking\BP\bp_allsampled_cam_motion_130920\BlackPanther-s30-c5-7_res\track_id_analysis_130920.csv"
steps = 4
fps = int(np.ceil(40 / steps))

today = date.today()
date = today.strftime("%d%m%y")
FAR = False

export_path = (os.path.join(os.path.dirname(path), 'false_alarm_analysis_results_' + date))
if not os.path.isdir(export_path):
    os.mkdir(export_path)


with open(path) as fp:
    reader = csv.reader(fp, delimiter=",", quotechar='"')
    next(reader, None)  # skip the headers
    data_read_all = [row for row in reader]

with open(path_trk, 'r') as f:
    reader = csv.reader(f, delimiter=",", quotechar='"')
    next(reader, None)
    trk = [row for row in reader]

""" FILTERING PARAMS AND BAD SCENES """
# if no filtering is needed, comment this section.
bad_scenes = ['Negba_Night_2Drones', 'C0023', 'C0019', 'Negba_Night_Matrix600&Star', 'Drone_Night6_yuv', 'Darom_Day_Drone3',
              'Darom_Day_Drone1', 'BalloonLatrun031119_1620']
data_read = []
for row in data_read_all:
    try:
        if int(float(row[5]))/fps < 3:
            # an object's life time is shorter than 3 seconds
            continue
        if int(row[13]) == 10 or int(row[13]) == 11:
            # an object's source is 10 or 11
            continue
        if int(row[8]) == 3 and int(float(row[11])) > 50:
            # if an object is classified as UF0 with confidence greater than 50%, change its class into drone.
            row[8] = '0'
            data_read.append(row)
        if int(row[8]) == 8 and int(float(row[11])) < 60:
            # an object is classified as drone with lights with confidence less than 60%.
            continue
        if int(row[8]) == 9 and int(float(row[11])) < 50:
            # an object is classified as single front light with confidence less than 50%.
            continue
        else:
            data_read.append(row)
    except:
        continue


""" MAPPING """
# row split variables:
movie_id = 0
frames_num = 1
track_id = 2
min_area = 3
max_area = 4
det_total_life_time = 5
det_life_time = 6
max_lost_time_on_det = 7
major_class = 8
total_time_as_threat = 9
longest_time_as_threat = 10
median_conf = 11
min_snr = 12
source = 13


def create_per_class_lists(lst):
    threats = [0, 5, 8, 9]
    if len(lst[0]) == 14:
        list_drones = [r for r in lst if int(r[8]) == 0]
        list_balloons = [r for r in lst if int(r[8]) == 5]
        list_ufos = [r for r in lst if int(r[8]) == 3]
        list_drone_with_lights = [r for r in lst if int(r[8]) == 8]
        list_single_front_light = [r for r in lst if int(r[8]) == 9]
        list_threats = [r for r in lst if int(r[8]) in threats]
    else:
        list_drones = [r for r in lst if int(r[2]) == 0]
        list_balloons = [r for r in lst if int(r[2]) == 5]
        list_ufos = [r for r in lst if int(r[2]) == 3]
        list_drone_with_lights = [r for r in lst if int(r[2]) == 8]
        list_single_front_light = [r for r in lst if int(r[2]) == 9]
        list_threats = [r for r in lst if int(r[2]) in threats]
    # return [list_drones, list_ufos, list_balloons, list_drone_with_lights, list_single_front_light, list_threats]
    return [list_drones, list_balloons, list_drone_with_lights, list_single_front_light, list_threats]


def create_per_scene_lists(lst):
    key_func = lambda x: x[0]
    groupby_scene = []
    for key, group in itertools.groupby(lst, key_func):
        groupby_scene.append(list(group))
    return groupby_scene


def plot_objects_analysis(lst, val, title, c, label, grouped_by_scene_lists):
    # if c == len(grouped_by_scene_lists) - 1:
    #     label = 'All'
    # list = [int(float(r[val])) for r in lst if r[val] != ""]
    list = [int(float(r[val])) for r in lst if r[val] != "" and int(float(r[val])) != 0]
    if len(list) == 0:
        print('list is empty')
    lst_2 = [r for r in lst if r[val] != "" and int(float(r[val])) != 0]
    groupby_scene = create_per_scene_lists(lst_2)
    total_frames = np.sum([int(r[0][1]) for r in groupby_scene if r[0][1] != ""])
    # total_frames = np.sum([int(r[1]) for r in lst if r[1] != ""])
    time = range(int(np.divide(total_frames, fps)))
    hist, bins = np.histogram(list, bins=100)
    # plt.plot(bins[:-1], hist)
    # plt.show()
    hist_cumsum = np.cumsum(hist)
    sub_cumsum = np.subtract(len(list), hist_cumsum)
    frame_rate = np.divide(bins, fps)
    if FAR:
        per_hour = np.divide(sub_cumsum, (np.max(time) / 3600))
        plt.plot(frame_rate[:-1], per_hour, label=label + ', ' + 'N=' + str(len(list)))
    else:
        plt.plot(frame_rate[:-1], sub_cumsum, label=label + ', ' + 'N=' + str(len(list)))
    # plt.ylim([-0.05, 1.05])
    # plt.legend(prop={'size': 5.0})
    plt.legend()
    if FAR:
        plt.ylabel('FA Rate [# / hour]')
    else:
        plt.ylabel('# of FA')
    plt.xlabel(title)
    # plt.xscale('log')
    return plt


def plot_objects_analysis_conf(lst, val, title, c, label, color, ax):
    list = [int(float(r[val])) for r in lst]
    if len(list) == 0:
        print('list is empty')
    hist, bins = np.histogram(list, bins=np.arange(1, 100))
    hist_cum = np.cumsum(hist)
    hist_cum_sub = np.subtract(hist_cum[-1], hist_cum)
    ax.plot(bins[:-1], np.divide(hist_cum_sub, 1), label=label + ', ' + 'N=' + str(len(list)))
    # ax.plot(bins[:-1], np.divide(hist, 1), label=label + ', ' + 'N=' + str(len(list)))
    # plt.ylim([-0.05, 1.05])
    # plt.legend(prop={'size': 7.0})
    ax.legend()
    ax.set_ylabel('# of TP/FP')
    ax.set_xlabel(title)
    # plt.xscale('log')
    return ax, hist, len(list)


def plot_objects_analysis_conf_hist(lst, val, title, c, label, color, ax):
    list = [int(float(r[val])) for r in lst]
    if len(list) == 0:
        print('list is empty')
    if val == 12:
        list = [np.absolute(r) for r in list]
        hist, bins = np.histogram(list, bins=np.arange(0, 30))
    else:
        hist, bins = np.histogram(list, bins=np.arange(1, 100))
    hist_cum = np.cumsum(hist)
    hist_cum_sub = np.subtract(hist_cum[-1], hist_cum)
    # ax.plot(bins[:-1], np.divide(hist_cum_sub, 1), label=label + ', ' + 'N=' + str(len(list)))
    ax.plot(bins[:-1], np.divide(hist, 1), label=label + ', ' + 'N=' + str(len(list)))
    # plt.ylim([-0.05, 1.05])
    # plt.legend(prop={'size': 7.0})
    ax.legend()
    ax.set_ylabel('# of TP/FP')
    ax.set_xlabel(title)
    # plt.xscale('log')
    return ax, hist, len(list)


def plot_objects_analysis_norm(lst, val, title, label, color):
    # list = [int(float(r[val])) for r in lst if r[val] != ""]
    list = [int(float(r[val])) for r in lst if r[val] != "" and int(float(r[val])) != 0]
    lst_2 = [r for r in lst if r[val] != "" and int(float(r[val])) != 0]
    lst_3 = [r for r in data_read_all]
    if len(list) == 0:
        print('list is empty')
    groupby_scene = create_per_scene_lists(trk)
    # total_frames = sum([int(r[0][1]) for r in groupby_scene])
    # total_frames = np.sum([int(r[0][20]) for r in groupby_scene])
    total_frames = 42000
    time = range(int(np.divide(total_frames, fps)))
    hist, bins = np.histogram(list, bins=range(1, 100))
    # plt.plot(bins[:-1], hist)
    # plt.show()
    hist_cumsum = np.cumsum(hist)
    sub_cumsum = np.subtract(len(list), hist_cumsum)
    per_hour = np.divide(sub_cumsum, (np.max(time) / 3600))
    frame_rate = np.divide(bins, fps)
    plt.plot(frame_rate[:-1], per_hour, label=label + ', ' + 'N=' + str(len(list)), color=color)
    # plt.ylim([-0.05, 1.05])
    plt.legend()
    plt.ylabel('FA Rate [# / hour]')
    plt.xlabel(title)
    # plt.xscale('log')
    return plt


def objects_analysis_per_rate(lst, val, title, label, c, scene, fr, ph, sec):
    threats = ['0', '5', '8', '9']
    list = [int(float(r[val])) for r in lst if r[val] != "" and int(float(r[val])) != 0 and r[8] in threats]
    if len(list) == 0:
        print('list is empty')
    lst_2 = [r for r in lst if r[val] != "" and int(float(r[val])) != 0 and r[8] in threats]
    groupby_scene = create_per_scene_lists(lst_2)
    total_frames = [int(r[0][1]) for r in groupby_scene if r[0][1] != ""][0]
    # total_frames = np.sum([int(r[1]) for r in lst if r[1] != ""])
    time = range(int(np.divide(total_frames, fps)))
    hist, bins = np.histogram(list, bins=100)
    hist_cumsum = np.cumsum(hist)
    sub_cumsum = np.subtract(len(list), hist_cumsum)
    per_hour = np.divide(sub_cumsum, (np.max(time)/3600))
    # per_hour = sub_cumsum
    frame_rate = np.divide(bins, fps)
    frame_rate = frame_rate[:-1]
    frame_rate_ints = np.array([int(r) for r in frame_rate])
    if sec == 0:
        fr_list = np.append(fr, frame_rate[sec])
        val_ph = per_hour[sec]
        ph_list = np.append(ph, val_ph)
        scene_list = np.append(scene, c)
    else:
        a = frame_rate[np.where(frame_rate_ints == sec)]
        fr_list = np.append(fr, a[0])
        val_ph = per_hour[np.where(frame_rate_ints == sec)]
        # if val_ph.shape[0] != 0:
        ph_list = np.append(ph, val_ph[0])
        scene_list = np.append(scene, c)
    return scene_list, fr_list, ph_list


def display_plots(color, lst):
    # fig = plt.figure()
    # fig.suptitle('False Alarm Analysis', fontsize=16)
    plot_objects_analysis_norm(lst, total_time_as_threat, 'Time [sec @ ' + str(fps) + 'Hz]', 'total time as threat',
                               color)
    # plot_objects_analysis(longest_time_as_threat, 'Time [sec @ ' + str(fps) + 'Hz]', 'longest time as threat')
    # fig.tight_layout()
    # fig.subplots_adjust(top=0.91)
    # plt.savefig('/home/shira/Desktop/' + 'false_alarms.png', bbox_inches='tight')
    # plt.show()


def display_plots_per_class():
    label = None
    lst = [r for r in data_read if r[11] != ""]
    grouped_by_class_lists = create_per_class_lists(lst)
    fig = plt.figure()
    fig.suptitle('False Alarm Analysis - per Class', fontsize=16)
    for c, lst in enumerate(grouped_by_class_lists):
        labels = ['Drones', 'UFOs', 'Balloons', 'Drone with Lights', 'Single Front Light', 'Threats']
        plot_objects_analysis(lst, total_time_as_threat, 'Time [sec @ ' + str(fps) + 'Hz]', c, labels[c], "")
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    plt.savefig(os.path.join(export_path, 'false_alarms_per_class.png'), bbox_inches='tight')
    plt.show()


def display_plots_per_scene(a, b):
    grouped_by_scene_lists = create_per_scene_lists(data_read)
    grouped_by_scene_lists = grouped_by_scene_lists[a:b]
    fig = plt.figure()
    fig.suptitle('False Alarm Analysis - per Scene', fontsize=16)
    grouped_by_scene_lists.append(data_read)
    labels = []
    for c, lst in enumerate(grouped_by_scene_lists):
        labels.append(lst[0][0])
        try:
            plot_objects_analysis(lst, total_time_as_threat, 'Time [sec @ ' + str(fps) + 'Hz]', c, labels[c],
                                  grouped_by_scene_lists)
        except:
            continue
        # fig.tight_layout()
    plt.savefig(os.path.join(export_path, 'false_alarms_per_scene' + '.png'), bbox_inches='tight')
    plt.show()


def objects_analysis_rate_per_sec(sec, j, color):
    grouped_by_scene_lists = create_per_scene_lists(data_read)

    # best_empty_scenes = [r[0][0] for r in grouped_by_scene_lists if r[0][8] == '']
    # best_single_fa = [r[0][0] for r in grouped_by_scene_lists if r[0][8] != '' and len(r) == 1]
    # best_scenes_list = best_empty_scenes + best_single_fa
    # with open(os.path.join(export_path, 'best_scenes_' + date + '.csv'), 'w') as bs:
    #     wr = csv.writer(bs)
    #     wr.writerows([[r] for r in best_scenes_list])

    # grouped_by_scene_lists = [r for r in grouped_by_scene_lists_all if r[0][8] in threats or r[0][8] == '']
    scene_list = [(c, i[0][0]) for c, i in enumerate(grouped_by_scene_lists)]
    with open(os.path.join(export_path, "scene_list" + ".csv"), 'w') as file:
        wr = csv.writer(file)
        wr.writerows(scene_list)
    scene_list_frms = [[(c, i[0][0]), i[0][1]] for c, i in enumerate(grouped_by_scene_lists) if i[0][9] != '']
    with open(os.path.join(export_path, "scene_list_frms" + ".csv"), 'w') as file:
        wr = csv.writer(file)
        wr.writerows(scene_list_frms)
    labels = []
    fr = np.empty(0)
    ph = np.empty(0)
    scene = np.empty(0)
    for c, lst in enumerate(grouped_by_scene_lists):
        labels.append(lst[0][0])
        try:
            scene, fr, ph = objects_analysis_per_rate(lst, total_time_as_threat, 'title', labels[c], c, scene, fr, ph,
                                                      sec)
        except:
            continue
    arg_ph = ph.argsort()[::-1]
    sort_ph = np.sort(ph)[::-1]
    scene_sort = []
    for i in arg_ph:
        try:
            scene_sort.append(scene[i])
        except:
            pass
    x = range(len(sort_ph))
    my_xticks = [str(int(r)) for r in scene_sort]
    ax = j.bar(x, sort_ph, label='L @ ' + str(sec), color=color)
    # ax = j.bar(scene, ph, label='L @ ' + str(sec), color=color)
    if len(ax) != 0:
        j.legend()
    j.set_ylabel('FA Rate [# / hour]')
    # j.set_ylabel(' # of FA')
    j.set_xlabel('Scene Num')
    j.set_xticks(np.arange(len(x)))
    j.set_xticklabels(my_xticks, rotation=90, ha="right", fontsize=5.5)
    # j.set_xlim([-5.00, 76.05])
    # j.set_ylim([-0.05, 25.05])
    # plt.ylabel('FA Rate [# / hour]')
    # plt.xlabel('Scene Num')
    return ax


def analyze_scenes_per_life_time():
    fig = plt.figure(figsize=(15, 8))
    ax1 = plt.subplot2grid(shape=(2, 8), loc=(0, 0), colspan=2)
    ax2 = plt.subplot2grid((2, 8), (0, 2), colspan=2)
    ax3 = plt.subplot2grid((2, 8), (0, 4), colspan=2)
    ax4 = plt.subplot2grid((2, 8), (0, 6), colspan=2)
    ax5 = plt.subplot2grid((2, 8), (1, 1), colspan=2)
    ax6 = plt.subplot2grid((2, 8), (1, 3), colspan=2)
    ax7 = plt.subplot2grid((2, 8), (1, 5), colspan=2)
    fig.suptitle('False Alarm Analysis', fontsize=16)
    fig.tight_layout()
    fig.subplots_adjust(top=0.91)
    colors = ['b', 'g', 'c', 'violet', 'purple', 'm', 'y']
    for c, (i, j) in enumerate(zip([0, 3, 5, 10, 15, 30, 45], (ax1, ax2, ax3, ax4, ax5, ax6, ax7))):
        objects_analysis_rate_per_sec(i, j, colors[c])
    plt.savefig(os.path.join(export_path, 'false_alarms_per_sec' + '.png'), bbox_inches='tight')
    plt.show()


def display_grouped_bad_scenes():
    bad_scenes = [29, 73, 68, 56, 28, 31]
    gr_1 = [int(r[0]) for r in data_read]
    gr_2 = [int(r[0]) for r in data_read]
    gr_3 = [int(r[0]) for r in data_read]
    gr_4 = [int(r[0]) for r in data_read]
    gr_5 = [int(r[0]) for r in data_read]
    fig = plt.figure(figsize=(12, 7))
    ax1 = plt.subplot2grid(shape=(2, 6), loc=(0, 0), colspan=2)
    ax2 = plt.subplot2grid((2, 6), (0, 2), colspan=2)
    ax3 = plt.subplot2grid((2, 6), (0, 4), colspan=2)
    ax4 = plt.subplot2grid((2, 6), (1, 1), colspan=2)
    ax5 = plt.subplot2grid((2, 6), (1, 3), colspan=2)
    fig.suptitle('False Alarm Analysis', fontsize=16)
    fig.tight_layout()
    fig.subplots_adjust(top=0.91)
    colors = ['b', 'g', 'c', 'violet', 'purple']
    for c, (i, j) in enumerate(zip([gr_1, gr_2, gr_3, gr_4, gr_5], (ax1, ax2, ax3, ax4, ax5))):
        display_plots(i, j, colors[c])
        # display_plots('r')
    plt.savefig(os.path.join(export_path, 'false_alarms_per_sec' + '.png'), bbox_inches='tight')
    plt.show()


def display_grouped_bad_scenes_single():
    threats = ['0', '5', '8', '9']
    # with open(os.path.join(export_path, 'scene_list.csv')) as fp:
    #     reader = csv.reader(fp, delimiter=",", quotechar='"')
    #     scene_list = [row for row in reader]
    # bad_scenes_str = [r[1] for r in scene_list if r[0] in bad_scenes]
    lst = [r for r in data_read if r[0] not in bad_scenes and r[8] in threats]
    gr_1 = lst
    gr_2 = [r for r in data_read if r[8] in threats]
    fig = plt.figure(figsize=(12, 7))
    fig.suptitle('False Alarm Analysis', fontsize=16)
    fig.tight_layout()
    fig.subplots_adjust(top=0.91)
    colors = ['b', 'g', 'c', 'violet', 'purple']
    for c, (i) in enumerate([gr_1, gr_2]):
        display_plots(colors[c], i)
    plt.savefig(os.path.join(export_path, 'false_alarms_comparison' + '.png'), bbox_inches='tight')
    plt.show()


def analyze_bad_scenes():
    with open( path_trk, 'r') as f:
        reader = csv.reader(f, delimiter=",", quotechar='"')
        trk = [row for row in reader]
    with open(os.path.join(export_path, 'scene_list.csv')) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        scene_list = [row for row in reader]
    with open(os.path.join(export_path, 'scene_list_frms.csv')) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        scene_list_frms = [row for row in reader]
    bad_scenes_str = [r[1] for r in scene_list if int(r[0]) in bad_scenes]
    lst = [r for r in trk if r[0] in bad_scenes_str]
    grouped_by_scene_lists = create_per_scene_lists(lst)
    with open(os.path.join(export_path, 'bad_scene_analysis.csv'), 'w') as w:
        wr = csv.writer(w)
        for r in grouped_by_scene_lists:
            movie_str = r[0][0]
            movie_id = [r[0] for r in scene_list if movie_str in r][0]
            frames_num = [int(r[1]) for r in scene_list_frms if movie_id in r[0]][0]
            num_of_gts = len([c[1] for c in r if int(c[1]) > 0])
            wr.writerow([movie_str, num_of_gts, frames_num])


def analyze_conf_id():
    with open(path_trk) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        next(reader, None)  # skip the headers
        data_read_trk = [row for row in reader]
    lst = [r for r in data_read if r[11] != ""]
    lst_trk = [r for r in data_read_trk if r[-2] != ""]
    lst_all = [r for r in data_read_trk]
    grouped_by_class_lists = create_per_class_lists(lst)
    grouped_by_class_lists_trk = create_per_class_lists(lst_trk)
    grouped_by_class_lists_trk_all = create_per_class_lists(lst_all)
    fig = plt.figure(figsize=(15, 8))
    ax1 = plt.subplot2grid(shape=(2, 6), loc=(0, 0), colspan=2)
    ax2 = plt.subplot2grid((2, 6), (0, 2), colspan=2)
    ax3 = plt.subplot2grid((2, 6), (0, 4), colspan=2)
    ax4 = plt.subplot2grid((2, 6), (1, 0), colspan=2)
    ax5 = plt.subplot2grid((2, 6), (1, 2), colspan=2)
    ax6 = plt.subplot2grid((2, 6), (1, 4), colspan=2)
    fig.suptitle('False Alarm Analysis - per Class', fontsize=16)
    fig.tight_layout()
    fig.subplots_adjust(top=0.91)
    colors = ['b', 'g', 'c', 'violet', 'purple', 'm', 'y']
    hist_tp_lst, hist_fa_lst, len_tps_lst, len_fas_lst = [], [], [], []
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists), (ax1, ax2, ax3, ax4, ax5, ax6))):
        labels = ['Drones_FA', 'UFOs_FA', 'Balloons_FA', 'Drone with Lights_FA', 'Single Front Light_FA', 'Threats_FA']
        _, hist_fa, len_fas = plot_objects_analysis_conf_hist(lst, median_conf, 'Confidence', c, labels[c], colors[c], ax)
        hist_fa_lst.append(hist_fa)
        len_fas_lst.append(len_fas)
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_trk), (ax1, ax2, ax3, ax4, ax5, ax6))):
        labels = ['Drones_TP', 'UFOs_TP', 'Balloons_TP', 'Drone with Lights_TP', 'Single Front Light_TP', 'Threats_TP']
        _, hist_tp, len_tps = plot_objects_analysis_conf_hist(lst, -1, 'Confidence', c, labels[c], colors[c], ax)
        hist_tp_lst.append(hist_tp)
        len_tps_lst.append(len_tps)
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_trk), (ax1, ax2, ax3, ax4, ax5, ax6))):
        gt_lst_label = len(grouped_by_class_lists_trk_all[c])
        hist_tp_lst_label = hist_tp_lst[c]
        hist_fa_lst_label = hist_fa_lst[c]
        hist_tp_lst_label_cum = np.cumsum(hist_tp_lst_label)
        hist_fa_lst_label_cum = np.cumsum(hist_fa_lst_label)
        # fns = np.subtract(gt_lst_label, len_tps_lst[c])
        # fns_cum = np.cumsum(fns)
        cumsum_sub_fps = np.subtract(len_fas_lst[c], hist_fa_lst_label_cum)
        cumsum_sub_tps = np.subtract(len_tps_lst[c], hist_tp_lst_label_cum)
        fns_cumsum_sub = gt_lst_label - cumsum_sub_tps
        f1 = []
        for i in range(len(hist_fa_lst_label_cum)):
            prec, recall, f = calculte_fscore(cumsum_sub_tps[i], cumsum_sub_fps[i], fns_cumsum_sub[i])
            f1.append(f)
        try:
            max_f = np.nanargmax(f1)
        except:
            max_f = np.argmax(f1)
        ax.plot(range(len(hist_fa_lst_label_cum)), np.divide(fns_cumsum_sub, 10), label='FNs [/10]')
        ax.axvline(x=max_f, linestyle='--', label='Argmax: ' + str(max_f), color='m')
        # ax.plot(range(len(hist_fp_lst_label_cum)), f1, label='f1 score')
        ax.legend()
        ax2 = ax.twinx()
        # ax2.set_ylabel('score')
        ax2.plot(range(len(hist_fa_lst_label_cum)), f1, label='f1 score', color='r')
        ax2.tick_params(axis='y')
        ax2.legend(loc='upper left')
    plt.savefig(os.path.join(export_path, 'false_alarms_per_class_conf_id.png'), bbox_inches='tight')
    plt.show()


def analyze_snr_id():
    with open(path_trk) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        next(reader, None)  # skip the headers
        data_read_trk = [row for row in reader]
    lst = [r for r in data_read if r[12] != ""]
    lst_trk = [r for r in data_read_trk if r[-3] != ""]
    lst_all = [r for r in data_read_trk]
    grouped_by_class_lists = create_per_class_lists(lst)
    grouped_by_class_lists_trk = create_per_class_lists(lst_trk)
    grouped_by_class_lists_trk_all = create_per_class_lists(lst_all)
    fig = plt.figure(figsize=(15, 8))
    ax1 = plt.subplot2grid(shape=(2, 6), loc=(0, 0), colspan=2)
    ax2 = plt.subplot2grid((2, 6), (0, 2), colspan=2)
    ax3 = plt.subplot2grid((2, 6), (0, 4), colspan=2)
    ax4 = plt.subplot2grid((2, 6), (1, 0), colspan=2)
    ax5 = plt.subplot2grid((2, 6), (1, 2), colspan=2)
    ax6 = plt.subplot2grid((2, 6), (1, 4), colspan=2)
    fig.suptitle('False Alarm Analysis - per Class - by ID', fontsize=16)
    fig.tight_layout()
    fig.subplots_adjust(top=0.91)
    colors = ['b', 'g', 'c', 'violet', 'purple', 'm', 'y']
    hist_tp_lst, hist_fa_lst, len_tps_lst, len_fas_lst = [], [], [], []
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists), (ax1, ax2, ax3, ax4, ax5, ax6))):
        labels = ['Drones_FA', 'UFOs_FA', 'Balloons_FA', 'Drone with Lights_FA', 'Single Front Light_FA', 'Threats_FA']
        _, hist_fa, len_fas = plot_objects_analysis_conf_hist(lst, min_snr, 'SNR', c, labels[c], colors[c], ax)
        hist_fa_lst.append(hist_fa)
        len_fas_lst.append(len_fas)
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_trk), (ax1, ax2, ax3, ax4, ax5, ax6))):
        labels = ['Drones_TP', 'UFOs_TP', 'Balloons_TP', 'Drone with Lights_TP', 'Single Front Light_TP', 'Threats_TP']
        _, hist_tp, len_tps = plot_objects_analysis_conf_hist(lst, -3, 'SNR', c, labels[c], colors[c], ax)
        hist_tp_lst.append(hist_tp)
        len_tps_lst.append(len_tps)
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_trk), (ax1, ax2, ax3, ax4, ax5, ax6))):
        gt_lst_label = len(grouped_by_class_lists_trk_all[c])
        hist_tp_lst_label = hist_tp_lst[c]
        hist_fa_lst_label = hist_fa_lst[c]
        hist_tp_lst_label_cum = np.cumsum(hist_tp_lst_label)
        hist_fa_lst_label_cum = np.cumsum(hist_fa_lst_label)
        # fns = np.subtract(gt_lst_label, len_tps_lst[c])
        # fns_cum = np.cumsum(fns)
        cumsum_sub_fps = np.subtract(len_fas_lst[c], hist_fa_lst_label_cum)
        cumsum_sub_tps = np.subtract(len_tps_lst[c], hist_tp_lst_label_cum)
        fns_cumsum_sub = gt_lst_label - cumsum_sub_tps
        f1 = []
        for i in range(len(hist_fa_lst_label_cum)):
            prec, recall, f = calculte_fscore(cumsum_sub_tps[i], cumsum_sub_fps[i], fns_cumsum_sub[i])
            f1.append(f)
        try:
            max_f = np.nanargmax(f1)
        except:
            max_f = np.argmax(f1)
        bins = np.arange(0, len(fns_cumsum_sub) + 1)
        ax.plot(bins[:-1], np.divide(fns_cumsum_sub, 10), label='FNs [/10]')
        ax.axvline(bins[:-1][max_f], linestyle='--', label='Argmax: ' + str(max_f), color='m')
        # ax.plot(range(len(hist_fp_lst_label_cum)), f1, label='f1 score')
        ax.legend()
        ax2 = ax.twinx()
        # ax2.set_ylabel('score')
        ax2.plot(np.arange(0, len(f1)), f1, label='f1 score', color='r')
        ax2.tick_params(axis='y')
        ax2.legend(loc='upper left')
    plt.savefig(os.path.join(export_path, 'false_alarms_per_class_snr_id.png'), bbox_inches='tight')
    plt.show()


def create_gt_lists_for_fscore():
    threats = [0, 5, 8, 9]
    for f in os.listdir(os.path.dirname(path)):
        if f.endswith('pickle'):
            with open((os.path.join(os.path.dirname(path), f)), 'rb') as p:
                comb_lst = pickle.load(p)
                gt_lst = [[] for _ in range(10)]
                for c, v in enumerate(comb_lst):
                    if len(v) == 0:
                        continue
                    for i in range(10):
                        gt_lst[i].append(len(v[i]))
            break
    gt_lst_sum = [np.sum(r) for r in gt_lst]
    gt_lst_threats = [(c, v) for (c, v) in enumerate(gt_lst_sum) if c in threats]
    all = np.sum([r[1] for r in gt_lst_threats])
    gt_lst_threats.append((111, all))
    return gt_lst_threats


def calculte_fscore(tps, fps, fns):
    prec = round(tps / (tps + fps), 3)
    recall = round(tps / (tps + fns), 3)
    f1_score = round(2 * ((prec * recall) / (prec + recall)), 3)
    return prec, recall, f1_score


def analyze_conf_tps_fps():
    with open(path_fp) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        next(reader, None)  # skip the headers
        data_read_fp = [row for row in reader]
    with open(path_tp) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        next(reader, None)  # skip the headers
        data_read_tp = [row for row in reader]
    label = None
    lst_fp = [r for r in data_read_fp]
    lst_tp = [r for r in data_read_tp if int(r[-2]) > 0]
    grouped_by_class_lists_fp = create_per_class_lists(lst_fp)
    grouped_by_class_lists_tp = create_per_class_lists(lst_tp)
    fig = plt.figure(figsize=(15, 8))
    ax1 = plt.subplot2grid(shape=(2, 6), loc=(0, 0), colspan=2)
    ax2 = plt.subplot2grid((2, 6), (0, 2), colspan=2)
    ax3 = plt.subplot2grid((2, 6), (0, 4), colspan=2)
    ax4 = plt.subplot2grid((2, 6), (1, 0), colspan=2)
    ax5 = plt.subplot2grid((2, 6), (1, 2), colspan=2)
    ax6 = plt.subplot2grid((2, 6), (1, 4), colspan=2)
    fig.suptitle('False Alarm Analysis - per Class', fontsize=16)
    fig.tight_layout()
    fig.subplots_adjust(top=0.91)
    colors = ['b', 'g', 'c', 'violet', 'purple', 'm', 'y']
    hist_tp_lst, hist_fp_lst, len_tps_lst, len_fps_lst = [], [], [], []
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_fp), (ax1, ax2, ax3, ax4, ax5))):
        labels = ['Drones_FP', 'UFOs_FP', 'Balloons_FP', 'Drone with Lights_FP', 'Single Front Light_FP', 'Threats_FP']
        _, hist_fp, len_fps = plot_objects_analysis_conf(lst, -1, 'Confidence', c, labels[c],  colors[c], ax)
        hist_fp_lst.append(hist_fp)
        len_fps_lst.append(len_fps)
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_tp), (ax1, ax2, ax3, ax4, ax5))):
        labels = ['Drones_TP', 'UFOs_TP', 'Balloons_TP', 'Drone with Lights_TP', 'Single Front Light_TP', 'Threats_TP']
        _, hist_tp, len_tps = plot_objects_analysis_conf(lst, -2, 'Confidence', c, labels[c], colors[c], ax)
        hist_tp_lst.append(hist_tp)
        len_tps_lst.append(len_tps)
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)

    gt_lst_sum = create_gt_lists_for_fscore()
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_tp), (ax1, ax2, ax3, ax4, ax5))):
        gt_lst_label = gt_lst_sum[c][1]
        hist_tp_lst_label = hist_tp_lst[c]
        hist_fp_lst_label = hist_fp_lst[c]
        hist_tp_lst_label_cum = np.cumsum(hist_tp_lst_label)
        hist_fp_lst_label_cum = np.cumsum(hist_fp_lst_label)
        # fns = np.subtract(gt_lst_label, len_tps_lst[c])
        # fns_cum = np.cumsum(fns)
        cumsum_sub_fps = np.subtract(len_fps_lst[c], hist_fp_lst_label_cum)
        cumsum_sub_tps = np.subtract(len_tps_lst[c], hist_tp_lst_label_cum)
        fns_cumsum_sub = gt_lst_label - cumsum_sub_tps
        f1 = []
        for i in range(len(hist_fp_lst_label_cum)):
            prec, recall, f = calculte_fscore(cumsum_sub_tps[i], cumsum_sub_fps[i], fns_cumsum_sub[i])
            f1.append(f)
        try:
            max_f = np.nanargmax(f1)
        except:
            max_f = np.argmax(f1)
        ax.plot(range(len(hist_fp_lst_label_cum)), fns_cumsum_sub, label='FNs')
        ax.axvline(x=max_f, linestyle='--', label='Argmax: ' + str(max_f), color='m')
        # ax.plot(range(len(hist_fp_lst_label_cum)), f1, label='f1 score')
        ax.legend()

        ax2 = ax.twinx()
        # ax2.set_ylabel('score')
        ax2.plot(range(len(hist_fp_lst_label_cum)), f1, label='f1 score', color='r')
        ax2.tick_params(axis='y')
        ax2.legend(loc='upper left')
    plt.savefig(os.path.join(export_path, 'false_alarms_per_class_conf.png'), bbox_inches='tight')
    plt.show()


def analyze_conf_tps_fps_hist():
    with open(path_fp) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        next(reader, None)  # skip the headers
        data_read_fp = [row for row in reader]
    with open(path_tp) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        next(reader, None)  # skip the headers
        data_read_tp = [row for row in reader]
    label = None
    lst_fp = [r for r in data_read_fp]
    lst_tp = [r for r in data_read_tp if int(r[-2]) > 0]
    grouped_by_class_lists_fp = create_per_class_lists(lst_fp)
    grouped_by_class_lists_tp = create_per_class_lists(lst_tp)
    fig2 = plt.figure(figsize=(15, 8))
    ax1 = plt.subplot2grid(shape=(2, 6), loc=(0, 0), colspan=2)
    ax2 = plt.subplot2grid((2, 6), (0, 2), colspan=2)
    ax3 = plt.subplot2grid((2, 6), (0, 4), colspan=2)
    ax4 = plt.subplot2grid((2, 6), (1, 0), colspan=2)
    ax5 = plt.subplot2grid((2, 6), (1, 2), colspan=2)
    ax6 = plt.subplot2grid((2, 6), (1, 4), colspan=2)
    fig2.suptitle('False Alarm Analysis - per Class', fontsize=16)
    fig2.tight_layout()
    fig2.subplots_adjust(top=0.91)
    colors = ['b', 'g', 'c', 'violet', 'purple', 'm', 'y']
    hist_tp_lst, hist_fp_lst, len_tps_lst, len_fps_lst = [], [], [], []
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_fp), (ax1, ax2, ax3, ax4, ax5))):
        labels = ['Drones_FP', 'Balloons_FP', 'Drone with Lights_FP', 'Single Front Light_FP', 'Threats_FP']
        _, hist_fp, len_fps = plot_objects_analysis_conf_hist(lst, -2, 'Confidence', c, labels[c],  colors[c], ax)
        hist_fp_lst.append(hist_fp)
        len_fps_lst.append(len_fps)
        fig2.tight_layout()
        fig2.subplots_adjust(top=0.91)
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_tp), (ax1, ax2, ax3, ax4, ax5))):
        labels = ['Drones_TP', 'Balloons_TP', 'Drone with Lights_TP', 'Single Front Light_TP', 'Threats_TP']
        _, hist_tp, len_tps = plot_objects_analysis_conf_hist(lst, -3, 'Confidence', c, labels[c], colors[c], ax)
        hist_tp_lst.append(hist_tp)
        len_tps_lst.append(len_tps)
        fig2.tight_layout()
        fig2.subplots_adjust(top=0.91)

    gt_lst_sum = create_gt_lists_for_fscore()
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_tp), (ax1, ax2, ax3, ax4, ax5))):
        gt_lst_label = gt_lst_sum[c][1]
        hist_tp_lst_label = hist_tp_lst[c]
        hist_fp_lst_label = hist_fp_lst[c]
        hist_tp_lst_label_cum = np.cumsum(hist_tp_lst_label)
        hist_fp_lst_label_cum = np.cumsum(hist_fp_lst_label)
        # fns = np.subtract(gt_lst_label, len_tps_lst[c])
        # fns_cum = np.cumsum(fns)
        cumsum_sub_fps = np.subtract(len_fps_lst[c], hist_fp_lst_label_cum)
        cumsum_sub_tps = np.subtract(len_tps_lst[c], hist_tp_lst_label_cum)
        fns_cumsum_sub = gt_lst_label - cumsum_sub_tps
        f1, prec_lst, recall_lst = [], [], []
        for i in range(len(hist_fp_lst_label_cum)):
            prec, recall, f = calculte_fscore(cumsum_sub_tps[i], cumsum_sub_fps[i], fns_cumsum_sub[i])
            f1.append(f)
            prec_lst.append(prec)
            recall_lst.append(recall)
        try:
            max_f = np.nanargmax(f1)
        except:
            max_f = np.argmax(f1)
        ax.plot(range(len(hist_fp_lst_label_cum)), np.divide(fns_cumsum_sub, 10), label='FNs [/10]')
        ax.axvline(x=max_f, linestyle='--', label='Argmax: ' + str(max_f), color='m')
        # ax.plot(range(len(hist_fp_lst_label_cum)), f1, label='f1 score')
        ax.legend()

        ax2 = ax.twinx()
        # ax2.set_ylabel('score')
        # ax2.plot(range(len(hist_fp_lst_label_cum)), f1, label='f1 score', color='r')
        ax2.plot(range(len(hist_fp_lst_label_cum)), prec_lst, label='prec', color='r')
        ax2.plot(range(len(hist_fp_lst_label_cum)), recall_lst, label='recall', color='black')
        ax2.tick_params(axis='y')
        ax2.legend(loc='upper left')
    plt.savefig(os.path.join(export_path, 'false_alarms_per_class_conf_hist.png'), bbox_inches='tight')
    plt.show()


def analyze_snr_tps_fps_hist():
    with open(path_fp) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        next(reader, None)  # skip the headers
        data_read_fp = [row for row in reader]
    with open(path_tp) as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        next(reader, None)  # skip the headers
        data_read_tp = [row for row in reader]
    lst_fp = [r for r in data_read_fp]
    lst_tp = [r for r in data_read_tp if int(r[-2]) > 0]
    grouped_by_class_lists_fp = create_per_class_lists(lst_fp)
    grouped_by_class_lists_tp = create_per_class_lists(lst_tp)
    fig2 = plt.figure(figsize=(15, 8))
    ax1 = plt.subplot2grid(shape=(2, 6), loc=(0, 0), colspan=2)
    ax2 = plt.subplot2grid((2, 6), (0, 2), colspan=2)
    ax3 = plt.subplot2grid((2, 6), (0, 4), colspan=2)
    ax4 = plt.subplot2grid((2, 6), (1, 0), colspan=2)
    ax5 = plt.subplot2grid((2, 6), (1, 2), colspan=2)
    ax6 = plt.subplot2grid((2, 6), (1, 4), colspan=2)
    fig2.suptitle('False Alarm Analysis - per Class', fontsize=16)
    fig2.tight_layout()
    fig2.subplots_adjust(top=0.91)
    colors = ['b', 'g', 'c', 'violet', 'purple', 'm', 'y']
    hist_tp_lst, hist_fp_lst, len_tps_lst, len_fps_lst = [], [], [], []
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_fp), (ax1, ax2, ax3, ax4, ax5))):
        labels = ['Drones_FP', 'UFOs_FP', 'Balloons_FP', 'Drone with Lights_FP', 'Single Front Light_FP', 'Threats_FP']
        _, hist_fp, len_fps = plot_objects_analysis_conf_hist(lst, -1, 'SNR', c, labels[c],  colors[c], ax)
        hist_fp_lst.append(hist_fp)
        len_fps_lst.append(len_fps)
        fig2.tight_layout()
        fig2.subplots_adjust(top=0.91)
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_tp), (ax1, ax2, ax3, ax4, ax5))):
        labels = ['Drones_TP', 'UFOs_TP', 'Balloons_TP', 'Drone with Lights_TP', 'Single Front Light_TP', 'Threats_TP']
        _, hist_tp, len_tps = plot_objects_analysis_conf_hist(lst, -1, 'SNR', c, labels[c], colors[c], ax)
        hist_tp_lst.append(hist_tp)
        len_tps_lst.append(len_tps)
        fig2.tight_layout()
        fig2.subplots_adjust(top=0.91)

    gt_lst_sum = create_gt_lists_for_fscore()
    for c, (lst, ax) in enumerate(zip((grouped_by_class_lists_tp), (ax1, ax2, ax3, ax4, ax5))):
        gt_lst_label = gt_lst_sum[c][1]
        hist_tp_lst_label = hist_tp_lst[c]
        hist_fp_lst_label = hist_fp_lst[c]
        hist_tp_lst_label_cum = np.cumsum(hist_tp_lst_label)
        hist_fp_lst_label_cum = np.cumsum(hist_fp_lst_label)
        # fns = np.subtract(gt_lst_label, len_tps_lst[c])
        # fns_cum = np.cumsum(fns)
        cumsum_sub_fps = np.subtract(len_fps_lst[c], hist_fp_lst_label_cum)
        cumsum_sub_tps = np.subtract(len_tps_lst[c], hist_tp_lst_label_cum)
        fns_cumsum_sub = gt_lst_label - cumsum_sub_tps
        f1 = []
        for i in range(len(hist_fp_lst_label_cum)):
            prec, recall, f = calculte_fscore(cumsum_sub_tps[i], cumsum_sub_fps[i], fns_cumsum_sub[i])
            f1.append(f)
        try:
            max_f = np.nanargmax(f1)
        except:
            max_f = np.argmax(f1)
        bins = np.arange(0, len(fns_cumsum_sub)+1)
        ax.plot(range(len(hist_fp_lst_label_cum)), np.divide(fns_cumsum_sub, 10), label='FNs [/10]')
        ax.axvline(x=bins[:-1][max_f], linestyle='--', label='Argmax: ' + str(max_f), color='m')
        # ax.plot(range(len(hist_fp_lst_label_cum)), f1, label='f1 score')
        ax.legend()

        ax2 = ax.twinx()
        # ax2.set_ylabel('score')
        ax2.plot(range(len(hist_fp_lst_label_cum)), f1, label='f1 score', color='r')
        ax2.tick_params(axis='y')
        ax2.legend(loc='upper left')
    plt.savefig(os.path.join(export_path, 'false_alarms_per_class_snr_hist.png'), bbox_inches='tight')
    plt.show()


""" MORE OPTIONS """
### Different kinds of false alarm analysis:
path_fp = r"Z:\Benchmarking\BP\bp_allsampled_cam_motion_130920\BlackPanther-s30-c5-7_res\falses_list_130920.csv"
path_tp = r"Z:\Benchmarking\BP\bp_allsampled_cam_motion_130920\BlackPanther-s30-c5-7_res\tps_list_130920.csv"
# analyze_conf_id()  # analyze confidence value per class in order to find a suitable threshold.
# analyze_conf_tps_fps()  # analyze confidence value of the tps/fps lists (no track id filtering) per class in order to find a suitable threshold. for this analysis, please fill the path for the fps and tps lists exported by the main script.
analyze_conf_tps_fps_hist()  # similar to analyze_conf_tps_fps, including precision and recall histogram. for this analysis, please fill the path for the fps and tps lists exported by the main script.
# analyze_snr_id()  # analyze snr value per class in order to find a suitable threshold.
# analyze_snr_tps_fps_hist()  # analyze snr value of the tps/fps lists (no track id filtering) per class in order to find a suitable threshold. for this analysis, please fill the path for the fps and tps lists exported by the main script.


""" MAIN """
# Number of fa's vs. time

# False alarm analysis per class in threats:
# display_plots_per_class()

# False alarm analysis per scene:
# display_plots_per_scene(0, -1)

# Detecting bad scenes by false alarm analysis:
# analyze_scenes_per_life_time()

# Showing analysis with and without bad scenes:
display_grouped_bad_scenes_single()



plt.close()
print('Done')
