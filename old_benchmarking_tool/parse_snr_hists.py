import pickle
import numpy as np
import matplotlib.pyplot as plt

with open('/home/shira/Desktop/h_detected_1005.pickle', 'rb') as f:
    h_detected = pickle.load(f)
with open('/home/shira/Desktop/h_not_detected_1005.pickle', 'rb') as f:
    h_not_detected = pickle.load(f)

sum_tp_fn = np.add(h_detected[0], h_not_detected[0])
recall = np.divide(h_detected[0], sum_tp_fn)
np.savetxt("/home/shira/Desktop/h_detected_1005.csv", h_detected[0], delimiter=",")
np.savetxt("/home/shira/Desktop/h_not_detected_1005.csv", h_not_detected[0], delimiter=",")

fig = plt.figure(figsize=(6, 4))
ax = fig.add_subplot(111)
ax.set_title('recall')
cmap = plt.get_cmap('Set3')
cmap.set_bad(color='gray')
plt.imshow(recall.T, origin='lower', extent=[0, 85, -3, 12], cmap=cmap, aspect='auto')
cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
cax.get_xaxis().set_visible(False)
cax.get_yaxis().set_visible(False)
cax.patch.set_alpha(0)
cax.set_frame_on(False)
plt.colorbar(shrink=0.9, pad=0.005)
plt.show(fig)

# plt.savefig('/home/shira/Desktop/recall_1005.png', bbox_inches='tight')

plt.close('all')
pass