import os
import numpy as np
import configparser
from datetime import date

today = date.today()
date = today.strftime("%d%m%y")

""" CONFIG """
try:
    config = configparser.ConfigParser()
    config.read('benchmark_main.ini')
except:
    print('Config file not found')
    exit()

classes = list(range(config.getint('dirs', 'classes')))
export_path = config.get('dirs', 'export_path')


def parse_labels(label):
    if label == 0:
        return 'drone', 'threat'
    elif label == 1:
        return 'airplane', 'non-threat'
    elif label == 2:
        return 'bird', 'non-threat'
    elif label == 3:
        return 'UFO', 'threat'
    elif label == 4:
        return 'airplane with lights', 'non-threat'
    elif label == 5:
        return 'balloon', 'threat'
    elif label == 6:
        return 'human', 'non-threat'
    elif label == 7:
        return 'vehicle', 'non-threat'
    elif label == 8:
        return 'drone with lights', 'threat'
    elif label == 9:
        return 'single front light', 'threat'
    else:
        return 'Unknown', 'Unknown'
    

def get_fp_from_all_scenes(flag):
    # path = '/home/shira/Desktop/tst'
    fp_all = []
    if flag == '':
        for f in os.listdir(path):
            if 'cm_all' in f:
                file = open(os.path.join(path, f), 'r')
                lines = file.readlines()
                for c, line in enumerate(lines):
                    if 'set 1' in line:
                        try:
                            fp_all.append(float(lines[c + 1][:-1].split(':')[-1]))
                            break
                        except:
                            break
    else:
        for cls in classes:
            _, grp = parse_labels(cls)
            if flag == 'threats' and grp == 'threat' or flag == 'non-threats' and grp == 'non-threat' or flag == 'class':
                for f in os.listdir(path):
                    if 'cm_per_class' in f:
                        file = open(os.path.join(path, f), 'r')
                        lines = file.readlines()
                        for c, line in enumerate(lines):
                            if '*** cls_' + str(cls) in line:
                                cls_id = line.split('_')[1][0]
                                fp_all.append(float(lines[c + 2][:-1].split(':')[-1]))
                                break
    return np.sum(fp_all)


def compute_confusion_metrics(fn_all_sum, fp_all_sum, tp_a_all_sum, tp_b_all_sum, tp_c_all_sum, tp_d_all_sum, tp_e_all_sum):
    fp_1 = fp_all_sum + tp_c_all_sum + tp_d_all_sum + tp_e_all_sum
    tp_1 = tp_a_all_sum + tp_b_all_sum
    fn_1 = fn_all_sum + tp_c_all_sum + tp_d_all_sum + tp_e_all_sum
    prec_1 = round(tp_1 / (tp_1 + fp_1), 3)
    recall_1 = round(tp_1 / (tp_1 + fn_1), 3)
    f1_score_1 = round(2 * ((prec_1 * recall_1) / (prec_1 + recall_1)), 3)

    fp_2 = fp_all_sum + tp_e_all_sum
    tp_2 = tp_a_all_sum + tp_b_all_sum + tp_c_all_sum + tp_d_all_sum
    fn_2 = fn_all_sum + tp_e_all_sum
    prec_2 = round(tp_2 / (tp_2 + fp_2), 3)
    recall_2 = round(tp_2 / (tp_2 + fn_2), 3)
    f1_score_2 = round(2 * ((prec_2 * recall_2) / (prec_2 + recall_2)), 3)
    return prec_1, recall_1, f1_score_1, prec_2, recall_2, f1_score_2


def get_parsed_metrics_from_cm_per_class(path):
    title = os.path.basename(path)
    cls_id = 'None'
    for cls in classes:
            fp_all, fn_all, tp_a_all, tp_b_all, tp_c_all, tp_d_all, tp_e_all, nms_all = [], [], [], [], [], [], [], []
            for f in os.listdir(path):
                if 'cm_per_class' in f:
                    file = open(os.path.join(path, f), 'r')
                    lines = file.readlines()
                    for c, line in enumerate(lines):
                        if '*** cls_' + str(cls) in line:
                            cls_id = line.split('_')[1][0]
                            fn_all.append(float(lines[c + 3][:-1].split(':')[-1]))
                            tp_a_all.append(float(lines[c + 4][:-1].split(':')[-1]))
                            tp_b_all.append(float(lines[c + 5][:-1].split(':')[-1]))
                            tp_c_all.append(float(lines[c + 6][:-1].split(':')[-1]))
                            tp_d_all.append(float(lines[c + 7][:-1].split(':')[-1]))
                            tp_e_all.append(float(lines[c + 8][:-1].split(':')[-1]))
                            nms_all.append(float(lines[c + 9][:-1].split(':')[-1]))
                            break
            fn_all_sum = np.sum(fn_all)
            fp_all_sum = get_fp_from_all_scenes('class')
            tp_a_all_sum = np.sum(tp_a_all)
            tp_b_all_sum = np.sum(tp_b_all)
            tp_c_all_sum = np.sum(tp_c_all)
            tp_d_all_sum = np.sum(tp_d_all)
            tp_e_all_sum = np.sum(tp_e_all)
            nms_all_sum = np.sum(nms_all)
            prec_class, recall_class, f1_score_class, prec_group, recall_group, f1_score_group \
                = compute_confusion_metrics(fn_all_sum, fp_all_sum, tp_a_all_sum, tp_b_all_sum, tp_c_all_sum,
                                                tp_d_all_sum, tp_e_all_sum)
            res_file = open(os.path.join(path, title + '_parsed_per_class_' + cls_id + '_' + date + '.txt'), 'w')
            res_file.write(
                        'prec_class_all: ' + str(prec_class) + '\n' +
                        'recall_class_all: ' + str(recall_class) + '\n' +
                        'f1_score_class_all: ' + str(f1_score_class) + '\n' +
                        'prec_group_all: ' + str(prec_group) + '\n' +
                        'recall_group_all: ' + str(recall_group) + '\n' +
                        'f1_score_group_all: ' + str(f1_score_group) + '\n' +
                        'nms_all: ' + str(nms_all_sum))
            res_file.close()
            # file.close()


def get_parsed_metrics_from_cm_group(flag, path):
    title = os.path.basename(path)
    fp_all, fn_all, tp_a_all, tp_b_all, tp_c_all, tp_d_all, tp_e_all, nms_all = [], [], [], [], [], [], [], []
    for cls in classes:
        _, grp = parse_labels(cls)
        if flag == 'threats' and grp == 'threat' or flag == 'non-threats' and grp == 'non-threat':
            for f in os.listdir(path):
                if 'cm_per_class' in f:
                    file = open(os.path.join(path, f), 'r')
                    lines = file.readlines()
                    for c, line in enumerate(lines):
                        if '*** cls_' + str(cls) in line:
                            cls_id = line.split('_')[1][0]
                            fn_all.append(float(lines[c + 3][:-1].split(':')[-1]))
                            tp_a_all.append(float(lines[c + 4][:-1].split(':')[-1]))
                            tp_b_all.append(float(lines[c + 5][:-1].split(':')[-1]))
                            tp_c_all.append(float(lines[c + 6][:-1].split(':')[-1]))
                            tp_d_all.append(float(lines[c + 7][:-1].split(':')[-1]))
                            tp_e_all.append(float(lines[c + 8][:-1].split(':')[-1]))
                            nms_all.append(float(lines[c + 9][:-1].split(':')[-1]))
                            break
    fn_all_sum = np.sum(fn_all)
    fp_all_sum = get_fp_from_all_scenes(flag)
    tp_a_all_sum = np.sum(tp_a_all)
    tp_b_all_sum = np.sum(tp_b_all)
    tp_c_all_sum = np.sum(tp_c_all)
    tp_d_all_sum = np.sum(tp_d_all)
    tp_e_all_sum = np.sum(tp_e_all)
    nms_all_sum = np.sum(nms_all)
    prec_class, recall_class, f1_score_class, prec_group, recall_group, f1_score_group \
        = compute_confusion_metrics(fn_all_sum, fp_all_sum, tp_a_all_sum, tp_b_all_sum, tp_c_all_sum,
                                              tp_d_all_sum, tp_e_all_sum)
    res_file = open(os.path.join(path, title + '_parsed_per_class_' + flag + '_' + date + '.txt'), 'w')
    res_file.write(
        'prec_class_all: ' + str(prec_class) + '\n' +
        'recall_class_all: ' + str(recall_class) + '\n' +
        'f1_score_class_all: ' + str(f1_score_class) + '\n' +
        'prec_group_all: ' + str(prec_group) + '\n' +
        'recall_group_all: ' + str(recall_group) + '\n' +
        'f1_score_group_all: ' + str(f1_score_group) + '\n' +
        'nms_all: ' + str(nms_all_sum))
    res_file.close()
    # file.close


def get_parsed_metrics_from_cm_all(path):
    title = os.path.basename(path)
    fp_all, fn_all, tp_a_all, tp_b_all, tp_c_all, tp_d_all, tp_e_all, nms_all = [], [], [], [], [], [], [], []
    for f in os.listdir(path):
        if 'cm_all' in f:
            file = open(os.path.join(path, f), 'r')
            lines = file.readlines()
            for c, line in enumerate(lines):
                if 'set 1' in line:
                    try:
                        fn_all.append(float(lines[c + 2][:-1].split(':')[-1]))
                        tp_a_all.append(float(lines[c + 3][:-1].split(':')[-1]))
                        tp_b_all.append(float(lines[c + 4][:-1].split(':')[-1]))
                        tp_c_all.append(float(lines[c + 5][:-1].split(':')[-1]))
                        tp_d_all.append(float(lines[c + 6][:-1].split(':')[-1]))
                        tp_e_all.append(float(lines[c + 7][:-1].split(':')[-1]))
                        nms_all.append(float(lines[c + 8][:-1].split(':')[-1]))
                        break
                    except:
                        break
    fn_all_sum = np.sum(fn_all)
    fp_all_sum = get_fp_from_all_scenes('')
    tp_a_all_sum = np.sum(tp_a_all)
    tp_b_all_sum = np.sum(tp_b_all)
    tp_c_all_sum = np.sum(tp_c_all)
    tp_d_all_sum = np.sum(tp_d_all)
    tp_e_all_sum = np.sum(tp_e_all)
    nms_all_sum = np.sum(nms_all)
    prec_class, recall_class, f1_score_class, prec_group, recall_group, f1_score_group \
        = compute_confusion_metrics(fn_all_sum, fp_all_sum, tp_a_all_sum, tp_b_all_sum, tp_c_all_sum,
                                    tp_d_all_sum, tp_e_all_sum)
    res_file = open(os.path.join(path, title + '_parsed_all' + '_' + date + '.txt'), 'w')
    res_file.write(
        'prec_class_all: ' + str(prec_class) + '\n' +
        'recall_class_all: ' + str(recall_class) + '\n' +
        'f1_score_class_all: ' + str(f1_score_class) + '\n' +
        'prec_group_all: ' + str(prec_group) + '\n' +
        'recall_group_all: ' + str(recall_group) + '\n' +
        'f1_score_group_all: ' + str(f1_score_group) + '\n' +
        'nms_all: ' + str(nms_all_sum))
    res_file.close()
    # file.close()


def find_best_f_score(dir):
    res = open(os.path.join(export_path, 'res_f_score_' + date + '.txt'), 'w')
    for root, dirs, files in os.walk(dir):
        for dir in dirs:
            for file in os.listdir(os.path.join(root, dir)):
                if 'parsed_all_' in file:
                    with open((os.path.join(root, dir, file)), 'r') as f:
                        lines = f.readlines()
                        f_score_class = lines[2]
                        res.write(dir + ': ' + f_score_class + '\n')
                        break
    res.close()


def get_fn_per_class(path):
    title = os.path.basename(path)
    for cls in classes:
            fn_all = []
            for f in os.listdir(path):
                if 'cm_per_class' in f:
                    file = open(os.path.join(path, f), 'r')
                    lines = file.readlines()
                    for c, line in enumerate(lines):
                        if '*** cls_' + str(cls) in line:
                            cls_id = line.split('_')[1][0]
                            fn_all.append(float(lines[c + 3][:-1].split(':')[-1]))
                            break
            fn_all_sum = np.sum(fn_all)

            res_file = open(os.path.join(path, title + '_fn_per_class_' + cls_id + '_' + date + '.txt'), 'w')
            res_file.write('fn: ' + str(fn_all_sum) + '\n')
            res_file.close()
            # file.close()


""" MAIN """
dir = export_path

for p in os.listdir(dir):
    path = os.path.join(dir, p)
    if os.path.isfile(path):
        continue
    # extract metrics per class:
    get_parsed_metrics_from_cm_per_class(path)

    # extract metrics for threats:
    get_parsed_metrics_from_cm_group('threats', path)

    # extract metrics for non-threats:
    get_parsed_metrics_from_cm_group('non-threats', path)

    # extract metrics for all:
    get_parsed_metrics_from_cm_all(path)

    # extract only fn per class:
    # get_fn_per_class(path)

print('Done')
