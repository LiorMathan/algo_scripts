import os
import cv2
import csv
import math
import pickle
import copy
import itertools
import configparser
import numpy as np
from datetime import date
import matplotlib.pyplot as plt

path = r"Z:\Benchmarking\BP\bp_allsampled_cam_motion_130920\BlackPanther-s30-c5-7_res\track_id_analysis_130920.csv"
steps = 4
fps = int(np.ceil(40 / steps))


today = date.today()
date = today.strftime("%d%m%y")

export_path = (os.path.join(os.path.dirname(path), 'track_id_analysis_results_' + date))
if not os.path.isdir(export_path):
    os.mkdir(export_path)

with open(path) as fp:
    reader = csv.reader(fp, delimiter=",", quotechar='"')
    next(reader, None)  # skip headers
    data_read = [row for row in reader]

##########################
""" FILTERING PARAMS AND BAD SCENES """
# if needed, you can take out bad movies for the analysis, and compare the results with or without them.
# also, you can control the desired threats list and conditions for filtering.
bad_scenes = ['Latrun_13_08_19_Afternoon_500m', 'Latrun_13_08_19_Afternoon_1000_to_2000_Big_Drone', 'C0023', 'C0019',
              'Latrun_13_08_19_Afternoon_500_1500m_Small_Drone_HD']
data_read = [r for r in data_read if r[0] not in bad_scenes]
data_read_filtered = []
threats = ['0', '5', '8', '9']
for row in data_read:
    if row[2] not in threats or float(row[4]) < 3:
        # an object is not threats list or that its life time is shorter than 3 seconds.
        continue
    if row[19] not in threats:
        # if an object is classified as UF0 with confidence greater than 50%, change its class into drone. 
        # else: ignore this object (turn it into an empty line)
        if row[19] == '3' and float(row[18]) >= 50:
            row[19] = '0'
        else:
            row[6:14] = '', '', '', '', '', '', '', ''
        data_read_filtered.append(row)
    else:
        data_read_filtered.append(row)

# if not using any filtering, comment this line:
data_read = data_read_filtered
##########################

""" MAPPING """
# row split variables:
movie_id = 0
track_id = 1
gt_major_class = 2
was_detected = 3
gt_total_life_time = 4
gt_life_time = 5
det_life_time = 6
det_rate = 7
time_till_detection = 8
id_swaps_on_det = 9
max_lost_time_on_det = 10
longest_class_loss = 11
longest_in_group_class_loss = 12
longest_out_group_class_loss = 13
min_area = 14
max_area = 15
min_snr = 16
max_snr = 17
median_conf = 18
det_major_class = 19
frames_num = 20

""" FUNCTIONS """


def group_by_snr_thresh(flag):
    threats = [0, 5, 8, 9]
    if flag == 'drones':
        group_1 = [r for r in data_read if np.absolute(float(r[16])) > 1.2 and int(r[1]) >= 0 and int(r[2]) == 0]
        group_2 = [r for r in data_read if np.absolute(float(r[16])) > 1.4 and int(r[1]) >= 0 and int(r[2]) == 0]
        group_3 = [r for r in data_read if np.absolute(float(r[16])) > 1.6 and int(r[1]) >= 0 and int(r[2]) == 0]
        group_4 = [r for r in data_read if np.absolute(float(r[16])) > 1.8 and int(r[1]) >= 0 and int(r[2]) == 0]
        group_5 = [r for r in data_read if int(r[2]) == 0]
    elif flag == 'all':
        group_1 = [r for r in data_read if np.absolute(float(r[16])) > 1.2 and int(r[1]) >= 0 and int(r[2]) in threats]
        group_2 = [r for r in data_read if np.absolute(float(r[16])) > 1.4 and int(r[1]) >= 0 and int(r[2]) in threats]
        group_3 = [r for r in data_read if np.absolute(float(r[16])) > 1.6 and int(r[1]) >= 0 and int(r[2]) in threats]
        group_4 = [r for r in data_read if np.absolute(float(r[16])) > 1.8 and int(r[1]) >= 0 and int(r[2]) in threats]
        group_5 = [r for r in data_read if int(r[2]) in threats]
    elif flag == 'balloons':
        group_1 = [r for r in data_read if np.absolute(float(r[16])) > 1.2 and int(r[1]) >= 0 and int(r[2]) == 5]
        group_2 = [r for r in data_read if np.absolute(float(r[16])) > 1.4 and int(r[1]) >= 0 and int(r[2]) == 5]
        group_3 = [r for r in data_read if np.absolute(float(r[16])) > 1.6 and int(r[1]) >= 0 and int(r[2]) == 5]
        group_4 = [r for r in data_read if np.absolute(float(r[16])) > 1.8 and int(r[1]) >= 0 and int(r[2]) == 5]
        group_5 = [r for r in data_read if int(r[1]) >= 0 and int(r[2]) == 5]
    elif flag == 'ufo':
        group_1 = [r for r in data_read if np.absolute(float(r[16])) > 1.2 and int(r[1]) >= 0 and int(r[2]) == 3]
        group_2 = [r for r in data_read if np.absolute(float(r[16])) > 1.4 and int(r[1]) >= 0 and int(r[2]) == 3]
        group_3 = [r for r in data_read if np.absolute(float(r[16])) > 1.6 and int(r[1]) >= 0 and int(r[2]) == 3]
        group_4 = [r for r in data_read if np.absolute(float(r[16])) > 1.8 and int(r[1]) >= 0 and int(r[2]) == 3]
        group_5 = [r for r in data_read if int(r[1]) >= 0 and int(r[2]) == 3]
    elif flag == 'drone_with_lights':
        group_1 = [r for r in data_read if np.absolute(float(r[16])) > 1.2 and int(r[1]) >= 0 and int(r[2]) == 8]
        group_2 = [r for r in data_read if np.absolute(float(r[16])) > 1.4 and int(r[1]) >= 0 and int(r[2]) == 8]
        group_3 = [r for r in data_read if np.absolute(float(r[16])) > 1.6 and int(r[1]) >= 0 and int(r[2]) == 8]
        group_4 = [r for r in data_read if np.absolute(float(r[16])) > 1.8 and int(r[1]) >= 0 and int(r[2]) == 8]
        group_5 = [r for r in data_read if int(r[1]) >= 0 and int(r[2]) == 8]
    elif flag == 'single_front_light':
        group_1 = [r for r in data_read if np.absolute(float(r[16])) > 1.2 and int(r[1]) >= 0 and int(r[2]) == 9]
        group_2 = [r for r in data_read if np.absolute(float(r[16])) > 1.4 and int(r[1]) >= 0 and int(r[2]) == 9]
        group_3 = [r for r in data_read if np.absolute(float(r[16])) > 1.6 and int(r[1]) >= 0 and int(r[2]) == 9]
        group_4 = [r for r in data_read if np.absolute(float(r[16])) > 1.8 and int(r[1]) >= 0 and int(r[2]) == 9]
        group_5 = [r for r in data_read if int(r[1]) >= 0 and int(r[2]) == 9]

    return [group_1, group_2, group_3, group_4, group_5]


def group_by_life_time():
    group_1 = [r for r in data_read if 0 <= float(int(r[5])/fps) < 1]
    group_2 = [r for r in data_read if 1 <= float(int(r[5])/fps) < 5]
    group_3 = [r for r in data_read if 5 <= float(int(r[5])/fps) < 10]
    group_4 = [r for r in data_read if 10 <= float(int(r[5])/fps) < 20]
    group_5 = [r for r in data_read if float(int(r[5])/fps) >= 20]
    return [group_1, group_2, group_3, group_4, group_5, data_read]


def group_by_life_time_cum():
    # cumulative life time
    group_1 = [r for r in data_read if float(int(r[5])/fps) > 1]
    group_2 = [r for r in data_read if float(int(r[5])/fps) > 5]
    group_3 = [r for r in data_read if float(int(r[5])/fps) > 10]
    group_4 = [r for r in data_read if float(int(r[5])/fps) > 20]
    group_5 = [r for r in data_read if float(int(r[5])/fps) > 1]
    return [group_1, group_2, group_3, group_4, group_5]


def create_per_scene_lists(data_read):
    key_func = lambda x: x[0]
    groupby_scene = []
    for key, group in itertools.groupby(data_read, key_func):
        groupby_scene.append(list(group))
    return groupby_scene


def create_per_class_lists():
    threats = [0, 5, 8, 9]
    if life_time_class:
        # list_drones = [r for r in data_read if int(r[2]) == 0 and float(int(r[5])/fps) > 1]
        list_drones = [r for r in data_read if int(r[2]) == 0 and float(r[5]) / fps > 1]
        list_balloons = [r for r in data_read if int(r[2]) == 5 and float(r[5])/fps > 1]
        list_ufos = [r for r in data_read if int(r[2]) == 3 and float(r[5])/fps > 1]
        list_threats = [r for r in data_read if int(r[2]) in threats and float(r[5])/fps > 1]
        all = [r for r in data_read if float(r[5])/fps > 1]
    else:
        list_drones = [r for r in data_read if int(r[2]) == 0]
        list_balloons = [r for r in data_read if int(r[2]) == 5]
        list_ufos = [r for r in data_read if int(r[2]) == 3]
        list_threats = [r for r in data_read if int(r[2]) in threats]
        all = data_read
    return [list_drones, list_balloons, list_ufos, list_threats, all]


def create_per_group_lists(flag):
    threats = [0, 5, 8, 9]
    if flag == 'drones':
        group_1 = [r for r in data_read if np.absolute(float(r[16])) > 1.2 and int(r[1]) >= 0 and int(r[2]) == 0]
        group_2 = [r for r in data_read if np.absolute(float(r[16])) > 1.2 and np.absolute(int(r[2])) == 0]
        group_3 = [r for r in data_read if np.absolute(float(r[16])) > 1.6 and int(r[1]) >= 0 and int(r[2]) == 0]
        group_4 = [r for r in data_read if np.absolute(float(r[16])) > 1.8 and int(r[1]) >= 0 and int(r[2]) == 0]
        group_5 = [r for r in data_read if int(r[2]) == 0]
    elif flag == 'all':
        group_1 = [r for r in data_read if np.absolute(float(r[16])) > 1.2 and int(r[1]) >= 0 and int(r[2]) in threats]
        group_2 = [r for r in data_read if np.absolute(float(r[16])) > 1.4 and int(r[1]) >= 0 and int(r[2]) in threats]
        group_3 = [r for r in data_read if np.absolute(float(r[16])) > 1.6 and int(r[1]) >= 0 and int(r[2]) in threats]
        group_4 = [r for r in data_read if np.absolute(float(r[16])) > 1.8 and int(r[1]) >= 0 and int(r[2]) in threats]
        group_5 = [r for r in data_read if int(r[2]) in threats]
    return [group_1, group_2, group_3, group_4, group_5]


def plot_objects_analysis(grouped_by_snr_thresh_lists, lst, val, title, i, label, flag):
    # val is needed for parsing according to the mapping value.
    if val == 7:
        list = [int(float(r[val])) for r in lst if r[val] != ""]
    else:
        # list = [int(r[val]) for r in lst if r[val] != ""]
        list = [int(float(r[val])) for r in lst if r[val] != ""]
    if len(list) == 0:
        hist, bins = np.histogram(list, bins=range(1))
    else:
        hist, bins = np.histogram(list, bins=range(np.max(list)+1))
    frame_rate = np.divide(bins, fps)
    hist_cumsum = np.cumsum(hist)
    if val == 10 or val == 11:
        sub_cumsum = np.subtract(len(list), hist_cumsum)
        dev = np.divide(sub_cumsum, len(list))
        ax = i.plot(frame_rate[:-1], dev, label=str(label) + ', ' + 'N=' + str(len(list)))
        i.set_ylabel('% of ' + str(len(list)) + ' Items')
        i.set_ylim([-0.05, 1.05])
    elif val == 7:
        sub_cumsum = np.subtract(len(list), hist_cumsum)
        dev = np.divide(sub_cumsum, len(list))
        bins_dev = np.divide(bins, 100)
        ax = i.plot(bins_dev[:-1], dev, label=str(label) + ', ' + 'N=' + str(len(list)))
        i.set_ylabel('% of ' + str(len(list)) + ' Items')
    else:
        dev = np.divide(hist_cumsum, len(lst))
        ax = i.plot(frame_rate[:-1], dev, label=str(label) + ', ' + 'N=' + str(len(lst)))
        i.set_ylabel('% of ' + str(len(grouped_by_snr_thresh_lists[-1])) + ' Items')
    if flag == '':
        i.legend(prop={'size': 4.5})
    else:
        i.legend()
    i.set_xlabel(title)
    return ax


def display_plots_per_class():
    grouped_by_class_lists = create_per_class_lists()
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 8))
    if life_time_class:
        fig.suptitle('Tracking Analysis Per Class (over 1 sec life-time)', fontsize=16)
    else:
        fig.suptitle('Tracking Analysis Per Class', fontsize=16)
    for c, lst in enumerate(grouped_by_class_lists):
        if c == 0:
            label = 'Drones'
        elif c == 1:
            label = 'Balloons'
        elif c == 2:
            label = 'UFOs'
        elif c == 3:
            label = 'Threats'
        elif c == 4:
            label = 'All'
        plot_objects_analysis(grouped_by_class_lists, lst, time_till_detection, 'TIME TO DETECTION [sec @ ' + str(fps) + 'Hz]', ax1, label, "fl")
        plot_objects_analysis(grouped_by_class_lists, lst, max_lost_time_on_det, 'Max TARGET LOSS Time [sec @ ' + str(fps) + 'Hz]', ax2, label, "fl")
        plot_objects_analysis(grouped_by_class_lists, lst, det_rate, 'NET TARGET COVERAGE RATE [%]', ax3, label, "fl")
        plot_objects_analysis(grouped_by_class_lists, lst, longest_class_loss, 'Max TARGET-CLASS LOSS Time [sec @ ' + str(fps) + 'Hz]', ax4, label, "fl")
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    plt.savefig(os.path.join(export_path, 'tracking_analysis_class' + '.png'), bbox_inches='tight')
    plt.show()


def display_plots_per_scene():
    grouped_by_scene_lists = create_per_scene_lists(data_read)
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 8))
    fig.suptitle('Tracking Analysis Per Scene', fontsize=16)
    grouped_by_scene_lists.append(data_read)
    labels = []
    for c, lst in enumerate(grouped_by_scene_lists):
        labels.append(lst[0][0])
        plot_objects_analysis(grouped_by_scene_lists, lst, time_till_detection, 'TIME TO DETECTION [sec @ ' + str(fps) + 'Hz]', ax1, labels[c], '')
        plot_objects_analysis(grouped_by_scene_lists, lst, max_lost_time_on_det, 'Max TARGET LOSS Time [sec @ ' + str(fps) + 'Hz]', ax2, labels[c], '')
        plot_objects_analysis(grouped_by_scene_lists, lst, det_rate, 'NET TARGET COVERAGE RATE [%]', ax3, labels[c], '')
        plot_objects_analysis(grouped_by_scene_lists, lst, longest_class_loss, 'Max TARGET-CLASS LOSS Time [sec @ ' + str(fps) + 'Hz]', ax4, labels[c], '')
        # fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    plt.savefig(os.path.join(export_path, 'tracking_analysis_scene' + '.png'), bbox_inches='tight')
    plt.show()


def display_plots_per_life_time():
    grouped_by_life_time_lists = group_by_life_time()
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 8))
    fig.suptitle('Tracking Analysis Per Life Time', fontsize=16)
    for c, lst in enumerate(grouped_by_life_time_lists):
        if c == 0:
            label = '0-1 sec'
        elif c == 1:
            label = '1-5 sec'
        elif c == 2:
            label = '5-10 sec'
        elif c == 3:
            label = '10-20 sec'
        elif c == 4:
            label = '>20 sec'
        elif c == 5:
            label = 'All'
        plot_objects_analysis(grouped_by_life_time_lists, lst, time_till_detection, 'TIME TO DETECTION [sec @ ' + str(fps) + 'Hz]', ax1, label, 'fl')
        plot_objects_analysis(grouped_by_life_time_lists, lst, max_lost_time_on_det, 'Max TARGET LOSS Time [sec @ ' + str(fps) + 'Hz]', ax2, label, 'fl')
        plot_objects_analysis(grouped_by_life_time_lists, lst, det_rate, 'NET TARGET COVERAGE RATE [%]', ax3, label, 'fl')
        plot_objects_analysis(grouped_by_life_time_lists, lst, longest_class_loss, 'Max TARGET-CLASS LOSS Time [sec @ ' + str(fps) + 'Hz]', ax4, label, 'fl')
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    # plt.savefig(os.path.join(export_path, 'tracking_analysis_life_time' + '.png'), bbox_inches='tight')
    plt.show()


def display_plots_per_life_time_cum():
    grouped_by_life_time_cum_lists = group_by_life_time_cum()
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 8))
    fig.suptitle('Tracking Analysis Per Life Time', fontsize=16)
    for c, lst in enumerate(grouped_by_life_time_cum_lists):
        if c == 0:
            label = '>1 sec'
        elif c == 1:
            label = '>5 sec'
        elif c == 2:
            label = '>10 sec'
        elif c == 3:
            label = '>20 sec'
        elif c == 4:
            label = 'All'
        plot_objects_analysis(grouped_by_life_time_cum_lists, lst, time_till_detection, 'TIME TO DETECTION [sec @ ' + str(fps) + 'Hz]', ax1, label, 'fl')
        plot_objects_analysis(grouped_by_life_time_cum_lists, lst, max_lost_time_on_det, 'Max TARGET LOSS Time [sec @ ' + str(fps) + 'Hz]', ax2, label, 'fl')
        plot_objects_analysis(grouped_by_life_time_cum_lists, lst, det_rate, 'NET TARGET COVERAGE RATE [%]', ax3, label, 'fl')
        plot_objects_analysis(grouped_by_life_time_cum_lists, lst, longest_class_loss, 'Max TARGET-CLASS LOSS Time [sec @ ' + str(fps) + 'Hz]', ax4, label, 'fl')
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    plt.savefig(os.path.join(export_path, 'tracking_analysis_life_time_cum' + '.png'), bbox_inches='tight')
    plt.show()


def display_plots_per_snr_thresh(flag):
    grouped_by_snr_thresh_lists = group_by_snr_thresh(flag)
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 8))
    fig.suptitle('Tracking Analysis per SNR' + ' - ' + flag, fontsize=16)
    for c, lst in enumerate(grouped_by_snr_thresh_lists):
        if c == 0:
            label = '>1.2'
        elif c == 1:
            label = '>1.4'
        elif c == 2:
            label = '>1.6'
        elif c == 3:
            label = '>1.8'
        elif c == 4:
            label = 'All'
        plot_objects_analysis(grouped_by_snr_thresh_lists, lst, time_till_detection, 'TIME TO DETECTION [sec @ ' + str(fps) + 'Hz]', ax1, label, 'fl')
        plot_objects_analysis(grouped_by_snr_thresh_lists, lst, max_lost_time_on_det, 'Max TARGET LOSS Time [sec @ ' + str(fps) + 'Hz]', ax2, label, 'fl')
        plot_objects_analysis(grouped_by_snr_thresh_lists, lst, det_rate, 'NET TARGET COVERAGE RATE [%]', ax3, label, 'fl')
        plot_objects_analysis(grouped_by_snr_thresh_lists, lst, longest_class_loss, 'Max TARGET-CLASS LOSS Time [sec @ ' + str(fps) + 'Hz]', ax4, label, 'fl')
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    plt.savefig(os.path.join(export_path, 'tracking_analysis_snr_thresh' + '_' + flag + '.png'), bbox_inches='tight')
    plt.show()


def display_plots_per_groups(flag):
    grouped_by_class_lists = create_per_group_lists(flag)
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 8))
    fig.suptitle('Tracking Analysis', fontsize=16)
    for c, lst in enumerate(grouped_by_class_lists):
        if c == 0:
            label = ''
        elif c == 1:
            label = ''
        elif c == 2:
            label = ''
        elif c == 3:
            label = ''
        elif c == 4:
            label = ''
        plot_objects_analysis(grouped_by_class_lists, lst, time_till_detection, 'TIME TO DETECTION [sec @ ' + str(fps) + 'Hz]', ax1, label, "fl")
        plot_objects_analysis(grouped_by_class_lists, lst, max_lost_time_on_det, 'Max TARGET LOSS Time [sec @ ' + str(fps) + 'Hz]', ax2, label, "fl")
        plot_objects_analysis(grouped_by_class_lists, lst, det_rate, 'NET TARGET COVERAGE RATE [%]', ax3, label, "fl")
        plot_objects_analysis(grouped_by_class_lists, lst, longest_class_loss, 'Max TARGET-CLASS LOSS Time [sec @ ' + str(fps) + 'Hz]', ax4, label, "fl")
        fig.tight_layout()
        fig.subplots_adjust(top=0.91)
    plt.savefig(os.path.join(export_path, 'tracking_analysis_class' + '.png'), bbox_inches='tight')
    plt.show()


def total_not_detected():
    groupby_scene = create_per_scene_lists(data_read)
    total_not_det = []
    for scene in groupby_scene:
        not_det = []
        for row in scene:
            if row[6] == '' and row[17] != '3':
                not_det.append(row)
        total_not_det.append(not_det)


""" MORE OPTIONS """
### Different kinds of track id analysis:
life_time_class = False  # when set to True, the filtering is also by the life time of the object - greater than 1 sec.
# display_plots_per_class()  # track id analysis by class
# display_plots_per_scene()  # track id analysis by movie
# display_plots_per_life_time_cum()  # track id analysis by cumulative life time

""" MAIN """
display_plots_per_snr_thresh('all')  # track id analysis by snr for all threats
display_plots_per_snr_thresh('drones')  # track id analysis by snr for drones only

plt.close()
print("Done")