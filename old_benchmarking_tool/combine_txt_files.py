import numpy as np
import os

subroot = '/home/shira/workspace/workingzone'
resFile = open(subroot + '/' + 'RANDOM_00010_r0_2_detections.txt', "w+")
for f in os.listdir(subroot):
    if 'random_det_list' in f:
        file = open(subroot + '/' + f, 'r')
        lines = file.readlines()
        if len(lines) == 0:
            continue
        else:
            for line in lines:
                resFile.write(line)

print("Done")