import os
import cv2
import csv
import math
import copy
import pickle
import time
import itertools
import numpy as np
import configparser
import matplotlib.pyplot as plt
from datetime import date
from iteration_utilities import duplicates

""" CONFIG """
# read the config file
try:
    config = configparser.ConfigParser()
    config.read('benchmark_main.ini')
except:
    print('Config file not found')
    exit()

""" FLAGS """
# extracting config file flags
preview = config.getboolean('flags', 'preview')
simulator = config.getboolean('flags', 'simulator')
mp4 = config.getboolean('flags', 'mp4')
raw = config.getboolean('flags', 'raw')
png = config.getboolean('flags', 'png')
pause_after_set = config.getboolean('flags', 'pause_after_set')
snr = config.getboolean('flags', 'snr')
combined_db = config.getboolean('flags', 'combined_db')
trackid = config.getboolean('flags', 'trackid')
bp = config.getboolean('flags', 'bp')
dg = config.getboolean('flags', 'dg')
export_logs = config.getboolean('flags', 'export_logs')
system_checking = config.getboolean('flags', 'system_checking')
export_cm_vals = config.getboolean('flags', 'export_cm_vals')
partial_frames = config.getboolean('flags', 'partial_frames')
dual_database = config.getboolean('flags', 'dual_database')

""" GLOBALS """
# set global variables
export_path = config.get('dirs', 'export_path')
today = date.today()
date = today.strftime("%d%m%y")
PAD_INDEX = config.getint('globals', 'PAD_INDEX')
IOU_THRESHOLD = config.getint('globals', 'IOU_THRESHOLD')
FRAME_WIDTH = config.getint('globals', 'FRAME_WIDTH')
FRAME_HEIGHT = config.getint('globals', 'FRAME_HEIGHT')
FRAME_SIZE = FRAME_WIDTH * FRAME_HEIGHT

if simulator:
    classes = list(range(config.getint('simulator', 'classes')))
    comb_gt_list = config.get('simulator', 'comb_gt_list')
    comb_det_list = config.get('simulator', 'comb_det_list')
else:
    comb_gt_list = config.get('dirs', 'comb_gt_list')
    # comb_det_list = config.get('dirs', 'comb_det_list')
    comb_det_dir = config.get('dirs', 'comb_det_dir')
    if preview:
        comb_video_path = config.get('dirs', 'comb_video_path')
    classes = list(range(config.getint('dirs', 'classes'))) # TODO: read labels list from an external file
    steps = 1
    # steps = config.getint('dirs', 'steps')
    if dual_database:
        dual_param = config.get('dirs', 'dual_param')
        dual_param = dual_param.strip('][').split(',')
        for i in range(len(dual_param)):
            dual_param[i] = int(dual_param[i])

""" FUNCTIONS """


def parse_labels(label):
    # gets a label (int) and returns the class id string and the class type string
    if label == 0:
        return 'drone', 'threat'
    elif label == 1:
        return 'airplane', 'non-threat'
    elif label == 2:
        return 'bird', 'non-threat'
    elif label == 3:
        return 'UFO', 'threat'
    elif label == 4:
        return 'airplane with lights', 'non-threat'
    elif label == 5:
        return 'balloon', 'threat'
    elif label == 6:
        return 'human', 'non-threat'
    elif label == 7:
        return 'vehicle', 'non-threat'
    elif label == 8:
        return 'drone with lights', 'threat'
    elif label == 9:
        return 'single front light', 'threat'
    else:
        return 'Unknown', 'non-threat'


def find_diff_frames(scene_gt_list, scene_det_list):
    # gets two lists and returns the different frames values (frames that are included in the gt list but not in
    # the detections lists
    scene_gt_lines = [line.rstrip() for line in open(scene_gt_list, 'r')]
    if system_checking:
        scene_det_lines = [line.rstrip() for line in open(scene_det_list, 'r') if int(line.split()[9]) == 0]
    else:
        scene_det_lines = [line.rstrip() for line in open(scene_det_list, 'r') if int(line.split()[9]) == 2]
    frid_gt, frid_det = [], []
    for l1 in scene_det_lines:
        frid_det.append(int(l1.split()[0]))
    det_uni = sorted(np.unique(frid_det))
    for l2 in scene_gt_lines:
        frid_gt.append(int(l2.split()[1]))
    gt_uni = sorted(np.unique(frid_gt))
    list_difference = [item for item in gt_uni if item not in det_uni]
    return list_difference


def get_matching_frames(scene_gt_list, scene_det_list):
    # gets two lists and returns those lists sorted by frames in the form of "matching pairs" - for each gt, match with
    # each detection.
    try:
        scene_gt_lines = [line.rstrip() for line in open(scene_gt_list, 'r')]
    except:
        print("Error occurred while trying to open scene_gt_list\n")
    try:
        if system_checking:
            scene_det_lines = [line.rstrip() for line in open(scene_det_list, 'r') if int(line.split()[9]) == 0]
        else:
            scene_det_lines = [line.rstrip() for line in open(scene_det_list, 'r') if int(line.split()[9]) == 2]
    except:
        print("Error occurred while trying to open scene_det_list\n")

    scene_gt_lines.sort(key=lambda x: int(x.split()[1]))
    if partial_frames:
        frames_indexes = []
        for i in np.arange(start_frame, end_frame+1, steps):
            frames_indexes.append(int(i))

        scene_gt_lines = [r for r in scene_gt_lines if int(r.split()[1]) in frames_indexes]
    scene_det_lines.sort(key=lambda x: int(x.split()[0]))
    diff = find_diff_frames(scene_gt_list, scene_det_list)
    matched_frames_gt, matched_frames_det = [], []
    for g in scene_gt_lines:
        single_frame_gt, single_frame_det = [], []
        gt_frame_id = str(int(g.split(' ')[1]))
        for d in scene_det_lines:
            det_frame_id = d.split(' ')[0]
            if gt_frame_id == det_frame_id:
                single_frame_gt.append(g)
                single_frame_det.append(d)
            if int(det_frame_id) > int(gt_frame_id):
                break
        if int(gt_frame_id) in diff:  # det list is empty
            single_frame_gt.append(g)
            single_frame_det.append(gt_frame_id)
        if len(single_frame_gt) and len(single_frame_det) != 0:
            matched_frames_gt.append(single_frame_gt)
            matched_frames_det.append(single_frame_det)
    for i in reversed(range(len(matched_frames_gt) - 1)):
        if matched_frames_gt[i][0].split(' ')[1] == matched_frames_gt[i + 1][0].split(' ')[1]:
            matched_frames_gt[i].extend(matched_frames_gt[i + 1])
            matched_frames_gt.pop(i + 1)
    for j in reversed(range(len(matched_frames_det) - 1)):
        if matched_frames_det[j][0].split(' ')[0] == matched_frames_det[j + 1][0].split(' ')[0]:
            matched_frames_det[j].extend(matched_frames_det[j + 1])
            matched_frames_det.pop(j + 1)
    return matched_frames_gt, matched_frames_det


def parse_parameters(line):
    # gets a line (detection or gt) and returns the relevant parameters - box params, class id and confidence for
    # detections.
    if line.count(' ') == 11 or line.count(' ') == 10 or line.count(' ') == 9:
        is_gt = False
    else:
        is_gt = True
    xc = int(line.split(' ')[3])
    yc = int(line.split(' ')[4])
    w = int(line.split(' ')[5])
    h = int(line.split(' ')[6])
    if is_gt:
        class_id = int(line.split(' ')[2])
        return xc, yc, w, h, class_id
    else:
        conf = int(line.split(' ')[2])
        class_id = int(line.split(' ')[1])
        return xc, yc, w, h, class_id, conf


def convert_rec_points(xc, w, yc, h):
    # gets the box parameters and returns it the the form of 4 points.
    start = (int((xc - (w / 2))), int((yc + (h / 2))))
    tr = (int((xc + (w / 2))), int((yc + (h / 2))))
    bl = (int((xc - (w / 2))), int((yc - (h / 2))))
    end = (int((xc + (w / 2))), int((yc - (h / 2))))
    return start, tr, bl, end


def compute_rle(line_gt, line_det):
    # gets a pair of gt line and detections line, and returns the relative location error.
    xc_det, yc_det, w_det, h_det, class_id_det, conf = parse_parameters(line_det)
    xc_gt, yc_gt, w_gt, h_gt, class_id_gt = parse_parameters(line_gt)
    if h_gt == 0:
        h_gt = 1e-16
    if w_gt == 0:
        w_gt = 1e-16
    center_det = [xc_det, yc_det]
    center_gt = [xc_gt, yc_gt]
    delta_x = abs(int(center_det[0]) - int(center_gt[0]))
    delta_y = abs(int(center_det[1]) - int(center_gt[1]))
    return round(math.sqrt((delta_x / (w_gt / 2)) ** 2 + (delta_y / (h_gt / 2)) ** 2), 2)


def compute_iou(box_a, box_b):
    # gets two boxes and returns the intersection over union measure.
    xa = max(box_a[0], box_b[0])
    ya = max(box_a[3], box_b[3])
    xb = min(box_a[2], box_b[2])
    yb = min(box_a[1], box_b[1])
    inter_area = max(0, xb - xa + 1) * max(0, yb - ya + 1)
    box_a_area = (box_a[2] - box_a[0] + 1) * (box_a[1] - box_a[3] + 1)
    box_b_area = (box_b[2] - box_b[0] + 1) * (box_b[1] - box_b[3] + 1)
    iou = inter_area / float(box_a_area + box_b_area - inter_area)
    return round(iou, 2)


def group_similar_values(g, arr):
    # gets a pair of lists - matched gts and detections per frame, and combines the values per gt (returns two lists).
    total_g, total_arr = [], []
    i = 0
    if len(g) == 0:
        new_g, new_arr = [], []
        for j in range(len(arr)):
            new_arr.append((arr[j]))
            new_g.append(None)
        total_g.append(new_g)
        total_arr.append(new_arr)
    if len(arr) == 0:
        new_g, new_arr = [], []
        for j in range(len(g)):
            new_arr.append(None)
            new_g.append((g[j]))
        total_g.append(new_g)
        total_arr.append(new_arr)
    while i < len(g) and len(arr) != 0:
        new_g, new_arr = [], []
        for j in range(i, len(g)):
            if g[j] == g[i]:
                new_g.append((g[j]))
                new_arr.append((arr[j]))
            else:
                break
        i += len(new_g)
        total_g.append(new_g)
        total_arr.append(new_arr)
    return total_arr, total_g


def read_raw(f):
    # gets the frame number and returns the raw frame.
    global video_path
    try:
        vid = open(video_path, 'rb')
    except:
        print("Error occurred while trying to open video_path\n")
    vid.seek(0, os.SEEK_SET)
    vid.seek(f * FRAME_SIZE * 2, os.SEEK_SET)
    frame = np.fromfile(vid, np.int16, FRAME_SIZE, '').astype(float)
    frame.shape = (FRAME_HEIGHT, FRAME_WIDTH)
    min_frame = np.min(frame)
    max_frame = np.max(frame)
    frame = 255 * ((frame - min_frame) / (max_frame - min_frame))
    frame[frame < 0] = 0
    frame[frame > 255] = 255
    frame = frame.astype(np.uint8)
    new_frame = np.ones((FRAME_HEIGHT, FRAME_WIDTH, 3), np.uint8)
    new_frame[:, :, 0] = new_frame[:, :, 0] * frame
    new_frame[:, :, 1] = new_frame[:, :, 1] * frame
    new_frame[:, :, 2] = new_frame[:, :, 2] * frame
    return new_frame


def read_mp4(f):
    # gets the frame number and returns the video frame.
    try:
        cap = cv2.VideoCapture(video_path)
    except:
        print("Error occurred while trying to open video_path\n")
    # while cap.isOpened():
    cap.set(cv2.CAP_PROP_POS_FRAMES, f)
    ret, frame = cap.read()
    # if not ret:
    #     break
    return frame


def display_img(det_movie_id, det_frame_id, scene_gt_list, flag):
    # gets movie id, frame id, list (gt or detection) and a flag that indicates if this is the first frame shown,
    # write text on the top of the frame and returns the frame itself.
    if png:
        images = [p for p in os.listdir(os.path.dirname(scene_gt_list)) if p.endswith('png')]
        frame = [i for i in images if int(det_frame_id) == int(i.split('.')[0].split('_')[-1])]
    if flag is 'first':
        try:
            if png:
                read_frame = cv2.imread(os.path.join(os.path.dirname(scene_gt_list), frame[0]))
            if raw:
                read_frame = read_raw(int(det_frame_id))
            if mp4:
                read_frame = read_mp4(int(det_frame_id))
        except:
            print("Error occurred while trying to read frames\n")
        if simulator:
            read_frame = 255 * np.ones((480, 640, 3), np.uint8)
    else:
        read_frame = rec
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(read_frame, det_movie_id + ' ' + '#' + det_frame_id, (10, 20), font, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
    cv2.putText(read_frame, 'set: ' + str(s + 1), (10, 40), cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
    cv2.namedWindow('frame', cv2.WINDOW_NORMAL)
    # cv2.imshow('frame', read_frame)
    return read_frame


def create_rle_matrix(scene_gt_list, scene_det_list):
    # gets two lists (gt and detections) and returns a rle matrix with the values calculated for each pair (gt and det)
    # in each frame.
    frames_gt, frames_det = get_matching_frames(scene_gt_list, scene_det_list)
    comb_arr = []
    for (c, (single_frame_gt, single_frame_det)) in enumerate(zip(frames_gt, frames_det)):
        arr, g = [], []
        for (c2, (gt, det)) in enumerate(zip(single_frame_gt, single_frame_det)):
            if len(gt.split()) == 2 or len(det.split()) == 1:
                arr.append(np.nan)
            else:
                arr.append(compute_rle(gt, det))
            g.append(gt)
        total_arr, _ = group_similar_values(g, arr)
        asarr = np.array(total_arr).T
        pa = np.pad(asarr, ((0, (PAD_INDEX - asarr.shape[0])), (0, (PAD_INDEX - asarr.shape[1]))),
                    mode='constant', constant_values=np.nan)
        comb_arr.append(pa)
    comb_asarr = np.array(comb_arr)
    return comb_asarr


def create_iou_matrix(scene_gt_list, scene_det_list):
    # gets two lists (gt and detections) and returns a iou matrix with the values calculated for each pair (gt and det)
    # in each frame.
    frames_gt, frames_det = get_matching_frames(scene_gt_list, scene_det_list)
    comb_arr, frame_key = [], []
    for single_frame_gt, single_frame_det in zip(frames_gt, frames_det):
        arr, g = [], []
        for (c, (gt, det)) in enumerate((zip(single_frame_gt, single_frame_det))):
            if len(gt.split()) == 2 or len(det.split()) == 1:
                det_frame_id = det.split(' ')[0]
                arr.append(np.nan)
            else:
                det_frame_id = det.split(' ')[0]
                x_det, y_det, w_det, h_det, class_id_det, conf = parse_parameters(det)
                x_gt, y_gt, w_gt, h_gt, class_id_gt = parse_parameters(gt)
                start_det, tr, bl, end_det = convert_rec_points(x_det, w_det, y_det, h_det)
                start_gt, tr, bl, end_gt = convert_rec_points(x_gt, w_gt, y_gt, h_gt)
                box_det = list(itertools.chain(*[start_det, end_det]))
                box_gt = list(itertools.chain(*[start_gt, end_gt]))
                arr.append(compute_iou(box_det, box_gt))
            g.append(gt)
        frame_key.append(int(det_frame_id))
        total_arr, _ = group_similar_values(g, arr)
        asarr = np.array(total_arr).T
        pa = np.pad(asarr, ((0, (PAD_INDEX - asarr.shape[0])), (0, (PAD_INDEX - asarr.shape[1]))),
                    mode='constant', constant_values=np.nan)
        comb_arr.append(pa)
    comb_asarr = np.array(comb_arr)
    return frame_key, comb_asarr


def create_class_matrix(scene_gt_list, scene_det_list):
    # this function is not in use.
    # gets two lists (gt and detections) and returns a class id matrix with the values calculated for each pair
    # (gt and det) in each frame.
    frames_gt, frames_det = get_matching_frames(scene_gt_list, scene_det_list)
    comb_arr = []
    for single_frame_gt, single_frame_det in zip(frames_gt, frames_det):
        arr, g = [], []
        for (c, (gt, det)) in enumerate((zip(single_frame_gt, single_frame_det))):
            xc_det, yc_det, w_det, h_det, class_id_det, conf = parse_parameters(det)
            xc_gt, yc_gt, w_gt, h_gt, class_id_gt = parse_parameters(gt)
            class_str_det, class_group_det = parse_labels(class_id_det)
            class_str_gt, class_group_gt = parse_labels(class_id_gt)
            g.append(gt)
            if len(gt.split()) == 2 or len(det.split()) == 1:
                arr.append(np.nan)
            else:
                if class_str_det == class_str_gt:
                    arr.append(float(0))
                elif class_str_det != class_str_gt and class_group_det == class_group_gt:
                    arr.append(float(1))
                else:
                    arr.append(float(2))
        total_arr, _ = group_similar_values(g, arr)
        asarr = np.array(total_arr).T
        pa = np.pad(asarr, ((0, (PAD_INDEX - asarr.shape[0])), (0, (PAD_INDEX - asarr.shape[1]))),
                    mode='constant', constant_values=np.nan)
        comb_arr.append(pa)
    comb_asarr = np.array(comb_arr)
    return comb_asarr


def get_detection_index(line_gt, line_det):
    # gets a pair of gt and det, and returns the type of TP match (please see the benchmarking manual for further info).
    xc_det, yc_det, w_det, h_det, class_id_det, conf = parse_parameters(line_det)
    xc_gt, yc_gt, w_gt, h_gt, class_id_gt = parse_parameters(line_gt)
    class_str_det, class_group_det = parse_labels(class_id_det)
    class_str_gt, class_group_gt = parse_labels(class_id_gt)
    if class_id_gt == class_id_det and class_group_gt == 'non-threat':
        index = 'A'
    elif class_id_gt == class_id_det and class_group_gt == 'threat':
        index = 'B'
    elif class_id_gt != class_id_det and class_group_det == class_group_gt and class_group_gt == 'threat':
        index = 'C'
    elif class_id_gt != class_id_det and class_group_det == class_group_gt and class_group_gt == 'non-threat':
        index = 'D'
    elif class_id_gt != class_id_det and class_group_det != class_group_gt:
        index = 'E'
    return index


def alpha_blending(src_1, src_2, alpha):
    # gets two frames and returns the product of the alpha blending between them.
    src_2 = cv2.resize(src_2, src_1.shape[1::-1])
    dst = cv2.addWeighted(src_1, alpha, src_2, (1 - alpha), 0)
    # cv2.imshow('frame', dst)
    # wait_key(0)
    return dst


def generate_scene_class_key(scene_gt_list, scene_det_list):
    # gets two lists (gt and detections) and returns a list of np.arrays of scene type per frame (threat or non threat)
    # and a list of np.arrays of the class id per frame.
    frames_gt, frames_det = get_matching_frames(scene_gt_list, scene_det_list)
    scene_class_group, scene_class_id = [], []
    for (f, (single_frame_gt, single_frame_det)) in enumerate(zip(frames_gt, frames_det)):
        g, d = [], []
        for (_, (gt, det)) in enumerate(zip(single_frame_gt, single_frame_det)):
            g.append(gt)
            d.append(det)
        dets_1, gs_1 = group_similar_values(g, d)
        class_group, class_id = [], []
        for (c, (i, j)) in enumerate(zip(gs_1, dets_1)):
            if len(i[0].split()) == 2:
                class_gt = np.nan
            else:
                class_gt = int(i[0].split(' ')[2])  # changed from i[c] to i[0]
            class_str_gt, class_group_gt = parse_labels(class_gt)
            class_group.append(class_group_gt)
            class_id.append(class_gt)
        scene_class_group.append(class_group)
        scene_class_id.append(class_id)
        scene_class_group = [np.array(i) for i in scene_class_group]
        scene_class_id = [np.array(i) for i in scene_class_id]
    return scene_class_group, scene_class_id


def generate_class_filtered_matrix(scene_gt_list, scene_det_list, flag, matrix):
    # gets two lists (gt and detections), class type wanted (threat or non-threat) and the specific matrix (rle,
    # iou...), and returns a filtered matrix by the class type wanted (e.g only threats)
    scene_class_group, scene_class_id = generate_scene_class_key(scene_gt_list, scene_det_list)
    flag_id = [np.where(i == flag) for i in scene_class_group]
    flag_id = [np.array(i).tolist()[0] for i in flag_id]
    matrix_flag = []
    for i in range(len(matrix)):
        matrix_flag.append(np.pad(matrix[i][:, flag_id[i]], ((0, (PAD_INDEX - matrix[i][:, flag_id[i]].shape[0])),
                                                             (0, (PAD_INDEX - matrix[i][:, flag_id[i]].shape[1]))),
                                  mode='constant', constant_values=np.nan))
    matrix_flag = np.array(matrix_flag)
    return matrix_flag


def compute_confusion_metrics(fn, fp, tp_a, tp_b, tp_c, tp_d, tp_e):
    # gets the cm values and calculates the metrics (1 is for class, 2 is for group).
    # this function is not in use
    fp_1 = np.sum(fp) + np.sum(tp_c) + np.sum(tp_d) + np.sum(tp_e)
    tp_1 = np.sum(tp_a) + np.sum(tp_b)
    fn_1 = np.sum(fn) + np.sum(tp_c) + np.sum(tp_d) + np.sum(tp_e)
    prec_1 = round(tp_1 / (tp_1 + fp_1), 3)
    recall_1 = round(tp_1 / (tp_1 + fn_1), 3)
    f1_score_1 = round(2 * ((prec_1 * recall_1) / (prec_1 + recall_1)), 3)

    fp_2 = np.sum(fp) + np.sum(tp_e)
    tp_2 = np.sum(tp_a) + np.sum(tp_b) + np.sum(tp_c) + np.sum(tp_d)
    fn_2 = np.sum(fn) + np.sum(tp_e)
    prec_2 = round(tp_2 / (tp_2 + fp_2), 3)
    recall_2 = round(tp_2 / (tp_2 + fn_2), 3)
    f1_score_2 = round(2 * ((prec_2 * recall_2) / (prec_2 + recall_2)), 3)
    return prec_1, recall_1, f1_score_1, prec_2, recall_2, f1_score_2


def compute_confusion_metrics_per_class(fn_mat, fp_mat, tp_a_mat, tp_b_mat, tp_c_mat, tp_d_mat, tp_e_mat, i, j):
    # gets the cm values matrix and calculates the metrics per class (1 is for class, 2 is for group, i is a set index,
    # j is a class index)
    fp_1 = np.sum(np.sum(fp_mat[i])) + np.sum(tp_c_mat[i][:, j]) + np.sum(tp_d_mat[i][:, j]) + np.sum(tp_e_mat[i][:, j])
    tp_1 = np.sum(tp_a_mat[i][:, j]) + np.sum(tp_b_mat[i][:, j])
    fn_1 = np.sum(fn_mat[i][:, j]) + np.sum(tp_c_mat[i][:, j]) + np.sum(tp_d_mat[i][:, j]) + np.sum(tp_e_mat[i][:, j])
    prec_1 = round(tp_1 / (tp_1 + fp_1), 3)
    recall_1 = round(tp_1 / (tp_1 + fn_1), 3)
    f1_score_1 = round(2 * ((prec_1 * recall_1) / (prec_1 + recall_1)), 3)

    fp_2 = np.sum(np.sum(fp_mat[i])) + np.sum(tp_e_mat[i][:, j])
    tp_2 = np.sum(tp_a_mat[i][:, j]) + np.sum(tp_b_mat[i][:, j]) + np.sum(tp_c_mat[i][:, j]) + np.sum(tp_d_mat[i][:, j])
    fn_2 = np.sum(fn_mat[i][:, j]) + np.sum(tp_e_mat[i][:, j])
    prec_2 = round(tp_2 / (tp_2 + fp_2), 3)
    recall_2 = round(tp_2 / (tp_2 + fn_2), 3)
    f1_score_2 = round(2 * ((prec_2 * recall_2) / (prec_2 + recall_2)), 3)
    return prec_1, recall_1, f1_score_1, prec_2, recall_2, f1_score_2


def devide_by_set(list, frames_gt):
    # gets a list of confision matrix value (fp, tp, fn or nms) and a list of gts, and returns a list for each set (
    # 1 - all of gts, 2 - threats, 3 - non-threats)
    if len(list) == 0:
        list_set_1, list_set_2, list_set_3 = None, None, None
    else:
        list_set_1 = [list[i] for i in range(0, len(list), int(len(list) / len(frames_gt)))]
        list_set_2 = [list[i] for i in range(1, len(list), int(len(list) / len(frames_gt)))]
        list_set_3 = [list[i] for i in range(2, len(list), int(len(list) / len(frames_gt)))]
    return list_set_1, list_set_2, list_set_3


def display_dets_only(line_det, img, cnt_2):
    # gets a line of detection, a frame and a counter value (to specify if the detection is a fp or a tp)
    # and returns a frame with the drawing of the detection box and details.
    xc_det, yc_det, w_det, h_det, class_id_det, conf = parse_parameters(line_det)
    start_det, tr_det, bl_det, end_det = convert_rec_points(xc_det, w_det, yc_det, h_det)
    font = cv2.FONT_HERSHEY_SIMPLEX
    if len(cnt_2) == 0:
        img_det = cv2.rectangle(img, start_det, end_det, (0, 0, 0), 1)
        cv2.putText(img_det, str(class_id_det) + ', ' + str(conf),
                    (int(bl_det[0]), int(bl_det[1]) - 3), font, 0.4, (0, 0, 0), 1, cv2.LINE_AA)
    else:
        img_det = cv2.rectangle(img, start_det, end_det, (128, 128, 128), 1)
        cv2.putText(img_det, str(class_id_det) + ', ' + str(conf),
                    (int(bl_det[0]), int(bl_det[1]) - 3), font, 0.4, (128, 128, 128), 1, cv2.LINE_AA)
    return img_det


def display_gts_only(line_gt, img):
    # get a line of a gt and a frame and returns a frame with the drawing of the gt box and details.
    xc_gt, yc_gt, w_gt, h_gt, class_id_gt = parse_parameters(line_gt)
    start_gt, tr_gt, bl_gt, end_gt = convert_rec_points(xc_gt, w_gt, yc_gt, h_gt)
    font = cv2.FONT_HERSHEY_SIMPLEX
    org = (int(start_gt[0] + ((end_gt[0] - start_gt[0]) / 2.3)), int(end_gt[1] - ((end_gt[1] - start_gt[1]) / 1.4)))
    if class_group_gt is 'threat':
        img_gt = cv2.rectangle(img, start_gt, end_gt, (160, 160, 160), -1)
        cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (0, 255, 0), 2, cv2.LINE_AA)
    else:
        img_gt = cv2.rectangle(img, start_gt, end_gt, (160, 160, 160), -1)
        cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (204, 102, 0), 1, cv2.LINE_AA)
    return img_gt


def create_combined_gs_dets(frames_gt, frames_det):
    # gest two lists (gt and detections per frame) and return a combined gt list (grouped by gt)
    combined_gs = []
    for (f, (single_frame_gt, single_frame_det)) in enumerate(zip(frames_gt, frames_det)):
        g, d = [], []
        for (_, (gt, det)) in enumerate(zip(single_frame_gt, single_frame_det)):
            g.append(gt)
            d.append(det)
        dets, gs = group_similar_values(g, d)
        combined_gs.append(gs)
    return combined_gs


def create_argmax_list(frames_gt, frames_det, iou_matrix):
    # gets a list of gts, a list of detections and the iou matrix and returns an argmax list - contains the gts
    # (matrix column value) which has the highest iou value per frame, and a look-up-table for those values.
    combined_gs = create_combined_gs_dets(frames_gt, frames_det)
    argmax_list, argmax_list_lut = [], []
    for g, v in enumerate(combined_gs):
        argmax_f, argmax_gt_lut = [], []
        for c in range(len(v)):
            if np.nanmax(iou_matrix[g][:, c]) != 0.0:
                try:
                    argmax_f.append(np.nanargmax(iou_matrix[g][:, c]))
                except ValueError:
                    argmax_f.append(np.nan)
                argmax_gt_lut.append(c)
        argmax_list.append(argmax_f)
        argmax_list_lut.append(argmax_gt_lut)
    return argmax_list, argmax_list_lut


def create_nms_list(frames_gt, frames_det, iou_matrix):
    # gets a list of gts, a list of detections and the iou matrix and returns an nms list - contains the gts
    # (matrix column value) of which the iou was bigger than the threshold but was not only one, meaning its the
    # "second best" gt that was detected and therefore belongs to the nms list.
    combined_gs = create_combined_gs_dets(frames_gt, frames_det)
    nms_list, nms_list_lut = [], []
    for g, v in enumerate(combined_gs):
        nms_f, nms_gt_lut = [], []
        for c in range(len(v)):
            if np.nanmax(iou_matrix[g][:, c]) != 0.0:
                pos_list = [iou_matrix[g][:, c][i] for i in range(len(iou_matrix[g][:, c])) if
                            iou_matrix[g][:, c][i] > 0]
                if len(pos_list) > 1:
                    for x in pos_list:
                        if x != np.max(pos_list):
                            nms_f.append(x)
                    nms_gt_lut.append(c)
        nms_list.append(nms_f)
        nms_list_lut.append(nms_gt_lut)
    return nms_list, nms_list_lut


def display_cm_on_img():
    # this functions takes an image (specific frame) and draws the cm values on top of it.
    cv2.putText(rec, 'fp: ' + str(fp_val), (10, 60), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255, 0, 255), 1, cv2.LINE_AA)
    cv2.putText(rec, 'fn: ' + str(len(fn_val)), (10, 80), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255, 0, 255), 1,
                cv2.LINE_AA)
    cv2.putText(rec, 'nms: ' + str(sum([len(x) for x in nms_val])), (10, 200), cv2.FONT_HERSHEY_DUPLEX, 0.4,
                (255, 0, 255), 1,
                cv2.LINE_AA)
    if len(tp_val_a) != 0:
        cv2.putText(rec, 'tp a: ' + str(len(tp_val_a)), (10, 100), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255, 0, 255), 1,
                    cv2.LINE_AA)
    if len(tp_val_b) != 0:
        cv2.putText(rec, 'tp b: ' + str(len(tp_val_b)), (10, 120), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255, 0, 255), 1,
                    cv2.LINE_AA)
    if len(tp_val_c) != 0:
        cv2.putText(rec, 'tp c: ' + str(len(tp_val_c)), (10, 140), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255, 0, 255), 1,
                    cv2.LINE_AA)
    if len(tp_val_d) != 0:
        cv2.putText(rec, 'tp d: ' + str(len(tp_val_d)), (10, 160), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255, 0, 255), 1,
                    cv2.LINE_AA)
    if len(tp_val_e) != 0:
        cv2.putText(rec, 'tp e: ' + str(len(tp_val_e)), (10, 180), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255, 0, 255), 1,
                    cv2.LINE_AA)
    elif len(tp_val_a) == 0 and len(tp_val_b) == 0 and len(tp_val_c) == 0 and len(tp_val_d) == 0 and len(tp_val_e) == 0:
        cv2.putText(rec, 'tp: ' + str(0), (10, 100), cv2.FONT_HERSHEY_DUPLEX, 0.4, (255, 0, 255), 1,
                    cv2.LINE_AA)


def main_visualization(line_gt, line_det, img, rle_val, iou_val, c, c2, index, argmax, argmax_val, d_nms):
    # this is the main visualization function. it recieves all the parameters of the gts, detections, counter values,
    # argmax information, and extracts the output frame with the relevant information drawn on it.
    xc_det, yc_det, w_det, h_det, class_id_det, conf = parse_parameters(line_det)
    xc_gt, yc_gt, w_gt, h_gt, class_id_gt = parse_parameters(line_gt)
    class_str_det, class_group_det = parse_labels(class_id_det)
    class_str_gt, class_group_gt = parse_labels(class_id_gt)
    start_det, tr_det, bl_det, end_det = convert_rec_points(xc_det, w_det, yc_det, h_det)
    start_gt, tr_gt, bl_gt, end_gt = convert_rec_points(xc_gt, w_gt, yc_gt, h_gt)
    font = cv2.FONT_HERSHEY_SIMPLEX
    org = (int(start_gt[0] + ((end_gt[0] - start_gt[0]) / 2.3)), int(end_gt[1] - ((end_gt[1] - start_gt[1]) / 1.4)))
    if c2 in d_nms and c in nms_list_lut_frame:
        img_det = cv2.rectangle(img, start_det, end_det, (255, 102, 178), 1)
        cv2.putText(img_det, str(class_id_det) + ', ' + str(conf),
                    (int(bl_det[0]), int(bl_det[1]) - 3), font, 0.4, (255, 102, 178), 1, cv2.LINE_AA)
        return img_det
    if c2 == argmax and argmax_val != 0:
        if index is 'A':
            img_gt = cv2.rectangle(img, start_gt, end_gt, (0, 0, 0), -1)
            cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (204, 102, 0), 1, cv2.LINE_AA)
            img_det = cv2.rectangle(img_gt, start_det, end_det, (204, 102, 0), 1)
            cv2.putText(img_det, str(class_id_det) + ', ' + str(conf) + ', ' + str(rle_val) + ', ' +
                        str(iou_val), (int(bl_det[0]), int(bl_det[1]) - 3), font, 0.4, (204, 102, 0), 1, cv2.LINE_AA)
        elif index is 'B':
            img_gt = cv2.rectangle(img, start_gt, end_gt, (0, 0, 0), -1)
            cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (0, 255, 0), 1, cv2.LINE_AA)
            img_det = cv2.rectangle(img_gt, start_det, end_det, (0, 255, 0), 1)
            cv2.putText(img_det, str(class_id_det) + ', ' + str(conf) + ', ' + str(rle_val) + ', ' +
                        str(iou_val), (int(bl_det[0]), int(bl_det[1]) - 3), font, 0.4, (0, 255, 0), 1, cv2.LINE_AA)
        elif index is 'C':
            img_gt = cv2.rectangle(img, start_gt, end_gt, (0, 0, 0), -1)
            cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (0, 255, 255), 1, cv2.LINE_AA)
            img_det = cv2.rectangle(img_gt, start_det, end_det, (0, 255, 255), 1)
            cv2.putText(img_det, str(class_id_det) + ', ' + str(conf) + ', ' + str(rle_val) + ', ' +
                        str(iou_val), (int(bl_det[0]), int(bl_det[1]) - 3), font, 0.4, (0, 255, 255), 1, cv2.LINE_AA)
        elif index is 'D':
            img_gt = cv2.rectangle(img, start_gt, end_gt, (0, 0, 0), -1)
            cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (255, 255, 0), 1, cv2.LINE_AA)
            img_det = cv2.rectangle(img_gt, start_det, end_det, (255, 255, 0), 1)
            cv2.putText(img_det, str(class_id_det) + ', ' + str(conf) + ', ' + str(rle_val) + ', ' +
                        str(iou_val), (int(bl_det[0]), int(bl_det[1]) - 3), font, 0.4, (255, 255, 0), 1, cv2.LINE_AA)
        elif index is 'E':
            img_gt = cv2.rectangle(img, start_gt, end_gt, (0, 0, 0), -1)
            cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (0, 0, 255), 1, cv2.LINE_AA)
            img_det = cv2.rectangle(img_gt, start_det, end_det, (0, 0, 255), 1)
            cv2.putText(img_det, str(class_id_det) + ', ' + str(conf) + ', ' + str(rle_val) + ', ' +
                        str(iou_val), (int(bl_det[0]), int(bl_det[1]) - 3), font, 0.4, (0, 0, 255), 1, cv2.LINE_AA)
        return img_gt
    else:
        if c == 0:
            img_det = cv2.rectangle(img, start_det, end_det, (0, 0, 0), 1)
            cv2.putText(img_det, str(class_id_det) + ', ' + str(conf),
                        (int(bl_det[0]), int(bl_det[1]) - 3), font, 0.4, (0, 0, 0), 1, cv2.LINE_AA)
            if c2 < argmax or argmax_val == 0:
                if class_group_gt is 'threat':
                    img_gt = cv2.rectangle(img, start_gt, end_gt, (160, 160, 160), -1)
                    cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (0, 255, 0), 2, cv2.LINE_AA)
                else:
                    img_gt = cv2.rectangle(img, start_gt, end_gt, (160, 160, 160), -1)
                    cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (204, 102, 0), 1, cv2.LINE_AA)
                return img_gt
            else:
                return img_det
        else:
            if c2 < argmax or argmax_val == 0:
                if class_group_gt is 'threat':
                    img_gt = cv2.rectangle(img, start_gt, end_gt, (160, 160, 160), -1)
                    cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (0, 255, 0), 2, cv2.LINE_AA)
                else:
                    img_gt = cv2.rectangle(img, start_gt, end_gt, (160, 160, 160), -1)
                    cv2.putText(img_gt, str(class_id_gt), org, font, 0.4, (204, 102, 0), 1, cv2.LINE_AA)
                return img_gt
            else:
                return img


def wait_key(delay):
    # gets the delay valus for the waitkey function.
    key = cv2.waitKey(delay)
    if key == ord('p'):
        cv2.waitKey(-1)


def append_to_tp_list(index, c, s, f, class_id_gt):
    # gets the tp index, counter, set number, frame number, and classid and insert the relevant value to the tp's
    # matrix and tp's list.
    if index is 'A':
        tp_val_a.append(c)
        tp_a_mat[s][f][class_id_gt] += 1
    elif index is 'B':
        tp_val_b.append(c)
        tp_b_mat[s][f][class_id_gt] += 1
    elif index is 'C':
        tp_val_c.append(c)
        tp_c_mat[s][f][class_id_gt] += 1
    elif index is 'D':
        tp_val_d.append(c)
        tp_d_mat[s][f][class_id_gt] += 1
    elif index is 'E':
        tp_val_e.append(c)
        tp_e_mat[s][f][class_id_gt] += 1


def get_gt_detection_lists(argmax_list):
    # gets the argmax list and returns a list of detected frames and not-detected frames
    detected, not_detected = [], []
    for i in range(len(argmax_list)):
        if len(argmax_list[i]) == 0:
            not_detected.append(i)
        elif str(argmax_list[i][0]) == 'nan':
            continue
        elif len(argmax_list[i]) > 1:
            for j in range(len(argmax_list[i])):
                detected.append(i)
        elif snr and int(scene_class_id[i][0]) not in [0, 3, 8, 9]:
            continue
        else:
            detected.append(i)
    return detected, not_detected


def get_snr_and_area_lists(detected, not_detected):
    # gets lists of detected and not detected frames and returns lists of area and snr values of objects detected and
    # not-detected.
    snr_detected, area_detected = [], []
    snr_not_detected, area_not_detected = [], []
    for i in range(len(frames_gt)):
        if i in detected:
            w = int(frames_gt[i][0].split(' ')[5])
            h = int(frames_gt[i][0].split(' ')[6])
            if movie_project_gt == 'medusa':
                snr_detected.append(round(-float(frames_gt[i][0].split()[-1]), 3))
            else:
                snr_detected.append(round(float(frames_gt[i][0].split()[-1]), 3))
            area_detected.append(math.sqrt(w * h))
        elif i in not_detected:
            w = int(frames_gt[i][0].split(' ')[5])
            h = int(frames_gt[i][0].split(' ')[6])
            if movie_project_gt == 'medusa':
                snr_not_detected.append(round(-float(frames_gt[i][0].split()[-1]), 3))
            else:
                snr_not_detected.append(round(float(frames_gt[i][0].split()[-1]), 3))
            area_not_detected.append(math.sqrt(w * h))
        else:
            continue
    return snr_detected, area_detected, snr_not_detected, area_not_detected


def display_2d_hist(x, y, x2, y2):
    # gets 2d x and y lists and returns a 2d histogram.
    plt.cla()
    plt.clf()
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    # fig1, (ax1, ax2) = plt.subplots(nrows=2, figsize=(7, 6))
    h1 = ax1.hist2d(x, y, bins=[400, 300], range=[[0, 85], [-3, 12]])
    # ax1.set_xlabel('object size')
    ax1.set_ylabel('snr')
    ax1.set_title('detected objects')
    # cbar1 = plt.colorbar(h1[3], ax=ax1)
    # cbar1.ax.set_ylabel('counts', rotation=270)
    h2 = ax2.hist2d(x2, y2, bins=[400, 300], range=[[0, 85], [-3, 12]])
    ax2.set_xlabel('object size')
    ax2.set_ylabel('snr')
    ax2.set_title('un-detected objects')
    # cbar2 = plt.colorbar(h2[3], ax=ax2)
    # cbar2.ax.set_ylabel('counts', rotation=270)
    plt.pause(0.0001)
    return h1, h2


def process_snr():
    # this function prepares accumulated lists of snr and area values of detected and not-detected objects, creates
    # a 2d histogram and exports it into a pickle file.
    detected, not_detected = get_gt_detection_lists(argmax_list)
    snr_detected, area_detected, snr_not_detected, area_not_detected = get_snr_and_area_lists(detected, not_detected)
    area_detected_accum.extend(area_detected)
    snr_detected_accum.extend(snr_detected)
    area_not_detected_accum.extend(area_not_detected)
    snr_not_detected_accum.extend(snr_not_detected)
    h_detected, h_not_detected = display_2d_hist(area_detected_accum, snr_detected_accum, area_not_detected_accum,
                                                 snr_not_detected_accum)
    print('movie id in process:', movie_id_gt)
    plt.savefig('/home/shira/Desktop/2d_hist.png', bbox_inches='tight')
    pickle.dump(h_detected, open(os.path.join(export_path, 'h_detected.pickle'), 'wb'))
    pickle.dump(h_not_detected, open(os.path.join(export_path, 'h_not_detected.pickle'), 'wb'))


def export_metrics():
    # this function is not in use - metrics are created in a different script.

    # prec_class_all, recall_class_all, f1_score_class_all, prec_group_all, recall_group_all, f1_score_group_all = \
    #     compute_confusion_metrics(fn_set1, fp_set1, tp_a_set1, tp_b_set1, tp_c_set1, tp_d_set1, tp_e_set1)
    # prec_class_threat, recall_class_threat, f1_score_class_threat, prec_group_threat, recall_group_threat, \
    # f1_score_group_threat = compute_confusion_metrics(fn_set2, fp_set2, tp_a_set2, tp_b_set2, tp_c_set2,
    #                                                   tp_d_set2,
    #                                                   tp_e_set2)
    # prec_class_non_threat, recall_class_non_threat, f1_score_class_non_threat, prec_group_non_threat, \
    # recall_group_non_threat, f1_score_group_non_threat = compute_confusion_metrics(fn_set3, fp_set3, tp_a_set3,
    #                                                                                tp_b_set3, tp_c_set3,
    #                                                                                tp_d_set3,
    #                                                                                tp_e_set3)
    path, title = os.path.split(scene_det_list)
    title = title.split('.')[0]
    # res_file = open(os.path.join(export_path, title + '_metrics_all_' + date + '.txt'), "w+")
    # res_file.write('set 1: all of gt' + '\n'
    #                + 'prec_class_all: ' + str(prec_class_all) + '\n'
    #                + 'recall_class_all: ' + str(recall_class_all) + '\n'
    #                + 'f1_score_class_all: ' + str(f1_score_class_all) + '\n'
    #                + 'prec_group_all: ' + str(prec_group_all) + '\n'
    #                + 'recall_group_all: ' + str(recall_group_all) + '\n'
    #                + 'f1_score_group_all: ' + str(f1_score_group_all) + '\n' + '\n'
    #                + 'set 2: threats' + '\n'
    #                + 'prec_class_threat: ' + str(prec_class_threat) + '\n'
    #                + 'recall_class_threat: ' + str(recall_class_threat) + '\n'
    #                + 'f1_score_class_threat: ' + str(f1_score_class_threat) + '\n'
    #                + 'prec_group_threat: ' + str(prec_group_threat) + '\n'
    #                + 'recall_group_threat: ' + str(recall_group_threat) + '\n'
    #                + 'f1_score_group_threat: ' + str(f1_score_group_threat) + '\n' + '\n'
    #                + 'set 3: non-threats' + '\n'
    #                + 'prec_class_non_threat: ' + str(prec_class_non_threat) + '\n'
    #                + 'recall_class_non_threat: ' + str(recall_class_non_threat) + '\n'
    #                + 'f1_score_class_non_threat: ' + str(f1_score_class_non_threat) + '\n'
    #                + 'prec_group_non_threat: ' + str(prec_group_non_threat) + '\n'
    #                + 'recall_group_non_threat: ' + str(recall_group_non_threat) + '\n'
    #                + 'f1_score_group_non_threat: ' + str(f1_score_group_non_threat) + '\n')
    # res_file.close()


def export_cm_values_all():
    # this functions writes to a file the values of the confusion matrix for each set.
    path, title = os.path.split(scene_det_list)
    title = title.split('.')[0]
    res_file = open(os.path.join(export_path, title + '_cm_all_' + date + '.txt'), "w+")
    res_file.write('set 1: all of gt' + '\n'
                   + 'fp_set1: ' + str(np.sum(fp_set1)) + '\n'
                   + 'fn_set1: ' + str(np.sum(fn_set1)) + '\n'
                   + 'tp_a_set1: ' + str(np.sum(tp_a_set1)) + '\n'
                   + 'tp_b_set1: ' + str(np.sum(tp_b_set1)) + '\n'
                   + 'tp_c_set1: ' + str(np.sum(tp_c_set1)) + '\n'
                   + 'tp_d_set1: ' + str(np.sum(tp_d_set1)) + '\n'
                   + 'tp_e_set1: ' + str(np.sum(tp_e_set1)) + '\n'
                   + 'nms_set1: ' + str(np.sum(nms_set1)) + '\n' + '\n'
                   + 'set 2: threats' + '\n'
                   + 'fp_set2: ' + str(np.sum(fp_set2)) + '\n'
                   + 'fn_set2: ' + str(np.sum(fn_set2)) + '\n'
                   + 'tp_a_set2: ' + str(np.sum(tp_a_set2)) + '\n'
                   + 'tp_b_set2: ' + str(np.sum(tp_b_set2)) + '\n'
                   + 'tp_c_set2: ' + str(np.sum(tp_c_set2)) + '\n'
                   + 'tp_d_set2: ' + str(np.sum(tp_d_set2)) + '\n'
                   + 'tp_e_set2: ' + str(np.sum(tp_e_set2)) + '\n' + '\n'
                   + 'set 3: non-threats' + '\n'
                   + 'fp_set3: ' + str(np.sum(fp_set3)) + '\n'
                   + 'fn_set3: ' + str(np.sum(fn_set3)) + '\n'
                   + 'tp_a_set3: ' + str(np.sum(tp_a_set3)) + '\n'
                   + 'tp_b_set3: ' + str(np.sum(tp_b_set3)) + '\n'
                   + 'tp_c_set3: ' + str(np.sum(tp_c_set3)) + '\n'
                   + 'tp_d_set3: ' + str(np.sum(tp_d_set3)) + '\n'
                   + 'tp_e_set3: ' + str(np.sum(tp_e_set3)) + '\n' + '\n')
    res_file.close()


def export_cm_values_per_class():
    # this functions writes to a file the values of the confusion matrix for each set per class.
    path, title = os.path.split(scene_det_list)
    title = title.split('.')[0]
    res_file = open(os.path.join(export_path, title + '_cm_per_class_' + date + '.txt'), "w+")
    for j in range(len(classes)):
        res_file.write('*** cls_' + str(j) + ' ***' + '\n')
        for i in range(3):
            if i == 0:
                res_file.write('set 1: all of gt' + '\n')
            elif i == 1:
                res_file.write('set 2: threats' + '\n')
            elif i == 2:
                res_file.write('set 3: non-threats' + '\n')
            res_file.write('fp_mat: ' + str(np.sum(fp_mat[i][:, j])) + '\n'
                           + 'fn_mat: ' + str(np.sum(fn_mat[i][:, j])) + '\n'
                           + 'tp_a_mat: ' + str(np.sum(tp_a_mat[i][:, j])) + '\n'
                           + 'tp_b_mat: ' + str(np.sum(tp_b_mat[i][:, j])) + '\n'
                           + 'tp_c_mat: ' + str(np.sum(tp_c_mat[i][:, j])) + '\n'
                           + 'tp_d_mat: ' + str(np.sum(tp_d_mat[i][:, j])) + '\n'
                           + 'tp_e_mat: ' + str(np.sum(tp_e_mat[i][:, j])) + '\n'
                           + 'nms_mat: ' + str(np.sum(nms_mat[i][:, j])) + '\n' + '\n')
    res_file.close()


def export_metrics_per_class():
    # this function is not in use.
    # this functions writes to a file the values of the metrics for each set per class.
    path, title = os.path.split(scene_det_list)
    title = title.split('.')[0]
    res_file = open(os.path.join(export_path, title + '_metrics_per_class_' + date + '.txt'), "w+")
    for j in range(len(classes)):
        res_file.write('*** cls_' + str(j) + ' ***' + '\n')
        for i in range(3):
            prec_class, recall_class, f1_score_class, prec_group, recall_group, f1_score_group \
                = compute_confusion_metrics_per_class(fn_mat, fp_mat, tp_a_mat, tp_b_mat, tp_c_mat, tp_d_mat,
                                                      tp_e_mat, i, j)
            if i == 0:
                res_file.write('set 1: all of gt' + '\n')
            elif i == 1:
                res_file.write('set 2: threats' + '\n')
            elif i == 2:
                res_file.write('set 3: non-threats' + '\n')
            res_file.write('prec_class_all: ' + str(prec_class) + '\n'
                                                                  'recall_class_all: ' + str(recall_class) + '\n'
                                                                                                             'f1_score_class_all: ' + str(
                f1_score_class) + '\n'
                                  'prec_group_all: ' + str(prec_group) + '\n'
                                                                         'recall_group_all: ' + str(recall_group) + '\n'
                                                                                                                    'f1_score_group_all: ' + str(
                f1_score_group) + '\n' + '\n')
    res_file.close()


def find_max_lost_track_id(f_lst):
    # gets a list of dets/gts and returns the maximum lost time - the biggest gap between frames.
    lost = []
    for i in range(len(f_lst) - 1):
        if len(f_lst[i].split()) == 10 or len(f_lst[i].split()) == 11:
            if int(f_lst[i].split()[0]) + steps != int(f_lst[i + 1].split()[0]) and int(f_lst[i].split()[0]) != int(f_lst[i + 1].split()[0]):
                lost.append([f_lst[i].split()[7], f_lst[i].split()[0], int(f_lst[i + 1].split()[0]) - int(f_lst[i].split()[0]) - steps])
        else:
            if int(f_lst[i].split()[1]) + steps != int(f_lst[i + 1].split()[1]) and int(f_lst[i].split()[1]) != int(f_lst[i + 1].split()[1]):
                lost.append([f_lst[i].split()[-3], f_lst[i].split()[1], int(f_lst[i + 1].split()[1]) - int(f_lst[i].split()[1]) - steps])
    if len(lost) == 0:
        return 0, 0
    else:
        sum_lost = sum([int(y[2]) for y in lost if int(y[2])])
        for j in reversed(range(len(lost) - 1)):
            if int(lost[j][0]) == int(lost[j + 1][0]):
                lost[j].extend(lost[j + 1])
                lost.pop(j + 1)
        max_lost = []
        for k in range(len(lost)):
            max_val = []
            for l in range(2, len(lost[k]), 3):
                max_val.append(lost[k][l])
            max_lost.append([lost[k][0], np.max(max_val)])
        return max_lost[0][1], sum_lost


def life_time_track_ids(id_by_group):
    # this function is not in use
    life_time = []
    for i in range(len(id_by_group)):
        if len(id_by_group[0][0].split()) == 10 or len(id_by_group[0][0].split()) == 11:
            life_time.append(
                [id_by_group[i][1].split()[7], int(id_by_group[i][-1].split()[0]) - int(id_by_group[i][0].split()[0])])
        else:
            life_time.append(
                [id_by_group[i][1].split()[-1], int(id_by_group[i][-1].split()[1]) - int(id_by_group[i][0].split()[1])])
    return life_time


def group_track_ids(ids_list):
    # gets a list of dets/gts and returns a sorted and grouped list of lists by track id.
    uniq_ids = []
    if len(ids_list) == 0:
        return ids_list
    ids_list = [r for r in ids_list if len(r) > 0]
    if len(ids_list[0][0].split()) == 10 or len(ids_list[0][0].split()) == 11:
        ids_list.sort(key=lambda y: int(y[0].split()[7]))
    else:
        ids_list = [x for x in ids_list if len(x) > 0]
        ids_list.sort(key=lambda y: int(y[0].split()[-2]))
    for x in reversed(range(len(ids_list) - 1)):
        if len(ids_list[x][0].split()) == 10 or len(ids_list[x][0].split()) == 11:
            if int(ids_list[x][0].split()[7]) not in uniq_ids:
                uniq_ids.append(int(ids_list[x][0].split()[7]))
            if int(ids_list[x][0].split()[7]) == int(ids_list[x + 1][0].split()[7]):
                ids_list[x].extend(ids_list[x + 1])
                ids_list.pop(x + 1)
        else:
            if int(ids_list[x][0].split()[-2]) not in uniq_ids:
                uniq_ids.append(int(ids_list[x][0].split()[-2]))
            if int(ids_list[x][0].split()[-2]) == int(ids_list[x + 1][0].split()[-2]):
                ids_list[x].extend(ids_list[x + 1])
                ids_list.pop(x + 1)
    if len(ids_list[0][0].split()) == 10 or len(ids_list[0][0].split()) == 11:
        ids_list.sort(key=lambda y: int(y[0].split()[0]))
    else:
        ids_list.sort(key=lambda y: int(y[0].split()[1]))
    return ids_list


def choose_best_det(atts_lst, multi_dets, arg_dets, arg_dets_lut):
    # gets a list of detections per frame (atts_lst), a list of frames in which there are multiple detections
    # (multi_dets), a list of argmax valus of the detections and their look-up table. returns a list of only the "best
    # detections" per frame (one per frame).
    atts_lst_copy = copy.deepcopy(atts_lst)
    for i in range(len(atts_lst_copy)):
        if len(atts_lst_copy[i]) > 1:
            for j in range(len(multi_dets)):
                if len(atts_lst_copy[i][0].split()) == 9 or len(atts_lst_copy[i][0].split()) == 8:
                    if int(atts_lst_copy[i][0].split()[1]) == multi_dets[j]:
                        atts_lst_copy[i] = [atts_lst_copy[i][arg_dets_lut[j][x]] for x, v in enumerate(arg_dets_lut[j])]
                        break
                else:
                    if int(atts_lst_copy[i][0].split()[0]) == multi_dets[j]:
                        try:
                            atts_lst_copy[i] = [atts_lst_copy[i][arg_dets[j][x]] for x, v in enumerate(arg_dets[j])]
                        except:
                            continue
                        break
    for k, _ in enumerate(atts_lst_copy):
        if len(atts_lst_copy[k]) > 1:
            for i in reversed(range(len(atts_lst_copy[k]) - 1)):
                atts_lst_copy.append([atts_lst_copy[k][i]])
                atts_lst_copy[k].pop(i)
    return atts_lst_copy


def find_min_max_area(gt_id_by_group):
    # gets a list of gts/dets per id, and returns the min and max area value.
    area = []
    for j in gt_id_by_group:
        if len(j.split()) == 10 or len(j.split()) == 11:
            area.append(int(j.split()[5]) * int(j.split()[6]))
        else:
            area.append(int(j.split()[-3]) * int(j.split()[-4]))
    min_area = np.min(area)
    max_area = np.max(area)
    return min_area, max_area


def find_min_max_snr(gt_id_by_group):
    # gets a list of gts/dets per id, and returns the min and max snr value.
    snr = []
    for j in gt_id_by_group:
        snr.append(float(j.split()[-1]))
    min_snr = np.min(snr)
    max_snr = np.max(snr)
    return min_snr, max_snr


def group_det_by_track(gt_id_by_group, atts_det_best):
    # gets a list of gts grouped by id and a list of the best detections per frame, returns a list of grouped detections
    # by the gts id.
    lst = []
    for i in range(len(gt_id_by_group)):
        ls = []
        for j in gt_id_by_group[i]:
            frid = int(j.split()[1])
            for k in range(len(atts_det_best)):
                if int(atts_det_best[k][0].split()[0]) == frid:
                    ls.append(atts_det_best[k][0])
                    break
        lst.append(ls)
    return lst


def create_falses_list(atts_det_best):
    # gets a list of the best detetection per frame, and a list of frames (indexes), returns a list of detections that
    # are classified as false positives.
    global frames_det
    f_d = [i for c, i in enumerate(frames_det) if len(i[0].split()) > 2]
    a = atts_det_best
    for i in reversed(range(len(f_d))):
        if len(f_d[i]) > 1:
            for c, j in enumerate(f_d[i]):
                if c == 0:
                    f_d.append([j])
                else:
                    if [j] not in f_d:
                        f_d.append([j])
            f_d.pop(i)
    f_d.sort(key=lambda y: int(y[0].split()[0]))
    falses_list = [r for r in f_d if r not in a]
    return falses_list


def export_all_gts_per_class(atts_gt):
    # gets a list of gts per frame and returns a list of those gts grouped by classes.
    positive_ids = [r for r in atts_gt if int(r[0].split()[-2]) > 0]
    per_class_gts = []
    for i in classes:
        lst_per_class = [r for r in atts_gt if int(r[0].split()[2]) == i ]
        per_class_gts.append(lst_per_class)
    return per_class_gts


def prepare_id_lists_by_group(detected):
    # this function is responsible for the process of creating list of gts and detections that are grouped by the track
    # ids given to the gts.
    global frames_gt, frames_det
    frames_gt_copy = copy.deepcopy(frames_gt)
    frames_det_copy = copy.deepcopy(frames_det)
    f_i = [(c, i) for c, i in enumerate(frames_gt) if len(i[0].split()) > 2]
    if len(f_i) != 0:
        f_d = [(c, i) for c, i in enumerate(frames_det) if len(i[0].split()) > 2]

        atts_gt = [f_i[i][1] for i in range(len(f_i))]
        per_class_gts = export_all_gts_per_class(atts_gt)
        atts_det = [f_d[i][1] for i in range(len(f_d)) if f_d[i][0] in detected]

        multi_dets = [int(atts_gt[i][0].split()[1]) for i in range(len(atts_gt)) if len(atts_gt[i]) > 1]
        ind_list = []
        for i in np.arange(start_frame, end_frame+1, steps):
            ind_list.append(int(i))
        multi_dets_ind = [ind_list.index(i) for i in multi_dets]
        # multi_dets_ind = [ind_list.index(i) for i in multi_dets if i in ind_list]
        arg_dets = [argmax_list[i] for i in multi_dets_ind]
        arg_dets_lut = [argmax_list_lut[i] for i in multi_dets_ind]

        combined_gs_gt = create_combined_gs_dets(frames_gt_copy, frames_det_copy)
        combined_gs_gt_samp = [[x[0]] for r in combined_gs_gt for x in r]  # taking them out of the lists
        combined_gs_gt_samp_detected = [r for r in combined_gs_gt_samp if len(r[0].split()) > 2]

        # atts_gt_best = choose_best_det(atts_gt, multi_dets, arg_dets, arg_dets_lut)
        atts_det_best = choose_best_det(atts_det, multi_dets, arg_dets, arg_dets_lut)

        # split into groups of track id:
        # gt_id_by_group_old = group_track_ids(atts_gt_best)
        gt_id_by_group = group_track_ids(combined_gs_gt_samp_detected)

        # creating the dets list grouped as the gts
        det_id_by_group = group_det_by_track(gt_id_by_group, atts_det_best)

        gt_id_by_group_copy = copy.deepcopy(gt_id_by_group)
        det_id_by_group_copy = copy.deepcopy(det_id_by_group)
        [y.sort(key=lambda x: int(x.split()[1])) for y in gt_id_by_group_copy]
        [y.sort(key=lambda x: int(x.split()[0])) for y in det_id_by_group_copy if len(y) > 0]
        return gt_id_by_group_copy, det_id_by_group_copy, atts_det_best, per_class_gts
    else:
        return [], [], [], []


def find_major_class(gt_id_by_group):
    # gets a list of gts/dets group of a specific id and returns the major class in this group.
    cls_list, cls_threat = [], []
    for j in gt_id_by_group:
        if len(j.split()) == 10 or len(j.split()) == 11:
            cls_list.append(int(j.split()[1]))
            if int(j.split()[1]) in [0, 5, 8, 9]:
                cls_threat.append(int(j.split()[1]))
        else:
            cls_list.append(int(j.split()[2]))
    uni_threat, cnts_threat = np.unique(cls_threat, return_counts=True)
    if len(uni_threat) == 0 or len(cnts_threat) == 0:
        uni, cnts = np.unique(cls_list, return_counts=True)
        args = np.argmax(cnts)
        return uni[args]
    else:
        args_threats = np.argmax(cnts_threat)
        return uni_threat[args_threats]


def find_time_as_threat(gt_id_by_group):
    # gets a list of gts/dets group of a specific id and returns the maximum time (frames) that the object was
    # classified as a threat - total time as threat and max time as threat.
    threat_list, cum, sums = [], [], []
    for i in range(len(gt_id_by_group)):
        if i < len(gt_id_by_group) - 1:
            _, group_current = parse_labels(int(gt_id_by_group[i].split()[1]))
            _, group_next = parse_labels(int(gt_id_by_group[i + 1].split()[1]))
            if group_current == 'threat':
                threat_list.append(int(gt_id_by_group[i].split()[0]))
                cum.append(int(gt_id_by_group[i].split()[0]))
            if group_next != 'threat':
                sums.append(len(cum))
                cum = []
        else:
            _, group_current = parse_labels(int(gt_id_by_group[i].split()[1]))
            if group_current == 'threat':
                threat_list.append(int(gt_id_by_group[i].split()[0]))
                cum.append(int(gt_id_by_group[i].split()[0]))
    if len(sums) == 0:
        try:
            longest_cum = (threat_list[-1] - threat_list[0])
        except:
            longest_cum = 0
    else:
        longest_cum = np.max(sums)
    try:
        return (threat_list[-1] - threat_list[0]), longest_cum
    except:
        return 0, 0


def find_class_loss(gt_id_by_group):
    # gets a list of gts/dets group of a specific id and returns the maximum class-loss time (frames) of an object
    # to another class inside its group (threat/non-threat) or outside of it.
    lost, lost_in_group, lost_out_group = [], [], []
    for i in range(len(gt_id_by_group) - 1):
        if int(gt_id_by_group[i].split()[2]) != int(gt_id_by_group[i + 1].split()[2]):
            _, group_current = parse_labels(int(gt_id_by_group[i].split()[2]))
            _, group_next = parse_labels(int(gt_id_by_group[i + 1].split()[2]))
            if int(gt_id_by_group[i + 1].split()[1]) - int(gt_id_by_group[i].split()[1]) != 1:
                lost.append([gt_id_by_group[i].split()[2], gt_id_by_group[i].split()[1],
                             int(gt_id_by_group[i + 1].split()[1]) - int(gt_id_by_group[i].split()[1])])
                if group_current == group_next:
                    lost_in_group.append(
                        [gt_id_by_group[i].split()[2], gt_id_by_group[i].split()[1],
                         int(gt_id_by_group[i + 1].split()[1]) - int(gt_id_by_group[i].split()[1])])
                else:
                    lost_out_group.append(
                        [gt_id_by_group[i].split()[2], gt_id_by_group[i].split()[1],
                         int(gt_id_by_group[i + 1].split()[1]) - int(gt_id_by_group[i].split()[1])])
    return lost, lost_in_group, lost_out_group


def find_longs_class_loss(lost):
    # gets a list of gts/dets group of a specific id and returns the longest time (frames) an object class has been
    # lost (changes its class).
    if len(lost) == 0:
        return 0
    else:
        for j in reversed(range(len(lost) - 1)):
            if int(lost[j][0]) == int(lost[j + 1][0]):
                lost[j].extend(lost[j + 1])
                lost.pop(j + 1)
        max_lost = []
        for k in range(len(lost)):
            max_val = []
            for l in range(2, len(lost[k]), 3):
                max_val.append(lost[k][l])
            max_lost.append([lost[k][0], np.max(max_val)])
        return max_lost[0][1]


def find_id_swaps(det_id_by_group):
    # gets a list of dets group of a specific gt id and returns the number of time the detection's id had changed.
    ids = []
    for c, v in enumerate(det_id_by_group):
        ids.append(int(v.split()[-3]))
    uni = np.unique(ids)
    return uni


def find_median_conf(lst):
    # gets a list of detections and returns the median confidence value.
    conf = []
    for j in lst:
        conf.append(float(j.split()[2]))
    med_conf = np.median(conf)
    return med_conf


def find_source(lst):
    # gets a list of detections and returns the major source value of it.
    src_list = []
    for j in lst:
        src_list.append(int(j.split()[-3]))
    uni, cnts = np.unique(src_list, return_counts=True)
    args = np.argmax(cnts)
    return uni[args]


def false_alarm_analysis(falses_by_track):
    # this function is responsible of producing the false alarm analysis and exporting it. it gets the falses list
    # and extracts all of the necessary details.
    falses_by_track_copy = copy.deepcopy(falses_by_track)
    trk_movie_id = movie_id_det
    frames_num = len(frames_det)
    if len(falses_by_track_copy) == 0:
        rows_det = [trk_movie_id, str(frames_num), '', '', '', '', '', '', '', '', '', '', '', '']
        wr_faa.writerow(rows_det)
    else:
        for j in range(len(falses_by_track_copy)):
            if len(falses_by_track_copy[j]) == 0:
                continue
            total_time_as_threat, max_time_as_threat = find_time_as_threat(falses_by_track_copy[j])
            total_time_as_threat = total_time_as_threat / steps
            max_time_as_threat = max_time_as_threat / steps
            det_trk_id = falses_by_track_copy[j][0].split()[-4]
            det_min_area, det_max_area = find_min_max_area(falses_by_track_copy[j])
            med_conf = find_median_conf(falses_by_track_copy[j])
            min_snr, _ = find_min_max_snr(falses_by_track_copy[j])
            source = find_source(falses_by_track_copy[j])
            major_class_det = find_major_class(falses_by_track_copy[j])
            det_max_lost_time_j, sum_lost_det_j = find_max_lost_track_id(falses_by_track_copy[j])
            det_max_lost_time_j = det_max_lost_time_j / steps
            sum_lost_det_j = sum_lost_det_j / steps
            life_time_det_j = int(falses_by_track_copy[j][-1].split()[0]) - int(falses_by_track_copy[j][0].split()[0])
            life_time_det_j = life_time_det_j / steps + 1
            det_neto_life_time_j = len(falses_by_track_copy[j])
            rows_det = [trk_movie_id, str(frames_num), det_trk_id, str(det_min_area), str(det_max_area),
                        str(life_time_det_j),
                        str(det_neto_life_time_j), str(det_max_lost_time_j), str(major_class_det),
                        str(total_time_as_threat),
                        str(max_time_as_threat),
                        str(med_conf), str(min_snr), str(source)]
            wr_faa.writerow(rows_det)


def extract_track_id_details(gt_id_by_group, det_id_by_group, atts_det_best):
    # this function is responsible of producing the track id analysis and exporting it. it gets the lists of detections
    # and gts grouped by id and extracts all of the necessary details.
    det_id_by_group_copy = copy.deepcopy(det_id_by_group)
    gt_id_by_group_copy = copy.deepcopy(gt_id_by_group)
    path, title = os.path.split(scene_gt_list)
    title = title.split('.')[0]
    trk_movie_id = movie_id_gt
    for i in range(len(gt_id_by_group)):
        trk_id = gt_id_by_group[i][0].split()[-2]
        life_time_gt = int(gt_id_by_group[i][-1].split()[1]) - int(gt_id_by_group[i][0].split()[1])
        life_time_gt = life_time_gt / steps + 1
        gt_max_lost_time, sum_lost_gt = find_max_lost_track_id(gt_id_by_group_copy[i])
        gt_max_lost_time = gt_max_lost_time / steps
        sum_lost_gt = sum_lost_gt / steps
        gt_neto_life_time = len(gt_id_by_group[i])
        major_class = find_major_class(gt_id_by_group[i])
        min_area, max_area = find_min_max_area(gt_id_by_group[i])
        min_snr, max_snr = find_min_max_snr(gt_id_by_group[i])
        frames_num = len(frames_gt)
        if len(det_id_by_group[i]) != 0:
            was_detected = True
        else:
            was_detected = False
            rows = [trk_movie_id, trk_id, str(major_class), str(was_detected), str(life_time_gt),
                    str(gt_neto_life_time), "", "", "", "", "", "", "", "", str(min_area), str(max_area),
                    str(min_snr), str(max_snr), "", "", str(frames_num)]
            wr.writerow(rows)
            continue

        life_time_det = int(det_id_by_group[i][-1].split()[0]) - int(det_id_by_group[i][0].split()[0])
        life_time_det = life_time_det / steps + 1
        time_till_det = int(det_id_by_group[i][0].split()[0]) - int(gt_id_by_group[i][0].split()[1])
        time_till_det = time_till_det / steps
        det_id_swaps = len(find_id_swaps(det_id_by_group_copy[i])) - 1
        det_max_lost_time, sum_lost_det = find_max_lost_track_id(det_id_by_group_copy[i])
        det_major_class = find_major_class(det_id_by_group_copy[i])
        det_max_lost_time = det_max_lost_time / steps
        sum_lost_det = sum_lost_det / steps
        det_neto_life_time = life_time_det - sum_lost_det
        lost, lost_in_group, lost_out_group = find_class_loss(gt_id_by_group[i])
        longs_class_loss = find_longs_class_loss(lost)
        longs_class_loss = longs_class_loss / steps
        longs_in_group_class_loss = find_longs_class_loss(lost_in_group)
        longs_in_group_class_loss = longs_in_group_class_loss / steps
        longs_out_group_class_loss = find_longs_class_loss(lost_out_group)
        longs_out_group_class_loss = longs_out_group_class_loss / steps
        det_rate = round((det_neto_life_time / gt_neto_life_time) * 100, 2)
        median_conf = find_median_conf(det_id_by_group_copy[i])

        rows = [trk_movie_id, trk_id, str(major_class), str(was_detected), str(life_time_gt),
                str(gt_neto_life_time),
                str(det_neto_life_time), str(det_rate), str(time_till_det), str(det_id_swaps),
                str(det_max_lost_time),
                str(longs_class_loss), str(longs_in_group_class_loss), str(longs_out_group_class_loss),
                str(min_area),
                str(max_area), str(min_snr), str(max_snr), str(median_conf), str(det_major_class), str(frames_num)]
        wr.writerow(rows)


def extract_track_id_details_for_log(gt_id_by_group, det_id_by_group, atts_det_best):
    # this function is responsible of producing the track id (log) analysis (different from the
    # extract_track_id_details functions) and exporting it. it gets the lists of detections
    # and gts grouped by id and extracts all of the necessary details.
    det_id_by_group_copy = copy.deepcopy(det_id_by_group)
    gt_id_by_group_copy = copy.deepcopy(gt_id_by_group)
    path, title = os.path.split(scene_gt_list)
    title = title.split('.')[0]
    trk_movie_id = movie_id_gt
    threats = [0, 5, 8, 9]
    ids_list = []
    # for c, gr in enumerate(gt_id_by_group_copy):
    #     ids_list.append([c, int(gr[0].split()[7])])
    # pairs = [r for r in ids_list if r[1] < 0]
    for i, group in enumerate(gt_id_by_group_copy):
        # if len(det_id_by_group[i]) != 0:
        #     was_detected = True
        # else:
        #     continue
        rows = []
        trk_id = abs(int(gt_id_by_group_copy[i][0].split()[7]))
        # if int(trk_id) < 0:
        #     continue
        frame_num = gt_id_by_group_copy[i][0].split()[1]
        line_1 = str(trk_movie_id) + ' ' + str(trk_id) + ' ' + str(frame_num)
        for line in group:
            elements = line.split()
            class_id = abs(int(elements[2]))
            if int(class_id) not in threats:
                continue
            xc = elements[3]
            yc = elements[4]
            w = elements[5]
            h = elements[6]
            snr_val = elements[8]
            line_2 = (str(xc) + ' ' + str(yc) + ' ' + str(w) + ' ' + str(h) + ' ' + str(snr_val))
            rows.append(line_2)
        if len(rows) != 0:
            combined = [r for r in line_1.split()]
            for r in rows:
                combined.append((r))
            wr_log.writerow(combined)


def extract_det_details_for_log(gt_id_by_group, det_id_by_group, falses_by_track):
    # this function is responsible of producing the detections analysis (log) analysis and exporting it.
    # it gets the lists of falses, detections and gts grouped by id and extracts all of the necessary details.
    fps = int(np.ceil(15 / steps))
    det_id_by_group_copy = copy.deepcopy(det_id_by_group)
    gt_id_by_group_copy = copy.deepcopy(gt_id_by_group)
    falses_by_track_copy = copy.deepcopy(falses_by_track)
    path, title = os.path.split(scene_det_list)
    title = title.split('.')[0]
    trk_movie_id = movie_id_det
    threats = [0, 5, 8, 9]
    combined_lines_det = [line for r in det_id_by_group_copy for line in r]
    lines_list = []
    for i, group in enumerate(det_id_by_group_copy):
        if len(det_id_by_group_copy[i]) == 0:
            continue
        for line in group:
            if line in lines_list:
                continue
            elements = line.split()
            frame_id = elements[0]
            class_id = elements[1]
            if int(class_id) not in threats:
                continue
            # if int(elements[8]) == 10 or int(elements[8]) == 11:
            #     continue
            xc = elements[3]
            yc = elements[4]
            w = elements[5]
            h = elements[6]
            wr_det_log.writerow([str(trk_movie_id), str(frame_id), str(xc), str(yc), str(w), str(h)])
            lines_list.append(line)
    for j, track in enumerate(falses_by_track_copy):
        if len(falses_by_track_copy[j]) == 0:
            continue
        for line_fa in track:
            fa_elements = line_fa.split()
            frame_id_fa = fa_elements[0]
            class_id_fa = fa_elements[1]
            if int(class_id_fa) not in threats:
                continue
            conf_fa = fa_elements[2]
            xc_fa = fa_elements[3]
            yc_fa = fa_elements[4]
            w_fa = fa_elements[5]
            h_fa = fa_elements[6]
            life_time_det_j = int(falses_by_track_copy[j][-1].split()[0]) - int(falses_by_track_copy[j][0].split()[0])
            life_time_det_j = life_time_det_j / steps + 1
            if life_time_det_j/fps < 3:
                continue
            if int(fa_elements[8]) == 10 or int(fa_elements[8]) == 11:
                continue
            # if int(class_id_fa) == 3 and int(conf_fa) > 50:
            #     class_id_fa = '0'
            # elif int(class_id_fa) == 3 and int(conf_fa) <= 50:
            #     continue
            if int(class_id_fa) == 8 and int(conf_fa) < 60:
                continue
            if int(class_id_fa) == 9 and int(conf_fa) < 50:
                continue
            wr_det_log.writerow([str(trk_movie_id), str(frame_id_fa), str(xc_fa), str(yc_fa), str(w_fa), str(h_fa)])


def parse_conf_mat():
    # if not in systems checking mode, this function is responsible of producing the mean and std of the confidence
    # values of the detections per class and exporting it.
    res_file_conf = open(os.path.join(export_path, 'conf_analysis_' + date + '.txt'), "w+")
    res_file_conf.write('*** confidence score (mean + std) per class ***' + '\n')
    for c, v in enumerate(classes):
        non_zeros = [x for x in conf_mat[0][:][:, c] if x != 0]
        mean = round(np.mean(non_zeros), 3)
        std = round(np.std(non_zeros), 3)
        res_file_conf.write('class id ' + str(c) + ': ' + str(mean) + ' + ' + str(std) + '\n')
    res_file_conf.close()


def extracting_movie_id_gt():
    # this functions uses the project information from the config flag and extracts the movie id, project id
    # and dual parameter (if defined) from the gt line.
    movie_project_gt = None
    dual_param_parsed = None
    if mp4 and bp:
        movie_id_gt = os.path.basename(gt_).split('_BP_combined')[0]
    elif mp4 and dg:
        movie_id_gt = os.path.basename(gt_).split('_DualGround')[0].split('_')[0]
    elif dg:
        movie_id_gt = os.path.basename(gt_).split('_DualGround')[0].split('_')[0]
    elif mp4:
        movie_id_gt = gt_.split('_')[-7].split(os.sep)[1]
    elif bp:
        movie_id_gt = os.path.basename(gt_).split('_BP_combined')[0]
        # movie_id_gt = os.path.basename(gt_).split('_BP')[0]
    elif simulator:
        movie_id_gt = gt_.split('_')[-4]
    elif snr:
        movie_id_gt = gt_.split('_')[-4].split(os.sep)[1]
        movie_project_gt = gt_.split('_')[-1].split('.txt')[0]
    else:
        # movie_id_gt = gt_.split('_')[-4].split(os.sep)[1]
        # movie_id_gt = gt_.split('_')[0].split(os.sep)[-1]
        movie_id_gt = os.path.basename(gt_).split('_')[0]
        if dual_database:
            dual_param_parsed = os.path.basename(gt_).split('_')[1].split('r')[1]
    return movie_id_gt, movie_project_gt, dual_param_parsed


def extracting_video_path():
    # if in preview mode, this functions extracts the movie video path.
    video_path = None
    if preview and raw or mp4:
        comb_video_lines = [line.rstrip() for line in open(comb_video_path, 'r')]
        for vid in comb_video_lines:
            _, movie_id_vid = os.path.split(vid)
            if bp:
                movie_id_vid = os.path.splitext(movie_id_vid)[0]
            else:
                movie_id_vid = movie_id_vid.split('_')[0]
            if not bp and int(movie_id_gt) == int(movie_id_vid):
                video_path = vid
                break
            elif bp and movie_id_gt == movie_id_vid:
                video_path = vid
                break
    return video_path


def extracting_movie_id_det():
    # this functions uses the project information from the config flag and extracts the movie id and project id
    # (if defined) from the det line.
    movie_project_det = None
    if bp and mp4:
        movie_id_det = '_'.join(os.path.basename(det).split('_detections.txt')[0].split('_')[1:])
    elif mp4 and dg:
        movie_id_det = os.path.basename(det).split('_')[0]
    elif dg:
        movie_id_det = os.path.basename(det).split('_')[0]
    elif mp4:
        movie_id_det = det.split('_')[-7]
    elif bp:
        # movie_id_det = os.path.basename(det).split('_detections.txt')[0].split(lst_id)[1][1:]
        movie_id_det = os.path.basename(det).split('_detections.txt')[0].split(lst_id)[1].split('_')[1]
    else:
        movie_id_det = os.path.basename(det).split('_')[1]
        movie_project_det = det.split(os.sep)[-1].split('_')[0]
    return movie_id_det, movie_project_det


def define_scene_lists():
    # this functions uses necessary flags from the config file and movie id parameters in order to determine
    # the final gt and det lists (by scene) for the whole analysis.
    scene_gt_list, scene_det_list = None, None
    if combined_db:
        if int(movie_id_gt) == int(movie_id_det) and movie_project_gt == movie_project_det:
            scene_det_list = det
            scene_gt_list = gt_
    elif bp and movie_id_gt == movie_id_det:
        scene_det_list = det
        scene_gt_list = gt_
    elif not bp and int(movie_id_gt) == int(movie_id_det):
        scene_det_list = det
        scene_gt_list = gt_
    return scene_gt_list, scene_det_list


def create_pre_processing_lists():
    # this function combines all of the necessary actions for creating the list for the preprocessing stages.
    frames_gt, frames_det = get_matching_frames(scene_gt_list, scene_det_list)  # set for snr
    frame_key, iou_matrix, iou_matrix_set, rle_matrix_set = create_iou_rle_mats()

    scene_class_group_pre, scene_class_id_pre = generate_scene_class_key(scene_gt_list, scene_det_list)  # set for snr
    scene_class_group = [list(i) for i in scene_class_group_pre]
    scene_class_id = [list(i) for i in scene_class_id_pre]  # set for snr

    argmax_list, argmax_list_lut = create_argmax_list(frames_gt, frames_det, iou_matrix)  # set for snr
    nms_list, nms_list_lut = create_nms_list(frames_gt, frames_det, iou_matrix)
    return frames_gt, frames_det, frame_key, iou_matrix, iou_matrix_set, scene_class_group, scene_class_id, \
           argmax_list, argmax_list_lut, nms_list, nms_list_lut, rle_matrix_set


def process_track_id():
    # this function is responsible of performing the whole track id and false alarm analysis.
    detected, not_detected = get_gt_detection_lists(argmax_list)
    detected_nms, _ = get_gt_detection_lists(nms_list)
    attribution_list = sorted(detected + detected_nms)
    gt_id_by_group, det_id_by_group, atts_det_best, per_class_gts = prepare_id_lists_by_group(detected)
    falses_list = create_falses_list(atts_det_best)
    falses_by_track = group_track_ids(falses_list)
    false_alarm_analysis(falses_by_track)
    if export_logs:
        # if len(falses_by_track) != 0 and len(det_id_by_group) != 0:
        extract_det_details_for_log(gt_id_by_group, det_id_by_group, falses_by_track)
    if len(gt_id_by_group) != 0:
        extract_track_id_details(gt_id_by_group, det_id_by_group, atts_det_best)
        if export_logs:
            extract_track_id_details_for_log(gt_id_by_group, det_id_by_group, atts_det_best)

    return falses_by_track, gt_id_by_group, det_id_by_group, per_class_gts


def initializing_cm_mats():
    # this function creates an empty matrix for each cm value.
    fn_mat = np.zeros((3, len(frames_gt), len(classes)))
    fp_mat = np.zeros((3, len(frames_gt), len(classes)))
    tp_a_mat = np.zeros((3, len(frames_gt), len(classes)))
    tp_b_mat = np.zeros((3, len(frames_gt), len(classes)))
    tp_c_mat = np.zeros((3, len(frames_gt), len(classes)))
    tp_d_mat = np.zeros((3, len(frames_gt), len(classes)))
    tp_e_mat = np.zeros((3, len(frames_gt), len(classes)))
    nms_mat = np.zeros((3, len(frames_gt), len(classes)))
    return fn_mat, fp_mat, tp_a_mat, tp_b_mat, tp_c_mat, tp_d_mat, tp_e_mat, nms_mat


def create_iou_rle_mats():
    # this function generates iou and rle matrices using the gt and det list per scene.
    rle_matrix = create_rle_matrix(scene_gt_list, scene_det_list)
    frame_key, iou_matrix = create_iou_matrix(scene_gt_list, scene_det_list)  # set for snr

    rle_matrix_threat = generate_class_filtered_matrix(scene_gt_list, scene_det_list, 'threat', rle_matrix)
    rle_matrix_non_threat = generate_class_filtered_matrix(scene_gt_list, scene_det_list, 'non-threat',
                                                           rle_matrix)
    iou_matrix_threat = generate_class_filtered_matrix(scene_gt_list, scene_det_list, 'threat', iou_matrix)
    iou_matrix_non_threat = generate_class_filtered_matrix(scene_gt_list, scene_det_list, 'non-threat',
                                                           iou_matrix)

    iou_matrix_set = [iou_matrix, iou_matrix_threat, iou_matrix_non_threat]
    rle_matrix_set = [rle_matrix, rle_matrix_threat, rle_matrix_non_threat]
    return frame_key, iou_matrix, iou_matrix_set, rle_matrix_set


def sets_creation_for_frame(f, single_frame_gt, single_frame_det):
    # this functions creates 3 different sets per scene. 1 = including all of the gts, 2 = only threats gts, 3 =
    # only non-threats gts. it uses the frame number and the gt and det lines and returns the relevant values
    # per each set.
    # important - we are using only set 1 in our analysis!
    all_gt_argmax = [i for i in argmax_list[f]]
    all_gt_argmax_lut = [i for i in argmax_list_lut[f]]
    nms_list_frame = [i for i in nms_list[f]]
    nms_list_lut_frame = [i for i in nms_list_lut[f]]
    argmax_list_group = []
    for c, v in zip(all_gt_argmax_lut, all_gt_argmax):
        argmax_list_group.append(scene_class_group[f][c])
    g, g2, g3, d = [], [], [], []
    # sets creation:
    for (_, (gt, det)) in enumerate(zip(single_frame_gt, single_frame_det)):
        if len(gt.split()) == 2:
            g, g2, g3 = [], [], []
            d.append(det)
        else:
            xc_gt, yc_gt, w_gt, h_gt, class_id_gt = parse_parameters(gt)
            class_str_gt, class_group_gt = parse_labels(class_id_gt)
            g.append(gt)
            if len(det.split()) == 1:
                d = []
            else:
                d.append(det)
            if class_group_gt is 'threat':
                g2.append(gt)
            else:
                g3.append(gt)
    dets, gs = group_similar_values(g, d)
    dets_2, gs_2 = group_similar_values(g2, d)
    dets_3, gs_3 = group_similar_values(g3, d)
    return dets, gs, dets_2, gs_2, dets_3, gs_3, nms_list_frame, nms_list_lut_frame, all_gt_argmax, \
           argmax_list_group


def handling_multiple_det_lists():
    # this function handles the detections directory and returns the content of the sub-directory in it, and
    # loads the content of the pickle file in it - if exists.
    for root, dirs, files in os.walk(comb_det_dir):
        if len(files) == 0:
            det_dict = None
        else:
            for f in files:
               if f.endswith('pickle'):
                   with open((os.path.join(root, f)), 'rb') as p:
                        det_dict = pickle.load(p)
                        break
        dirs_list = []
        for dir in dirs:
            dirs_list.append(os.path.join(root, dir))
        break
    return det_dict, dirs_list


def create_tps_fps_lists(gt_id_by_group, det_id_by_group):
    # this function gets the gt and dets lists grouped by id, and export two lists - falses list and true positives
    # list into a file.
    tps_list = []
    for i in range(len(gt_id_by_group)):
        if len(det_id_by_group[i]) != 0:
            frms = [int(f.split()[0]) for f in det_id_by_group[i]]
            for x, j in enumerate(gt_id_by_group[i]):
                if int(j.split()[1]) in frms:
                    matched_det = [r for r in det_id_by_group[i] if int(j.split()[1]) == int(r.split()[0])]
                    j = j + ' ' + matched_det[0].split()[2]
                    tps_list.append(j)
    movie_id = movie_id_gt
    falses_list = [c for r in falses_list_id for c in r]
    fp_row = [[movie_id, r.split()[0], r.split()[1], r.split()[2], r.split()[10]] for r in falses_list]
    tp_row = [[r.split()[0], r.split()[1], r.split()[2], r.split()[-1], r.split()[-3], r.split()[8]] for r in tps_list]
    wr_tp.writerows(tp_row)
    wr_fp.writerows(fp_row)


""" MAIN """
# extracting info from the directories:
det_dict, dirs_list = handling_multiple_det_lists()
comb_gt_lines = [line.rstrip() for line in open(comb_gt_list, 'r')]

# running in a loop on each detections directory:
for ci, det_lst in enumerate(dirs_list):
    tic = time.time()
    export_path = config.get('dirs', 'export_path')
    comb_det_lines = [os.path.join(os.path.abspath(det_lst), f) for f in os.listdir(det_lst) if f.endswith('.txt')]
    if bp:
        lst_id = os.path.basename(comb_det_lines[0].split('_')[0])
    else:
        lst_id = os.path.basename(os.path.dirname(comb_det_lines[0]))
    export_path = os.path.join(export_path, lst_id + '_res')
    if not os.path.isdir(export_path):
        os.makedirs(export_path)

    if snr:
        plt.ion()
        fig = plt.figure(figsize=(6, 7))
        snr_detected_accum, area_detected_accum, snr_not_detected_accum, area_not_detected_accum = [], [], [], []

    if trackid:
        # open trackid csv:
        res_file_trk = open(os.path.join(export_path, 'track_id_analysis_' + date + '.csv'), "w+")
        wr = csv.writer(res_file_trk)
        wr.writerow(("movie id", "track id", "gt major class", "was detected", "gt total life time", "gt life time",
                     "det life time", "% det rate",
                     "time till detection", "id swaps on det", "max lost time on det", "longest class loss",
                     "longest in-group class loss", "longest out-group class loss", "min area", "max area", "min snr",
                     "max snr", "median conf", "det major class", "frames_num"))

        if export_logs:
            # open trackid_log csv:
            res_file_trk_log = open(os.path.join(export_path, 'track_id_analysis_log_' + date + '.csv'), "w+")
            wr_log = csv.writer(res_file_trk_log)

            # open det_log csv:
            res_file_det_log = open(os.path.join(export_path, 'det_analysis_log_' + date + '.csv'), "w+")
            wr_det_log = csv.writer(res_file_det_log)

        # open faa csv:
        res_file_faa = open(os.path.join(export_path, 'false_alarm_analysis_' + date + '.csv'), "w+")
        wr_faa = csv.writer(res_file_faa)
        wr_faa.writerow(
            ("movie id", "frames num", "track id", "min area", "max area", "det total life time", "det life time",
             "max lost time on det", "major class", "total time as threat", "longest time as threat", "median conf", "min snr", "source"))

        # open tp and fp csv:
        res_file_fp = open(os.path.join(export_path, 'falses_list_' + date + '.csv'), "w+")
        wr_fp = csv.writer(res_file_fp)
        res_file_tp = open(os.path.join(export_path, 'tps_list_' + date + '.csv'), "w+")
        wr_tp = csv.writer(res_file_tp)
        wr_tp.writerow(("movie id", "frame id", "class", "conf", "track id", "snr"))
        wr_fp.writerow(("movie id", "frame id", "class", "conf", "snr"))


    # initialization:
    conf_mat = np.zeros((3, 100000, len(classes)))
    cnt_conf = 0
    falses_list_comb = []
    tps_list_comb = []
    comb_per_class_gts = []

    # running in a loop on every gt list (per scene):
    for c_, gt_ in enumerate(comb_gt_lines):
        # extracting info from gt list:
        lines_gt = [line.rstrip() for line in open(gt_, 'r')]
        if len(lines_gt) == 0:
            continue
        movie_id_gt, movie_project_gt, dual_param_parsed = extracting_movie_id_gt()
        if dual_database and int(dual_param_parsed) not in dual_param:
            continue
        video_path = extracting_video_path()
        if preview and video_path is None:
            continue
        print(movie_id_gt, 'is in process...', '(' + str(c_ + 1) + '/' + str(len(comb_gt_lines)) + ')')

        # running in a loop on every det list (per scene)::
        for det in comb_det_lines:
            # extracting info from det list:
            movie_id_det, movie_project_det = extracting_movie_id_det()
            lines_det = [line.rstrip() for line in open(det, 'r')]
            if len(lines_det) == 0:
                continue
            scene_gt_list, scene_det_list = define_scene_lists()
            if scene_gt_list is None and scene_det_list is None:
                continue
            if mp4:
                fourcc = cv2.VideoWriter_fourcc(*'mp4v')
                out = cv2.VideoWriter(os.path.join(export_path, movie_id_gt + '.mp4'), fourcc, 10, (FRAME_WIDTH, FRAME_HEIGHT))

            """ PRE PROCESS """
            # creating lists and info for preprocess:
            if det_dict is None:
                start_frame = int(lines_det[0].split()[0])
                end_frame = int(lines_det[-1].split()[0])
            else:
                start_frame_det = int(det.split('_')[-2])
                dict_id = [(c, det_dict[c]['sceneName'].split('_')[0]) for (c, i) in enumerate(det_dict) if
                 det_dict[c]['sceneName'].split('_')[0] == movie_id_det and det_dict[c]['startFrame'] == start_frame_det][0][0]
                start_frame = det_dict[dict_id]['startFrame']
                end_frame = det_dict[dict_id]['endFrame']
                steps = float(det_dict[dict_id]['skips'])

            try:
                frames_gt, frames_det, frame_key, iou_matrix, iou_matrix_set, scene_class_group, scene_class_id, \
                argmax_list, argmax_list_lut, nms_list, nms_list_lut, rle_matrix_set = create_pre_processing_lists()
            except:
                continue


            """ TRACK ID """
            if trackid:
                falses_list_id, gt_id_by_group, det_id_by_group , per_class_gts = process_track_id()
                try:
                    create_tps_fps_lists(gt_id_by_group, det_id_by_group)
                except:
                    comb_per_class_gts.append(per_class_gts)

            """ MAT PER CLASS """
            fn_mat, fp_mat, tp_a_mat, tp_b_mat, tp_c_mat, tp_d_mat, tp_e_mat, nms_mat = initializing_cm_mats()

            """ SNR """
            if snr:
                process_snr()
                break

            """ MAIN """
            fp, fn, tp_a, tp_b, tp_c, tp_d, tp_e, nms = [], [], [], [], [], [], [], []

            # running in a loop on every line of detection and line of gt in a frame:
            for (f, (single_frame_gt, single_frame_det)) in enumerate(zip(frames_gt, frames_det)):
                # extracting lists and info for the scene:
                dets, gs, dets_2, gs_2, dets_3, gs_3, nms_list_frame, nms_list_lut_frame, all_gt_argmax, \
                argmax_list_group = sets_creation_for_frame(f, single_frame_gt, single_frame_det)

                # running in a loop on a set in a frame (other sets are currently irrelevant - working on "set 1" only
                # which includes all of the gt's!
                for (s, set) in enumerate([(gs, dets)]):
                # for (s, set) in enumerate([(gs, dets), (gs_2, dets_2), (gs_3, dets_3)]):
                    fn_val, tp_val_a, tp_val_b, tp_val_c, tp_val_d, tp_val_e, nms_val = [], [], [], [], [], [], []
                    # determining the detections number:
                    det_num = len(set[1][0])
                    if len(list(duplicates(argmax_list[f]))) > 0:
                        try:
                            ind = [i for i, e in enumerate(argmax_list[1]) if e == 0]
                        except:
                            pass
                        if s == 0:
                            det_num += 1

                    # running in a loop on a gt and det line in the set:
                    for (c, (i, j)) in enumerate(zip(set[0], set[1])):
                        if len(set[0][0]) == 0 or len(set[1][0]) == 0:
                            cnt = []
                            det_num = 0

                        # if both gt and det lists are empty:
                        elif set[0][0][0] is None and len(set[1][0][0].split()) != 10:
                            cnt = []
                            det_num = 0
                            det_frame_id = j[0]
                            if preview:
                                try:
                                    rec = display_img(movie_id_gt, det_frame_id, scene_gt_list, 'first')
                                except:
                                    continue
                                # cv2.imshow('frame', rec)
                                # wait_key(1)

                        # if gt list is empty:
                        elif set[0][0][0] is None:
                            if c == 0:
                                flag = 'first'
                            else:
                                flag = ''
                            cnt = []
                            for (c2, (k, l)) in enumerate(zip(i, j)):
                                cnt_2 = []
                                if s == 1 and c2 in all_gt_argmax and argmax_list_group[
                                    all_gt_argmax.index(c2)] == 'non-threat':
                                    cnt.append(c2)
                                    cnt_2.append(c2)
                                if s == 2 and c2 in all_gt_argmax and argmax_list_group[
                                    all_gt_argmax.index(c2)] == 'threat':
                                    cnt.append(c2)
                                    cnt_2.append(c2)
                                xc_det, yc_det, w_det, h_det, class_id_det, conf = parse_parameters(l)
                                class_str_det, class_group_det = parse_labels(class_id_det)
                                fp_mat[s][f][class_id_det] += 1
                                det_frame_id = l.split(' ')[0]
                                if preview:
                                    img = display_img(movie_id_gt, det_frame_id, scene_gt_list, flag)
                                    if c == 0 and c2 == 0 + len(cnt):
                                        rec = display_dets_only(l, img, cnt_2)
                                    else:
                                        rec = display_dets_only(l, rec, cnt_2)
                                    # cv2.imshow('frame', rec)
                                    # wait_key(1)
                                    # alpha_blending(rec, img, 0.1)
                                    # wait_key(1)

                        # if det list is empty:
                        elif set[1][0][0] is None:
                            det_num = 0
                            if c == 0:
                                flag = 'first'
                            else:
                                flag = ''
                            cnt, cnt_2 = [], []
                            for (c2, (k, l)) in enumerate(zip(i, j)):
                                fn_val.append(c2)
                                xc_gt, yc_gt, w_gt, h_gt, class_id_gt = parse_parameters(k)
                                class_str_gt, class_group_gt = parse_labels(class_id_gt)
                                fn_mat[s][f][class_id_gt] += 1
                                if s == 1 and class_group_gt == 'non-threat':
                                    continue
                                if s == 2 and class_group_gt == 'threat':
                                    continue
                                det_movie_id = k.split(' ')[0]
                                gt_frame_id = str(int(k.split(' ')[1]))
                                if preview:
                                    img = display_img(det_movie_id, gt_frame_id, scene_gt_list, flag)
                                    if c == 0 and c2 == 0 + len(cnt):
                                        rec = display_gts_only(k, img)
                                    else:
                                        rec = display_gts_only(k, rec)
                                    # cv2.imshow('frame', rec)
                                    # wait_key(1)
                                    # alpha_blending(rec, img, 0.1)
                                    # wait_key(1)

                        # gt and det are not empty:
                        else:
                            d_nms = [cnt for cnt, i in enumerate(iou_matrix[f][:, c]) if i in nms_list_frame]
                            if c in nms_list_lut_frame:
                                nms_val.append(d_nms)
                                # det_num -= len(d_nms)
                            argmax = np.nanargmax(iou_matrix_set[s][f][:, c])
                            argmax_val = np.nanmax(iou_matrix_set[s][f][:, c])
                            if argmax_val == 0:
                                fn_val.append(c)
                            if c == 0:
                                flag = 'first'
                            else:
                                flag = ''
                            cnt = []

                            # running in a loop on each pair of gt and detection:
                            for (c2, (k, l)) in enumerate(zip(i, j)):
                                # conditions on sets 1 and 2 - not relevent - not in use.
                                if s == 1 and c2 in all_gt_argmax and argmax_list_group[
                                    all_gt_argmax.index(c2)] == 'non-threat':
                                    cnt.append(c2)
                                    continue
                                if s == 2 and c2 in all_gt_argmax and argmax_list_group[
                                    all_gt_argmax.index(c2)] == 'threat':
                                    cnt.append(c2)
                                    continue

                                # extracting info for the matching pair:
                                xc_det, yc_det, w_det, h_det, class_id_det, conf = parse_parameters(l)
                                xc_gt, yc_gt, w_gt, h_gt, class_id_gt = parse_parameters(k)

                                # filling the cm values in their matrices accordingly (fps, fns, tps...) this is the
                                # main analysis:
                                if c2 in d_nms and c in nms_list_lut_frame:
                                    if fp_mat[s][f][class_id_det] > 0:
                                        fp_mat[s][f][class_id_det] -= 1
                                    nms_mat[s][f][class_id_det] += 1
                                if argmax_val == 0 and c2 == 0:
                                    fn_mat[s][f][class_id_gt] += 1
                                class_str_det, class_group_det = parse_labels(class_id_det)
                                class_str_gt, class_group_gt = parse_labels(class_id_gt)
                                det_movie_id = k.split(' ')[0]
                                det_frame_id = l.split(' ')[0]
                                if preview:
                                    img = display_img(det_movie_id, det_frame_id, scene_gt_list, flag)
                                key = frame_key.index(int(det_frame_id))
                                rle_val = rle_matrix_set[s][key][:, c][c2]
                                iou_val = iou_matrix_set[s][key][:, c][c2]
                                iou_max = iou_matrix_set[s][key][:, c][argmax]
                                index = get_detection_index(k, l)
                                if c2 == argmax and argmax_val != 0:
                                    append_to_tp_list(index, c, s, f, class_id_gt)
                                    conf_mat[s][cnt_conf][class_id_gt] = conf
                                    cnt_conf += 1
                                    if c2 == len(i) - 1 and c == 0:
                                        fp_mat[s][f][class_id_det] += 1
                                    if fp_mat[s][f][class_id_det] > 0 and c2 <= len(i) - 1:
                                        fp_mat[s][f][class_id_det] -= 1
                                elif c2 != argmax and c == 0:
                                    fp_mat[s][f][class_id_det] += 1
                                elif argmax_val == 0 and c == 0 and c2 == 0:
                                    fp_mat[s][f][class_id_det] += 1
                                if preview:
                                    img_copy = img.copy()
                                    if c == 0 and c2 == 0 + len(cnt):
                                        rec = main_visualization(k, l, img_copy, rle_val, iou_val, c, c2, index, argmax,
                                                                 argmax_val, d_nms)
                                    else:
                                        rec = main_visualization(k, l, rec, rle_val, iou_val, c, c2, index, argmax,
                                                                 argmax_val, d_nms)
                                    # cv2.imshow('frame', rec)
                                    # wait_key(1)
                    if preview:
                        dsti = alpha_blending(rec, img, 0.4)
                        cv2.imshow('frame', dsti)
                        wait_key(1)
                        out.write(dsti)


                    # appending the cm values:
                    fp_val = (det_num - len(cnt)) - (
                            len(tp_val_a) + len(tp_val_b) + len(tp_val_c) + len(tp_val_d) + len(tp_val_e))
                    if fp_val < 1:
                        fp_val = 0
                    fp.append(fp_val)
                    tp_a.append(len(tp_val_a))
                    tp_b.append(len(tp_val_b))
                    tp_c.append(len(tp_val_c))
                    tp_d.append(len(tp_val_d))
                    tp_e.append(len(tp_val_e))
                    fn.append(len(fn_val))
                    nms.append(sum([len(x) for x in nms_val]))

                    if preview:
                        display_cm_on_img()
                        # cv2.imshow('frame', rec)
                        if pause_after_set:
                            wait_key(0)
                        else:
                            wait_key(1)
            if preview:
                out.release()
                cv2.destroyAllWindows()
                if mp4:
                    try:
                        cap.release()
                    except:
                        pass


            """ POST PROCESS """
            # dividing the results per set:
            fp_set1, fp_set2, fp_set3           = devide_by_set(fp, frames_gt)
            tp_a_set1, tp_a_set2, tp_a_set3     = devide_by_set(tp_a, frames_gt)
            tp_b_set1, tp_b_set2, tp_b_set3     = devide_by_set(tp_b, frames_gt)
            tp_c_set1, tp_c_set2, tp_c_set3     = devide_by_set(tp_c, frames_gt)
            tp_d_set1, tp_d_set2, tp_d_set3     = devide_by_set(tp_d, frames_gt)
            tp_e_set1, tp_e_set2, tp_e_set3     = devide_by_set(tp_e, frames_gt)
            fn_set1, fn_set2, fn_set3           = devide_by_set(fn, frames_gt)
            nms_set1, nms_set2, nms_set3        = devide_by_set(nms, frames_gt)

            """ CM VALUES """
            if export_cm_vals:
                export_cm_values_all()
                export_cm_values_per_class()
    if trackid:
        pickle.dump(comb_per_class_gts, open(os.path.join(export_path, 'comb_per_class_gts.pickle'), 'wb'))
    if not system_checking:
        parse_conf_mat()
    if snr:
        plt.close('all')
    try:
        output.close()
    except:
        continue
    output_faa.close()
    output_tp.close()
    output_fp.close()
    toc = time.time()
    print(round((toc - tic) / 60, 2), 'min')


print('Done')
