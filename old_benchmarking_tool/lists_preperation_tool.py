import os
import numpy as np
from datetime import date

###############################
""" FLAGS AND PARAMS """
gt_dir = r'Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual ground\GT_with_properties'
export_path = r'C:\Users\liorm\Documents\ThirdeyeSystems\Benchmarking\benchmarkingtool\outs'
data_base = 'DualGround'

vis = False
raw = True
snr = True

if raw:
    img_width = 640
    img_height = 480
if vis:
    img_width = 720
    img_height = 576
###############################

today = date.today()
date = today.strftime("%d%m%y")

snr_thresh = 1.2
area_threah = 10

isdir = os.path.isdir(export_path)
if not isdir:
    os.mkdir(export_path)

def create_gt_list(gt_dir):
    for root, dirs, files in os.walk(gt_dir):
        for d in dirs:
            title, ext = os.path.splitext(d)
            movie_id = title.split('_')[0]
            print(movie_id, 'is in process')
            # movie_list = [25, 55, 84, 112, 120, 121, 140, 141, 151, 213]
            # if int(movie_id) not in movie_list:
            #     continue
            res_file = open(os.path.join(export_path, title + '.txt'), "w+")
            for f in os.listdir(os.path.join(root, d)):
                title, ext = os.path.splitext(f)
                if snr:
                    frame_id = title.split('_')[-3]
                else:
                    frame_id = title.split('_')[-1]
                file = open((os.path.join(root, d, f)), 'r')
                lines = file.readlines()
                if len(lines) == 0:
                    res_file.write(str(movie_id) + " " + str(frame_id) + '\n')
                    # continue
                else:
                    for line in lines:
                        val = [s for s in line.strip().split(' ')]
                        if snr:
                            res_file.write(str(movie_id) + " " + str(frame_id) + " " + str(val[0])
                                           + " " + str(int(float(val[1]) * img_width))
                                           + " " + str(int(float(val[2]) * img_height))
                                           + " " + str(int(float(val[3]) * img_width))
                                           + " " + str(int(float(val[4]) * img_height))
                                           + " " + str(float(val[5])) + '\n')
                        else:
                            res_file.write(str(movie_id) + " " + str(frame_id) + " " + str(val[0])
                                           + " " + str(int(float(val[1]) * img_width))
                                           + " " + str(int(float(val[2]) * img_height))
                                           + " " + str(int(float(val[3]) * img_width))
                                           + " " + str(int(float(val[4]) * img_height)) + '\n')

            res_file.close()
        break


def create_comb_gt_list(gt_dir):
    for root, dirs, files in os.walk(gt_dir):
        comb_gt_list = open(os.path.join(export_path, 'comb_gt_list_' + data_base + '_' + date + '.txt'), "w")
        for f in files:
            file = open((os.path.join(root, f)), 'r')
            comb_gt_list.write(str(file.name) + '\n')
        break


def create_comb_det_list(det_dir):
    for root, dirs, files in os.walk(det_dir):
        comb_det_list = open(os.path.join(export_path, 'comb_det_list_' + data_base + '_' + date + '.txt'), "w")
        for f in files:
            # if 'ELTTest' not in f:
            #     continue
            file = open((os.path.join(root, f)), 'r')
            comb_det_list.write(str(file.name) + '\n')
        break
        comb_det_list.close()


def combine_two_lists():
    filenames = ['/home/shira/Desktop/comb_det_list_elt.txt', '/home/shira/Desktop/comb_det_list_elt_medusa.txt']
    with open('/home/shira/Desktop/comb_det_list_thermal_ag.txt', 'w') as outfile:
        for f in filenames:
            with open(f, 'r') as infile:
                outfile.write(infile.read())


def create_comb_gt_list_bp(gt_dir_bp):
    bp_dir_list = ['Airplanes', 'Balloons', 'Drones', 'Birds']
    comb_gt_list = open(os.path.join(export_path, 'comb_gt_list_' + data_base + '_' + date + '.txt'), "w")
    for root, dirs, files in os.walk(gt_dir_bp):
        for d in dirs:
            if d not in bp_dir_list:
                continue
            for f in os.listdir(os.path.join(root, d)):
                if not f.endswith('BP.txt') or f.endswith('SNR_BP.txt'):
                    continue
                file = open((os.path.join(root, d, f)), 'r')
                comb_gt_list.write(str(file.name) + '\n')
        break
    comb_gt_list.close()
    return comb_gt_list.name


def create_comb_gt_list_bp_snr(gt_dir_bp):
    bp_dir_list = ['Airplanes', 'Balloons', 'Drones', 'Birds']
    comb_gt_list = open(os.path.join(export_path, 'comb_gt_list_snr_' + data_base + '_' + date + '.txt'), "w")
    for root, dirs, files in os.walk(gt_dir_bp):
        for d in dirs:
            if d not in bp_dir_list:
                continue
            for f in os.listdir(os.path.join(root, d)):
                if not f.endswith('SNR_BP.txt'):
                    continue
                file = open((os.path.join(root, d, f)), 'r')
                comb_gt_list.write(str(file.name) + '\n')
        break
    comb_gt_list.close()
    return comb_gt_list.name


def combine_gt_id_snr(comb_gt_list, comb_gt_list_snr):
    comb_gt_lines = [line.rstrip() for line in open(comb_gt_list, 'r')]
    comb_gt_lines_snr = [line.rstrip() for line in open(comb_gt_list_snr, 'r')]
    comb_gt = []
    res_combined_comb = open(os.path.join(export_path, 'bp_comb_gt_list_combined' + '_' + date + '.txt'), 'w')
    # for gt_, gt_snr in zip(comb_gt_lines, comb_gt_lines_snr):
    for gt_ in comb_gt_lines:
        for gt_snr in comb_gt_lines_snr:
            if not gt_.split('.txt')[0].split(os.sep)[-1] == gt_snr.split('.txt')[0].split(os.sep)[-1].replace("_SNR", ""):
                continue
            res_combined = open(os.path.join(export_path, os.path.splitext(os.path.basename(gt_))[0] + '_combined' + '.txt'), 'w')
            lines_gt = [line.rstrip() for line in open(gt_, 'r')]
            try:
                lines_gt_snr = [line.rstrip() for line in open(gt_snr, 'r')]
            except FileNotFoundError:
                continue
            if len(lines_gt) == 0 or len(lines_gt_snr) == 0:
                continue
            for c, (l, s) in enumerate(zip(lines_gt, lines_gt_snr)):
                if len(l.split()) > 2:
                    lines_gt[c] += ' ' + s.split()[-1]
            wr = [res_combined.write(wr + '\n') for wr in lines_gt]
            comb_gt.append(lines_gt)
            res_combined_comb.write(res_combined.name + '\n')
            break
    return res_combined_comb.name


def pre_process_snr_thresh(comb_gt_list, comb_gt_list_snr):
    res_combined_comb = combine_gt_id_snr(comb_gt_list, comb_gt_list_snr)
    res_combined_comb_lines = [line.rstrip() for line in open(res_combined_comb, 'r')]
    for m in res_combined_comb_lines:
        m_lines = [line.rstrip() for line in open(m, 'r')]
        for c, f in enumerate(m_lines):
            if len(f.split()) > 2:
                w = int(f.split()[-4])
                h = int(f.split()[-3])
                area = w * h
                if np.absolute(float(f.split()[-1])) < snr_thresh or area < 10:
                    m_lines[c] = f.split()
                    m_lines[c][2] = '-' + m_lines[c][2]
                    m_lines[c][-2] = '-' + m_lines[c][-2]
                    m_lines[c] = ' '.join(m_lines[c])
        res_combined = open(os.path.join(export_path, os.path.splitext(os.path.basename(m))[0] + '.txt'), 'w')
        wr = [res_combined.write(wr + '\n') for wr in m_lines]
    return res_combined_comb


def create_comb_gt_list_bp_combined():
    comb_gt_list = create_comb_gt_list_bp(gt_dir_bp)
    comb_gt_list_snr = create_comb_gt_list_bp_snr(gt_dir_bp)
    pre_process_snr_thresh(comb_gt_list, comb_gt_list_snr)


def pre_process_cam_motion(comb_gt_list, cam_motion_det):
    steps = 4
    window_size = 10
    res_combined_comb_lines = [line.rstrip() for line in open(comb_gt_list, 'r')]
    for m in res_combined_comb_lines:
        m_lines = [line.rstrip() for line in open(m, 'r')]
        scene_id_gt = m_lines[0].split()[0]
        for det in os.listdir(cam_motion_det):
            scene_id_det = '_'.join(det.split('_motion')[0].split('_')[1:])
            if scene_id_det != scene_id_gt:
                continue
            res_cam_motion_det_lines = [line.rstrip() for line in open(os.path.join(cam_motion_det, det), 'r')]
            det_frames_list = [int(r.split()[0]) for r in res_cam_motion_det_lines]
            new_gt = []
            for c, f in enumerate(m_lines):
                if np.mod(c, steps) != 0:
                    new_gt.append(m_lines[c])
                else:
                    elements = f.split()
                    frame_id_gt = int(elements[1])
                    if frame_id_gt not in det_frames_list:
                        new_gt.append(m_lines[c])
                    for c2, d in enumerate(res_cam_motion_det_lines):
                        elements_det = d.split()
                        frame_id_det = int(elements_det[0])
                        if frame_id_det == frame_id_gt:
                            if c2 - window_size < 0:
                                new_gt.append(f)
                            else:
                                window = [int(r.split()[1]) for r in res_cam_motion_det_lines[c2 - window_size + 1:c2 + 1]]
                                if 1 in window:
                                    if len(elements) > 2:
                                        elements_line = m_lines[c]
                                        elements_line = elements
                                        if int(elements[2]) > 0:
                                            elements_line[2] = '-' + str(int(elements_line[2]) * 1)
                                            elements_line[-2] = '-' + str(int(elements_line[-2]) * 1)
                                        else:
                                            elements_line[2] = str(int(elements_line[2]) * 1)
                                            elements_line[-2] = str(int(elements_line[-2]) * 1)
                                        elements_line = ' '.join(elements_line)
                                        new_gt.append(elements_line)
                                    else:
                                        new_gt.append(f)
                                else:
                                    new_gt.append(f)
            with open(os.path.join(export_path, os.path.splitext(os.path.basename(m))[0] + '_motion.txt'), 'w') as res_combined:
                wr = [res_combined.write(wr + '\n') for wr in new_gt]


""" MAIN """
create_comb_gt_list(gt_dir)


print("Done")



