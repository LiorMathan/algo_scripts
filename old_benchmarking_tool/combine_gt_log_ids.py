import csv
import numpy as np
import itertools


path = '/mnt/share/Local/Benchmarking/BP/bp_allsampled_cam_motion_130920/BlackPanther-s30-c5-7_res/track_id_analysis_log_170920.csv'
with open(path) as fp:
    reader = csv.reader(fp, delimiter=",", quotechar='"')
    # next(reader, None)  # skip the headers
    data_read = [row for row in reader]

groups_movies = []
for k, g in itertools.groupby(data_read, lambda x: x[0]):
   groups_movies.append(list(g))

groups_all = []
for mov in groups_movies:
    group_ids = []
    mov.sort(key=lambda y: int(y[1]))
    for k, g in itertools.groupby(mov, lambda x: x[1]):
        group_ids.append(list(g))
    groups_all.append(group_ids)

with open('/home/idan-3rdeye/Desktop/track_id_analysis_log_210920_sorted_50.csv', "w+") as res_file:
    wr_file = csv.writer(res_file)
    for group in groups_all:
        if 'Balloon' in group[0][0][0]:  # remove balloon movies
            continue
        for m in group:
            if len(m) < 2:
                list = m[0][:3] + [' '.join(r.split()[1:]) for r in m[0][3:]]
                wr_file.writerow(list)
            else:
                comb = m[0] + [r + ' -999' for r in m[1][3:]]  # mark objects which dont follow snr term with '-999'
                # comb = m[0] + m[1][3:]
                comb = [r for r in comb if r != '']
                sorted_list = sorted(comb[3:], key=lambda x: int(x.split()[0]))
                # comb = comb[:3] + [' '.join(r.split()[1:]) for r in sorted_list]
                comb = comb[:3] + [' '.join(r.split()) for r in sorted_list]
                # wr_file.writerow(comb)

                if comb[0] == 'C0013':
                    comb = comb[:3] + comb[191:]
                separated_lines_snr, new_list, new_list_2 = [], [], []
                for c, line in enumerate(comb[3:]):
                    if '-999' not in line:
                        if new_list:
                            separated_lines_snr.append(new_list)
                            new_list = []
                        new_list_2.append(line)
                    else:
                        if new_list_2:
                            separated_lines_snr.append(new_list_2)
                            new_list_2 = []
                        new_list.append(line[:-5])

                thresh = 50
                new_separated_lines_snr, temp_list = [], []
                for k in separated_lines_snr:
                    temp_list.append(k)
                    if len([r for t in temp_list for r in t]) < thresh:
                        continue
                    else:
                        new_separated_lines_snr.append([r for t in temp_list for r in t])
                        temp_list = []
                items = [r for t in temp_list for r in t]
                if len(items) < thresh:
                    try:
                        new_separated_lines_snr[-1] = (new_separated_lines_snr[-1] + items)
                    except:
                        new_separated_lines_snr.append(items)
                else:
                    new_separated_lines_snr.append(items)

                for l in new_separated_lines_snr:
                    first_frame = l[0].split()[0]
                    comb = comb[:2] + [first_frame] + [' '.join(r.split()[1:]) for r in l]
                    wr_file.writerow(comb)


pass