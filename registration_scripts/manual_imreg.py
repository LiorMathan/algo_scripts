from tkinter import *
from tkinter import messagebox
from Reader import *
import matplotlib.pyplot as plt
import os
import numpy as np
import cv2

ERROR = -1
SUCCESS = 1
single_transformation = False

class ManualRegister:
    def __init__(self):
        self.root = Tk()
        self.root.config(background='#2E3C56')
        self.root.title('Manual Image Registration')
        self.width = 700
        self.height = 400
        self.root.geometry(str(self.width)+"x"+str(self.height))
        self.reader = None
        self.path1_label = Label(master=self.root, text="Please Enter Thermal Movie Path / Path to Images Folder:")
        self.path1_label.config(background='#2E3C56', fg='#C1FFA4')
        self.path1_label.place(x=10, y=10)
        sv1 = StringVar()
        sv1.trace("w", lambda name, index, mode, sv=sv1: self.on_changed_entry(path=1))
        self.path1_entry = Entry(master=self.root, width=100, textvariable=sv1)
        self.path1_entry.place(x=10, y=30)
        self.path2_label = Label(master=self.root, text="Please Enter VIS Movie Path / Path to Images Folder:")
        self.path2_label.config(background='#2E3C56', fg='#C1FFA4')
        self.path2_label.place(x=10, y=120)
        sv2 = StringVar()
        sv2.trace("w", lambda name, index, mode, sv=sv2: self.on_changed_entry(path=2))
        self.path2_entry = Entry(master=self.root, width=100, textvariable=sv2)
        self.path2_entry.place(x=10, y=140)
        self.resolution1_label = Label(master=self.root, text="Resolution:")
        self.resolution2_label = Label(master=self.root, text="Resolution:")
        self.resolution1_label.config(background='#2E3C56', fg='#C1FFA4')
        self.resolution2_label.config(background='#2E3C56', fg='#C1FFA4')
        self.resolution1 = Listbox(master=self.root, height=1)
        self.resolution2 = Listbox(master=self.root, height=1)
        self.update_resolutions(index=1)
        self.update_resolutions(index=2)
        self.output_label = Label(master=self.root, text="Output Path:")
        self.output_label.config(background='#2E3C56', fg='#C1FFA4')
        self.output_label.place(x=10, y=230)
        self.output_entry = Entry(master=self.root, width=100)
        self.output_entry.place(x=10, y=250)

        self.view_button = Button(master=self.root, text="Start", command=self.register, padx=70, bd=4)
        self.view_button.config(background='#C1FFA4', fg='#2E3C56')
        self.view_button.place(x=250, y=350)

        self.output = None
        self.index = 0
        self.transformation = None
        self.inverse_transformation = None

        self.root.mainloop()
    

    def update_resolutions(self, index):
        if (index == 1):
            self.resolution1.insert(1, "default")
            self.resolution1.insert(2, "640, 480")
            self.resolution1.insert(3, "720, 576")
            self.resolution1.insert(4, "1024, 768")
            self.resolution1.select_set(0)
        else:
            self.resolution2.insert(1, "default")
            self.resolution2.insert(2, "640, 480")
            self.resolution2.insert(3, "720, 576")
            self.resolution2.insert(4, "1024, 768")
            self.resolution2.select_set(0)


    def on_changed_entry(self, path):
        if (path == 1):
            if (self.path1_entry.get().endswith(".rawc") or self.path1_entry.get().endswith(".rgb")):
                self.resolution1_label.place(x=10, y=55)
                self.resolution1.place(x=10, y=75)
            else:
                self.resolution1_label.place_forget()
                self.resolution1.place_forget()
        else:
            if (self.path2_entry.get().endswith(".rawc") or self.path2_entry.get().endswith(".rgb")):
                self.resolution2_label.place(x=10, y=175)
                self.resolution2.place(x=10, y=195)
            else:
                self.resolution2_label.place_forget()
                self.resolution2.place_forget()


    def get_resolution(self):
        width1 = None
        width2 = None
        height1 = None
        height2 = None
        if (self.resolution1.get(ACTIVE) == "640, 480"):
            width1 = 640
            height1 = 480
        elif (self.resolution1.get(ACTIVE) == "720, 576"):
            width1 = 720
            height1 = 576
        elif (self.resolution1.get(ACTIVE) == "1024, 768"):
            width1 = 1024
            height1 = 768
        if (self.resolution2.get(ACTIVE) == "640, 480"):
            width2 = 640
            height2 = 480
        elif (self.resolution2.get(ACTIVE) == "720, 576"):
            width2 = 720
            height2 = 576   
        elif (self.resolution2.get(ACTIVE) == "1024, 768"):
            width2 = 1024
            height2 = 768
         
        return width1, height1, width2, height2


    def get_paths(self):
        path1 = self.path1_entry.get()
        path2 = self.path2_entry.get()
        is_dual = False

        if len(path1) > 0 and len(path2) > 0:
            is_dual = True

        if not os.path.exists(path1):
            messagebox.showerror(title="Invalid input", message="Path to VIS movie does not exsists!")
            return ERROR, ERROR, ERROR
        if is_dual is True:
            if not os.path.exists(path2):
                messagebox.showerror(title="Invalid input", message="Path to Thermal movie does not exsists!")
                return ERROR, ERROR, ERROR

        return path1, path2, is_dual


    def update_reader(self):
        path1, path2, is_dual = self.get_paths()
        if (path1 == ERROR):
            return ERROR
        width1, height1, width2, height2 = self.get_resolution()

        self.reader = Reader(path1=path1, path2=path2, width1=width1, height1=height1, width2=width2, height2=height2, dual=is_dual)
        return SUCCESS


    def get_transformation(self, src_frame, dst_frame, src_points, dst_points):
        M = cv2.getAffineTransform(src=np.float32(src_points), dst=np.float32(dst_points))
        M_inverse = cv2.invertAffineTransform(M)
        transform_M = np.array(M)
        transform_M_inverse = np.array(M_inverse)

        src_warped = cv2.warpAffine(src_frame, M, (src_frame.shape[1], src_frame.shape[0]))
        dst_warped = cv2.warpAffine(dst_frame, M_inverse, (dst_frame.shape[1], dst_frame.shape[0]))

        found_src_points = []
        found_dst_points = []
        for i in range(3):
            found_src_points.append(np.array(M_inverse).dot(np.array([dst_points[i][0], dst_points[i][1], 1])))
            dst_warped = cv2.circle(img=dst_warped, center=(int(found_src_points[i][0]), int(found_src_points[i][1])), radius=5, color=(255,0,0), thickness=-1)
            dst_warped = cv2.circle(img=dst_warped, center=(int(src_points[i][0]), int(src_points[i][1])), radius=3, color=(0,255,0), thickness=-1)
            found_dst_points.append(np.array(M).dot(np.array([src_points[i][0], src_points[i][1], 1])))
            src_warped = cv2.circle(img=src_warped, center=(int(found_dst_points[i][0]), int(found_dst_points[i][1])), radius=5, color=(255,0,0), thickness=-1)
            src_warped = cv2.circle(img=src_warped, center=(int(dst_points[i][0]), int(dst_points[i][1])), radius=3, color=(0,255,0), thickness=-1)
            
        plt.ion()
        plt.subplot(2,2,1)
        plt.title('Origin thermal image')
        plt.imshow(src_frame, vmin=0, vmax=255)

        plt.subplot(2,2,4)
        plt.title('Transformed thermal image')
        plt.imshow(src_warped, vmin=0, vmax=255)
        
        plt.subplot(2,2,2)
        plt.title('Origin vis image')
        plt.imshow(dst_frame, vmin=0, vmax=255)

        plt.subplot(2,2,3)
        plt.title('Transformed vis image')
        plt.imshow(dst_warped, vmin=0, vmax=255)

        # plt.pause(0.001)
        plt.show()

        return transform_M, transform_M_inverse
        

    def process_points(self, src_frame, dst_frame, src_points, dst_points):
        result_transformation, inverse_transformation = self.get_transformation(src_frame, dst_frame, src_points, dst_points)

        messagebox_done_transform = messagebox.askquestion('Affine Transformation', 'Are you satisfied with the result?', icon='question')
        if messagebox_done_transform == 'yes':
            self.transformation = result_transformation
            self.inverse_transformation = inverse_transformation
            # cv2.destroyAllWindows()
            for i in range(self.index):
                output_file = os.path.join(self.output, os.path.basename(self.reader.input_path).split('.')[0] + "_" + str(i).zfill(6)) + ".txt"
                if not os.path.exists(output_file):
                    with open(output_file, "w") as f:
                        f.write("Source: " + self.reader.input_path + " \nDestination: " + self.reader.second_input_path + " \n")
                        for row in range(2):
                            for col in range(3):
                                f.write(str(self.transformation[row][col]) + " ")
                            f.write('\n')
                        f.write('\n')
                        for row in range(2):
                            for col in range(3):
                                f.write(str(self.inverse_transformation[row][col]) + " ")
                            f.write('\n')
        else:
            cv2.destroyAllWindows()


    def complete_missing_transformations(self):
        if self.transformation is None:
            return
        for i in range(self.reader.num_frames1):
                output_file = os.path.join(self.output, os.path.basename(self.reader.input_path).split('.')[0] + "_" + str(i).zfill(6)) + ".txt"
                if not os.path.exists(output_file):
                    with open(output_file, "w") as f:
                        f.write("Source: " + self.reader.input_path + " \nDestination: " + self.reader.second_input_path + " \n")
                        for row in range(2):
                            for col in range(3):
                                f.write(str(self.transformation[row][col]) + " ")
                            f.write('\n')
                        f.write('\n')
                        for row in range(2):
                            for col in range(3):
                                f.write(str(self.inverse_transformation[row][col]) + " ")
                            f.write('\n')


    def create_output_folder(self):
        try:
            self.output = os.path.join(self.output, os.path.basename(self.reader.input_path).split('.')[0])
            os.mkdir(self.output)
        except:
            pass


    def select_points(self, thermal_frame, vis_frame):
        thermal_points = []
        vis_points = []
        thermal_display = thermal_frame.copy()
        vis_display = vis_frame.copy()

        for point in range(3):
            r_thermal = cv2.selectROI(windowName="Select Thermal Points", img=thermal_display)
            r_vis = cv2.selectROI(windowName="Select Vis Points", img=vis_display)
            thermal_points.append([int(r_thermal[0]+r_thermal[2]/2), int(r_thermal[1]+r_thermal[3]/2)]) # append selected (x, y) to thermal points list 
            vis_points.append([int(r_vis[0]+r_vis[2]/2), int(r_vis[1]+r_vis[3]/2)]) # append selected (x, y) to vis points list
            thermal_display = cv2.circle(img=thermal_display, center=(int(r_thermal[0]+r_thermal[2]/2), int(r_thermal[1]+r_thermal[3]/2)), radius=5, color=(0,0,255), thickness=-1)
            vis_display = cv2.circle(img=vis_display, center=(int(r_vis[0]+r_vis[2]/2), int(r_vis[1]+r_vis[3]/2)), radius=5, color=(0,0,255), thickness=-1)

        return (thermal_points, vis_points)


    def extract_affine_matrix(self, filename, inverse):
        with open(filename) as transformation_file:
            if inverse:
                for i in range(5):
                    transformation_file.readline()
            else:
                transformation_file.readline()
                transformation_file.readline()
            matrix_txt = transformation_file.readlines()
            affine_mat = []
            for j in range(2):
                affine_mat.append([float(item) for item in matrix_txt[j].split()])
            # print(str(affine_mat))
        return affine_mat


    def update_transformation_from_file(self):
        transformation_file = os.path.join(self.output, os.path.basename(self.reader.input_path).split('.')[0] + "_" + str(self.index).zfill(6)) + ".txt"
        if os.path.exists(transformation_file):
            self.transformation = np.array(self.extract_affine_matrix(filename=transformation_file, inverse=False))
            self.inverse_transformation = np.array(self.extract_affine_matrix(filename=transformation_file, inverse=True))


    def init_register(self):
        self.index = 0
        self.transformation = None
        self.update_reader()
        self.output = self.output_entry.get()
        self.create_output_folder()
        self.update_transformation_from_file()


    def display_weighted(self, buttom, top):
        buttom_copy = buttom.copy()
        top_copy = top.copy()
        top_copy = cv2.warpAffine(top_copy, self.inverse_transformation, (top_copy.shape[1], top_copy.shape[0]))
        top_copy = top_copy[:buttom.shape[0], :buttom.shape[1], :]
        alpha = 0.4
        cv2.addWeighted(top_copy, alpha, buttom_copy, 1 - alpha, 0, buttom_copy)

        cv2.imshow("Blended", buttom_copy)


    def register(self):
        self.init_register()
        # for i in range(100, min(400, self.reader.num_frames1)):
        for i in range(self.reader.num_frames1):
            self.index = i
            select_coord = False
            thermal_frame = self.reader.get_frame(index=i, bgr=True)
            vis_frame = self.reader.get_frame(index=i, second_movie=True, bgr=True)
            
            if self.transformation is not None:
                # if i % 10 == 0 or i % 11 == 0 or i % 12 == 0:
                self.display_weighted(thermal_frame, vis_frame)
            # display frames until space is pressed:
            cv2.imshow("Thermal", thermal_frame)
            cv2.imshow("Vis", vis_frame)

            key = cv2.waitKey(200)
            if key == 32: # if space in pressed, pause video
                # cv2.destroyAllWindows()
                select_coord = True
            if key == 27: # if ESC is pressed, exit loop
                # cv2.destroyAllWindows()
                # exit()
                cv2.destroyWindow("Blended")
                cv2.destroyWindow("Thermal")
                cv2.destroyWindow("Vis")
                return 
            
            done = False
            while select_coord and not done:
                (thermal_points, vis_points) = self.select_points(thermal_frame=thermal_frame, vis_frame=vis_frame)
                
                messagebox_is_done = messagebox.askquestion('Selected Coordinates', 'Use the selected coordinates?', icon='question')
                if messagebox_is_done == 'yes':
                    cv2.destroyAllWindows()
                    done = True
                    self.process_points(src_frame=thermal_frame, dst_frame=vis_frame, src_points=thermal_points, dst_points=vis_points)
                else:
                    cv2.destroyAllWindows()

            if single_transformation is True and done is True:
                break
            
        # self.complete_missing_transformations()
        cv2.destroyAllWindows()

    
        
if __name__ == '__main__':
    manual_register = ManualRegister()

# Y:\\ThirdEye Dropbox\\Production\\Database\\Movies\\Dual\\Dual Air\\Affine_Transformation_Matrix
