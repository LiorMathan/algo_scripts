import cv2
import numpy as np
import os
import time
from Reader import *
from Target import *
from datetime import datetime
from create_dual_gt import extract_affine_matrix, create_dir

basedir            = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Panorama/Bools/Movies"
matrix_basedir     = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Affine_Transformation_Matrix"
project_name       = "DualAir"
label_set          = "AG"
cmos_extensions    = [".rawc", ".rgb"]
thermal_extensions = [".raw2"]
create_cmos        = True 
create_thermal     = True 
debug              = True 

## main configuration - AG:
output_basedir     = "/mnt/share/Local/SharingIsCaring/amram/test/create_warped_crops"
cmos_crops_dirs    = ["/mnt/share/Local/SharingIsCaring/amram/test/create_warped_crops/CMOS/High_Altitude_PanoramaBools"]
thermal_crops_dirs = ["/mnt/share/Local/SharingIsCaring/amram/test/create_warped_crops/Thermal/High_Altitude_PanoramaBools"]


def get_crop_summary_lines(crop_dir_path, basename, file_suffix):
    try:
        input_summary_file = open(crop_dir_path.replace(basename, "{}_{}".format(basename, file_suffix)), "r")
        lines = [line.split(' ') for line in input_summary_file.readlines() if len(line) >= 2]
        input_summary_file.close()
    except:
        try:
            input_summary_file = open(crop_dir_path.replace(basename, "cropsMovie_{}_{}".format(basename, file_suffix)), "r")
            lines = [line.split(' ') for line in input_summary_file.readlines() if len(line) >= 2]
            input_summary_file.close()
        except:
            print("{} file doesn't exist!".format(file_suffix))
            return None
    
    return lines


def extract_crops_data(crop_data_dictionary, crop_summary_lines):
    for line in crop_summary_lines:
        frame_index = int(line[0])
        left = int(line[2])
        top = int(line[3])
        width = int(line[4])
        height = int(line[5])

        if frame_index not in crop_data_dictionary:
            crop_data_dictionary[frame_index] = []
        
        crop_data_dictionary[frame_index].append(([left, top, width, height], []))


def extract_annotations_data(crop_data_dictionary, annotation_summary_lines):
    for line in annotation_summary_lines:
        frame_index = int(line[0])
        crop_index = int(line[1]) # range starts from 1, not 0.
        annotation = line[2:]

        crop_data_dictionary[frame_index][crop_index-1][1].append(annotation)


''' 
extracts data from two summary files that were created earlier, during the cropping process:
1. 'crop summary' - ends in 'cropSummary.txt', each line in this file describes a crop with the following format: "<index of original frame> <index of crop> <left border> <top border> <width> <height> "
2. 'GT summary' - ends in 'annotationSummary.txt', each line in this file describes an annotation with the following format: "<index of original frame> <index of crop> <label> <center x-coordinate> <center y-coordinate> <width> <height> "
returns a dictionary: 
key- frame_index.
value- list of tuples: (
    crop - [left_border, top_border, width, height]
    annotation_list - [[label, cx, cy, width, height], ...]
    )
'''
def get_crop_data_dictionary(crop_summary_lines, annotation_summary_lines):
    crop_data_dictionary = {}
    extract_crops_data(crop_data_dictionary, crop_summary_lines)
    extract_annotations_data(crop_data_dictionary, annotation_summary_lines)
    
    return crop_data_dictionary


def is_validate_crop(output_filename):
    check_not_none = cv2.imread(output_filename, cv2.IMREAD_UNCHANGED)
    
    return check_not_none is not None


def is_annotation_in_crop(rectangle_borders, crop):
    left = rectangle_borders[0][0]
    top = rectangle_borders[0][1]
    right = rectangle_borders[1][0]
    bottom = rectangle_borders[1][1]
    rectangle = crop[top:bottom, left:right]
    rectangle = cv2.cvtColor(rectangle, cv2.COLOR_BGR2GRAY)
    
    return cv2.countNonZero(rectangle) > 0 # i.e., rectangle is not black


def is_validate_annotation(annotation, crop):
    target = Target.init_from_yolo_line(' '.join(annotation), crop.shape[1], crop.shape[0], label_set)
    
    return is_annotation_in_crop(rectangle_borders=target.get_as_rectangle_start_end(), crop=crop)


def write_warped_crop_annotations(output_filename, annotation_list):
    gt_output = open(output_filename.replace(".png", ".txt"), "w")
    for annotation in annotation_list:
        gt_output.write(' '.join(annotation))
    gt_output.close()


def debug_warped_crops(cmos_to_thermal, output_filename, src_frame, dst_frame, warped_frame, src_crop, dst_crop):
    output_crop = cv2.imread(output_filename, cv2.IMREAD_UNCHANGED)
    with open(output_filename.replace('.png', '.txt'), "r") as f:
        for line in f.readlines():
            target = Target.init_from_yolo_line(line, output_crop.shape[1], output_crop.shape[0], label_set)
            target.draw(output_crop)
            target.draw(src_crop)
    
    cv2.imshow("source frame", src_frame)
    cv2.imshow("destination frame", dst_frame)
    cv2.imshow("warped destination frame", warped_frame)
    cv2.imshow("source crop", src_crop)
    cv2.imshow("destination crop", dst_crop)
    cv2.imshow("destination crop written", output_crop)
    cv2.waitKey(0)


def write_warped_crop(output_filename, crop, annotation_list, crop_print_message):
    cv2.imwrite(output_filename, crop, [cv2.IMWRITE_PNG_COMPRESSION, 0])
        
    if not is_validate_crop(output_filename):
        print("bad crop:", output_filename)
        return False

    for annotation in annotation_list.copy():
        if not is_validate_annotation(annotation=annotation, crop=crop):
            annotation_list.remove(annotation)
    if len(annotation_list) == 0:
        return False
    
    try:
        write_warped_crop_annotations(output_filename=output_filename, annotation_list=annotation_list)
    except:
        print("exception occurred while writing annotations! " + crop_print_message)
        return False
    
    return True


def create_crops_for_frame(crop_list, frame_index, src_frame, dst_frame, warped_frame, output_folder, moviename, cmos_to_thermal):
    for crop_index in range(1, len(crop_list)+1):
        crop_tuple = crop_list[crop_index-1]
        tile = crop_tuple[0]
        annotation_list = crop_tuple[1]

        src_crop = src_frame[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2]].copy()
        dst_crop_warped = warped_frame[tile[1]:tile[1] + tile[3], tile[0]:tile[0] + tile[2]].copy()

        crop_name = str(frame_index).zfill(6) + "_" + str(crop_index).zfill(3)
        output_filename = os.path.join(output_folder, os.path.basename(output_folder) + "_" + crop_name) + ".png"
        
        crop_print_message = "frame #{}, crop #{}, movie = {}".format(frame_index, crop_index, moviename)

        success = write_warped_crop(output_filename=output_filename, crop=dst_crop_warped, annotation_list=annotation_list, crop_print_message=crop_print_message)
        if not success:
            os.remove(output_filename)
            continue

        if debug:
            print(crop_print_message)
            debug_warped_crops(cmos_to_thermal, output_filename, src_frame, dst_frame, warped_frame, src_crop, dst_crop_warped)


''' 
creates warped crops for a specific movie in a single direction (VIS->IR / IR->VIS).
src_reader (Reader):                the source raw movie's reader
dst_reader (Reader):                the destination raw movie's reader
crop_dir_path (str):                a path for the source movie's dual crops directory
output_folder (str):                a path for the movie's output warped crops directory
tform (np.array)                    the movie's registration matrix used for this specific direction
cmos_to_thermal (bool):             True if the desired direction is VIS->IR, otherwise (IR->VIS) False.
'''
def create_dual_crops_for_movie_single_direction(src_reader, dst_reader, crop_dir_path, output_folder, tform, cmos_to_thermal):
    create_dir(output_folder)
    moviename = os.path.basename(crop_dir_path)
    
    crop_summary_lines = get_crop_summary_lines(crop_dir_path=crop_dir_path, basename=moviename, file_suffix="cropSummary.txt")
    annotation_summary_lines = get_crop_summary_lines(crop_dir_path=crop_dir_path, basename=moviename, file_suffix="annotationSummary.txt")
    for lines in [crop_summary_lines, annotation_summary_lines]:
        if lines is None:
            # TODO create crops/tiles based on gt files, not on summary files
            return
    
    crop_data_dictionary = get_crop_data_dictionary(crop_summary_lines, annotation_summary_lines)
        
    for frame_index in crop_data_dictionary.keys():
        src_frame = src_reader.get_frame(index=frame_index, bgr=not cmos_to_thermal, thermal_endian=cmos_to_thermal)
        dst_frame = dst_reader.get_frame(index=frame_index, bgr=cmos_to_thermal, thermal_endian=not cmos_to_thermal)
        
        if src_frame is None or dst_frame is None:
            continue

        warped_frame = cv2.warpAffine(dst_frame, tform, (src_frame.shape[1], src_frame.shape[0]))
        create_crops_for_frame(crop_list=crop_data_dictionary[frame_index], frame_index=frame_index, src_frame=src_frame, dst_frame=dst_frame, warped_frame=warped_frame, output_folder=output_folder, moviename=moviename, cmos_to_thermal=cmos_to_thermal)
        

''' 
returns the resolution of both CMOS and thermal frames of a dual movie. 
'''
def get_frame_resolutions(cmos_movie, is_panorama):
    cmos_width, cmos_height = 720, 576 
    thermal_width, thermal_height = 640, 480

    if not is_panorama:
        if '640-480' in cmos_movie:        
            cmos_width, cmos_height = 640, 480
        elif '1024-768' in cmos_movie:
            cmos_width, cmos_height = 1024, 768
        elif 'Dual ground' in cmos_movie and int(os.path.basename(cmos_width).split['_'][0]) <= 70:
            cmos_width, cmos_height = 640, 480

    return cmos_width, cmos_height, thermal_width, thermal_height


''' 
creates warped crops for a specific movie.
output_basedir (str):       a path used as a base for creating the movie's output warped crops directories
output_type (str):          e.g., 'High_Altitude', 'Low_Altitude', 'High_Altitude_PanoramaBools' etc...
cmos_movie (str):           a path to a specific VIS movie's dual crops directory
thermal_movie (str):        a path to a specific IR movie's dual crops directory
is_panorama (bool):         True if the movie is defined as 'panorama-bools', otherwise False.
'''
def create_dual_crops_for_movie(output_basedir, output_type, cmos_crop_dir, thermal_crop_dir, is_panorama):
    try:
        matrix_file = os.path.join(matrix_basedir, ("{0}" + os.sep + "{0}_000000.txt").format(os.path.basename(thermal_crop_dir).replace(project_name + "_", '').replace('.raw2', '')))
        tform = extract_affine_matrix(filename=matrix_file, inverse=False)
        inverse_tform = extract_affine_matrix(filename=matrix_file, inverse=True)
    except:
        print("registration-matrix file doesn't exist!")
        return

    thermal_id = os.path.basename(thermal_crop_dir).split('_')[1]
    cmos_id = os.path.basename(cmos_crop_dir).split('_')[1]

    substring = "Panorama" if "Panorama" in output_type else output_type
    cmos_movies = extract_movies(basedir, cmos_extensions, substring=substring)
    thermal_movies = extract_movies(basedir, thermal_extensions, substring=substring)

    cmos_movie, thermal_movie = None, None
    for movie in cmos_movies:
        if cmos_id in movie:
            cmos_movie = movie
            break
    for movie in thermal_movies:
        if thermal_id in movie:
            thermal_movie = movie
            break

    if cmos_movie is None or thermal_movie is None:
        return

    cmos_width, cmos_height, thermal_width, thermal_height = get_frame_resolutions(cmos_movie, is_panorama)

    cmos_reader = Reader(path1=cmos_movie, width1=cmos_width, height1=cmos_height)
    thermal_reader = Reader(path1=thermal_movie, width1=thermal_width, height1=thermal_height)

    if create_thermal:
        direction = "Thermal_for_CMOS"
        print("creating " + direction + "...")
        create_dual_crops_for_movie_single_direction(
            src_reader=cmos_reader, 
            dst_reader=thermal_reader, 
            crop_dir_path=cmos_crop_dir, 
            output_folder=os.path.join(output_basedir, direction, output_type, os.path.basename(thermal_crop_dir)), 
            tform=tform, 
            cmos_to_thermal=False)

    if create_cmos:
        direction = "CMOS_for_Thermal"
        print("creating " + direction + "...")
        create_dual_crops_for_movie_single_direction(
            src_reader=thermal_reader, 
            dst_reader=cmos_reader, 
            crop_dir_path=thermal_crop_dir, 
            output_folder=os.path.join(output_basedir, direction, output_type, os.path.basename(cmos_crop_dir)), 
            tform=inverse_tform,  
            cmos_to_thermal=True)
   

def extract_movies(dirname, extensions, substring=""):
    movies = []
    for root, dirs, files in os.walk(dirname):
        for file in files:
            extension = os.path.splitext(file)[-1]
            if extension in extensions and substring in root:
                movies.append(os.path.join(root, file))
    
    return movies


''' 
execute only when dual GTs and dual crops exist: creates warped crops (transformed by registration) based on two dual crops. 
output_basedir (str):       a path used as a base for creating the movies' output fused dual crops directories
cmos_crops_dirs (str):      a list of paths to input VIS dual crops directories
thermal_crops_dirs (str):   a list of paths to input IR dual crops directories. if None-> the list derives from cmos_crops_dirs.
'''
def create_dual_crops(output_basedir, cmos_crops_dirs, thermal_crops_dirs=None):
    if thermal_crops_dirs is None:
        thermal_crops_dirs = [dirname.replace("CMOS", "Thermal") for dirname in cmos_crops_dirs]
    
    assert len(cmos_crops_dirs) == len(thermal_crops_dirs)
    for i in range(len(cmos_crops_dirs)):
        cmos_crops_dir = cmos_crops_dirs[i]
        thermal_crops_dir = thermal_crops_dirs[i]
        output_type = os.path.basename(cmos_crops_dir) # "High_Altitude", e.g.
        is_panorama = "panorama" in output_type or "Panorama" in output_type
        
        print("\ncreating crops from:", output_type)

        if create_cmos:
            create_dir(os.path.join(output_basedir, "CMOS_for_Thermal"))
            create_dir(os.path.join(output_basedir, "CMOS_for_Thermal", output_type))
        if create_thermal:
            create_dir(os.path.join(output_basedir, "Thermal_for_CMOS"))
            create_dir(os.path.join(output_basedir, "Thermal_for_CMOS", output_type))
        
        cmos_all_movies = sorted([os.path.join(cmos_crops_dir, movie) for movie in os.listdir(cmos_crops_dir) if os.path.isdir(os.path.join(cmos_crops_dir, movie))])
        thermal_all_movies = sorted([os.path.join(thermal_crops_dir, movie) for movie in os.listdir(thermal_crops_dir) if os.path.isdir(os.path.join(thermal_crops_dir, movie))])

        if is_panorama:
            cmos_movie_ids = [os.path.basename(movie).split('_')[1]+os.path.basename(movie).split('_')[4] for movie in cmos_all_movies]
            thermal_movie_ids = [os.path.basename(movie).split('_')[1]+os.path.basename(movie).split('_')[4] for movie in thermal_all_movies]
        else:
            cmos_movie_ids = [int(os.path.basename(movie).split('_')[1]) for movie in cmos_all_movies]
            thermal_movie_ids = [int(os.path.basename(movie).split('_')[1]) for movie in thermal_all_movies]

        cmos_movies = sorted([cmos_all_movies[cmos_movie_ids.index(movie_id)] for movie_id in cmos_movie_ids if movie_id in thermal_movie_ids])
        thermal_movies = sorted([thermal_all_movies[thermal_movie_ids.index(movie_id)] for movie_id in thermal_movie_ids if movie_id in cmos_movie_ids])
            
        assert len(cmos_movies) == len(thermal_movies)
        for j in range(len(cmos_movies)):
            print("processing directory {}/{}: {} | current time - {}".format(j+1, len(cmos_movies), os.path.basename(cmos_movies[j]), datetime.now().strftime("%H:%M:%S")))
            create_dual_crops_for_movie(
                output_basedir=output_basedir, output_type=output_type, 
                cmos_crop_dir=cmos_movies[j], thermal_crop_dir=thermal_movies[j], 
                is_panorama=is_panorama)


if __name__ == '__main__':
    create_dual_crops(output_basedir=output_basedir, cmos_crops_dirs=cmos_crops_dirs, thermal_crops_dirs=thermal_crops_dirs)
    print("Done!")
