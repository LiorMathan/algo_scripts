import numpy as np

tform_path = "/mnt/share/Server/ThirdEye Dropbox/Production/Operations/Deliveries/CHM/093 - CHM4aD/x4/r0_4/r0_4_000000.txt"

def extract_affine_matrix(filename, inverse):
    with open(filename) as transformation_file:
        if inverse:
            for i in range(5):
                transformation_file.readline()
        else:
            transformation_file.readline()
            transformation_file.readline()
        matrix_txt = transformation_file.readlines()
        affine_mat = []
        for j in range(2):
            affine_mat.append([float(item) for item in matrix_txt[j].split()])
        # print(str(affine_mat))
    return np.array(affine_mat)


tform = extract_affine_matrix(tform_path, inverse=False)
scale_x = np.linalg.norm(tform[:,0])
scale_y = np.linalg.norm(tform[:,1])

print("Scale x: " + str(scale_x))
print("Scale y: " + str(scale_y))