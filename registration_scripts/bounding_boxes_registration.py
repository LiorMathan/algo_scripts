import cv2
import numpy as np
import copy
from Reader import *
from Target import *
import evaluation_functions as evf

########################### TO DO ####################

# cmos_path = r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\High_Altitude\full_movies\ALL\CMOS_RAW\00011_r2_3.rawc"
# thermal_path = r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\High_Altitude\full_movies\ALL\IR_RAW\00011_r0_3.raw2"

# cmos_gt = r"Z:\SharingIsCaring\LiorMathan\ConcatenatedFrames\_00011_r2_3_detections1.txt"
# thermal_gt = r"Z:\SharingIsCaring\LiorMathan\ConcatenatedFrames\_00011_r2_3_detections2.txt"

# tform = np.array([[2.4024427480916035, -0.016183206106870414, -396.120610687023], 
#                   [0.030381679389312938, 2.4612213740458015, -302.5625954198473]])

# Please notice: the transformation matrix is a result of an opencv function,
# which workes on an image- 1st column refers to rows (y-coordinates), 2nd column refers to cols (x-coordinates).

######################################################


def extract_affine_matrix(filename, inverse):
    with open(filename) as transformation_file:
        if inverse:
            for i in range(5):
                transformation_file.readline()
        else:
            transformation_file.readline()
            transformation_file.readline()
        matrix_txt = transformation_file.readlines()
        affine_mat = []
        for j in range(2):
            affine_mat.append([float(item) for item in matrix_txt[j].split()])
        # print(str(affine_mat))

    return affine_mat


def get_bb_list(filename, index, gt=True):
    bb_list = []

    with open(filename, "r") as f:
        line = f.readline()
        tokens = line.split()
        while (int(tokens[1]) != index):
            line = f.readline()
            tokens = line.split()
        while (int(tokens[1]) == index):
            if gt:
                label = int(tokens[2])
            else: # detections
                label = int(tokens[1])
            w = int(tokens[5])
            h = int(tokens[6])
            cx = int(tokens[3])
            cy = int(tokens[4])

            to_add = [label, cx, cy, w, h]
            bb_list.append(to_add)

            line = f.readline()
            tokens = line.split()

    return bb_list


def get_targets_list(filename, index, resolution, label_set="AG", gt=True):
    targets_list = []

    with open(filename, "r") as f:
        line = f.readline()
        tokens = line.split()
        while (int(tokens[1]) != index):
            line = f.readline()
            tokens = line.split()
        while (int(tokens[1]) == index):
            if gt:
                to_append = Target.init_from_yolo_line(line='_'.join(tokens[2:]), frame_width=resolution[0], frame_height=resolution[1], label_set=label_set)
            else: 
                to_append = Target.init_from_detection_line(line=line, label_set=label_set)

            targets_list.append(to_append)

            line = f.readline()
            tokens = line.split()

    return targets_list


def apply_transformation_on_BB(original_list, tform):
    transformed_list = []

    scale_x = np.linalg.norm(tform[:,0])
    scale_y = np.linalg.norm(tform[:,1])
    
    is_target = type(original_list[0]) is not list

    for bb in original_list:
        if is_target:
            cx, cy = bb.cx, bb.cy
            w, h = bb.width, bb.height
        else:
            cx, cy = bb[1], bb[2]
            w, h = bb[3], bb[4]

        new_center = np.matmul(tform, np.array([cx, cy, 1]))
        new_w = int(scale_x * w)
        new_h = int(scale_y * h)

        if is_target:
            transformed = Target(label=bb.label, cx=int(new_center[0]), cy=int(new_center[1]), width=new_w, height=new_h, confidence=bb.confidence, label_set=bb.label_set)
        else:
            transformed = [bb[0], int(new_center[0]), int(new_center[1]), new_w, new_h]
        transformed_list.append(transformed)

    return transformed_list


def draw_labeles(frame, bb_list, winname="frame", color=(0, 0, 255), thickness=1):
    for bb in bb_list:
        top     = int(bb[2] - (bb[4] / 2))
        left    = int(bb[1] - (bb[3] / 2))
        bottom  = int(bb[2] + (bb[4] / 2))
        right   = int(bb[1] + (bb[3] / 2))

        cv2.rectangle(frame, (left, top), (right, bottom), color, thickness)


def get_center_deltas_vectors(annotations, merged_into_index_vector, partition):
    vectors = []
    for i in range(0, partition):
        if np.isnan(merged_into_index_vector[i]):
            continue
        else:
            match_index = int(merged_into_index_vector[i])
            original = annotations[i]
            match_annotation = annotations[match_index]

            current_vector = (
                (match_annotation.cx - original.cx),
                (match_annotation.cy - original.cy)
            )

            vectors.append(current_vector)

    return vectors


def transform_annotation_by_center_vectors(annotation, vectors, inverse):
    ((left, top), (right, bottom)) = annotation.get_as_rectangle_start_end()
    
    corner_xs = [left, right]
    corner_ys = [top, bottom]

    for vector in vectors:
        if (inverse):
            delta_cx = -int(np.ceil(vector[0]))
            delta_cy = -int(np.ceil(vector[1]))
        else:
            delta_cx = int(np.ceil(vector[0]))
            delta_cy = int(np.ceil(vector[1]))

        modified_annotation = copy.copy(annotation)
        modified_annotation.cx += delta_cx
        modified_annotation.cy += delta_cy

        ((left, top), (right, bottom)) = modified_annotation.get_as_rectangle_start_end()

        corner_xs.append(left)
        corner_xs.append(right)
        
        corner_ys.append(top)
        corner_ys.append(bottom)

    top     = np.min(corner_ys)
    left    = np.min(corner_xs)
    bottom  = np.max(corner_ys)
    right   = np.max(corner_xs)

    cx, cy, width, height = evf.get_cx_cy_width_height(top, left, bottom, right)
    transformed = Target(label=annotation.label, cx=cx, cy=cy, width=width, height=height, confidence=annotation.confidence, label_set=annotation.label_set)
    
    return transformed


def bound_bb_to_frame(annotation, resulotion, size_limit):
    if (annotation.width > size_limit):
        annotation.width = size_limit
    
    if (annotation.height > size_limit):
        annotation.height = size_limit

    ((left, top), (right, bottom)) = annotation.get_as_rectangle_start_end()

    top     = max(0, top)
    left    = max(0, left)
    bottom  = min(resulotion[1], bottom)
    right   = min(resulotion[0], right)

    cx, cy, width, height = evf.get_cx_cy_width_height(top, left, bottom, right)
    bounded = Target(label=annotation.label, cx=cx, cy=cy, width=width, height=height, confidence=annotation.confidence, label_set=annotation.label_set)

    return bounded


