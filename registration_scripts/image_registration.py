from __future__ import print_function
import numpy as np
import argparse
import imutils
import glob
import cv2
import os
from scipy import ndimage
from skimage import data
from skimage import transform
from skimage import filters
import matplotlib.pyplot as plt
import pyScriptsUtilities as utils
import imregpoc


frame_width_thermal = 640
frame_height_thermal = 480
frame_width_vis = 720
frame_height_vis = 576

visualize_flag = True



# converts a uint16 format nparray into uint8 format
def imagesc_gs(np_array, crop_edges=False):
    if crop_edges is False:
        min_value = np.amin(np_array)
        max_value = np.amax(np_array)
    else:
        min_value = np.percentile(np_array, 1)
        max_value = np.percentile(np_array, 99)

    max_value = 255
    original_range = max_value - min_value
    # to avoid dividing by 0
    if original_range == 0:
        original_range = 65535

    scale = 255.0/np.float(original_range)
    transform_image = (np_array - min_value).astype(np.float) * scale
    if crop_edges is True:
        transform_image[transform < 0] = 0
        transform_image[transform_image > 255] = 255
    transform_image = np.around(transform_image)
    transform_image = np.uint8(transform_image)

    return transform_image


def show(float_image, fname):
    image_visualize = imagesc_gs(float_image)
    # image_visualize = cv2.applyColorMap(image_visualize, cv2.COLORMAP_COOL)
    # plt.imshow(float_image, cmap="gray")
    # plt.colorbar()
    # plt.show()
    cv2.imshow(fname, image_visualize)
    cv2.waitKey(0)
    # cv2.destroyAllWindows()


def get_image_diffs(image, width, height, axis=0):
    result_image = image.copy()

    if axis == 0:
        for i in range(width):
            current_index = i % width
            next_index = (i+1) % width
            result_image[:, current_index] = np.subtract(result_image[:, current_index], image[:, next_index])
        #     result_image[:, current_index] = image[:, next_index]
        # result_image = cv2.absdiff(result_image, image)
    elif axis == 1:
        for i in range(height):
            current_index = i % height
            next_index = (i+1) % height
            result_image[current_index, :] = np.subtract(result_image[current_index, :], image[next_index, :])
        #     result_image[current_index, :] = image[next_index, :]
        # result_image = cv2.absdiff(result_image, image)    

    # plt.imshow(result_image, cmap='gray')
    # plt.show()
    if visualize_flag is True:
        show(result_image, "result_image")
    # show(image, "original")
    
    return result_image
 

def image_diffs(image, width, height):
    # calc diffs:
    diffs_x = get_image_diffs(image, width, height, axis=0)
    diffs_y = get_image_diffs(image, width, height, axis=1)

    # square:
    diffs_x_square = np.square(diffs_x)
    diffs_y_square = np.square(diffs_y)

    # sum:
    sum_both_axis = diffs_x_square + diffs_y_square

    # square root:
    sqrt_image = np.sqrt(sum_both_axis).astype(float)

    # normalization:
    sqrt_image_norm = imagesc_gs(sqrt_image)

    return sqrt_image_norm



def imreg(align_frame, ref_frame):
    sqrt_image_norm = image_diffs(align_frame, frame_width_thermal, frame_height_thermal)
    sqrt_image_vis_norm = image_diffs(ref_frame, frame_width_vis, frame_height_vis)
    
    # smoothing the images:
    # kernel = np.ones((3,3), np.float32) / 9
    # sqrt_image_norm = cv2.filter2D(sqrt_image_norm, -1, kernel)
    # sqrt_image_vis_norm = cv2.filter2D(sqrt_image_vis_norm, -1, kernel)
    # sqrt_image_norm = cv2.GaussianBlur(sqrt_image_norm,(3,3),0)
    # sqrt_image_vis_norm = cv2.GaussianBlur(sqrt_image_vis_norm,(3,3),0)

    # Create ORB detector with 5000 features. 
    orb1 = cv2.ORB_create(500)
    orb2 = cv2.ORB_create(500)
    
    # Find keypoints and descriptors. 
    # The first arg is the image, second arg is the mask 
    #  (which is not reqiured in this case). 
    kp1, d1 = orb1.detectAndCompute(sqrt_image_norm, None) 
    kp2, d2 = orb2.detectAndCompute(sqrt_image_vis_norm, None) 
    
    image1 = cv2.drawKeypoints(sqrt_image_norm, kp1, None, flags=None)
    image2 = cv2.drawKeypoints(sqrt_image_vis_norm, kp2, None, flags=None)

    if visualize_flag is True:
        cv2.imshow("ORB - Thermal", image1)
        cv2.imshow("ORB - VIS", image2)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    # Match features between the two images. 
    # We create a Brute Force matcher with  
    # Hamming distance as measurement mode. 
    matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck = True) 
    
    # Match the two sets of descriptors. 
    matches = matcher.match(d1, d2) 
    
    # Sort matches on the basis of their Hamming distance. 
    matches.sort(key = lambda x: x.distance) 
    
    # Take the top 90 % matches forward. 
    matches = matches[:int(len(matches)*90)] 
    no_of_matches = len(matches) 
    
    # Define empty matrices of shape no_of_matches * 2. 
    p1 = np.zeros((no_of_matches, 2)) 
    p2 = np.zeros((no_of_matches, 2)) 
    
    for i in range(len(matches)): 
        p1[i, :] = kp1[matches[i].queryIdx].pt 
        p2[i, :] = kp2[matches[i].trainIdx].pt 
    
    # Find the homography matrix. 
    homography, mask = cv2.findHomography(p1, p2, cv2.RANSAC) 
    
    # Use this matrix to transform the 
    # colored image wrt the reference image. 
    align_frame_uint8 = imagesc_gs(align_frame).astype(np.uint8)
    transformed_img = cv2.warpPerspective(align_frame_uint8, 
                        homography, (frame_width_vis, frame_height_vis))

    if visualize_flag is True:
        # show the output:
        cv2.imshow("output", transformed_img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        # hist_thermal,bins = np.histogram(sqrt_image.ravel(),max_value_thermal,[0,255])
        # hist_vis,bins = np.histogram(sqrt_image_vis.ravel(),max_value_vis,[0,255])

        # plt.style.use('seaborn-white')
        # ax1 = plt.subplot(2,1,1)
        # ax1.set_title("thermal")
        # plt.plot(hist_thermal)
        # ax2 = plt.subplot(2,1,2)
        # ax2.set_title("vis")
        # plt.plot(hist_vis)

        # plt.show()

        # Save the output. 
        cv2.imwrite('output2.jpg', transformed_img)
    
    return transformed_img


def show_key_points(image1, image2):
    image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)
    image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)

    # image1 = cv2.Canny(image1, 200, 1000)
    # image2 = cv2.Canny(image2, 20, 175)

    # initiate ORB:
    orb1 = cv2.ORB_create(5000)
    orb2 = cv2.ORB_create(5000)

    key_points1, descriptors1 = orb1.detectAndCompute(image1, None)
    key_points2, descriptors2 = orb2.detectAndCompute(image2, None)

    # sift = cv2.xfeatures2d.SIFT_create()
    # key_points1 = sift.detect(image1, None)
    # key_points2 = sift.detect(image2, None)


    image1 = cv2.drawKeypoints(image1, key_points1, None, flags=None)
    image2 = cv2.drawKeypoints(image2, key_points2, None, flags=None)

    cv2.imshow("ORB1", image1)
    cv2.imshow("ORB2", image2)
    cv2.waitKey(0) 


##### Main #####

# Open the image files. 
vis = utils.read_frame(0, "vis1.rawc", frame_height_vis, frame_width_vis)
thermal = utils.read_frame(0, "thermal1.raw2", frame_height_thermal, frame_width_thermal)
vis = cv2.cvtColor(vis, cv2.COLOR_YUV2GRAY_420)
vis = vis.astype(float)
thermal = thermal.astype(float)

# kernel = np.ones((3,3), np.float32) / 9
# thermal = cv2.filter2D(thermal, -1, kernel)
# vis = cv2.filter2D(vis, -1, kernel)
thermal = cv2.GaussianBlur(thermal,(3,3),0)
vis = cv2.GaussianBlur(vis,(3,3),0)

sqrt_image_norm = image_diffs(thermal, frame_width_thermal, frame_height_thermal)
sqrt_image_vis_norm = image_diffs(vis, frame_width_vis, frame_height_vis)

result = imregpoc.imregpoc(sqrt_image_norm, sqrt_image_vis_norm)
result.stitching()


# transformed = imreg(thermal, vis)
















# max_value_thermal = int(np.max(sqrt_image))
# min_value_thermal = int(np.min(sqrt_image))
# max_value_vis = int(np.max(sqrt_image_vis))
# min_value_vis = int(np.min(sqrt_image_vis))

# sqrt_image_norm = cv2.normalize(sqrt_image, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
# sqrt_image_vis_norm = cv2.normalize(sqrt_image_vis, None, 255, 0, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

# hist_thermal,bins = np.histogram(sqrt_image.ravel(),max_value_thermal,[0,255])
# hist_vis,bins = np.histogram(sqrt_image_vis.ravel(),max_value_vis,[0,255])


# plt.style.use('seaborn-white')
# ax1 = plt.subplot(2,1,1)
# ax1.set_title("thermal")
# plt.plot(hist_thermal)
# ax2 = plt.subplot(2,1,2)
# ax2.set_title("vis")
# plt.plot(hist_vis)

# plt.show()


# thermal_hist = np.histogram(sqrt_image, bins=10)
# vis_hist = np.histogram(sqrt_image_vis, bins=10)

 
# ax1 = plt.subplot(2,1,1)
# ax1.set_title("thermal")
# plt.plot(thermal_hist)
# ax2 = plt.subplot(2,1,2)
# ax2.set_title("vis")
# plt.plot(vis_hist)

# plt.show()







# thermal = cv2.imread("Z:\\OfflineDB\\Scripts\\crops_scripts\\thermal1.png")  # Image to be aligned.
# vis = cv2.imread("Z:\\OfflineDB\\Scripts\\crops_scripts\\vis1.png")    # Reference image.
# image1 = cv2.imread("Z:\\OfflineDB\\external\\RGB-T234\\car\\visible\\00090v.jpg")
# image2 = cv2.imread("Z:\\OfflineDB\\external\\RGB-T234\\car\\infrared\\00090i.jpg")

# cv2.imshow("original_image1", vis)
# cv2.imshow("original_image2", thermal)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# # zoom-in:
# thermal = cv2.resize(thermal, (int(thermal.shape[1] * 1.5), int(thermal.shape[0] * 1.5)))
# cv2.imshow("image1", vis)
# cv2.imshow("image2", thermal)
# cv2.waitKey(0)

# # cropping:
# thermal = thermal[0:vis.shape[0], 0:vis.shape[1]]
# cv2.imshow("cropped", thermal)
# cv2.waitKey(0)

# # rotation angle in degree
# thermal = ndimage.rotate(thermal, 45)
# cv2.imshow("rotated", thermal)
# cv2.waitKey(0)


# img1_color = cv2.resize(img1_color, (int(img1_color.shape[1]/3), int(img1_color.shape[0]/3)))
# img2_color = cv2.resize(img2_color, (int(img2_color.shape[1]/3), int(img2_color.shape[0]/3)))
# imreg(image1, image2)
# show_key_points(image1, image2)

# imreg(thermal, vis)


