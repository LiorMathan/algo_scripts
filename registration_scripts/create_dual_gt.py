import cv2
import numpy as np
import os
import time
import shutil
import random
import csv
import collections
import evaluation_functions as evf
import bounding_boxes_registration as bbr
from Reader import *
from Target import *
from datetime import datetime


matrix_basedir                    = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Affine_Transformation_Matrix"
statistics_file_cmos              = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Dual_GT/ThresholdStatisticsCMOS_test.csv"
statistics_file_thermal           = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Dual_GT/ThresholdStatisticsThermal_test.csv"
folder_for_statistics_file_copies = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Dual_GT"
external_debug_directory          = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Dual_GT/Debug_Frames"
debug_directory                   = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Dual_GT/Debug_Frames"
bad_movies_file                   = ""

problematic_registration_matrixes = set()
threshold       = 0.2
debug           = True
save_frames     = False
statistics_only = False
random_samples  = False
label_set       = "AG"

## main configuration - GG
# input_path = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground-ground/GT"
# output_path = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground-ground/Dual_GT"
# movies_dirs = ["/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground-ground/Avatar",
#                 "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground-ground/Germany",
#                 "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground-ground/Nordic_Wall",
#                 "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground-ground/Overrun_Prevention",
#                 "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground-ground/ViaVision"]

## main configuration - AG
movies_dirs = ["/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Low_Altitude/640-480",
                "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Low_Altitude/720-576"]
input_path  = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/GT"
output_path = "/mnt/share/Local/SharingIsCaring/amram/test/create_warped_crops/output_warped_crops/Dual_GT"

## main configuration - AG-PanoramaBools
# movies_dirs = ["/mnt/ssdHot/WorkMovies/PanoramaBools"]
# input_path  = "/mnt/ssdHot/WorkMovies/PanoramaBools/GT"
# output_path = "/mnt/ssdHot/WorkMovies/PanoramaBools/Dual_GT"


''' 
while creating dual GT files for a specific movie, a StatisticsData object is created in order to collect the following data and summarize it into a CSV file. 
'''
class StatisticsData:
    def __init__(self, vis_basename=None, thermal_basename=None, num_frames=0, len_cmos_annotations=0, 
                len_thermal_annotations=0, len_cmos_annotations_dual={}, len_thermal_annotations_dual={}):
        self.vis_basename = vis_basename
        self.thermal_basename = thermal_basename 
        self.num_frames = num_frames 
        self.len_cmos_annotations = len_cmos_annotations 
        self.len_thermal_annotations = len_thermal_annotations 
        self.len_cmos_annotations_dual = len_cmos_annotations_dual 
        self.len_thermal_annotations_dual = len_thermal_annotations_dual 
        self.all_cmos_annotations = [] 
        self.all_thermal_annotations = []


def create_dir(dir_path):
    try:
        os.mkdir(dir_path)
    except:
        pass


def get_annotations(gt_file, width, height):
    annotation_list = []
    gt = open(gt_file, "r")
    lines = gt.readlines()
    gt.close()
    for line in lines:
        annotation_list.append(Target.init_from_yolo_line(line=line, frame_width=width, frame_height=height, label_set=label_set))

    return annotation_list


def write_annotations(output_path, gt_list, width, height):
    res_gt = open(output_path, "w")
    for target in gt_list:
        line = target.get_as_yolo_line(frame_width=width, frame_height=height) + '\n'
        res_gt.write(line)
    res_gt.close()


def merge_annotations(annotations, merged_into_index_vector, partition, resolution, unite=True, stretch_original_channel=False, stretch_other_channel=False, size_limit=100):
    merged_list = []
    checked_indexes = set()

    if (stretch_original_channel or stretch_other_channel):
        transform_vectors = bbr.get_center_deltas_vectors(annotations, merged_into_index_vector, partition)
    else:
        transform_vectors = []

    for i in range(len(merged_into_index_vector)):
        if i in checked_indexes:
            continue

        checked_indexes.add(i)
        if np.isnan(merged_into_index_vector[i]):
            current_annotation = annotations[i]
            if i < partition and stretch_original_channel:
                current_annotation = bbr.transform_annotation_by_center_vectors(current_annotation, transform_vectors, False)
            elif i >= partition and stretch_other_channel:
                current_annotation = bbr.transform_annotation_by_center_vectors(current_annotation, transform_vectors, True)

        else:
            match_index = int(merged_into_index_vector[i])
            checked_indexes.add(match_index)
            if unite:
                current_annotation = evf.get_union_bb(annotations[i], annotations[match_index])
            else:
                if i < partition: # original annotation
                    current_annotation = annotations[i]
                else: # transformed annotation
                    current_annotation = annotations[match_index]

        if evf.is_bb_partialy_contained(current_annotation, resolution):
            current_annotation = bbr.bound_bb_to_frame(current_annotation, resolution, size_limit)
            merged_list.append(current_annotation)

    return merged_list


def create_output_dirs(input_gt_dir_cmos, input_gt_dir_thermal, output_gt_dir):
    output_path_united = os.path.join(output_gt_dir, "NMS_United")
    output_path_not_united = os.path.join(output_gt_dir, "NMS_NotUnited")
    create_dir(output_path_united)
    create_dir(output_path_not_united)
    
    output_path_united_cmos         = os.path.join(output_path_united, os.path.basename(input_gt_dir_cmos))
    output_path_not_united_cmos     = os.path.join(output_path_not_united, os.path.basename(input_gt_dir_cmos))    
    output_path_united_thermal      = os.path.join(output_path_united, os.path.basename(input_gt_dir_thermal))
    output_path_not_united_thermal  = os.path.join(output_path_not_united, os.path.basename(input_gt_dir_thermal))
    
    for dual_output_path in [output_path_united_cmos, output_path_not_united_cmos, output_path_united_thermal, output_path_not_united_thermal]:
        create_dir(dual_output_path)

    return (output_path_united_cmos, output_path_not_united_cmos, output_path_united_thermal, output_path_not_united_thermal)


def extract_affine_matrix(filename, inverse):
    with open(filename) as transformation_file:
        if inverse:
            for i in range(5):
                transformation_file.readline()
        else:
            transformation_file.readline()
            transformation_file.readline()
        matrix_txt = transformation_file.readlines()
        affine_mat = []
        for j in range(2):
            affine_mat.append([float(item) for item in matrix_txt[j].split()])
        
    return np.array(affine_mat)


def init_readers(movies):
    cmos_reader = thermal_reader = None
    for movie in movies:
        try:
            if "_r0_" in movie:
                thermal_reader = Reader(path1=movie)
            else:
                if '640-480' in movie:
                    cmos_resolution = (640, 480)
                elif '720-576' in movie:
                    cmos_resolution = (720, 576)
                else:
                    cmos_resolution = (None, None)
                cmos_reader = Reader(path1=movie, width1=cmos_resolution[0], height1=cmos_resolution[1])
        except:
            continue
    
    return (cmos_reader, thermal_reader)


def display_labeled_frame(dual_annotations, cmos_annotations, thermal_annotations, debug_frame, title, basename, print_annotations=False):
    try:
        for annotation in dual_annotations:
            annotation.draw(debug_frame, color=(255, 0, 0), thickness=2, show_label=False) # Blue
            if print_annotations:
                print(f"{title} Blue - {annotation}")
        for annotation in cmos_annotations:
            annotation.draw(debug_frame, color=(0, 255, 0), show_label=False) # Green
            if print_annotations:
                print(f"{title} Green - {annotation}")
        for annotation in thermal_annotations:
            annotation.draw(debug_frame, color=(0, 0, 255), show_label=False) # Red
            if print_annotations:
                print(f"{title} Red - {annotation}")
        cv2.imshow(title, debug_frame)
        if save_frames:
            cv2.imwrite(os.path.join(debug_directory, basename.replace('.txt', '.png')), debug_frame)
    except:
        pass


def debug_display(debug_name, cmos_frame, thermal_frame, total_cmos_annotations_with_union, total_thermal_annotations_with_union, cmos_annotations, thermal_annotations, cmos_transformed_annotations, thermal_transformed_annotations, transformation, inverse_transformation, cmos_basename=None, thermal_basename=None):
    cmos_copy = np.copy(cmos_frame)
    thermal_copy = np.copy(thermal_frame)

    # show original VIS frame
    display_labeled_frame(dual_annotations=total_cmos_annotations_with_union, 
                        cmos_annotations=cmos_annotations, 
                        thermal_annotations=thermal_transformed_annotations, 
                        debug_frame=cmos_copy, 
                        title=debug_name + " VIS", 
                        basename=cmos_basename, 
                        print_annotations=True)

    # show original IR frame
    display_labeled_frame(dual_annotations=total_thermal_annotations_with_union, 
                        cmos_annotations=cmos_transformed_annotations, 
                        thermal_annotations=thermal_annotations, 
                        debug_frame=thermal_copy, 
                        title=debug_name + " IR", 
                        basename=thermal_basename, 
                        print_annotations=True)

    if transformation is not None:
        # show Dual VIS frame (registrated)
        dst_frame = cv2.cvtColor(thermal_copy, cv2.COLOR_BGR2GRAY)
        warped_frame = cv2.warpAffine(dst_frame, transformation, (cmos_copy.shape[1], cmos_copy.shape[0]))
        debug_frame = cmos_copy + np.transpose(np.array([warped_frame, warped_frame, warped_frame]), axes=(1, 2, 0))
        display_labeled_frame(dual_annotations=total_cmos_annotations_with_union, 
                            cmos_annotations=cmos_annotations, 
                            thermal_annotations=thermal_transformed_annotations, 
                            debug_frame=debug_frame, 
                            title=debug_name + " Dual VIS", 
                            basename=cmos_basename)
        
        # show Dual IR frame (registrated)
        dst_frame = cv2.cvtColor(cmos_copy, cv2.COLOR_BGR2GRAY)
        warped_frame = cv2.warpAffine(dst_frame, inverse_transformation, (thermal_copy.shape[1], thermal_copy.shape[0]))
        debug_frame = thermal_copy + np.transpose(np.array([warped_frame, warped_frame, warped_frame]), axes=(1, 2, 0))
        display_labeled_frame(dual_annotations=total_thermal_annotations_with_union, 
                            cmos_annotations=cmos_transformed_annotations, 
                            thermal_annotations=thermal_annotations, 
                            debug_frame=debug_frame, 
                            title=debug_name + " Dual IR", 
                            basename=thermal_basename)


def handle_singular_reader(statistics, reader, input_gt_dir, output_path_united, output_path_not_united, is_thermal):
    statistics.num_frames = reader.num_frames1
    resolution = (reader.width1, reader.height1)
    if debug:
        create_dir(debug_directory)

    gts = []
    if os.path.exists(input_gt_dir):
        gts = sorted([file for file in os.listdir(input_gt_dir) if file.endswith(".txt")])
        if is_thermal:
            statistics.len_thermal_annotations += len(gts)
        else:
            statistics.len_cmos_annotations += len(gts)

    if not statistics_only:
        for i in range(len(gts)):
            annotations = []
            if len(gts) > i:
                annotations = get_annotations(gt_file=os.path.join(input_gt_dir, gts[i]), width=resolution[0], height=resolution[1])
            basename = os.path.basename(input_gt_dir).replace('gt', str(i).zfill(6)) + '.txt'
            write_annotations(output_path=os.path.join(output_path_united, basename), gt_list=annotations, width=resolution[0], height=resolution[1])
            write_annotations(output_path=os.path.join(output_path_not_united, basename), gt_list=annotations, width=resolution[0], height=resolution[1])


def handle_invalid_dual(input_gt_dir_cmos, input_gt_dir_thermal, output_gt_dir, cmos_reader, thermal_reader, statistics):
    if cmos_reader is None and thermal_reader is None:
        return
    
    cmos_gts = []
    thermal_gts = []
    if not statistics_only:
        output_path_united_cmos, output_path_not_united_cmos, output_path_united_thermal, output_path_not_united_thermal = create_output_dirs(input_gt_dir_cmos, input_gt_dir_thermal, output_gt_dir)

    if cmos_reader is None:
        handle_singular_reader(statistics=statistics, reader=thermal_reader, input_gt_dir=input_gt_dir_thermal, output_path_united=output_path_united_thermal, output_path_not_united=output_path_not_united_thermal, is_thermal=True)
    else:
        handle_singular_reader(statistics=statistics, reader=cmos_reader, input_gt_dir=input_gt_dir_cmos, output_path_united=output_path_united_cmos, output_path_not_united=output_path_not_united_cmos, is_thermal=False)

''' 
fill the StatisticsData object with all the information which doesn't depend on a specific threshold, 
i.e., all data related to the original GT files. 
'''
def extract_core_data_from_movie(input_gt_dir_cmos, input_gt_dir_thermal, cmos_reader, thermal_reader, statistics):
    cmos_gts = []
    thermal_gts = []
    cmos_resolution = (cmos_reader.width1, cmos_reader.height1)
    thermal_resolution = (thermal_reader.width1, thermal_reader.height1)
    if debug:
        try:
            os.mkdir(debug_directory)
        except:
            pass

    if os.path.exists(input_gt_dir_cmos):
        cmos_gts = sorted([file for file in os.listdir(input_gt_dir_cmos) if file.endswith(".txt")])
    if os.path.exists(input_gt_dir_thermal):
        thermal_gts = sorted([file for file in os.listdir(input_gt_dir_thermal) if file.endswith(".txt")])

    for i in range(max(len(cmos_gts), len(thermal_gts))):
        cmos_annotations, thermal_annotations = [], []

        if len(cmos_gts) > i:
            cmos_annotations = get_annotations(gt_file=os.path.join(input_gt_dir_cmos, cmos_gts[i]), width=cmos_resolution[0], height=cmos_resolution[1])
        if len(thermal_gts) > i:
            thermal_annotations = get_annotations(gt_file=os.path.join(input_gt_dir_thermal, thermal_gts[i]), width=thermal_resolution[0], height=thermal_resolution[1])
        
        statistics.len_cmos_annotations += len(cmos_annotations)
        statistics.len_thermal_annotations += len(thermal_annotations)

        statistics.all_cmos_annotations.append(cmos_annotations)
        statistics.all_thermal_annotations.append(thermal_annotations)


def get_dual_annotations(index, statistics):
    cmos_annotations, thermal_annotations = [], []
    if index < len(statistics.all_cmos_annotations):
        cmos_annotations = statistics.all_cmos_annotations[index]
    if index < len(statistics.all_thermal_annotations):
        thermal_annotations = statistics.all_thermal_annotations[index]
    
    return cmos_annotations, thermal_annotations


def get_total_annotations(cmos_combined_list, thermal_combined_list, cmos_resolution, thermal_resolution, cmos_partition, thermal_partition):
    merged_into_index_cmos     = evf.fuse_lists(dual_list=cmos_combined_list, threshold=threshold, razor=[100]*len(cmos_combined_list), method=evf.iom, partition=cmos_partition)
    merged_into_index_thermal  = evf.fuse_lists(dual_list=thermal_combined_list, threshold=threshold, razor=[100]*len(thermal_combined_list), method=evf.iom, partition=thermal_partition)
    
    total_cmos_annotations_with_union       = merge_annotations(annotations=cmos_combined_list, merged_into_index_vector=merged_into_index_cmos, partition=cmos_partition, resolution=cmos_resolution, unite=True, stretch_original_channel=True, stretch_other_channel=True)
    total_cmos_annotations_without_union    = merge_annotations(annotations=cmos_combined_list, merged_into_index_vector=merged_into_index_cmos, partition=cmos_partition, resolution=cmos_resolution, unite=False, stretch_original_channel=False, stretch_other_channel=True)
    total_thermal_annotations_with_union    = merge_annotations(annotations=thermal_combined_list, merged_into_index_vector=merged_into_index_thermal, partition=thermal_partition, resolution=thermal_resolution, unite=True, stretch_original_channel=True, stretch_other_channel=True)
    total_thermal_annotations_without_union = merge_annotations(annotations=thermal_combined_list, merged_into_index_vector=merged_into_index_thermal, partition=thermal_partition, resolution=thermal_resolution, unite=False, stretch_original_channel=False, stretch_other_channel=True)

    return [total_cmos_annotations_with_union, total_cmos_annotations_without_union, total_thermal_annotations_with_union, total_thermal_annotations_without_union]


def write_all_annotations(input_gt_dir_cmos, input_gt_dir_thermal, output_gt_dir, cmos_basename, thermal_basename, total_cmos_annotations_with_union, total_cmos_annotations_without_union, total_thermal_annotations_with_union, total_thermal_annotations_without_union, cmos_resolution, thermal_resolution):
    output_path_united_cmos, output_path_not_united_cmos, output_path_united_thermal, output_path_not_united_thermal = create_output_dirs(input_gt_dir_cmos, input_gt_dir_thermal, output_gt_dir)

    write_annotations(output_path=os.path.join(output_path_united_cmos, cmos_basename), gt_list=total_cmos_annotations_with_union, width=cmos_resolution[0], height=cmos_resolution[1])
    write_annotations(output_path=os.path.join(output_path_not_united_cmos, cmos_basename), gt_list=total_cmos_annotations_without_union, width=cmos_resolution[0], height=cmos_resolution[1])
    write_annotations(output_path=os.path.join(output_path_united_thermal, thermal_basename), gt_list=total_thermal_annotations_with_union, width=thermal_resolution[0], height=thermal_resolution[1])
    write_annotations(output_path=os.path.join(output_path_not_united_thermal, thermal_basename), gt_list=total_thermal_annotations_without_union, width=thermal_resolution[0], height=thermal_resolution[1])    


''' 
creates dual GT files for a specific movie. 
input_gt_dir_cmos (str):    directory of the input VIS movie's GT files
input_gt_dir_thermal (str): directory of the input IR movie's GT files
output_gt_dir (str):        a path used as a base for creating the movie's output dual GT directories
threshold (float):          minimal overlap area between two bounding boxes required to consider them the same object
cmos_reader (Reader):       frame extractor of the input VIS movie
thermal_reader (Reader):    frame extractor of the input IR movie
tform_path (str):           a path to the affine matrix that defines the registration process of the movie
tform_path_exists (bool):   True if tform_path leads to an exist affine matrix file, otherwise False.
statistics(StatisticsData): an object that summarizes the movie data into a CSV file
'''
def create_dual_gt_for_movie_with_threshold(input_gt_dir_cmos, input_gt_dir_thermal, output_gt_dir, threshold, cmos_reader, thermal_reader, tform_path, tform_path_exists, statistics):
    statistics.len_cmos_annotations_dual[threshold] = 0
    statistics.len_thermal_annotations_dual[threshold] = 0
    cmos_resolution = (cmos_reader.width1, cmos_reader.height1)
    thermal_resolution = (thermal_reader.width1, thermal_reader.height1)
    
    frames_displayed = 0
    for i in range(max(len(statistics.all_cmos_annotations), len(statistics.all_thermal_annotations))):
        if random_samples and debug:
            i = random.randint(1, max(len(statistics.all_cmos_annotations), len(statistics.all_thermal_annotations)) - 1)

        cmos_annotations, thermal_annotations = get_dual_annotations(index=i, statistics=statistics)
        cmos_transformed_annotations, thermal_transformed_annotations = [], []
        transformation = inverse_transformation = None

        try:
            if tform_path_exists:
                transformation          = extract_affine_matrix(filename=tform_path, inverse=False)
                inverse_transformation  = extract_affine_matrix(filename=tform_path, inverse=True)

                cmos_transformed_annotations     = bbr.apply_transformation_on_BB(original_list=cmos_annotations, tform=inverse_transformation)
                thermal_transformed_annotations  = bbr.apply_transformation_on_BB(original_list=thermal_annotations, tform=transformation)
                
                cmos_combined_list     = cmos_annotations + thermal_transformed_annotations
                thermal_combined_list  = thermal_annotations + cmos_transformed_annotations
                
                total_cmos_annotations_with_union, total_cmos_annotations_without_union, \
                total_thermal_annotations_with_union, total_thermal_annotations_without_union \
                = get_total_annotations(cmos_combined_list, thermal_combined_list, cmos_resolution, thermal_resolution, cmos_partition=len(cmos_annotations), thermal_partition=len(thermal_annotations))
                
            else:
                if statistics_only:
                    return
                total_cmos_annotations_with_union       = total_cmos_annotations_without_union        = cmos_annotations
                total_thermal_annotations_with_union    = total_thermal_annotations_without_union     = thermal_annotations

        except Exception as e:
            problematic_registration_matrixes.add(tform_path)
            return

        statistics.len_cmos_annotations_dual[threshold] += len(total_cmos_annotations_with_union)
        statistics.len_thermal_annotations_dual[threshold] += len(total_thermal_annotations_with_union)
        
        cmos_basename    = os.path.basename(input_gt_dir_cmos).replace('gt', str(i).zfill(6)) + '.txt'
        thermal_basename = os.path.basename(input_gt_dir_thermal).replace('gt', str(i).zfill(6)) + '.txt'
        if not statistics_only:
            write_all_annotations(input_gt_dir_cmos, input_gt_dir_thermal, output_gt_dir, cmos_basename, thermal_basename, total_cmos_annotations_with_union, total_cmos_annotations_without_union, total_thermal_annotations_with_union, total_thermal_annotations_without_union, cmos_resolution, thermal_resolution)

        if debug:
            if (len(total_cmos_annotations_with_union) + len(total_thermal_annotations_with_union) > 0):
                cmos_frame    = cmos_reader.get_frame(index=i, bgr=True)
                thermal_frame = thermal_reader.get_frame(index=i, bgr=True)
                debug_display("with_union", cmos_frame, thermal_frame, total_cmos_annotations_with_union, total_thermal_annotations_with_union, cmos_annotations, thermal_annotations, cmos_transformed_annotations, thermal_transformed_annotations, transformation, inverse_transformation, cmos_basename, thermal_basename)
                debug_display("without_union", cmos_frame, thermal_frame, total_cmos_annotations_without_union, total_thermal_annotations_without_union, cmos_annotations, thermal_annotations, cmos_transformed_annotations, thermal_transformed_annotations, transformation, inverse_transformation, cmos_basename, thermal_basename)
                cv2.waitKey(0)
                frames_displayed += 1
                if (frames_displayed == 5):
                    return


def get_folders_dictionary(input_path, is_panorama):
    folders = sorted([folder for folder in os.listdir(input_path) if os.path.isdir(os.path.join(input_path, folder))])
    folders_dict = collections.defaultdict(list)
    for folder in folders:
        split_name = folder.split('_')
        if is_panorama:
            folders_dict[split_name[0] + "_" + split_name[1] + "_" + split_name[3]].append(os.path.join(input_path, folder))
        else:
            folders_dict[split_name[0]].append(os.path.join(input_path, folder))

    return folders_dict


def get_movies_dictionary(external_dirs, is_panorama):
    movies_dict = collections.defaultdict(list)

    for i in external_dirs:
        print("Collecting scenes from " + i + "...")
        
        for directory in os.listdir(i):
            full_path_dir = os.path.join(i, directory)
            if os.path.isfile(full_path_dir):
                continue
            
            for directory2 in os.listdir(full_path_dir):
                if 'RAW' not in directory2:
                    continue
                full_path_dir = os.path.join(i, directory, directory2)
                if os.path.isfile(full_path_dir):
                    continue

                for file in os.listdir(full_path_dir):
                    extension = os.path.splitext(file)[-1]
                    if extension in (".raw2", ".rawc", ".rgb"):
                        split_name = file.split('_')
                        if is_panorama:
                            movies_dict[split_name[0] + "_" + split_name[1] + "_" + split_name[3][:-len(extension)]].append(os.path.join(full_path_dir, file))
                        else:
                            movies_dict[file.split('_')[0]].append(os.path.join(full_path_dir, file))
    
    return movies_dict


def open_new_statistics_file(file, thresholds):
    with open(file, 'w') as f:
        wr = csv.writer(f, dialect='excel')
        row = ["VIS basename", "IR basename", "Number of frames", "VIS objects", "IR objects"]
        for thr in thresholds:
            row.append("Dual objects (" + str(thr) + ")")
        wr.writerow(row)
        f.close()


def get_gt_paths(input_gt_paths, movies):
    thermal_gt_path = ""
    cmos_gt_path = ""
    for gt_path in input_gt_paths:
        if "_r0_" in gt_path:
            thermal_gt_path = gt_path
        else:
            cmos_gt_path = gt_path
    
    if thermal_gt_path == "":
        thermal_gt_path = cmos_gt_path.replace('_r2_', '_r0_').replace('_r1_', '_r0_')
    elif cmos_gt_path == "":
        for movie in movies:
            if '_r2_' in movie:
                cmos_gt_path = thermal_gt_path.replace('_r0_', '_r2_')
            elif '_r1_' in movie:
                cmos_gt_path = thermal_gt_path.replace('_r0_', '_r1_')
    
    return cmos_gt_path, thermal_gt_path


def create_dual_gt_for_movie(input_gt_paths, movies, thresholds, f_cmos_writer, f_thermal_writer):
    cmos_gt_path, thermal_gt_path = get_gt_paths(input_gt_paths, movies)

    if debug:
        debug_directory = os.path.join(external_debug_directory, os.path.basename(thermal_gt_path).replace('_gt', ''))

    statistics = StatisticsData(vis_basename=os.path.basename(cmos_gt_path),thermal_basename=os.path.basename(thermal_gt_path))
    
    cmos_reader, thermal_reader = init_readers(movies=movies)

    if cmos_reader is None or thermal_reader is None:
        if statistics_only:
            return
        handle_invalid_dual(input_gt_dir_cmos=cmos_gt_path, input_gt_dir_thermal=thermal_gt_path, output_gt_dir=output_path, cmos_reader=cmos_reader, thermal_reader=thermal_reader, statistics=statistics)
    else: # both movies exist 
        tform_path = os.path.join(matrix_basedir, ("{0}" + os.sep + "{0}_000000.txt").format(os.path.basename(thermal_reader.input_path).replace('.raw2', '')))
        tform_path_exists = os.path.exists(tform_path)
        if statistics_only and not tform_path_exists:
            return

        statistics.num_frames = cmos_reader.num_frames1
        extract_core_data_from_movie(input_gt_dir_cmos=cmos_gt_path, input_gt_dir_thermal=thermal_gt_path, cmos_reader=cmos_reader, thermal_reader=thermal_reader, statistics=statistics)
        for thr in thresholds:
            create_dual_gt_for_movie_with_threshold(input_gt_dir_cmos=cmos_gt_path, input_gt_dir_thermal=thermal_gt_path, output_gt_dir=output_path, threshold=thr, cmos_reader=cmos_reader, thermal_reader=thermal_reader, tform_path=tform_path, tform_path_exists=tform_path_exists, statistics=statistics)
    
    row = [statistics.vis_basename, statistics.thermal_basename, statistics.num_frames, 
                statistics.len_cmos_annotations, statistics.len_thermal_annotations]
    assert sorted(statistics.len_cmos_annotations_dual.keys()) == sorted(statistics.len_thermal_annotations_dual.keys())
    cmos_values, thermal_values = [], []
    for thr in sorted(statistics.len_cmos_annotations_dual.keys()):
        cmos_values.append(statistics.len_cmos_annotations_dual[thr])
        thermal_values.append(statistics.len_thermal_annotations_dual[thr])

    f_cmos_writer.writerow(row + cmos_values)
    f_thermal_writer.writerow(row + thermal_values)


def get_bad_movies_list():
    with open(bad_movies_file, "r") as f:
        return [line.rstrip() for line in f]


def is_valid_movies(movies):
    bad_movies = get_bad_movies_list()

    for movie in movies:
        for bad_movie_basename in bad_movies:
            if bad_movie_basename in movie:
                return False
    
    return True


def write_statistics_files_to_server():
    copy_cmos_basename = os.path.basename(statistics_file_cmos).replace(".csv", "_copy.csv")
    shutil.copyfile(statistics_file_cmos, os.path.join(folder_for_statistics_file_copies, copy_cmos_basename))

    copy_thermal_basename = os.path.basename(statistics_file_thermal).replace(".csv", "_copy.csv")
    shutil.copyfile(statistics_file_thermal, os.path.join(folder_for_statistics_file_copies, copy_thermal_basename))


def create_dual_gts(input_path, output_path, movies_dirs, threshold):
    global debug_directory
    print("creating dual GTs - current time: ", datetime.now().strftime("%H:%M:%S"))

    thresholds = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9] if statistics_only else [threshold]  
    open_new_statistics_file(statistics_file_cmos, thresholds)
    open_new_statistics_file(statistics_file_thermal, thresholds)
    
    is_panorama = "Panorama" in input_path or "panorama" in input_path
    folders_dict = get_folders_dictionary(input_path=input_path, is_panorama=is_panorama)
    movies_dict = get_movies_dictionary(external_dirs=movies_dirs, is_panorama=is_panorama)
    
    f_cmos = open(statistics_file_cmos, 'a')
    f_cmos_writer = csv.writer(f_cmos, dialect='excel')
    f_thermal = open(statistics_file_thermal, 'a')
    f_thermal_writer = csv.writer(f_thermal, dialect='excel')
    
    for movie_id in folders_dict.keys():
        print("movie #{} is being processed... {}".format(movie_id, datetime.now().strftime("%H:%M:%S")))
        create_dual_gt_for_movie(input_gt_paths=folders_dict[movie_id], movies=movies_dict[movie_id], thresholds=thresholds, f_cmos_writer=f_cmos_writer, f_thermal_writer=f_thermal_writer)
        
    f_cmos.close()
    f_thermal.close()
    
    write_statistics_files_to_server()
    
    if len(problematic_registration_matrixes) > 0:
        print("\nproblematic registration matrixes:")
        for path in list(problematic_registration_matrixes):
            print(path)


if __name__ == '__main__':
    create_dual_gts(input_path=input_path, output_path=output_path, movies_dirs=movies_dirs, threshold=threshold)
    print("Done!")