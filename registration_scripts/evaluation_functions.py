import cv2
import numpy as np
import os
from bounding_boxes_registration import *
from Target import *

###############################################


def get_top_left_bottom_right(bb):
    width, height = bb[3], bb[4]
    top = int(bb[2] - height / 2)
    left = int(bb[1] - width / 2)
    bottom = int(bb[2] + height / 2)
    right = int(bb[1] + width / 2)

    return [top, left, bottom, right]


def get_cx_cy_width_height(top, left, bottom, right):
    width = int(right - left)
    height = int(bottom - top)
    cx = int(np.ceil(left + width / 2))
    cy = int(np.ceil(top + height / 2))

    return [cx, cy, width, height]


# recives two targets and returns their union target, i.e, a BB containing both bb1 and bb2.
def get_union_bb(bb1, bb2):
    ((left1, top1), (right1, bottom1)) = bb1.get_as_rectangle_start_end()
    ((left2, top2), (right2, bottom2)) = bb2.get_as_rectangle_start_end()
    top, left, bottom, right = [min(top1, top2), min(left1, left2), max(bottom1, bottom2), max(right1, right2)]
    cx, cy, width, height = get_cx_cy_width_height(top, left, bottom, right)
    union_target = Target(label=bb1.label, cx=cx, cy=cy, width=width, height=height, confidence=bb1.confidence, label_set=bb1.label_set)
    
    return union_target


# recives two BBs in the following format: [label, cx, cy, w, h] 
# and returns the intersection value between them.
def intersection(bb1, bb2):
    top1, left1, bottom1, right1 = get_top_left_bottom_right(bb1)
    top2, left2, bottom2, right2 = get_top_left_bottom_right(bb2)

    if (bottom1 < top2 or bottom2 < top1):
        return 0
    elif (right1 < left2 or right2 < left1):
        return 0
    else:
        height_overlap = min(bottom1, bottom2) - max(top1, top2)
        width_overlap = min(right1, right2) - max(left1, left2)
        intersection_val = height_overlap * width_overlap
    
    return intersection_val


# recives two BBs in the following format: [label, cx, cy, w, h] 
# and returns the union value between them.
def union(bb1, bb2):
    top1, left1, bottom1, right1 = get_top_left_bottom_right(bb1)
    top2, left2, bottom2, right2 = get_top_left_bottom_right(bb2)

    area1 = (bottom1 - top1) * (right1 - left1)
    area2 = (bottom2 - top2) * (right2 - left2)
    intersection_val = intersection(bb1, bb2)

    union = area1 + area2 - intersection_val
    return union


# recives two BBs in the following format: [label, cx, cy, w, h] 
# and returns the IOU value between them.
def iou(bb1, bb2):
    intersection_val = intersection(bb1, bb2)
    union_val = union(bb1, bb2)

    iou_val = intersection_val / union_val
    return iou_val


# recives two BBs in the following format: [label, cx, cy, w, h] 
# and returns the IOM value between them.
def iom(bb1, bb2):
    intersection_val = intersection(bb1, bb2)
    top1, left1, bottom1, right1 = get_top_left_bottom_right(bb1)
    top2, left2, bottom2, right2 = get_top_left_bottom_right(bb2)
    area1 = (bottom1 - top1) * (right1 - left1)
    area2 = (bottom2 - top2) * (right2 - left2)
    if area1 < area2:
        minimum_val = area1
    else:
        minimum_val = area2

    iom_val = intersection_val / minimum_val
    return iom_val


# receives two lists: list1, list2 in the following format: [[label, cx, cy, w, h],...]
# and returns an IoU/IoM matrix, when rows refer to list1 and cols refer to list2
def overlap_ratio_matrix(list1, list2, method, partition):
    mat = np.zeros((len(list1), len(list2)))

    if partition is None:
        for i, bb1 in enumerate(list1):
            for j, bb2 in enumerate(list2):
                mat[i, j] = method(bb1, bb2)
    else:
        assert list1 == list2
        for i, bb1 in enumerate(list1[:partition]):
            for j in range(partition, len(list2)):
                bb2 = list2[j]
                mat[i, j] = method(bb1, bb2)
    
    return mat


def self_nms(self_similarity_mat, threshold, razor):
    np.fill_diagonal(self_similarity_mat, 0)

    thresh_mat = np.zeros_like(self_similarity_mat)
    thresh_mat[self_similarity_mat >= threshold] = 1

    razor_mat = np.zeros_like(thresh_mat)
    for i in range(len(razor_mat)):
        for j in range(len(razor_mat[i])):
            razor_mat[i][j] = thresh_mat[i][j] * razor[j]

    for i in range(len(razor_mat)):
        for j in range(len(razor_mat[i])):
            if razor_mat[i][j] == razor_mat[j][i] != 0:
                if i < j:
                    razor_mat[i][j] = 0
    
    razor_mask = np.zeros_like(razor_mat)
    for i in range(len(razor_mat)):
        for j in range(len(razor_mat[i])):
            if razor_mat[i][j] >= razor[i]:
                razor_mask[i][j] = razor_mat[i][j]

    max_razor = np.amax(razor_mask, axis=1) # TODO

    # tie-breaker:
    merged_into_index = np.zeros_like(max_razor)
    for i in range(len(razor_mat)):
        max_index = [j for j, e in enumerate(razor_mat[i]) if e == max_razor[i] and max_razor[i] != 0]
        
        if len(max_index) == 0:
            max_index = None
        elif len(max_index) > 1:
            max_iou_index = max_index[0]
            for j in max_index:
                if self_similarity_mat[i][j] > self_similarity_mat[i][max_iou_index]:
                    max_iou_index = j
            max_index = max_iou_index
        else:
            max_index = max_index[0]
        
        merged_into_index[i] = max_index
    
    return merged_into_index


# method = iom or iou
def fuse_lists(dual_list, threshold, razor, method, partition=None):
    if len(dual_list) == 0:
        merged_into_index = []
    else:
        combined_list = []
        if type(dual_list[0]) is not list: # list of Targets
            for i in range(len(dual_list)):
                combined_list.append(dual_list[i].get_as_yolo_list())
        else:
            combined_list = dual_list
        mat = overlap_ratio_matrix(combined_list, combined_list, method, partition)
        merged_into_index = self_nms(self_similarity_mat=mat, threshold=threshold, razor=razor)

    return merged_into_index


def is_bb_partialy_contained(bb, container_resulotion):
    if type(bb) is list:
        top, left, bottom, right = get_top_left_bottom_right(bb)
    else: # Target
        ((left, top), (right, bottom)) = bb.get_as_rectangle_start_end()
    
    if right < 0 or bottom < 0:
        return False
    elif left > container_resulotion[0]:
        return False
    elif top > container_resulotion[1]:
        return False

    return True 