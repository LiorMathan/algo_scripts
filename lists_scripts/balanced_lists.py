import sys
import numpy as np
import os
import random
import statistics
import csv
from datetime import datetime

current_prefix = "/mnt/ssdHot/Crops"
new_prefix = "/mnt/ssdHot/Crops" #"/home/shwarzeneger/workspace/share/storage_server"

list_analysis_out_path = "/mnt/ssdTF/DLRunsStaging/datasets/crops/ssdmobilenet/20210217/ag/MIX8Bit/list_analysis_from_balanced_lists_script.csv"

train_file_path_thermal = "/mnt/ssdTF/DLRunsStaging/datasets/crops/ssdmobilenet/20210217/ag/MIX8Bit/Training.txt"
validation_file_path_thermal = "/mnt/ssdTF/DLRunsStaging/datasets/crops/ssdmobilenet/20210217/ag/MIX8Bit/Validation.txt"
test_file_path_thermal = "/mnt/ssdTF/DLRunsStaging/datasets/crops/ssdmobilenet/20210217/ag/MIX8Bit/Test.txt"

train_file_path_vis = "/mnt/ssdTF/DLRunsStaging/datasets/crops/ssdmobilenet/20210217/ag/CMOS/Training.txt"
validation_file_path_vis = "/mnt/ssdTF/DLRunsStaging/datasets/crops/ssdmobilenet/20210217/ag/CMOS/Validation.txt"
test_file_path_vis = "/mnt/ssdTF/DLRunsStaging/datasets/crops/ssdmobilenet/20210217/ag/CMOS/Test.txt"

tvt_ratio = [0.9, 0.05, 0.05] # train, val, test

# single_file_path = "/mnt/share/Local/OfflineDB/DLRuns/test_lists/BP_GA_416_310220_Test.txt"
# dirs_file_path = "/mnt/ssdHot/DLRunsStaging/datasets/crops/ssdmobilenet/gg/all_thermal/dirlist.txt"


# number of *objects!!!*
scenario_dict_cmos = {

}

# number of *objects!!!*
scenario_dict_thermal = {
    "/mnt/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/CMOS/High_Altitude" : 'ALL_BALLANCED',
    "/mnt/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/CMOS/Low_Altitude" : 'ALL_BALLANCED',
    "/mnt/ssdHot/Crops/SSDMobileNet/AG/Chimera_CMOS" : 'ALL_BALLANCED',
    "/mnt/ssdHot/Crops/SSDMobileNet/AG/VISdata/VISDrone2020": 'ALL_BALLANCED',
    "/mnt/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/Thermal/High_Altitude" : 'ALL_BALLANCED',
    "/mnt/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/Thermal/Low_Altitude" : 'ALL_BALLANCED',
    "/mnt/ssdHot/Crops/SSDMobileNet/AG/Old_Chimera_Thermal" : 'ALL_BALLANCED'
}

# tests lists
test_movies_folder = "/mnt/share/Local/OfflineDB/Archive/Scripts/lists_scripts/TestMovies"

with open(os.path.join(test_movies_folder, 'BlackPanther.txt')) as f:
    test_list_BP = [line.rstrip() for line in f]

with open(os.path.join(test_movies_folder, 'DualAirCMOS.txt')) as f:
    test_list_DualAir_vis = [line.rstrip() for line in f]

with open(os.path.join(test_movies_folder, 'DualAirThermal.txt')) as f:
    test_list_DualAir_thermal = [line.rstrip() for line in f]

with open(os.path.join(test_movies_folder, 'DualGroundCMOS.txt')) as f:
    test_list_DualGround_vis = [line.rstrip() for line in f]

with open(os.path.join(test_movies_folder, 'DualGroundThermal.txt')) as f:
    test_list_DualGround_thermal = [line.rstrip() for line in f]

with open(os.path.join(test_movies_folder, 'DualXRThermal.txt')) as f:
    test_list_XR_thermal = [line.rstrip() for line in f]

with open(os.path.join(test_movies_folder, 'DualXRCMOS.txt')) as f:
    test_list_XR_vis = [line.rstrip() for line in f]

with open(os.path.join(test_movies_folder, 'DualELTThermal.txt')) as f:
    test_list_ELT_thermal = [line.rstrip() for line in f]

with open(os.path.join(test_movies_folder, 'DualELTCMOS.txt')) as f:
    test_list_ELT_vis = [line.rstrip() for line in f]

with open(os.path.join(test_movies_folder, 'Medusa.txt')) as f:
    test_list_medusa = [line.rstrip() for line in f]

with open(os.path.join(test_movies_folder, 'FlirBoson.txt')) as f:
    test_list_flir_boson = [line.rstrip() for line in f]


output = open(list_analysis_out_path, 'w', newline='')
writer = csv.writer(output, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
writer.writerow(['Spectrum', 'Set', 'Scenario', 'total folders', 'total crops', 'AVG crops in folder', 'STDEV crops in folder', 'total objects', 'AVG objects density in crop', 'AVG objects in folder',
                 'class 0', 'class 1', 'class 2', 'class 3', 'class 4', 'class 5', 'class 6', 'class 7'])


class Folder:
    def __init__(self, path, crops_path=None, class_counter=None, density=None):
        self.path = path
        self.crops_path = crops_path
        self.class_counter = class_counter
        self.density = density


def get_random_sample(from_list, k):
    result = []
    multiplications = int(k / len(from_list))
    remainder = (k % len(from_list))
    for i in range(multiplications):
        result.extend(from_list)

    result.extend(random.sample(from_list, remainder))
    random.shuffle(result)
    
    return result


def update_output_table(folders):
    if "Thermal" in folders[0].path:
        spectrum = "Thermal"
    else:
        spectrum = "CMOS"
    if "Dual" in folders[0].path:
        set_desc = "Dual"
    else:
        set_desc = "Single"
    scenario = os.path.basename(os.path.dirname(folders[0].path))
    total_folders = len(folders)
    total_crops = []
    total_class_counter = [0] * len(folders[0].class_counter)
    
    for i in range(total_folders):
        total_crops.append(len(folders[i].crops_path))
        total_class_counter = np.add(total_class_counter, folders[i].class_counter)

    total_objects = sum(total_class_counter)
    avg_crops_in_folder = statistics.mean(total_crops)
    try:
        stdev_crops_in_folder = statistics.stdev(total_crops)
    except:
        stdev_crops_in_folder = 0
    avg_object_density_in_crop = total_objects / sum(total_crops)
    avg_objects_in_folder = total_objects / total_folders

    writer.writerow([spectrum, set_desc, scenario, total_folders, sum(total_crops), avg_crops_in_folder, stdev_crops_in_folder, total_objects, avg_object_density_in_crop, avg_objects_in_folder,
                    total_class_counter[0], total_class_counter[1], total_class_counter[2], total_class_counter[3], total_class_counter[4], total_class_counter[5], total_class_counter[6], total_class_counter[7]])


def check_if_test(folder):
    is_test = False
    if folder in test_list_BP:
        is_test = True
    elif folder in test_list_DualAir_vis:
        is_test = True
    elif folder in test_list_DualAir_thermal:
        is_test = True
    elif folder in test_list_DualGround_vis:
        is_test = True
    elif folder in test_list_DualGround_thermal:
        is_test = True
    elif folder in test_list_XR_thermal:
        is_test = True
    elif folder in test_list_XR_vis:
        is_test = True
    elif folder in test_list_ELT_vis:
        is_test = True
    elif folder in test_list_ELT_thermal:
        is_test = True
    elif folder in test_list_medusa:
        is_test = True
    elif folder in test_list_flir_boson:
        is_test = True
    
    return is_test


def update_class_counter(folder):
    folder.class_counter = [0] * 8
    for crop_path in folder.crops_path:
        gt_path = crop_path.replace('.png', '.txt')
        if os.path.exists(gt_path):
            with open(gt_path, "r") as f:
                line = f.readline()
                while(line):
                    try:
                        label = int(line.split()[0])
                        folder.class_counter[label] += 1
                    except:
                        pass
                    line = f.readline()
        else:
            print(gt_path + " does not exists!")


def sample_scenario(external_folder, num_desired_objs):
    folders = []
    for folder in os.listdir(external_folder):
        if os.path.isdir(os.path.join(external_folder, folder)) and "archive" not in folder:
            if (check_if_test(folder) == True):
                continue
            folders.append(Folder(path=os.path.join(external_folder, folder)))
    
    total_density = 0
    total_crops_in_folder = 0
    total_objects_in_folder = 0
    num_folders = len(folders)
    i = 0
    while i < num_folders:
        print("Scenario: " + external_folder + ", Folder " + str(i+1) + " out of " + str(num_folders))
        crops_path = []
        class_counter = [0] * 8
        for file in os.listdir(folders[i].path):
            if file.endswith('.png'):
                crop_path = os.path.join(folders[i].path, file)
                crops_path.append(crop_path)
                gt_path = crop_path.replace('.png', '.txt')
                if os.path.exists(gt_path):
                    with open(gt_path, "r") as f:
                        line = f.readline()
                        while(line):
                            try:
                                label = int(line.split()[0])
                                class_counter[label] += 1
                            except:
                                pass
                            line = f.readline()
                else:
                    print(gt_path + " does not exists!")
        if len(crops_path) == 0 or sum(class_counter) == 0:
            folders.remove(folders[i])
            num_folders -= 1
            continue
        folders[i].crops_path = crops_path
        folders[i].class_counter = class_counter
        folders[i].density = len(folders[i].crops_path) / sum(folders[i].class_counter)
        total_crops_in_folder += len(folders[i].crops_path) 
        total_objects_in_folder += sum(folders[i].class_counter)
        print("Folder: " + str(i+1) + ", Objects:" + str(sum(folders[i].class_counter)) + ", Crops:" + str(len(folders[i].crops_path)) + ", Crops per object:" + str(folders[i].density))
        print("Folder: " + str(i+1) + ", Cumsum crops per object:" + str(total_crops_in_folder/total_objects_in_folder) + ", Cumsum crops in folder:" + str(total_crops_in_folder)+ ", Cumsum objects in folder:" + str(total_objects_in_folder))
        if num_desired_objs == "ALL":
            print("Folder: " + str(i+1) + ", Desired objects per folder:" + "All" + ", Num crops to sample:" + str(len(folders[i].crops_path)))
        elif num_desired_objs == "ALL_BALLANCED":
            print("Folder: " + str(i+1) + ", Desired objects per folder:" + str(int(total_objects_in_folder/(i+1))) + ", Num crops to sample:" + str(int(folders[i].density * int(total_objects_in_folder/(i+1)))))
        else:
            print("Folder: " + str(i+1) + ", Desired objects per folder:" + str(int(num_desired_objs/(i+1))) + ", Num crops to sample:" + str(int(folders[i].density * int(num_desired_objs / (i+1)))))

        i += 1

    if num_desired_objs == "ALL":
        desired_objects_per_folder = "ALL"
    elif num_desired_objs == "ALL_BALLANCED":
        desired_objects_per_folder = int(total_objects_in_folder / num_folders)
    else:
        desired_objects_per_folder = int(num_desired_objs / num_folders)
        
    print("Balancing " + external_folder + "... ")
    now = datetime.now()
    print(now.strftime("%H:%M:%S"))

    for i in range(num_folders):
        if desired_objects_per_folder != "ALL":
            num_crops_to_sample = int(folders[i].density * desired_objects_per_folder)
        else:
            num_crops_to_sample = len(folders[i].crops_path)
        folders[i].crops_path = get_random_sample(from_list=folders[i].crops_path, k=num_crops_to_sample)
        update_class_counter(folders[i])
    
    return folders


def write_output_file(path, name, crops, end, start=0):
    try:
        out_file = open(path, 'w+')
    except:
        print("problem occured while trying to open the " + name + " output file.\n")
        pass

    print("{}: writing {} crops...".format(name, end - start))
    for i in range(start, end):
        out_file.write(crops[i]+'\n')
    out_file.close()   


def create_balanced_list(dict, train_file_path, validation_file_path, test_file_path):
    crops = []
    for external_folder in dict:
        folders = sample_scenario(external_folder=external_folder, num_desired_objs=dict[external_folder])
        update_output_table(folders)
        for i in range(len(folders)):
            for j in range(len(folders[i].crops_path)):
                crops.append(folders[i].crops_path[j].replace(current_prefix, new_prefix))
    
    random.shuffle(crops)
    num_crops = len(crops)

    num_train = int(float(num_crops) * tvt_ratio[0])
    num_validation = int(float(num_crops) * tvt_ratio[1])
    # num_test = num_crops - num_train - num_validation 

    write_output_file(path=train_file_path, name="training", crops=crops, end=num_train)
    write_output_file(path=validation_file_path, name="validation", crops=crops, end=num_train+num_validation, start=num_train)
    write_output_file(path=test_file_path, name="test", crops=crops, end=num_crops, start=num_train+num_validation)


def extract_all_crops(rootdir, all_crops, sample_size=None):
    # recursivly go over all sub folders under rootdir
    for d, subd, files in os.walk(rootdir):
        if files:  # if files is not empty
            # filter out all files that are not images
            files = filter(lambda x: 'png' in x, files)
            # determine how to sample if at all
            if sample_size is not None:
                if sample_size > len(files):
                    replace = True
                else:
                    replace = False
                files = np.random.choice(files, sample_size, replace=replace)

            # convert list to full paths
            files = list(map(lambda x: os.path.join(subd, x), files))
            all_crops += files  
    

def save_extract_crops(rootdirs, crops_file_name):
    all_crops = []
    for rootdir in rootdirs:
        extract_all_crops(rootdir, all_crops, sample_size=None)
    # shuffle list
    random.shuffle(all_crops)
    # prepare list to be written to file in one go
    crops_s = '\n'.join(all_crops)
    # write list to file
    with open(crops_file_name, 'w') as f:
        f.write(crops_s)


if __name__ == '__main__':
    create_balanced_list(dict=scenario_dict_thermal, train_file_path=train_file_path_thermal, validation_file_path=validation_file_path_thermal, test_file_path=test_file_path_thermal)
    writer.writerow(['', ''])
    create_balanced_list(dict=scenario_dict_cmos, train_file_path=train_file_path_vis, validation_file_path=validation_file_path_vis, test_file_path=test_file_path_vis)
    output.close()
