from cv2 import cv2
import numpy as np
import os
from Reader import *
from Target import *
import random
from shutil import copyfile


# input_dir_list = ["/mnt/share/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/CMOS/High_Altitude",
#                   "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/CMOS/High_Altitude_PanoramaBools",
#                   "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/CMOS/Low_Altitude",
#                   "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/Thermal/High_Altitude",
#                   "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/Thermal/High_Altitude_PanoramaBools",
#                   "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/Thermal/Low_Altitude"]

input_dir_list = ["/mnt/share/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/CMOS/Low_Altitude"]

output_path = "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/AG_Test_Set"
debug_flag = False

def extract_crops_paths(input_dir, sample_size):
    crops_paths = []
    
    internal_dirs = os.listdir(input_dir)
    
    for dir in internal_dirs:
        if os.path.isdir(os.path.join(input_dir, dir)):
            for crop_path in os.listdir(os.path.join(input_dir, dir)):
                if crop_path.endswith(".png"):
                    crops_paths.append(os.path.join(input_dir, dir, crop_path))
    
    random.shuffle(crops_paths)
    
    return(crops_paths[:sample_size])


def create_crops_set(input_dir, output_dir, sample_size):
    print("Processing: " + input_dir)

    crops_path = extract_crops_paths(input_dir, sample_size)

    try:
        os.mkdir(output_dir)
    except:
        pass

    for crop_path in crops_path:
        new_crop_path = os.path.join(output_dir, os.path.basename(crop_path))
        new_annotation_path = os.path.join(output_dir, os.path.basename(crop_path).replace(".png", ".txt"))
        copyfile(crop_path, new_crop_path)
        copyfile(crop_path.replace(".png", ".txt"), new_annotation_path)
        
        if debug_flag:
            debug_frame = cv2.imread(new_crop_path)
            with open(new_annotation_path, "r") as f:
                ann_lines = f.readlines()
                for line in ann_lines:
                    target = Target.init_from_yolo_line(line, frame_width=300, frame_height=300, label_set="AG")
                    target.draw(img=debug_frame, show_label=True)
            cv2.imshow("annotated crop", debug_frame)
            cv2.waitKey(0)
    

def generate_crops_test_set(input_dir_list=input_dir_list, output_path=output_path, sample_size=500):
    for input_dir in input_dir_list:
        if "Thermal" in input_dir:
            current_output_path = os.path.join(output_path, "Thermal")
        else:
            current_output_path = os.path.join(output_path, "CMOS")
        try:
            os.mkdir(current_output_path)
        except:
            pass

        create_crops_set(input_dir=input_dir, output_dir=os.path.join(current_output_path, os.path.basename(input_dir)), sample_size=sample_size)


##### Main: #####
generate_crops_test_set()
