import argparse
import os
import random
import sys

import cv2
import numpy as np

##### This script receives a text file that contains directories path as an argument and returns shuffled crops lists #####

directories = []
crops = []

def extract_directories_from_file(dirs_file_path):
    dirs = []

    # open directories file:
    with open(dirs_file_path, 'r') as dirs_file:
        file_lines = dirs_file.readlines()

    for line in file_lines:
        if not (len(line) >= 2):  
            continue
        else: # line is not empty
            dirs.append(line[:-1]) # -1 for ignoring the '\n' - make sure last line is empty
    
    return dirs


def validate_crop(crop_path, falsePositives_ratio):
    
    success = True

    if not os.path.exists(crop_path):
        print(crop_path + " does not exists!")
        success = False
    else:
        gt_path = crop_path.replace('.png', '.txt')
        if not os.path.exists(gt_path):
            # print(gt_path + " does not exists!")
            success = falsePositives_ratio
    return success

def progress_message(curr_num, next_percent, final_num, skip=1):
    if curr_num >= int(next_percent * final_num/100):
        print("{}% completed".format(next_percent))
        return skip
    return 0

def extract_crops_from_directory(dir_path, validate, current_prefix, new_prefix, BP_ratio, falsePositives_ratio):
    new_crops = []
    files = os.listdir(dir_path)
    next_percent = 0 # for progress messages
    for i in range(len(files)):
        file = files[i]
        if (file.endswith(".png") or file.endswith('.jpg')) and ("Next" not in file):
            new_path = os.path.join(dir_path, file)
            if validate is True:
                if not validate_crop(new_path, falsePositives_ratio):
                    continue

            new_path = new_path.replace(current_prefix, new_prefix, 1)
            # new_path = new_path.replace("\\", "/")
            new_crops.append(new_path)
            next_percent += progress_message(curr_num=i, next_percent=next_percent, final_num=len(files), skip=25)
    print("100% completed\n")
    
    if "Black_Panther" in dir_path and "Negatives" not in dir_path:
        random.shuffle(new_crops)
        num_BP_needed = int(float(len(new_crops)) * BP_ratio)
        new_crops = new_crops[0:num_BP_needed]


    if "falsePositives" in dir_path or "Negatives" in dir_path: # if it's falsePositives/Negatives: take only part of the images
        random.shuffle(new_crops)
        num_crops_needed = int(float(len(new_crops)) * falsePositives_ratio)
        new_crops = new_crops[0:num_crops_needed]
    
    return new_crops
    

def write_output_file(path, name, end, start=0):
    with open(path, 'w+') as out_file:
        print("{}: writing {} crops...".format(name, end - start))
        for i in range(start, end):
            out_file.write(crops[i]+'\n')
    out_file.close()


def create_lists(train_file_path, validation_file_path, test_file_path, tvt_ratio, single_file_path, dirs_file_path,
 falsePositives_ratio, BP_ratio, isSingleFile, validate, current_prefix, new_prefix):

    directories = extract_directories_from_file(dirs_file_path)
    for i in range(len(directories)):
        directory = directories[i]
        print("directory {} out of {}: {} is being processed...".format(i+1, len(directories), os.path.basename(directory)))
        new_crops = extract_crops_from_directory(directory, validate, current_prefix, new_prefix, BP_ratio, falsePositives_ratio)
        crops.extend(new_crops)

    random.shuffle(crops)
    num_crops = len(crops)

    if isSingleFile:
        write_output_file(path=single_file_path, name="single", end=num_crops)

    else: # divide to train and validation
        num_train = int(float(num_crops) * tvt_ratio[0])
        num_validation = int(float(num_crops) * tvt_ratio[1])
        # num_test = num_crops - num_train - num_validation

        write_output_file(path=train_file_path, name="training", end=num_train)
        write_output_file(path=validation_file_path, name="validation", end=num_train+num_validation, start=num_train)
        write_output_file(path=test_file_path, name="test", end=num_crops, start=num_train+num_validation)


####### Flags #######
def parse_args():
    parser = argparse.ArgumentParser()
    arg = parser.add_argument
    ##### for train, val, test outputs #####
    arg('--train_file_path', type=str,
        help="output train set list path <train>.txt")
    arg('--validation_file_path', type=str,
        help="output val set list path <val>.txt")
    arg('--test_file_path .txt', type=str,
        help="output test set list path <test>.txt")
    arg('--tvt_ratio .txt', type=list, default=[0.8, 0.1, 0.1],
        help="train, val, test partition")

    ##### for single output #####
    arg('--single_file_path', type=str,
        help="output dataset list of imgaes path <single file>.txt -> if isSingleFile = True (cancels train, val, test partition)")    
    arg('--isSingleFile', type=bool, default=False,
        help="flag for single output of .txt file (not train, val, test)")

    ##### images file origin #####
    arg('--dirs_file_path', type=str,
        help="path to txt file that contains list of dirs with imgs & gt (not nested)")        
    

    ##### additional functionality #####
    arg('--falsePositives_ratio', type=float, default=0.2)
    arg('--BP_ratio', type=int, default=1)
    arg('--validate', type=bool, default=True)
    arg('--current_prefix', type=str,
        default="/mnt/share/Local")
    arg('--new_prefix', type=str,
        default="/mnt/share/Local")    
    return parser.parse_args()

##### MAIN #####

def main():
    args = parse_args()

    train_file_path = args.train_file_path
    validation_file_path = args.validation_file_path 
    test_file_path = args.test_file_path
    tvt_ratio = args.tvt_ratio
    single_file_path = args.single_file_path
    dirs_file_path = args.dirs_file_path
    falsePositives_ratio = args.falsePositives_ratio
    BP_ratio = args.BP_ratio
    isSingleFile = args.isSingleFile
    validate = args.validate
    current_prefix = args.current_prefix
    new_prefix = args.new_prefix

    create_lists(train_file_path, validation_file_path, test_file_path, tvt_ratio, single_file_path, dirs_file_path, falsePositives_ratio, BP_ratio, isSingleFile, validate, current_prefix, new_prefix)    


if __name__ == '__main__':
    main()


# lines = []
# files = open("/mnt/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/new_original.txt", 'r')
# lines.extend(files.readlines())
# files.close()
# files = open("/mnt/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/all_registrated_crops.txt", 'r')
# lines.extend(files.readlines())
# files.close()
# cmos = []
# ther = []

# random.shuffle(lines)
# for line in lines:
#     if "/CMOS" in line:
#         cmos.append(line)
#     else:
#         ther.append(line)
# print("cmos: {}\nthermal: {}".format(len(cmos),len(ther)))

# lsts = [cmos, ther]
# types1 = ["CMOS", "Thermal"]
# types2 = ["VIS", "Thermal"]
# for i in range(2):
#     crops.clear()
#     crops.extend(lsts[i])

#     num_crops = len(crops)
#     num_train = int(float(num_crops) * tvt_ratio[0])
#     num_validation = int(float(num_crops) * tvt_ratio[1])
#     # num_test = num_crops - num_train - num_validation
    
#     path = "/mnt/ssdTF/DLRunsStaging/datasets/crops/ssdmobilenet/ag/dualair_multiobjects/registrations/" + types1[i] + "/AG_" + types2[i] + "_300_231220_MultiClass_train_new.txt"
#     write_output_file(path=path, name="training", end=num_train)
#     write_output_file(path=path.replace("train", "val"), name="validation", end=num_train+num_validation, start=num_train)
#     write_output_file(path=path.replace("train", "test"), name="test", end=num_crops, start=num_train+num_validation)

# # modified temp:
# old_chimera_dirs_file = "/home/ironman/workspace/share/storage_server/OfflineDB/Scripts/AG_Chimera_old_dirList.txt"
# new_chimera_dirs_file = "/home/ironman/workspace/share/storage_server/OfflineDB/Scripts/AG_Chimera_4_dirList.txt"
# negatives_dirs_file = "/home/ironman/workspace/share/storage_server/OfflineDB/Scripts/AG_Chimera_Negatives_dirList.txt"
# mosaics_dirs_file = "/home/ironman/workspace/share/storage_server/OfflineDB/Scripts/AG_Chimera_Mosaics_dirList.txt"

# # modified temp:

# new_chimera_dirs = extract_directories_from_file(new_chimera_dirs_file)
# old_chimera_dirs = extract_directories_from_file(old_chimera_dirs_file)
# negatives_dirs = extract_directories_from_file(negatives_dirs_file)
# mosaics_dirs = extract_directories_from_file(mosaics_dirs_file)

# new_chimera_crops = []
# old_chimera_crops = []
# negatives_crops = []
# mosaics_crops = []

# for directory in new_chimera_dirs:
#     new_crops = extract_crops_from_directory(directory)
#     new_chimera_crops.extend(new_crops)

# for directory in old_chimera_dirs:
#     new_crops = extract_crops_from_directory(directory)
#     old_chimera_crops.extend(new_crops)

# for directory in negatives_dirs:
#     new_crops = extract_crops_from_directory(directory)
#     negatives_crops.extend(new_crops)

# for directory in mosaics_dirs:
#     new_crops = extract_crops_from_directory(directory)
#     mosaics_crops.extend(new_crops)

# random.shuffle(new_chimera_crops)
# random.shuffle(old_chimera_crops)
# random.shuffle(negatives_crops)
# random.shuffle(mosaics_crops)

# new_chimera_crops = new_chimera_crops[0:49999]
# old_chimera_crops = old_chimera_crops[0:19999]
# mosaics_crops = mosaics_crops[0:24999]
# negatives_crops = negatives_crops[0:4999]

# crops = []
# crops.extend(new_chimera_crops)
# crops.extend(old_chimera_crops)
# crops.extend(negatives_crops)
# crops.extend(mosaics_crops)

# random.shuffle(crops)
# num_crops = len(crops)

# num_train = int(float(num_crops) * 0.7)
# num_validation = num_crops - num_train

# # open train file for writing:
# try:
#     out_file_train = open(train_file_path, 'w+')
# except:
#     print("problem occured while trying to open the train output file.\n")
#     pass

# for i in range(num_train):
#     out_file_train.write(crops[i]+'\n')
# out_file_train.close()

# # open validation file for writing:
# try:
#     out_file_val = open(validation_file_path, 'w+')
# except:
#     print("problem occured while trying to open the validation output file.\n")
#     pass

# for i in range(num_train, num_crops):
#     out_file_val.write(crops[i]+'\n')
# out_file_val.close()


