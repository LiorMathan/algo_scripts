import numpy as np
import os
import random

src_lists = ["/mnt/ssdTF/DLRunsStaging/datasets/crops/ssdmobilenet/ag/20210215/AG_CMOS_300_20210203_Training.txt"]
sub_lists = ["/mnt/ssdHot/Crops/SSDMobileNet/AG/AG_Test_Set/cmos_test_set.txt"]


def extract_list(lists):
    print("extracting list...")
    paths_list = []
    for src_list in lists:
        with open(src_list, "r") as f:
            lines = f.read().splitlines()
            paths_list.extend(lines)
    
    return paths_list


def remove_sub_list(src_list, sub_list):
    print("removing sub list...")

    count = 0
    for src_path in src_list:
        for sub_path in sub_list:
            if os.path.basename(src_path) in sub_path:
                count += 1
                print(count)
                src_list.remove(src_path)
                sub_list.remove(sub_path)
                break


def write_to_file(filename, dst_list):
    print("writing to file...")
    with open(filename, "w") as f:
        for dst_path in dst_list:
            f.write(dst_path + "\n")
        

if __name__ == '__main__':
    all_paths = extract_list(src_lists)
    sub_paths = extract_list(sub_lists)
    remove_sub_list(all_paths, sub_paths)
    write_to_file(filename=src_lists[0], dst_list=all_paths)

