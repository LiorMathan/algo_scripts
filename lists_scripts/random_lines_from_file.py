import os
import random
import numpy as np
from Target import *


input_file = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground-ground/GT/00013_r0_3_gt.txt"
input_file2 = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/GT/00196_r0_6_gt.txt"
# input_file2 = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/GT/00154_r0_4_gt.txt"
output_file = "/mnt/share/ssdHot/AvatarDemo/targets/160221/cumsum/00013_r0_3_gt.txt"
random_ratio = 1
false_ratio = 0.03
width = 640
height = 480
label_set = "GG"


def get_random_confidence_list(num_frames):
    min_val, max_val = (-5, 6)
    random_values, cumsum_values, random_confidences = [], [], []
    
    for i in range(num_frames):
        current_random = random.choice(np.arange(min_val, max_val, 1))
        if (i == 0):
            current_random += 50
        random_values.append(current_random)
    
    cumsum_values = np.cumsum(random_values)
    
    for i in range(num_frames):
        random_confidences.append(min(max(0, cumsum_values[i]), 99))
    
    return random_confidences


def rand_lines(input_file=input_file, output_file=output_file, min_confidence=0):
    selected_targets = [[] for i in range(1000)]
    random_confidences = get_random_confidence_list(len(selected_targets))

    with open(input_file, "r") as in_file:
        lines = in_file.readlines()
        rand_indexes = random.choices(np.arange(start=0, stop=len(lines), step=1), k=int(len(lines)*random_ratio))
        rand_indexes.sort()
    
    if input_file2:
        with open(input_file2, "r") as in_file:
            lines2 = in_file.readlines()
            rand_indexes2 = random.choices(np.arange(start=0, stop=len(lines2), step=1), k=int(len(lines2)*false_ratio))
            rand_indexes2.sort()

    with open(output_file, "w") as out_file:
        for index in rand_indexes:
            line_tokens = lines[index].split()
            target = Target.init_from_yolo_line(" ".join(line_tokens[1:]), frame_width=width, frame_height=height, label_set=label_set)

            rand_certainty = min(max(0, random_confidences[int(line_tokens[0])] + random.choice(np.arange(start=-15, stop=16, step=1))), 99)
            target.set_confidence(rand_certainty)
            selected_targets[int(line_tokens[0])].append(target)
        
        for index in rand_indexes2:
            line_tokens = lines2[index].split()
            target = Target.init_from_yolo_line(" ".join(line_tokens[1:]), frame_width=width, frame_height=height, label_set=label_set)

            rand_certainty = random.choice(np.arange(start=1, stop=100, step=1))
            target.set_confidence(rand_certainty)
            selected_targets[int(line_tokens[0])].append(target)

        for i in range(len(selected_targets)):
            for target in selected_targets[i]:
                if target.get_confidence() >= min_confidence:
                    new_line = target.get_as_detection_line(i)
                    out_file.write(new_line + "\n")


if __name__ == '__main__':
    rand_lines(min_confidence=0)