import os
from shutil import copyfile



input_file = "/mnt/share/Local/OfflineDB/data-processing-scripts/lists_scripts/crops_list.txt"
output_directory = "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/AG_Test_Set/CMOS/Low_Altitude"


def extract_crops_paths(filename):
    crops_paths = []

    with open(filename, "r") as f:
        crops_paths = f.read().splitlines()
    
    return crops_paths


def modify_paths(crops_paths):
    for i, path in enumerate(crops_paths):
        old_path = "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/AG_Test_Set/CMOS/Low_Altitude"
        new_path = "/mnt/share/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects/CMOS/Low_Altitude"
        
        crop_basename = os.path.basename(path)
        directory_name = '_'.join(crop_basename.split('_')[:4])
        new_path = os.path.join(new_path, directory_name)

        crops_paths[i] = path.replace(old_path, new_path)
    

def copy_crops(crops_paths):
    for path in crops_paths:
        try:
            copyfile(path, os.path.join(output_directory, os.path.basename(path)))
            copyfile(path.replace('.png', '.txt'), os.path.join(output_directory, os.path.basename(path.replace('.png', '.txt'))))
        except:
            print("No such file: " + path)
            continue


crops_paths = extract_crops_paths(filename=input_file)
modify_paths(crops_paths)
copy_crops(crops_paths)