import numpy as np
import sys
import os
import random

crops_lists = ["/mnt/ssdTF/DLRunsStaging/AG_CMOS_300_201220_MultiClass_Balanced_Training.txt",
               "/mnt/ssdTF/DLRunsStaging/AG_CMOS_300_201220_MultiClass_Balanced_Validation.txt",
               "/mnt/ssdTF/DLRunsStaging/AG_Thermal_300_201220_MultiClass_Balanced_Training.txt",
               "/mnt/ssdTF/DLRunsStaging/AG_Thermal_300_201220_MultiClass_Balanced_Validation.txt"]
train_file_path = "/mnt/ssdTF/DLRunsStaging/AG_Dual_300_201220_MultiClass_Balanced_Training.txt"
validation_file_path = "/mnt/ssdTF/DLRunsStaging/AG_Dual_300_201220_MultiClass_Balanced_Validation.txt"

check_crops = False
current_prefix = "/mnt/ssdHot"
new_prefix = "/mnt/ssdHot"

def combine_shuffled_lists():
    combined_list = []
    for l in crops_lists:
        with open(l, "r") as f:
            combined_list.extend(f.readlines())
    
    return combined_list


def extract_directories_from_file():
    crops = []

    # open directories file:
    try:
        crops_file = open(crops_lists[0])
    except:
        print("file: " + crops_lists[0] + " isn't found")
        pass
    
    file_lines = crops_file.readlines()

    for line in file_lines:
        if not (len(line) >= 2):  
            continue
        else: # line is not empty
            crops.append(line[:-1]) # -1 for ignoring the '\n' - make sure last line is empty
    
    return crops


##### Main: #####
# crops = extract_directories_from_file()
crops = combine_shuffled_lists()
num_crops = len(crops)

random.shuffle(crops)

num_train = int(float(num_crops) * 0.8)
num_validation = num_crops - num_train

# open train file for writing:
try:
    out_file_train = open(train_file_path, 'w+')
except:
    print("problem occured while trying to open the train output file.\n")
    pass

for i in range(num_train):
    if check_crops is True:
        gt_file = crops[i].replace('.png', '.txt')
        gt_file = gt_file.replace(new_prefix, current_prefix)[:-2]
        if not os.path.exists(gt_file):
            print("File not found: " + gt_file)
            continue
    out_file_train.write(crops[i])
out_file_train.close()

# open validation file for writing:
try:
    out_file_val = open(validation_file_path, 'w+')
except:
    print("problem occured while trying to open the validation output file.\n")
    pass

for i in range(num_train, num_crops):
    if check_crops is True:
        gt_file = crops[i].replace('.png', '.txt')
        gt_file = gt_file.replace(new_prefix, current_prefix)[:-2]
        if not os.path.exists(gt_file):
            print("File not found: " + gt_file)
            continue
    out_file_val.write(crops[i])
out_file_val.close()
