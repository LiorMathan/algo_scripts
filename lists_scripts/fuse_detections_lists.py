from cv2 import cv2
import numpy as np
import os
from Reader import *
from Target import *
import evaluation_functions as evf
import bounding_boxes_registration as bbr
import random


cmos_path = "/mnt/share/ssdHot/AvatarDemo/targets/160221/cumsum/00013_r1_3_gt.txt"
thermal_path = "/mnt/share/ssdHot/AvatarDemo/targets/160221/cumsum/00013_r0_3_gt.txt"
cmos_dual_path = "/mnt/share/ssdHot/AvatarDemo/targets/160221/cumsum/00013_r1_3_dual_gt.txt"
thermal_dual_path = "/mnt/share/ssdHot/AvatarDemo/targets/160221/cumsum/00013_r0_3_dual_gt.txt"
transformation_file = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual ground-ground/Affine_Transformation_Matrix/00013_r0_3/00013_r0_3_000000.txt"


def extract_affine_matrix(filename, inverse):
    with open(filename) as transformation_file:
        if inverse:
            for i in range(5):
                transformation_file.readline()
        else:
            transformation_file.readline()
            transformation_file.readline()
        matrix_txt = transformation_file.readlines()
        affine_mat = []
        for j in range(2):
            affine_mat.append([float(item) for item in matrix_txt[j].split()])
        # print(str(affine_mat))
    return affine_mat


def get_targets_list(targets_path):
    targets_list = [[] for i in range(1000)]
    with open(targets_path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            line_tokens = line.split()
            new_target = Target.init_from_detection_line(line, label_set="GG")
            targets_list[int(line_tokens[0])].append(new_target)

    return targets_list


def create_razor_list(targets_list):
    razor_list = []

    for target in targets_list:
        razor_list.append(target.confidence)
    
    return razor_list


def merge_targets(targets, merged_into_index_vector, partition):
    merged_list = []
    checked_indexes = set()
    
    for i in range(len(merged_into_index_vector)):
        if i not in checked_indexes: 
            checked_indexes.add(i)
            if np.isnan(merged_into_index_vector[i]):
                current_annotation = targets[i]
            else:
                match_index = int(merged_into_index_vector[i])
                checked_indexes.add(match_index)
    
                if i < partition: # original annotation
                    current_annotation = targets[i]
                else: # transformed annotation
                    current_annotation = targets[match_index]

            merged_list.append(current_annotation)

    return merged_list


def fuse_target_lists():
    fused_targets_cmos      = [[] for i in range(1000)]
    fused_targets_thermal   = [[] for i in range(1000)]

    all_cmos_detections     = get_targets_list(targets_path=cmos_path)
    all_thermal_detections  = get_targets_list(targets_path=thermal_path)

    transformation          = np.array(extract_affine_matrix(filename=transformation_file, inverse=False))
    inverse_transformation  = np.array(extract_affine_matrix(filename=transformation_file, inverse=True))

    for i in range(1000):
        cmos_detections = all_cmos_detections[i]
        thermal_detections = all_thermal_detections[i]

        cmos_transformed_detections     = bbr.apply_transformation_on_targets(original_list=cmos_detections, tform=inverse_transformation)
        thermal_transformed_detections  = bbr.apply_transformation_on_targets(original_list=thermal_detections, tform=transformation)

        cmos_combined_list     = cmos_detections + thermal_transformed_detections
        thermal_combined_list  = thermal_detections + cmos_transformed_detections

        cmos_combined_razor_list    = create_razor_list(cmos_combined_list)
        thermal_combined_razor_list = create_razor_list(thermal_combined_list)

        merged_into_index_cmos     = evf.fuse_lists(dual_list=cmos_combined_list, threshold=0.4, razor=cmos_combined_razor_list, method="iom", partition=len(cmos_detections))
        merged_into_index_thermal  = evf.fuse_lists(dual_list=thermal_combined_list, threshold=0.4, razor=thermal_combined_razor_list, method="iom", partition=len(thermal_detections))

        fused_targets_cmos[i]      = merge_targets(targets=cmos_combined_list, merged_into_index_vector=merged_into_index_cmos, partition=len(cmos_detections))
        fused_targets_thermal[i]   = merge_targets(targets=thermal_combined_list, merged_into_index_vector=merged_into_index_thermal, partition=len(thermal_detections))

    with open(cmos_dual_path, "w") as f:
        for i in range(len(fused_targets_cmos)):
            for target in fused_targets_cmos[i]:
                f.write(target.get_as_detection_line(frame_number=i) + "\n")
    with open(thermal_dual_path, "w") as f:
        for i in range(len(fused_targets_thermal)):
            for target in fused_targets_thermal[i]:
                f.write(target.get_as_detection_line(frame_number=i) + "\n")
        

if __name__ == '__main__':
    fuse_target_lists()