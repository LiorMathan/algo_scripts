import os

DBdirsVIS = []
DBdirsThermal = []
DBdirsDual = []
DBdirsCostum = []


# VIS YOLO DB
# dirBP="/mnt/share/Local/OfflineDB/YoloReady"   # includes negatives
# dirBP_mosaic = "/mnt/share/Local/OfflineDB/YoloReady_Mosaic/Black_Panther"
# dirELT_VIS="/mnt/share/Local/OfflineDB/YoloReady_ELT/VIS"
# dirELT_VIS_FP="/mnt/share/Local/OfflineDB/YoloReady_ELT/falsePositives/VIS"
# dirELT_VIS_mosaic = "/mnt/share/Local/OfflineDB/YoloReady_Mosaic/ELT/VIS"
# # dirELT_VIS_FP_mosaic = "/home/ironman/workspace/share/storage_server/OfflineDB/YoloReady_Mosaic/ELT/falsePositives/VIS"
# DBdirsVIS.append(dirBP)
# DBdirsVIS.append(dirBP_mosaic)
# DBdirsVIS.append(dirELT_VIS)
# DBdirsVIS.append(dirELT_VIS_FP)
# DBdirsVIS.append(dirELT_VIS_mosaic)
# # DBdirsVIS.append(dirELT_VIS_FP_mosaic)

# # Thermal YOLO GA:
# dir_ELT_thermal = "/mnt/share/Local/OfflineDB/YOLO_GA/ELT/Thermal"
# dir_Medusa = "/mnt/share/Local/OfflineDB/YoloReady_Medusa"
# dir_Medusa_FP = "/mnt/share/Local/OfflineDB/YoloReady_Medusa/falsePositives"
# dir_Medusa_mosaics = "/mnt/share/Local/OfflineDB/YoloReady_Mosaic/Medusa"
# # dir_Medusa_FP_mosaics = "/mnt/share/Local/OfflineDB/YoloReady_Mosaic/Medusa/falsePositives"
# dir_XR_thermal = "/mnt/share/Local/OfflineDB/YOLO_GA/XR/Thermal"
# DBdirsThermal.append(dir_ELT_thermal)
# DBdirsThermal.append(dir_Medusa)
# DBdirsThermal.append(dir_Medusa_FP)
# DBdirsThermal.append(dir_Medusa_mosaics)
# DBdirsThermal.append(dir_XR_thermal)

# # VIS YOLO AG:
# dir_dualair_cmos_low = "/mnt/share/Local/OfflineDB/YOLO_AG/DualAir/CMOS/Low_Altitude"
# dir_dualair_cmos_high = "/mnt/share/Local/OfflineDB/YOLO_AG/DualAir/CMOS/High_Altitude"
# dir_visdrone = "/mnt/share/Local/OfflineDB/YOLO_AG/VISDrone2020"
# DBdirsVIS.append(dir_dualair_cmos_low)
# DBdirsVIS.append(dir_dualair_cmos_high)
# DBdirsVIS.append(dir_visdrone)

# # Thermal SSD AG:
# dir_dualair_thermal = "/mnt/share/Local/OfflineDB/SSDMoblieNet_AG/DualAir_MultiObjects/Thermal/Low_Altitude"
# dir_chimera_thermal = "/mnt/share/Local/OfflineDB/SSDMoblieNet_AG/Chimera_Thermal"
# DBdirsThermal.append(dir_dualair_thermal)
# DBdirsThermal.append(dir_chimera_thermal)

# # VIS SSD AG:
# dir_dualair_vis = "/mnt/share/Local/OfflineDB/SSDMoblieNet_AG/DualAir_MultiObjects/CMOS/Low_Altitude"
# dir_chimera_cmos = "/mnt/share/Local/OfflineDB/SSDMoblieNet_AG/Chimera_CMOS"
# dir_visdrone2020 = "/mnt/share/Local/OfflineDB/SSDMobileNet_VISdata"
# DBdirsVIS.append(dir_dualair_vis)
# DBdirsVIS.append(dir_chimera_cmos)
# DBdirsVIS.append(dir_visdrone2020)


# # Dual SSD AG:
# DBdirsDual.append(dir_dualair_thermal)
# DBdirsDual.append(dir_dualair_vis)
# DBdirsDual.append(dir_chimera_thermal)
# DBdirsDual.append(dir_chimera_cmos)
# DBdirsDual.append(dir_visdrone2020)

# # Thermal SSD GG:
dir_avatar_thermal = "/mnt/share/Local/OfflineDB/SSDMobileNet/GG/Avatar/Thermal"
dir_nordic_wall_thermal = "/mnt/share/Local/OfflineDB/SSDMobileNet/GG/Nordic_Wall/Thermal"
dir_overrun_thermal = "/mnt/share/Local/OfflineDB/SSDMobileNet/GG/Overrun_Prevention/Thermal"
dir_flir_adas_thermal = "/mnt/share/Local/OfflineDB/SSDMobileNet/GG/FLIR_ADAS/Thermal"
dir_flir_boson_thermal = "/mnt/share/Local/OfflineDB/SSDMobileNet/GG/FLIR_BOSON/Thermal"

DBdirsThermal.append(dir_avatar_thermal)
DBdirsThermal.append(dir_nordic_wall_thermal)
DBdirsThermal.append(dir_overrun_thermal)
DBdirsThermal.append(dir_flir_adas_thermal)
DBdirsThermal.append(dir_flir_boson_thermal)

# VIS SSD GG:
# dir_avatar_vis = r"Z:\OfflineDB\SSDMobileNet_GG\Avatar\CMOS"
# dir_nordic_wall_vis = "/mnt/share/Local/OfflineDB/SSDMobileNet_GG/Nordic_Wall/CMOS"
# dir_overrun_vis = "/mnt/share/Local/OfflineDB/SSDMobileNet_GG/Overrun_Prevention/CMOS"

# DBdirsVIS.append(dir_avatar_vis)
# DBdirsVIS.append(dir_nordic_wall_vis)
# DBdirsVIS.append(dir_overrun_vis)

# Thermal Frames GG:
# dir_avatar_thermal = '/mnt/share/Local/OfflineDB/Crops/Frames/GG/Avatar/Thermal'
# dir_nordic_wall_thermal = '/mnt/share/Local/OfflineDB/Crops/Frames/GG/Nordic_Wall/Thermal'
# dir_overrun_thermal = '/mnt/share/Local/OfflineDB/Crops/Frames/GG/Overrun_Prevention/Thermal'
# dir_flir_boson_thermal = '/mnt/share/Local/OfflineDB/Crops/Frames/GG/FLIR_BOSON'
# dir_germany_thermal = '/mnt/share/Local/OfflineDB/Crops/Frames/GG/Germany/Thermal'

# DBdirsThermal.append(dir_avatar_thermal)
# DBdirsThermal.append(dir_nordic_wall_thermal)
# DBdirsThermal.append(dir_overrun_thermal)
# DBdirsThermal.append(dir_flir_boson_thermal)
# DBdirsThermal.append(dir_germany_thermal)

# VIS Frames GG:
# dir_avatar_vis = '/mnt/share/Local/OfflineDB/Crops/Frames/GG/Avatar/CMOS'
# dir_nordic_wall_vis = '/mnt/share/Local/OfflineDB/Crops/Frames/GG/Nordic_Wall/CMOS'
# dir_overrun_vis = '/mnt/share/Local/OfflineDB/Crops/Frames/GG/Overrun_Prevention/CMOS'
# dir_germany_vis = '/mnt/share/Local/OfflineDB/Crops/Frames/GG/Germany/CMOS'

# DBdirsVIS.append(dir_avatar_vis)
# DBdirsVIS.append(dir_nordic_wall_vis)
# DBdirsVIS.append(dir_overrun_vis)
# DBdirsVIS.append(dir_germany_vis)

# # Thermal Frames AG High:
# dir_high_thermal = "/mnt/share/Local/OfflineDB/Crops/Frames/AG/High_Altitude/Thermal"
# DBdirsThermal.append(dir_high_thermal)

# # VIS Frames AG High:
# dir_high_vis = "/mnt/share/Local/OfflineDB/Crops/Frames/AG/High_Altitude/CMOS"
# DBdirsVIS.append(dir_high_vis)

# # Thermal Frames AG Low:
# dir_low_thermal = "/mnt/share/Local/OfflineDB/Crops/Frames/AG/Low_Altitude/Thermal"
# DBdirsThermal.append(dir_low_thermal)

# # VIS Frames AG Low:
# dir_low_vis = "/mnt/share/Local/OfflineDB/Crops/Frames/AG/Low_Altitude/CMOS"
# DBdirsVIS.append(dir_low_vis)


# # Custom and changing:
# DBdirsCostum.append("/mnt/share/Local/OfflineDB/VISDrone_Corrupted_200/HSV_Format")


dirlist=[]

# tests lists
with open(os.path.join('TestMovies', 'BlackPanther.txt')) as f:
    test_list_BP = [line.rstrip() for line in f]

with open(os.path.join('TestMovies', 'DualAirCMOS.txt')) as f:
    test_list_DualAir_vis = [line.rstrip() for line in f]

with open(os.path.join('TestMovies', 'DualAirThermal.txt')) as f:
    test_list_DualAir_thermal = [line.rstrip() for line in f]

with open(os.path.join('TestMovies', 'DualGroundCMOS.txt')) as f:
    test_list_DualGround_vis = [line.rstrip() for line in f]

with open(os.path.join('TestMovies', 'DualGroundThermal.txt')) as f:
    test_list_DualGround_thermal = [line.rstrip() for line in f]

with open(os.path.join('TestMovies', 'DualXRThermal.txt')) as f:
    test_list_XR_thermal = [line.rstrip() for line in f]

with open(os.path.join('TestMovies', 'DualXRCMOS.txt')) as f:
    test_list_XR_vis = [line.rstrip() for line in f]

with open(os.path.join('TestMovies', 'DualELTThermal.txt')) as f:
    test_list_ELT_thermal = [line.rstrip() for line in f]

with open(os.path.join('TestMovies', 'DualELTCMOS.txt')) as f:
    test_list_ELT_vis = [line.rstrip() for line in f]

with open(os.path.join('TestMovies', 'Medusa.txt')) as f:
    test_list_medusa = [line.rstrip() for line in f]

with open(os.path.join('TestMovies', 'FlirBoson.txt')) as f:
    test_list_flir_boson = [line.rstrip() for line in f]


# VIS DB
for dir in DBdirsVIS:
    for root,dirs,files in os.walk(dir):
        for d in dirs:
            if (d in test_list_BP) or (d in test_list_ELT_vis) or (d in test_list_DualAir_vis) or (d in test_list_DualGround_vis) or (d in test_list_XR_vis):
                continue
            elif (d.endswith('cropsMovies') or d.endswith('Negatives')):
                continue
            print("adding: " + d)
            dirlist.append(os.path.join(root, d))

with open('GG_Frames_VIS_dirList.txt', 'w') as fl:
    for item in dirlist:
        fl.write("%s\n" % item)

# Thermal DB
dirlist=[]
for dir in DBdirsThermal:
    for root,dirs,files in os.walk(dir):
        for d in dirs:
            if (d in test_list_DualAir_thermal) or (d in test_list_flir_boson) or (d in test_list_DualGround_thermal) or (d in test_list_ELT_thermal) or (d in test_list_medusa) or (d in test_list_XR_thermal):
                continue
            elif (d.endswith('cropsMovies') or d.endswith('Negatives') or d.endswith('falsePositives')):
                continue
            elif 'Negatives' in (os.path.join(root, d)):
                continue
            print("adding: " + d)
            dirlist.append(os.path.join(root, d))

with open('GG_Frames_Thermal_dirList.txt', 'w') as fl:
    for item in dirlist:
        fl.write("%s\n" % item)
fl.close()

# # Dual DB
# dirlist=[]
# for dir in DBdirsDual:
#     for root,dirs,files in os.walk(dir):
#         for d in dirs:
#             if (d in test_list_BP) or (d in test_list_ELT_vis) or (d in test_list_DualAir_vis) or (d in test_list_DualGround_vis) or (d in test_list_XR_vis):
#                 continue
#             if (d in test_list_DualAir_thermal) or (d in test_list_DualGround_thermal) or (d in test_list_ELT_thermal) or (d in test_list_medusa) or (d in test_list_XR_thermal):
#                 continue
#             if (d.endswith('cropsMovies') or d.endswith('Negatives') or d.endswith('falsePositives')):
#                 continue
#             elif 'Negatives' in (os.path.join(root, d)):
#                 continue
#             elif 'archive' in (os.path.join(root, d)):
#                 continue
#             dirlist.append(os.path.join(root, d))

# with open('AG_300_Dual_dirList.txt', 'w') as fl:
#     for item in dirlist:
#         fl.write("%s\n" % item)
# fl.close()

# # Custom and changing:
# dirlist=[]
# for dir in DBdirsCostum:
#     for root,dirs,files in os.walk(dir):
#         for d in dirs:
#             if ("qvga" not in d) and ("Negatives" not in d):
#                 dirlist.append(os.path.join(root, d))

# with open('AG_200_corrupted_HSV_dirList.txt', 'w') as fl:
#     for item in dirlist:
#         fl.write("%s\n" % item)
# fl.close()



print("Done")