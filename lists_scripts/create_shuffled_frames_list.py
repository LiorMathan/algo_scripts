import numpy as np
import csv
import os
import random

############ TODO: ############
csv_file = r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Resistant_Navigation\Resistant_Navigation.csv"
batch_size = 10


############ reading the csv file to list: ############
with open(csv_file, newline='') as f:
    reader = csv.reader(f)
    data_list = list(reader)
    data_list.remove(data_list[0]) # remove the header


############ moving test frames out: ############
test_list = []
with open(os.path.join('TestMovies', 'ResistantNavigationCMOS.txt')) as f:
    test_list_resistant_nav_vis = [line.rstrip() for line in f]
for i in range(len(data_list)):
    try:
        if os.path.basename(data_list[i][0])[:-11] in test_list_resistant_nav_vis:
            res = test_list.append(data_list.pop(i))
    except:
        break

test_out_file = csv_file.replace(".csv", "_Test.csv")
print("Saving test list to file...")
np.savetxt(test_out_file, test_list, delimiter=",", newline=' ', fmt='%s')


############ shuffeling the list: ############
indexes_list = []
index = 0
while (index < len(data_list)):
    indexes_list.append(index)
    index += batch_size

random.shuffle(indexes_list)
num_training = int(0.7 * len(indexes_list))

training_out_file = csv_file.replace(".csv", "_Shuffled_Training.csv")
print("Saving training list to file...")
output = open(training_out_file, 'w', newline='')
writer = csv.writer(output, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
for i in range(num_training):
    for j in range(batch_size):
        try:
            current_row = data_list[indexes_list[i] + j]
            writer.writerow(current_row)
        except:
            continue

output.close()

validation_out_file = csv_file.replace(".csv", "_Shuffled_Validation.csv")
print("Saving validation list to file...")
output = open(validation_out_file, 'w', newline='')
writer = csv.writer(output, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
for i in range(num_training, len(indexes_list)):
    for j in range(batch_size):
        try:
            current_row = data_list[indexes_list[i] + j]
            writer.writerow(current_row)
        except:
            continue

output.close()




