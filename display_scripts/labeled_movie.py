from cv2 import cv2
import numpy as np
import os
import random
import math
from Reader import *
from Target import *
import bounding_boxes_registration
import ProgressBar

from_config_file = False
config_filename = "/mnt/share/Local/OfflineDB/data-pre-processing-scripts/viewers_scripts/config_file.txt"

def create_labeled_movie():
    movie_path      = input("Please enter movie path: ") 
    detections_path = input("Please enter GT/detections path: ") 
    targets_type    = input("Please enter 'g' for GT and 'd' for detections: ")
    output_path     = input("Please enter output path: ")

    if movie_path.endswith(".raw2"):
        width, height = (640, 480)
    else:
        # width, height = (720, 576)
        width, height = (None, None)

    reader = Reader(path1=movie_path, width1=width, height1=height)
    width, height = (reader.width1, reader.height1)
    progress_bar = ProgressBar.ProgressBar(reader.num_frames1, bar_length=100, end_char='\n')

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    output_name = os.path.join(output_path, os.path.basename(movie_path).split('.')[0] + "_with_targets.mp4")
    video_writer = cv2.VideoWriter(output_name, fourcc, 20, (width, height))

    targets = [[] for i in range(reader.num_frames1)]
    if targets_type == 'd':
        with open(detections_path, "r") as f:
            lines = f.readlines()
            for line in lines:
                tokens = line.split()
                index = int(tokens[0])
                target = Target.init_from_detection_line(line, label_set="AG")
                targets[index].append(target)
    else:
        gt_files = os.listdir(detections_path)
        gt_files.sort()
        for i, gt_file in enumerate(gt_files):
            with open(os.path.join(detections_path, gt_file), "r") as f:
                lines = f.readlines()
                for line in lines:
                    target = Target.init_from_yolo_line(line, frame_width=width, frame_height=height, label_set="AG")
                    targets[i].append(target)

    for i in range(reader.num_frames1):
        progress_bar.print_progress(i + 1, prefix='converting to mp4...\n')
        frame = reader.get_frame(index=i, bgr=True)
        for target in targets[i]:
            target.draw(img=frame, thickness=2, expand=True)
        video_writer.write(frame)
        cv2.imshow('frame', frame)
        cv2.waitKey(0)

    video_writer.release()


def parse_config_file(config_filename=config_filename):
    with open(config_filename, "r") as f:
        config_lines = f.read().splitlines()
    return (config_lines)


def create_dual_labeled_movie():
    if from_config_file:
        vis_movie_path, thermal_movie_path, vis_detections_path, thermal_detections_path, targets_type, registration_path, output_path = parse_config_file()
    else:
        vis_movie_path          = input("Please enter VIS movie path: ") 
        thermal_movie_path      = input("Please enter thermal movie path: ") 
        vis_detections_path     = input("Please enter VIS GT/detections path: ")
        thermal_detections_path = input("Please enter thermal GT/detections path: ")
        targets_type            = input("Please enter 'g' for GT and 'd' for detections: ")
        registration_path       = input("Please enter registration path: ")
        output_path             = input("Please enter output path: ")

    vis_width, vis_height         = (640, 480)
    thermal_width, thermal_height = (640, 480)

    vis_reader     = Reader(path1=vis_movie_path, width1=vis_width, height1=vis_height)
    thermal_reader = Reader(path1=thermal_movie_path, width1=thermal_width, height1=thermal_height)
    
    fourcc       = cv2.VideoWriter_fourcc(*'mp4v')
    output_name  = os.path.join(output_path, os.path.basename(vis_movie_path).split('.')[0] + "_dual_with_targets.mp4")
    video_writer = cv2.VideoWriter(output_name, fourcc, 20, (vis_width+thermal_width, vis_height))

    vis_targets     = [[] for i in range(vis_reader.num_frames1)]
    thermal_targets = [[] for i in range(thermal_reader.num_frames1)]
    
    if targets_type == 'd':
        with open(vis_detections_path, "r") as f_vis, open(thermal_detections_path, "r") as f_thermal:
            vis_lines       = f_vis.readlines()
            thermal_lines   = f_thermal.readlines()
            for vis_line in vis_lines:
                tokens  = vis_line.split()
                index   = int(tokens[0])
                target  = Target.init_from_detection_line(vis_line, label_set="AG")
                vis_targets[index].append(target)
            for thermal_line in thermal_lines:
                tokens  = thermal_line.split()
                index   = int(tokens[0])
                target  = Target.init_from_detection_line(thermal_line, label_set="AG")
                thermal_targets[index].append(target)
    else:
        vis_gt_files     = os.listdir(vis_detections_path)
        thermal_gt_files = os.listdir(thermal_detections_path)
        vis_gt_files.sort()
        thermal_gt_files.sort()
        for i, vis_gt_file in enumerate(vis_gt_files):
            with open(os.path.join(vis_detections_path, vis_gt_file), "r") as f:
                vis_lines = f.readlines()
                for line in vis_lines:
                    target = Target.init_from_yolo_line(line, frame_width=vis_width, frame_height=vis_height, label_set="AG")
                    vis_targets[i].append(target)
        for i, thermal_gt_file in enumerate(thermal_gt_files):
            with open(os.path.join(thermal_detections_path, thermal_gt_file), "r") as f:
                thermal_lines = f.readlines()
                for line in thermal_lines:
                    target = Target.init_from_yolo_line(line, frame_width=thermal_width, frame_height=thermal_height, label_set="AG")
                    thermal_targets[i].append(target)

    
    transformation = np.array(bounding_boxes_registration.extract_affine_matrix(registration_path, inverse=False))
    inverse_transformation = np.array(bounding_boxes_registration.extract_affine_matrix(registration_path, inverse=True))

    for i in range(vis_reader.num_frames1):
        # if i % 3 == 0:
        #     vis_targets[i] = []
        vis_targets[i]      = random.choices(vis_targets[i], k=math.ceil(len(vis_targets[i]) * 0.7))
        thermal_targets[i]  = random.choices(thermal_targets[i], k=math.ceil(len(thermal_targets[i]) * 0.7))
        vis_transformed_targets     = bounding_boxes_registration.apply_transformation_on_targets(original_list=vis_targets[i], tform=inverse_transformation)
        thermal_transformed_targets = bounding_boxes_registration.apply_transformation_on_targets(original_list=thermal_targets[i], tform=transformation)

        vis_frame = vis_reader.get_frame(index=i, bgr=True)
        thermal_frame = thermal_reader.get_frame(index=i, bgr=True)

        for target, transformed_target in zip(vis_targets[i], vis_transformed_targets):
            target.draw(img=vis_frame, thickness=1, color=(0, 255, 0), show_label=True)
            transformed_target.draw(img=thermal_frame, thickness=1, color=(0, 255, 0), show_label=True, expand=True)
        for target, transformed_target in zip(thermal_targets[i], thermal_transformed_targets):
            target.draw(img=thermal_frame, thickness=1, color=(0, 0, 255), show_label=True)
            transformed_target.draw(img=vis_frame, thickness=1, color=(0, 0, 255), show_label=True, expand=True)
        
        padded_thermal_frame = cv2.copyMakeBorder(src=thermal_frame, top=0, bottom=(vis_height-thermal_height), left=0, right=0, borderType=cv2.BORDER_CONSTANT)
        dual_frame = cv2.hconcat([vis_frame, padded_thermal_frame])
        video_writer.write(dual_frame)
        cv2.imshow('dual frame', dual_frame)
        cv2.waitKey(10)

    video_writer.release()


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def display_menu():
    menu = {}
    menu['1'] = "Create labeled movie"
    menu['2'] = "Create dual labeled movie"

    menu['3'] = "Exit"

    while True:
        cv2.destroyAllWindows()
        cls()

        print('======== System Menu ========')
        options = menu.keys()
        for entry in options:
            print(entry, menu[entry])

        selection = input("Please Select: ")

        if selection == '1':
            create_labeled_movie()
        elif selection == '2':
            create_dual_labeled_movie()
        else:
            exit(1)

display_menu()
