import cv2
import csv
from Reader import *


input_path  = "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/AG/Chimera_Dual/High_Altitude/0001_170520_0001_Air_F8.5_T1700_Kfar Neter_A120_D90/Thermal_RAW_Movies_0/00011_r0_3.raw2"
output_path = "/mnt/share/Local/SharingIsCaring/LiorMathan/detections_numeric_validation/numeric_image_validation/00011_r0_3_0001.csv"

# image = cv2.imread(input_path)

reader = Reader(path1=input_path, width1=640, height1=480)
image  = reader.get_frame(index=0, thermal_endian=True)

cv2.imwrite(output_path.replace('.csv', '_rgb.png'), image)

rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
cv2.imwrite(output_path.replace('.csv', '_bgr.png'), rgb_image)

with open(output_path, 'w') as f:
    writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerows(rgb_image)
