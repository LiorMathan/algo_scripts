import sys
import os
import csv

external_folder = r"Z:\OfflineDB\SSDMobileNet_GG\Avatar\CMOS" #sys.argv[1]
from_summery_files = "True" #sys.argv[2]


if from_summery_files == "True":
    from_summery_files = True
else:
    from_summery_files = False

total_labels_counter = [0] * 8

output_file = os.path.join(external_folder, "crops_analysis.csv")
output = open(output_file, 'w', newline='')
writer = csv.writer(output, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
writer.writerow(['movie', 'number of crops', 'total objects', 'objects density in crop', 'class 0', 'class 1', 'class 2', 'class 3', 'class 4', 'class 5', 'class 6', 'class 7'])


if from_summery_files is True:
    total_crops = 0
    total_objects = 0
    num_folders = 0
    num_crops = 0
    flags = [False, False]
    for file in os.listdir(external_folder):
        if ("cropSummery" in file):
            flags[0] = True
            num_folders += 1
            crops_file = open(os.path.join(external_folder, file), "r")
            movie_name = file.split('_')[1:-1]
            num_crops = 0
            line = crops_file.readline()

            while(line):
                if not (len(line) >= 2):  
                    continue
                else: # line is not empty
                    num_crops += 1

                line = crops_file.readline()

            crops_file.close()
            writer.writerow([movie_name, num_crops])
            total_crops += num_crops

        if ("annotationSummery" in file or "GT" in file):
            flags[1] = True
            current_labels_counter = [0] * 8
            annotation_file = open(os.path.join(external_folder, file), "r")
            print(file + " is being processed...")
            line = annotation_file.readline()

            while(line):
                if not (len(line) >= 2):  
                    continue
                else: # line is not empty
                    elements = line.split(' ')

                    if ("annotationSummery" in file):
                        label = elements[2]
                    else: # "GT" in file
                        label = elements[0]

                    try:
                        label = int(label)
                        current_labels_counter[label] += 1
                        total_objects += 1
                    except:
                        continue
                
                line = annotation_file.readline()

            annotation_file.close()
        
        if flags[0] == flags[1] == True:
            flags[0] = flags[1] = False
            writer.writerow([file, num_crops, sum(current_labels_counter), sum(current_labels_counter)/num_crops, current_labels_counter[0], current_labels_counter[1], current_labels_counter[2], 
                                                                    current_labels_counter[3], current_labels_counter[4], current_labels_counter[5], 
                                                                    current_labels_counter[6], current_labels_counter[7]])
            for i in range(len(total_labels_counter)):
                total_labels_counter[i] += current_labels_counter[i]
else:
    total_crops = 0
    total_objects = 0
    num_folders = 0
    for file in os.listdir(external_folder):
        if "archive" in file or "Debug" in file:
            continue
        crops_folder = os.path.join(external_folder, file)
        if os.path.isdir(crops_folder):
            num_folders += 1
            current_labels_counter = [0] * 8
            current_crops = 0
            current_objects = 0
            print(file + " is being processed...")
            for gt in os.listdir(crops_folder):
                if gt.endswith(".txt"):
                    total_crops += 1
                    current_crops += 1
                    gt_path = os.path.join(crops_folder, gt)
                    with open(gt_path, "r") as annotation_file:
                        try:
                            line = annotation_file.readline()
                        except:
                            print("Error occured while trying to read line in: " + gt)
                            continue
                        while(line):
                            if not (len(line) >= 2):  
                                continue
                            else: # line is not empty
                                elements = line.split(' ')
                                try:
                                    label = int(elements[0])
                                    current_labels_counter[label] += 1
                                    current_objects += 1
                                    total_objects += 1
                                except:
                                    continue
                            line = annotation_file.readline()
            
            if current_objects == 0:
                try:
                    print(file + " is empty!")
                    os.remove(crops_folder)
                except:
                    pass
            else:
                writer.writerow([file, current_crops, current_objects, current_objects/current_crops, current_labels_counter[0], current_labels_counter[1], current_labels_counter[2], 
                                                                    current_labels_counter[3], current_labels_counter[4], current_labels_counter[5], 
                                                                    current_labels_counter[6], current_labels_counter[7]])
                for i in range(len(total_labels_counter)):
                    total_labels_counter[i] += current_labels_counter[i]

writer.writerow(['', '', '', '', '', '', '', '', '', '', ''])
writer.writerow(['', 'Total Crops', 'Total Objects', 'AVG objects density in crop', 'class 0', 'class 1', 'class 2', 'class 3', 'class 4', 'class 5', 'class 6', 'class 7'])
writer.writerow(['', total_crops, total_objects, total_objects/total_crops, total_labels_counter[0], total_labels_counter[1], total_labels_counter[2], 
                                                    total_labels_counter[3], total_labels_counter[4], total_labels_counter[5], 
                                                    total_labels_counter[6], total_labels_counter[7]])
writer.writerow(['Average amount of crops in folder:', int(total_crops/num_folders)])
writer.writerow(['Average amount of objects in folder:', int(total_objects/num_folders)])

output.close()
