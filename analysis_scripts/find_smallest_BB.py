import numpy as np
import os

external_folder = "Z:\OfflineDB\Drones"

class BB:
    def __init__(self, moviename, frame, area):
        self.moviename = moviename
        self.frame = frame
        self.area = area

boundingbox_list = []

for file in os.listdir(external_folder):
    if file.endswith("SNR_BP.txt"):
        with open(os.path.join(external_folder, file), "r") as current_file:
            for line in current_file:
                elements = line.split()
                if (len(elements) > 3):
                    width = int(elements[5])
                    height = int(elements[6])
                    area = width * height
                    new_BB = BB(moviename=elements[0], frame=elements[1], area=area)
                    boundingbox_list.append(new_BB)


boundingbox_list.sort(key=lambda x: x.area)
num_BB = len(boundingbox_list)
num_needed = int(0.01 * float(num_BB))

with open(os.path.join(external_folder, "smallest_BB.txt"), "w") as result_file:
    for i in range(num_needed):
        result_file.write(boundingbox_list[i].moviename + " " + boundingbox_list[i].frame + " " + str(boundingbox_list[i].area) + " \n")

