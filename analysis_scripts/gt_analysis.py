import sys
import os
import csv

external_folder = "/mnt/ssdHot/Crops/SSDMobileNet/AG/DualAir_MultiObjects"
output_file = os.path.join(external_folder, "gt_analysis.csv")
# output = open(output_file, "w")
labels_counter_vis = [0] * 8
labels_counter_thermal = [0] * 8

output = open(output_file, 'w')
writer = csv.writer(output, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
writer.writerow(['file', 'frame number', 'object number', 'label', 'cx', 'cy', 'width', 'height'])

for file in os.listdir(external_folder):
    if "_r0_" in file:
        is_thermal = True
    else:
        is_thermal = False

    gt_folder = os.path.join(external_folder, file)
    if os.path.isdir(gt_folder):
        print(file + " is being processed...")
        for gt in os.listdir(gt_folder):
            if gt.endswith(".txt"):
                frame_number = int(gt.split("_")[-1][:-len(".txt")])
                gt_path = os.path.join(gt_folder, gt)
                with open(gt_path, "r") as annotation_file:
                    line = annotation_file.readline()
                    num_objects = 0

                    while(line):
                        if not (len(line) >= 2):  
                            continue
                        else: # line is not empty
                            num_objects += 1
                            elements = line.split(' ')
                            try:
                                label = int(elements[0])
                                if is_thermal is True:
                                    labels_counter_thermal[label] += 1
                                else:
                                    labels_counter_vis[label] += 1

                                cx = elements[1]
                                cy = elements[2]
                                w = elements[3]
                                h = elements[4][:-1]
                                
                                writer.writerow([file, frame_number, num_objects, label, cx, cy, w, h])
                                # output.write(file + " frame: " + str(frame_number) + " label: " + str(label) + " cx: " + cx + " cy: " + cy + " w: " + w + " h: " + h + " \n")
                            except:
                                continue
                        line = annotation_file.readline()


labels_summery_filename = os.path.join(external_folder, "labels_quantity_summery.txt")
labels_file = open(labels_summery_filename, "w")
labels_file.write("========== Labels Quantity Summery: ========== \n")
labels_file.write("VIS: \n")
for i in range(len(labels_counter_vis)):
    labels_file.write("Label " + str(i) + " : " + str(labels_counter_vis[i]) + " \n")
labels_file.write("Thermal: \n")
for i in range(len(labels_counter_thermal)):
    labels_file.write("Label " + str(i) + " : " + str(labels_counter_thermal[i]) + " \n")

labels_file.close()
output.close()
