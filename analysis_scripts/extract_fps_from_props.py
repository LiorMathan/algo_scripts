props_file = input("Please enter props path: ") #r"Y:\ThirdEye Dropbox\Production\Database\Movies\Dual\Dual Air\Low_Altitude\0001_170620_0003_Air_F8.5_T1100_Or Akiva_A50_D90\CMOS_RAW_Movies_0\00030_r0_0.props"

TIMESTAMP_COLUMN = 10


def main():
    timestamps_diffs = []
    with open(props_file, 'r') as f:
        line = f.readline()
        prev_timestamp = int(line.split()[TIMESTAMP_COLUMN])
        line = f.readline()
        while(line):
            current_timestamp = int(line.split()[TIMESTAMP_COLUMN])
            diff = current_timestamp - prev_timestamp
            timestamps_diffs.append(diff)
            prev_timestamp = current_timestamp
            line = f.readline()
    
    mean_timestamp_diff = sum(timestamps_diffs) / len(timestamps_diffs)
    fps = int(1000 / mean_timestamp_diff)
    print(fps)

main()        
