import os
import csv
import statistics
from datetime import datetime

output_file = "/mnt/share/ssdHot/Crops/YOLO/AG/DualAir/crops_analysis.csv"

# AG:
thermal_folders = [
    "/mnt/share/ssdHot/Crops/YOLO/AG/DualAir/Thermal/High_Altitude"
    ]
                   
cmos_folders = [
    "/mnt/share/ssdHot/Crops/YOLO/AG/VISDrone2020",
    "/mnt/share/ssdHot/Crops/YOLO/AG/DualAir/CMOS/High_Altitude",
    "/mnt/share/ssdHot/Crops/YOLO/AG/DualAir/CMOS/Low_Altitude"
    ]

# # GG:
# thermal_folders = [r"Z:\OfflineDB\SSDMobileNet_GG\Avatar\Thermal",
#                    r"Z:\OfflineDB\SSDMobileNet_GG\FLIR_ADAS\Thermal",
#                    r"Z:\OfflineDB\SSDMobileNet_GG\FLIR_BOSON\Thermal",
#                    r"Z:\OfflineDB\SSDMobileNet_GG\Nordic_Wall\Thermal",
#                    r"Z:\OfflineDB\SSDMobileNet_GG\Overrun_Prevention\Thermal"]

# cmos_folders = [r"Z:\OfflineDB\SSDMobileNet_GG\Avatar\CMOS",
#                 r"Z:\OfflineDB\SSDMobileNet_GG\Nordic_Wall\CMOS",
#                 r"Z:\OfflineDB\SSDMobileNet_GG\Overrun_Prevention\CMOS"]


from_summary_files = True
frame_width = 640
total_labels_counter = [0] * 8

class Folder:
    def __init__(self, scene, num_crops, num_objects, labels_counter, narrow_objects_counter=0, wide_objects_counter=0):
        self.scene = scene
        self.num_crops = num_crops
        self.num_objects = num_objects
        self.labels_counter = labels_counter
        self.narrow_objects_counter = narrow_objects_counter
        self.wide_objects_counter = wide_objects_counter



output = open(output_file, 'w', newline='')
writer = csv.writer(output, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
writer.writerow(['Spectrum', 'Scene', 'total folders', 'total crops', 'AVG crops in folder', 'STDEV crops in folder', 
                 'total objects', 'AVG objects density in crop', 'STDEV objects density in crop', 'AVG objects in folder', 'STDEV objects in folder', 
                 'class 0', 'class 1', 'class 2', 'class 3', 'class 4', 'class 5', 'class 6', 'class 7', 'pedestrians with width < 25', 'pedestrians with width > 80'])


def get_summary(dirname):
    folders_summary = []
    for file in os.listdir(dirname):
        if "archive" in file or "Debug" in file:
            continue
        if from_summary_files is True:
            if file.endswith("annotationSummary.txt"):
                ann_summary_file = open(os.path.join(dirname, file), "r")
                current_labels_counter = [0] * 8
                current_crops = 0
                current_objects = 0
                narrow_counter = 0
                wide_counter = 0
                print(file + " is being processed...")
                now = datetime.now()
                current_time = now.strftime("%H:%M:%S")
                print("Current Time = ", current_time)

                prev_crop_index = 0
                for line in ann_summary_file.readlines():
                    elements = line.split(' ')
                    crop_index = int(elements[0])
                    if crop_index != prev_crop_index:
                        current_crops += 1
                        prev_crop_index = crop_index
                    label = int(elements[2])
                    current_labels_counter[label] += 1
                    current_objects += 1
                    width = float(elements[5]) * frame_width
                    if label == 0:
                        if width < 25:
                            narrow_counter += 1
                        elif width > 80:
                            wide_counter += 1
                
                current_folder = Folder(scene=dirname.split(os.path.sep)[-2:], num_crops=current_crops, num_objects=current_objects, labels_counter=current_labels_counter, narrow_objects_counter=narrow_counter, wide_objects_counter=wide_counter)
                folders_summary.append(current_folder)
            
        else:
            crops_folder = os.path.join(dirname, file)
            if os.path.isdir(crops_folder):
                current_labels_counter = [0] * 8
                current_crops = 0
                current_objects = 0
                print(file + " is being processed...")
                now = datetime.now()
                current_time = now.strftime("%H:%M:%S")
                print("Current Time = ", current_time)

                for gt in os.listdir(crops_folder):
                    if gt.endswith(".txt"):
                        current_crops += 1
                        gt_path = os.path.join(crops_folder, gt)
                        with open(gt_path, "r") as annotation_file:
                            try:
                                line = annotation_file.readline()
                            except:
                                print("Error occured while trying to read line in: " + gt)
                                continue
                            while(line):
                                if not (len(line) >= 2):  
                                    continue
                                else: # line is not empty
                                    elements = line.split(' ')
                                    try:
                                        label = int(elements[0])
                                        current_labels_counter[label] += 1
                                        current_objects += 1
                                    except:
                                        continue
                                line = annotation_file.readline()
                
                if current_objects == 0:
                    try:
                        print(file + " is empty!")
                        os.remove(crops_folder)
                    except:
                        pass
                else:
                    current_folder = Folder(scene=dirname.split(os.path.sep)[-2:], num_crops=current_crops, num_objects=current_objects, labels_counter=current_labels_counter)
                    folders_summary.append(current_folder)
    
    return folders_summary


def compute_and_write_statistics(spectrum_summary, spectrum):
    spectrum_crops_per_folder = []
    spectrum_objects_per_folder = []
    spectrum_total_labels = [0] * 8

    for index, folder_summary in enumerate(spectrum_summary):
        crops_per_folder = []
        objects_per_folder = []
        total_labels = [0] * 8
        total_narrow = 0
        total_wide = 0
        num_folders = len(folder_summary)
        for i in range(num_folders):
            crops_per_folder.append(folder_summary[i].num_crops)
            total_narrow += folder_summary[i].narrow_objects_counter
            total_wide += folder_summary[i].wide_objects_counter
            for j in range(len(total_labels)):
                total_labels[j] += folder_summary[i].labels_counter[j]
                spectrum_total_labels[j] += folder_summary[i].labels_counter[j]
            objects_per_folder.append(sum(folder_summary[i].labels_counter))
        
        total_crops = sum(crops_per_folder)
        total_objects = sum(objects_per_folder)
        total_folders = len(folder_summary)
        try:
            avg_crops_in_folder = statistics.mean(crops_per_folder)
            stdev_crops_in_folder = statistics.stdev(crops_per_folder)
        except:
            avg_crops_in_folder = 0
            stdev_crops_in_folder = 0
        objects_density_in_crop_per_folder = [(objects_per_folder[i] / crops_per_folder[i]) for i in range(len(objects_per_folder))]
        try:
            avg_objects_density_in_crop = statistics.mean(objects_density_in_crop_per_folder)
            stdev_objects_density_in_crop = statistics.stdev(objects_density_in_crop_per_folder)
        except:
            avg_objects_density_in_crop = 0
            stdev_objects_density_in_crop = 0
        try:
            avg_objects_in_folder = statistics.mean(objects_per_folder)
            stdev_objects_in_folder = statistics.stdev(objects_per_folder)
        except:
            avg_objects_in_folder = 0
            stdev_objects_in_folder = 0

        try:
            writer.writerow([spectrum, spectrum_summary[index][0].scene, total_folders, total_crops, avg_crops_in_folder, stdev_crops_in_folder,
                            total_objects, avg_objects_density_in_crop, stdev_objects_density_in_crop, avg_objects_in_folder, stdev_objects_in_folder,
                            total_labels[0], total_labels[1], total_labels[2], total_labels[3], total_labels[4], total_labels[5], total_labels[6], total_labels[7], total_narrow, total_wide])
        except:
            pass
        
        spectrum_crops_per_folder.extend(crops_per_folder)
        spectrum_objects_per_folder.extend(objects_per_folder)
        spectrum_objects_density_in_crop_per_folder = [spectrum_objects_per_folder[i] / spectrum_crops_per_folder[i] for i in range(len(spectrum_objects_per_folder))]

    writer.writerow(['Summary:', '', len(spectrum_crops_per_folder), sum(spectrum_crops_per_folder), statistics.mean(spectrum_crops_per_folder), statistics.stdev(spectrum_crops_per_folder),
                        sum(spectrum_objects_per_folder), statistics.mean(spectrum_objects_density_in_crop_per_folder), statistics.stdev(spectrum_objects_density_in_crop_per_folder), statistics.mean(spectrum_objects_per_folder), statistics.stdev(spectrum_objects_per_folder),
                        spectrum_total_labels[0], spectrum_total_labels[1], spectrum_total_labels[2], spectrum_total_labels[3], spectrum_total_labels[4], spectrum_total_labels[5], spectrum_total_labels[6], spectrum_total_labels[7]])

##################################################

thermal_summary = []
cmos_summary = []

for folder in thermal_folders:
    thermal_summary.append(get_summary(dirname=folder))
for folder in cmos_folders:
    cmos_summary.append(get_summary(dirname=folder))

compute_and_write_statistics(spectrum_summary=thermal_summary, spectrum="Thermal")
writer.writerow(['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''])
compute_and_write_statistics(spectrum_summary=cmos_summary, spectrum="CMOS")

output.close()
