from Reader import *
import os
import numpy as np
import cv2


dir_list = ["/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Low_Altitude/720-576",
            "/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/Dual/Dual Air/Low_Altitude/640-480"]


def count_scenes(dir_list, spectrum_list):
    counter = 0

    for i in dir_list:
        for directory in os.listdir(i):
            full_path_dir = os.path.join(i, directory)
            if os.path.isfile(full_path_dir):
                continue

            for directory2 in os.listdir(full_path_dir):
                if 'RAW' in directory2:
                    full_path_dir = os.path.join(i, directory, directory2)
                    if not os.path.isfile(full_path_dir):   
                        for sub_string in spectrum_list:
                            if sub_string in directory2:
                                counter += 1
                                print(str(counter) + " : " + full_path_dir)
                                continue
            
    return counter


def count_movies(dir_list, extensions_list):
    counter = 0

    for i in dir_list:
        for directory in os.listdir(i):
            full_path_dir = os.path.join(i, directory)
            if os.path.isfile(full_path_dir):
                continue

            for directory2 in os.listdir(full_path_dir):
                if 'RAW' in directory2:
                    if not os.path.isfile(full_path_dir):   
                        full_path_dir = os.path.join(i, directory, directory2)

                        for file in os.listdir(full_path_dir):
                            extension = os.path.splitext(file)[-1]
                            if extension in extensions_list:
                                counter += 1
                                print(str(counter) + " : " + file)
            
    return counter


def count_frames(dir_list, extensions_list):
    counter = 0

    for i in dir_list:
        for directory in os.listdir(i):
            full_path_dir = os.path.join(i, directory)
            if os.path.isfile(full_path_dir):
                continue

            for directory2 in os.listdir(full_path_dir):
                if 'RAW' in directory2:
                    if not os.path.isfile(full_path_dir):   
                        full_path_dir = os.path.join(i, directory, directory2)

                        for file in os.listdir(full_path_dir):
                            extension = os.path.splitext(file)[-1]
                            if extension in extensions_list:
                                if extension in [".rawc", ".rgb"]:
                                    width, height = (720, 576)
                                else:
                                    width, height = (640, 480)
                                reader = Reader(path1=os.path.join(full_path_dir, file), width1=width, height1=height)
                                counter += reader.num_frames1
                                print("total frames: " + str(counter))
            
    return counter



if __name__ == '__main__':
    num_scenes_vis      = count_scenes(dir_list, ["CMOS"])
    num_scenes_thermal  = count_scenes(dir_list, ["Thermal", "THERMAL"])
    num_movies_vis      = count_movies(dir_list, [".rawc", ".rgb"])
    num_movies_thermal  = count_movies(dir_list, [".raw2"])
    num_frames_vis      = count_frames(dir_list, [".rawc", ".rgb"])
    num_frames_thermal  = count_frames(dir_list, [".raw2"])

    print("\n###############################\n")
    print("Total vis scenes: " + str(num_scenes_vis))    
    print("Total thermal scenes: " + str(num_scenes_thermal))
    print("Total vis movies: " + str(num_movies_vis))    
    print("Total thermal movies: " + str(num_movies_thermal))    
    print("Total vis frames: " + str(num_frames_vis))    
    print("Total thermal frames: " + str(num_frames_thermal))